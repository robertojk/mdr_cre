﻿using MandiriCRE.ViewModel;
using System;
using System.Web;

namespace MandiriCRE
{
    /// <summary>
    /// Helper methods dealing with permissions.
    /// </summary>
    public static class PermmissionsHandler
    {
        /// <summary>
        /// Given a Model or ViewModel that has properties related to permissions,
        /// try to initialize their values by looking into the current Session
        /// and the expected role number string.
        /// </summary>
        /// <param name="permissionsModel"></param>
        /// <param name="session"></param>
        /// <param name="roleNumberStr"></param>
        public static void InitializePermissions(
            this IHasPermissions permissionsModel,
            HttpSessionStateBase session,
            string roleNumberStr
        )
        {
            if (permissionsModel == null) throw new ArgumentNullException(nameof(permissionsModel));
            if (session == null) throw new ArgumentNullException(nameof(session));
            if (roleNumberStr == null) throw new ArgumentNullException(nameof(roleNumberStr));

            var sessionAccessString = session["Access"]?.ToString();

            var isSuperAdmin = sessionAccessString == "superadmin";

            if (isSuperAdmin)
            {
                permissionsModel.IsAllowedToView = true;
                permissionsModel.IsAllowedToUpdate = true;
                permissionsModel.IsAllowedToDelete = true;
                permissionsModel.IsAllowedToDuplicate = true;
            }

            if (!string.IsNullOrWhiteSpace(sessionAccessString))
            {
                var index = sessionAccessString.IndexOf(roleNumberStr);

                // TODO: Previous developers set the method to increment as
                // index + 2, index + 5 etc.
                // Double check whether this logic changes if the role number is longer
                // than two characters.

                var allowedToViewString = sessionAccessString.Substring(index + 2, 1);
                if (allowedToViewString == "Y")
                {
                    permissionsModel.IsAllowedToView = true;
                }
                var allowedToUpdateString = sessionAccessString.Substring(index + 4, 1);
                if (allowedToUpdateString == "Y")
                {
                    permissionsModel.IsAllowedToUpdate = true;
                }
                var allowedToDeleteString = sessionAccessString.Substring(index + 5, 1);
                if (allowedToDeleteString == "Y")
                {
                    permissionsModel.IsAllowedToDelete = true;
                }
                var allowedToDuplicateString = sessionAccessString.Substring(index + 6, 1);
                if (allowedToDuplicateString == "Y")
                {
                    permissionsModel.IsAllowedToDuplicate = true;
                }
            }
        }
    }
}