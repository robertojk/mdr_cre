﻿#region Using

using System.Data.Entity.Migrations;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Linq;
using MandiriCRE.Models;
using System.Collections.Generic;

#endregion

namespace MandiriCRE
{
    public class UserManager : UserManager<IdentityUser>
    {
        private static readonly UserStore<IdentityUser> UserStore = new UserStore<IdentityUser>();
        private static readonly UserManager Instance = new UserManager();

        private UserManager()
            : base(UserStore)
        {
        }

        public static UserManager Create()
        {
            // We have to create our own user manager in order to override some default behavior:
            //
            //  - Override default password length requirement (6) with a length of 4
            //  - Override user name requirements to allow spaces and dots
            var passwordValidator = new MinimumLengthValidator(4);
            var userValidator = new UserValidator<IdentityUser, string>(Instance)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = true,
            };

            Instance.UserValidator = userValidator;
            Instance.PasswordValidator = passwordValidator;

            return Instance;
        }

        public static void Seed()
        {
            // Make sure we always have at least the demo user available to login with
            // this ensures the user does not have to explicitly register upon first use
            var demo = new IdentityUser
            {
                Id = "6bc8cee0-a03e-430b-9711-420ab0d6a596",
                Email = "demo@email.com",
                UserName = "John Doe",
                PasswordHash = "APc6/pVPfTnpG89SRacXjlT+sRz+JQnZROws0WmCA20+axszJnmxbRulHtDXhiYEuQ==",
                SecurityStamp = "18272ba5-bf6a-48a7-8116-3ac34dbb7f38"
            };

            UserStore.Context.Set<IdentityUser>().AddOrUpdate(demo);
            UserStore.Context.SaveChanges();
        }

        public static void UpdateTeam(string user, string teamid)
        {
            string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(connectionstring))
            {
                string sql = "UPDATE AspNetUsers SET Team_TeamID=@Teamid,status='Y' WHERE (Username = @Username)";

                using (SqlCommand cmd = new SqlCommand(sql, conn))
                {
                    cmd.Parameters.AddWithValue("@Teamid", teamid);
                    cmd.Parameters.AddWithValue("@Username", user.ToString());
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    //conn.Close();
                    //conn.Dispose();
                }

            }
        }

        public string CheckStatus(string user)
        {
            string status = "T";
            string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(connectionstring))
            {
                string sql = "Select status FROM AspNetUsers WHERE (Username = @Username)";
                using (SqlCommand cmd = new SqlCommand(sql, conn))
                {
                    cmd.Parameters.AddWithValue("@Username", user.ToString());
                    conn.Open();
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Department obj = new Department();

                            status = reader[0].ToString();

                        }
                    }
                }

            }

            return status;
        }

        public string defineMenu(string username,string menuname)
        {
            string Checkid = null;
            string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(connectionstring))
            {
                using (var cmd = new SqlCommand("sp_CheckMenu", conn))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@UserName",username);
                    cmd.Parameters.AddWithValue("@MenuName", menuname);
                    conn.Open();

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                           
                            Checkid = reader[0].ToString();
                        }
                    }
                }

            }

            return Checkid;

        }

        public string defineAccess(string username)
        {
            string CheckAccess = null;
            string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(connectionstring))
            {
                using (var cmd = new SqlCommand("sp_ViewUserAksesMenu", conn))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@UserName", username);
                    conn.Open();

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {

                            CheckAccess = reader[0].ToString();
                        }
                    }
                }

            }

            return CheckAccess;

        }

        //public List<Team> getListTeam()
        //{
        //    List<Team> retVal = new List<Team>();
        //    string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        //    using (SqlConnection conn = new SqlConnection(connectionstring))
        //    {
        //        using (var cmd = new SqlCommand("sp_ViewTeam", conn))
        //        {
        //            cmd.CommandType = System.Data.CommandType.StoredProcedure;
        //            conn.Open();

        //            using (var reader = cmd.ExecuteReader())
        //            {
        //                while (reader.Read())
        //                {
        //                    Team obj = new Team();
        //                    obj.TeamID = reader[0].ToString();
        //                    obj.TeamName = reader[1].ToString();
        //                    obj.Department_DepartmentID = reader[2].ToString();

        //                    retVal.Add(obj);
        //                }
        //            }
        //        }

        //    }

        //    return retVal;

        //}
    }
}