﻿using Newtonsoft.Json.Linq;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace MandiriCRE
{
    public static class JsonDownloader
    {
        /// <summary>
        /// Given a base url and url, try download a JSON from there. If any
        /// error occurs, return null or whatever has been set to be the
        /// default. Else, return the JSON string. 
        /// </summary>
        /// <param name="baseUrl"></param>
        /// <param name="url"></param>
        /// <returns></returns>
        public static async Task<string> GetJsonOrDefaultAsync(
            string baseUrl,
            string url,
            EmptyJsonDefault emptyJsonDefault = EmptyJsonDefault.Null
        )
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseUrl);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var res = await client.GetAsync(url);

                if (res.IsSuccessStatusCode)
                {

                    var resultString = res.Content.ReadAsStringAsync().Result;
                    if (IsValidJson(resultString))
                    {
                        return resultString;
                    }
                }
            }

            if (emptyJsonDefault == EmptyJsonDefault.Null)
            {
                return null;
            }
            else if (emptyJsonDefault == EmptyJsonDefault.EmptyArray)
            {
                return "[]";
            }
            else if (emptyJsonDefault == EmptyJsonDefault.EmptyObject)
            {
                return "{}";
            }
            else
            {
                throw new NotSupportedException($"Cannot return default JSON because the option '{emptyJsonDefault}' is not supported yet.");
            }
        }

        /// <summary>
        /// Checks that a given string is a valid JSON.
        /// </summary>
        /// <param name="potentialJson"></param>
        /// <returns></returns>
        public static bool IsValidJson(string potentialJson)
        {
            if (string.IsNullOrWhiteSpace(potentialJson)) return false;

            var trimmedPotentialJson = potentialJson.Trim();

            if (trimmedPotentialJson.StartsAndEndsWith("{", "}") || trimmedPotentialJson.StartsAndEndsWith("[", "]"))
            {
                try
                {
                    JToken.Parse(potentialJson);
                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
    }

    /// <summary>
    /// When a web client fails to download a JSON for whatever reason, return
    /// one of the three items below.
    /// </summary>
    public enum EmptyJsonDefault
    {
        Null,
        EmptyObject,
        EmptyArray
    }

}