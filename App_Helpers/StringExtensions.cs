using System;
using System.ComponentModel;
using System.Globalization;
using System.Linq;

namespace MandiriCRE
{
    /// <summary>
    /// Provides utility methods for converting string values to other data types.
    /// </summary>
    public static class StringExtensions
    {
        /// <summary>
        ///     Removes dashes ("-") from the given object value represented as a string and returns an empty string ("")
        ///     when the instance type could not be represented as a string.
        ///     <para>
        ///         Note: This will return the type name of given isntance if the runtime type of the given isntance is not a
        ///         string!
        ///     </para>
        /// </summary>
        /// <param name="value">The object instance to undash when represented as its string value.</param>
        /// <returns></returns>
        public static string UnDash(this object value)
        {
            return ((value as string) ?? string.Empty).UnDash();
        }

        /// <summary>
        ///     Removes dashes ("-") from the given string value.
        /// </summary>
        /// <param name="value">The string value that optionally contains dashes.</param>
        /// <returns></returns>
        public static string UnDash(this string value)
        {
            return (value ?? string.Empty).Replace("-", string.Empty);
        }

        /// <summary>
        /// Checks that a string starts and ends with a given prefix and suffix.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="prefix"></param>
        /// <param name="suffix"></param>
        /// <returns></returns>
        public static bool StartsAndEndsWith(this string value, string prefix, string suffix)
        {
            if (value == null) throw new ArgumentNullException(nameof(value));
            if (prefix == null) throw new ArgumentNullException(nameof(prefix));
            if (suffix == null) throw new ArgumentNullException(nameof(suffix));

            return value.StartsWith(prefix) && value.EndsWith(suffix);
        }
    }
}