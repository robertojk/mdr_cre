﻿namespace MandiriCRE.ViewModel
{
    public class TaxIndexViewModel
    {
        public string TaxesJson { get; set; }
        public bool IsAllowedToUpdate { get; set; }
        public bool IsAllowedToDuplicate { get; set; }
        public bool IsAllowedToView { get; set; }
        public bool IsAllowedToDelete { get; set; }
    }
}
