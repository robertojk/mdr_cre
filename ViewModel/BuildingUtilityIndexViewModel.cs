﻿namespace MandiriCRE.ViewModel
{
    public class BuildingUtilityIndexViewModel : IHasPermissions
    {
        public string BuildingUtilityJson { get; set; }
        public bool IsAllowedToUpdate { get; set; }
        public bool IsAllowedToDuplicate { get; set; }
        public bool IsAllowedToView { get; set; }
        public bool IsAllowedToDelete { get; set; }
    }
}