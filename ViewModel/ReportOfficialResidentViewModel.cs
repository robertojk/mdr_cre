﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MandiriCRE.ViewModel
{
    public class ReportOfficialResidentViewModel : IHasPermissions
    {
        public string ReportOfficialResidentIndexJson { get; set; }
        public bool IsAllowedToUpdate { get; set; }
        public bool IsAllowedToDuplicate { get; set; }
        public bool IsAllowedToView { get; set; }
        public bool IsAllowedToDelete { get; set; }
    }
}