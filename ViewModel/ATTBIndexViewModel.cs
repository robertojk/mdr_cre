﻿namespace MandiriCRE.ViewModel
{
    /// <summary>
    /// ViewModel for the ATTB Index view.
    /// </summary>
    public class ATTBIndexViewModel : IHasPermissions
    {
        public string ATTBJson { get; set; }
        public bool IsAllowedToUpdate { get; set; }
        public bool IsAllowedToDuplicate { get; set; }
        public bool IsAllowedToView { get; set; }
        public bool IsAllowedToDelete { get; set; }
    }
}