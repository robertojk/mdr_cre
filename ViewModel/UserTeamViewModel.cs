﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MandiriCRE.Models;
namespace MandiriCRE.ViewModel
{
    public class UserTeamViewModel
    {
        public User _user { get; set; }
        public List<Team> _listTeam { get; set; }
    }
}