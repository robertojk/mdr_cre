﻿namespace MandiriCRE.ViewModel
{
    /// <summary>
    /// Represents ViewModels where you can transfer strongly typed 
    /// permissions.
    /// </summary>
    public interface IHasPermissions
    {
        bool IsAllowedToUpdate { get; set; }
        bool IsAllowedToDuplicate { get; set; }
        bool IsAllowedToView { get; set; }
        bool IsAllowedToDelete { get; set; }
    }
}