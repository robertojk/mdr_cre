﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MandiriCRE.Models;
namespace MandiriCRE.ViewModel
{
    public class NotificationNewsViewModel
    {
        public List<NotificationViewAll> _listNotification { get; set; }
        public List<Articles> _listArticles { get; set; }
    }
}