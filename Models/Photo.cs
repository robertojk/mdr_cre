﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MandiriCRE.Models
{
    public class Photo
    {
        public string No { get; set; }
        public string FolderName { get; set; }
        public string Document_DocumentID { get; set; }
        public string DocumentName { get; set; }
        public string Path { get; set; }
        public string Folder_FolderID { get; set; }
        public string Name { get; set; }
        public string HomePhotoID { get; set; }
        public Int32 Total { get; set; }
    }
}