﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MandiriCRE.Models
{
    public class MapRole
    {
        public string MenuMapID { get; set; }
        public string Menu_MenuID { get; set; }
        public string MenuName { get; set; }
        public string Role_RoleID { get; set; }
        public string RoleName { get; set; }
        public string ViewAccess { get; set; }
        public string CreateAccess { get; set; }
        public string UpdateAccess { get; set; }
        public string DeleteAccess { get; set; }
        public string DuplicateAccess { get; set; }
        public string ApproveAccess { get; set; }
        public string DownloadAccess { get; set; }
        public string UploadAccess { get; set; }
    }
}