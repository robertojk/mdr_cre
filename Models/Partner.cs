﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MandiriCRE.Models
{
    public class Partner
    {
        public string No { get; set; }
        public string Phone { get; set; }
        public string UpdatedBy { get; set; }
        public string UpdatedDate { get; set; }
        public string VendorID { get; set; }
        public string PIC { get; set; }
        public string Address { get; set; }
        public string CreatedDate { get; set; }
        public string Mail { get; set; }
        public string Fax { get; set; }
        public string Industry { get; set; }
        public string VendorName { get; set; }
        public string CreatedBy { get; set; }
        public string JumlahPerjanjianCRE { get; set; }
    }
}