﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MandiriCRE.Models
{
    public class ATTBTypeModels
    {
        public string ATTBTypeID { get; set; }
        public string ATTBType { get; set; }
        public string Description { get; set; }
    }
}