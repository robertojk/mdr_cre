﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MandiriCRE.Models
{
    public class BuildingFloor
    {
        public string BuildingFloorID { get; set; }
        public string BuildingFloorName { get; set; }
        public string Description { get; set; }
        
    }
}