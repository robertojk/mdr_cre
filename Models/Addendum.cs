﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MandiriCRE.Models
{
    public class Addendum
    {
        public string No { get; set; }
        public Int32 AddendumID { get; set; }
        public string AddendumNo { get; set; }
        public Int32 AgreementID { get; set; }
        public string DocumentNo { get; set; }
        public string AgreementName { get; set; }
        public Int32 Workflow_WorkflowID { get; set; }
        public string StepName { get; set; }
        public string RolePending { get; set; }
        public string WFStatusName { get; set; }
        public string AddendumInfo { get; set; }
        public Int32 Procurement_ProcurementID { get; set; }
        public string ProcurementName { get; set; }
        public string AgreementDate { get; set; }
        public string InputDate { get; set; }
        public Int32 JobType_JobTypeID { get; set; }
        public string JobTypeName { get; set; }
        public string ObjectName { get; set; }
        public Int32 Department_DepartmentID { get; set; }
        public Int32 EmployeePIC { get; set; }
        public string EmployeePIC_Name { get; set; }
        public string VendorName { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string AgreementValue { get; set; }
        public string SLA { get; set; }
        public string AditionalInformation { get; set; }
        public string DepartmentName { get; set; }
    }
}