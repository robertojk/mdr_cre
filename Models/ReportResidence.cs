﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MandiriCRE.Models
{
    public class ReportResidence
    {
        public string ResidenceConditionID { get; set; }
        public string ResidenceConditionName { get; set; }
        public string FolderName { get; set; }
        public string UpdatedBy { get; set; }
        public string ResidentID { get; set; }
        public string RenovationCost { get; set; }
        public string CreatedDate { get; set; }
        public string Address { get; set; }
        public string PLNID { get; set; }
        public string CityID { get; set; }
        public string CityName { get; set; }
        public string PDAMID { get; set; }
        public string TelephoneID { get; set; }
        public string ResidenceStatusID { get; set; }
        public string ResidenceStatusName { get; set; }
        public string ClassID { get; set; }
        public string ClassName { get; set; }
        public string EmployeePositionID { get; set; }
        public string EmployeePositionName { get; set; }
        public string VendorResidentialManagement { get; set; }
        public string LandArea { get; set; }
        public string RenovationDate { get; set; }
        public string BuildingArea { get; set; }
        public string Path { get; set; }
        public string CreatedBy { get; set; }
        public string AcquisitionYear { get; set; }
        public string UpdatedDate { get; set; }
        public string ListNo { get; set; }
        public string DocFileName { get; set; }
        public string ResidentName { get; set; }
        public string RepairTypeID { get; set; }
        public string RepairTypeName { get; set; }
        public string Description { get; set; }
        public string AcquisitionValue { get; set; }
    }
}