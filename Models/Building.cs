﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MandiriCRE.Models
{
    public class Building
    {
        public string BuildingID { get; set; }
        public string BuildingName { get; set; }
        public string Description { get; set; }
        
    }
}