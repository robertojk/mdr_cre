﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MandiriCRE.Models
{
    public class RepairType
    {
        public string RepairTypeName { get; set; }
        public string RepairTypeID { get; set; }
        public string Description { get; set; }
    }
}