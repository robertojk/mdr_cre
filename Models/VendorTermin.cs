﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MandiriCRE.Models
{
    public class VendorTermin
    {
        public string VendorTerminID { get; set; }
        public string ProgressofWork { get; set; }
        public string ProgressofPayment { get; set; }
    }
}