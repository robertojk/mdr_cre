﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MandiriCRE.Models
{
    public class JobType
    {
        public string JobTypeID { get; set; }
        public string JobTypeName { get; set; }
    }
}