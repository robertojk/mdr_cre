//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MandiriCRE.Models
{
    using System;
    
    public partial class sp_LaporanSPOProcurement_Result
    {
        public string SPOProcurementDocID { get; set; }
        public string DocName { get; set; }
        public string RevisionDate { get; set; }
        public string Description { get; set; }
        public string CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public string DocumentID { get; set; }
        public string DocumentName { get; set; }
        public string Path { get; set; }
        public string FolderID { get; set; }
        public string FolderName { get; set; }
    }
}
