﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MandiriCRE.Models
{
    public class LaporanCeloxis
    {
        public string No { get; set; }
        public Int32 CeloxisDocID { get; set; }
        public string DocFileName { get; set; }
        public string DocPicName { get; set; }
        public string Description { get; set; }
        public string CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public Int32 DocumentID { get; set; }
        public string DocumentName { get; set; }
        public string Path { get; set; }
        public Int32 FolderID { get; set; }
        public string FolderName { get; set; }
        public Int32 DocumentPicID { get; set; }
        public string DocumentPicName { get; set; }
        public string PathPic { get; set; }
        public Int32 FolderPicID { get; set; }
        public string FolderPicName { get; set; }
        public string UserName { get; set; }
    }
}