﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MandiriCRE.Models
{
    public class BuildingUtility
    {
        public string No { get; set; }
        public string BuildingUtilityID { get; set; }
        public string BuildingID { get; set; }
        public string BuildingName { get; set; }
        public string YearID { get; set; }
        public string MonthID { get; set; }
        public string MonthName { get; set; }
        public string Pencadangan { get; set; }
        public string SubjectID { get; set; }
        public string Subject { get; set; }
        public string URCCodeID { get; set; }
        public string URCCode { get; set; }
        public string URCCodeDesc { get; set; }
        public string Invoice { get; set; }
        public string SetOff { get; set; }
        public string FPPDocID { get; set; }
        public string DocFileName { get; set; }
        public string Path { get; set; }
        public string FolderName { get; set; }
        public string CreatedDate { get; set; }
        public string ActionSave { get; set; }
        public string UserName { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }

    }
}