﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MandiriCRE.Models
{
    public class URCCode
    {
        public string URCCodeID { get; set; }
        public string URCCodeName { get; set; }
        public string Description { get; set; }
        
    }
}