﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MandiriCRE.Models
{
    public class SPKSPP
    {
        public Int32 AgremeentID { get; set; }
        public string AgreementDocID { get; set; }
        public string DocumentNo { get; set; }
        public string AddendumNo { get; set; }
        public string AgreementName { get; set; }
        public Int32 Department_DepartmentID { get; set; }
        public string DepartmentName { get; set; }
        public string VendorName { get; set; }
        public string PrintStatus { get; set; }
        public string DocName { get; set; }
        public string Description { get; set; }
        public string Path { get; set; }
        public string WorkflowStatus { get; set; }
    }
}