﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MandiriCRE.Models
{
    public class Tax
    {
        public string No { get; set; }
        public string TaxID { get; set; }
        public string GLID { get; set; }
        public string GLName { get; set; }
        public string PaymentID { get; set; }
        public string OtherPayment { get; set; }
        public string PaymentName { get; set; }
        public string TaxYearID { get; set; }
        public string TaxYear { get; set; }
        public string IDSAP { get; set; }
        public string FinanceReport { get; set; }
        public string ATTBTypeID { get; set; }
        public string ATTBType { get; set; }
        public string ATTBName { get; set; }
        public string Branch { get; set; }
        public string RegionAssetID { get; set; }
        public string RegionAsset { get; set; }
        public string NOP { get; set; }
        public string ObjectLocation { get; set; }
        public string Kelurahan { get; set; }
        public string Kecamatan { get; set; }
        public string City { get; set; }
        public string TaxPayerName { get; set; }
        public string TaxPayerAddress { get; set; }
        public string TaxPaid { get; set; }
        public string PaidBy { get; set; }
        public string Desciption { get; set; }
        public string TaxDocID { get; set; }
        public string DocFileName { get; set; }
        public string DocFileName2 { get; set; }
        public string DocFileName3 { get; set; }
        public string Path { get; set; }
        public string FolderName { get; set; }
        public string BuildingArea { get; set; }
        public string BuildingNJOP { get; set; }
        public string BuildingClass { get; set; }
        public string LandArea { get; set; }
        public string LandNJOP { get; set; }
        public string LandClass { get; set; }
        public string UserName { get; set; }
        public string ActionSave { get; set; }

        public string CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public string UpdatedDateReal { get; set; }
        public DateTime UpdatedDateRealDate { get; set; }

    }
}