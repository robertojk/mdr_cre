﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MandiriCRE.Models
{

    public class OwnershipModels
    {
        public string OwnershipID { get; set; }
        public string Ownership { get; set; }
        public string Description { get; set; }

    }
}