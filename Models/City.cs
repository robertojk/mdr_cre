﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MandiriCRE.Models
{
    public class City
    {
        public string CityID { get; set; }
        public string ProvinsiName { get; set; }
        public string ProvinsiID { get; set; }
        public string CityName { get; set; }
    }
}