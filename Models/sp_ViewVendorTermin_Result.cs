//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MandiriCRE.Models
{
    using System;
    
    public partial class sp_ViewVendorTermin_Result
    {
        public int VendorTerminID { get; set; }
        public decimal ProgressofWork { get; set; }
        public decimal ProgressofPayment { get; set; }
    }
}
