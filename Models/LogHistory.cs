﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MandiriCRE.Models
{
    public class LogHistory
    {
        public string LogID { get; set; }
        public string TableID { get; set; }
        public string TableName { get; set; }
        public string UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public string Action { get; set; }
    }
}