﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MandiriCRE.Models
{
    public class TimelineList
    {
        public Int32 ProcurementID { get; set; }
        public string ProcurementNo { get; set; }
        public string ProcurementName { get; set; }
        public string ProcurementDateStart { get; set; }
        public string ProcurementDateEnd { get; set; }
        public Int32 VendorDetailID { get; set; }
        public Int32 ProcurementVendorID { get; set; }
        public string ProcurementVendorName { get; set; }
        public string AssetID { get; set; }
        public Int32 AgreementID { get; set; }
        public string AgreementNo { get; set; }
        public string AgreementDateStart { get; set; }
        public string AgreementDateEnd { get; set; }
        public Int32 AgreementVendorID { get; set; }
        public string AgreementVendorName { get; set; }
        
    }
}