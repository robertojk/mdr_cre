﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MandiriCRE.Models
{
    public class LaporanRekapReviewPerjanjian
    {
        public Int32 AgreementID { get; set; }
        public string DocumentNo { get; set; }
        public string MemoLO { get; set; }
        public string AgreementDate { get; set; }
        public string InputDate { get; set; }
        public string AgreementName { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string SLA { get; set; }
        public string Base_SLA { get; set; }
        public string AditionalInformation { get; set; }
        public string Status { get; set; }
    }
}