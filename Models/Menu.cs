﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MandiriCRE.Models
{
    public class Menu
    {
        public string MenuId { get; set; }
        public string MenuName { get; set; }
        public string Url { get; set; }
        public string Description { get; set; }
        public string CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
       
    }
}