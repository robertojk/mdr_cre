﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MandiriCRE.Models
{
    public class Procurements
    {
        public string No { get; set; }
        public string ProcurementID { get; set; }
        public string DocumentNo { get; set; }
        public string ProcurementName { get; set; }
        public string DateStart { get; set; }
        public string DateEnd { get; set; }
        public string EmployeePIC { get; set; }
        public string EmployeePIC_Name { get; set; }
        public string status { get; set; }
        public string WorkflowID { get; set; }

    }
}