﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MandiriCRE.Models
{
    public class ProcurementHeader
    {
        public string ProcurementID { get; set; }
        public string DocumentNo { get; set; }
        public string ProcurementName { get; set; }
        public string DateStart { get; set; }
        public string DateEnd { get; set; }
        public string EmployeePICID { get; set; }
        public string EmployeePICName { get; set; }
        public string EmployeeManagerID { get; set; }
        public string EmployeeManagerName { get; set; }
        public string AdditionalInformation { get; set; }
        public string Workflow_WorkflowID { get; set; }
        public string StepName { get; set; }
        public string RolePending { get; set; }
        public string WFStatusName { get; set; }
        public string UserName { get; set; }
    }
}