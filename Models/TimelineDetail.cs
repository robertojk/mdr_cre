﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MandiriCRE.Models
{
    public class TimelineDetail
    {
        public Int32 VendorDetailID { get; set; }
        public Int32 VendorTimelineID { get; set; }
        public string Taskname { get; set; }
        public string PlanStartDate { get; set; }
        public string PlanEndDate { get; set; }
        public string DurationPlan { get; set; }
        public string ActualStartDate { get; set; }
        public string ActualEndDate { get; set; }
        public string DurationActual { get; set; }
    }
}