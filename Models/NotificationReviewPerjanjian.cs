﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MandiriCRE.Models
{
    public class NotificationReviewPerjanjian
    {
        public string AgreementID { get; set; }
        public string DocumentNo { get; set; }
        public string AgreementName { get; set; }
        public string AgreementDate { get; set; }
        public string Workflow_WorkflowID { get; set; }
        public string StepName { get; set; }
        public string RolePending { get; set; }
        public string WFStatusName { get; set; }
    }
}