﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MandiriCRE.Models
{
    public class ATTB
    {
        public string No { get; set; }
        public string ATTBID { get; set; }
        public string ListNo { get; set; }
        public string AssetNo { get; set; }
        public string IDSAP { get; set; }
        public string ActualAddress { get; set; }
        public string BrachCode { get; set; }
        public string NJOPPBB2017 { get; set; }
        public string NJOPPBB2018 { get; set; }
        public string ATTBTypeID { get; set; }
        public string ATTBType { get; set; }
        public string ATTBStatusID { get; set; }
        public string ATTBStatus { get; set; }
        public string RegionAssetID { get; set; }
        public string RegionAsset { get; set; }
        public string City { get; set; }
        public string OwnershipID { get; set; }
        public string Ownership { get; set; }
        public string AssetControlOther { get; set; }
        public string OwnershipNo { get; set; }
        public string OwnerName { get; set; }
        public string ReleaseDateCertificate { get; set; }
        public string DueDateCertificate { get; set; }
        public string LandArea { get; set; }
        public string BuildingArea { get; set; }
        public string NilaiBukuTanah { get; set; }
        public string NilaiBukuBangunan { get; set; }
        public string AssetControlID { get; set; }
        public string AssetControl { get; set; }
        public string PrimaryUserID { get; set; }
        public string PrimaryUser { get; set; }
        public string PrimaryUserOther { get; set; }
        public string AcquisitionValue { get; set; }
        public string ATTBDocID { get; set; }
        public string DocFileName { get; set; }
        public string DocFileName2 { get; set; }
        public string DocFileName3 { get; set; }
        public string DocFileName4 { get; set; }
        public string DocFileName5 { get; set; }
        public string DocFileName6 { get; set; }
        public string DocFileName7 { get; set; }
        public string DocFileName8 { get; set; }
        public string DocFileName9 { get; set; }
        public string DocFileName10 { get; set; }
        public string DocFileName11 { get; set; }
        public string DocFileName12 { get; set; }

        public string Path { get; set; }
        public string FolderName { get; set; }
        public string UserName { get; set; }
        public string ActionSave { get; set; }

        public string CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public string UpdatedDateReal { get; set; }
        public DateTime UpdatedDateRealDate { get; set; }

    }
}