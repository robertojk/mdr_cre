﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MandiriCRE.Models
{
    public class UserRole
    {
        public string UserId { get; set; }
        public string UserName { get; set; }

        public string Email { get; set; }
        public string Team_TeamID { get; set; }
        public string TeamName { get; set; }
        public string DepartmentName { get; set; }
        public string Status { get; set; }
        public string RoleId { get; set; }
        public string Name { get; set; }
    }
}