﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MandiriCRE.Models
{
    public class Agreement
    {
        public string No { get; set; }
        public string AgreementID { get; set; }
        public string DocumentNo { get; set; }
        public string ProcurementID { get; set; }
        public string AgreementDate { get; set; }
        public string InputDate { get; set; }
        public string JobTypeID { get; set; }
        public string JobType_JobTypeID { get; set; }
        public string AgreementName { get; set; }
        public string ObjectName { get; set; }
        public string DepartmentID { get; set; }
        public string DepartmentName { get; set; }
        public string EmployeePIC { get; set; }
        public string VendorID { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string AgreementValue { get; set; }
        public string SLA { get; set; }
        public string AdditionalInformation { get; set; }
        public string Workflow_WorkflowID { get; set; }
        public string StepName { get; set; }
        public string CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public string BASE_SLA { get; set; }
        public string UserName { get; set; }
        public string ActionSave { get; set; }
        public string Procurement_ProcurementID { get; set; }
        public string Department_DepartmentID { get; set; }
        public string Vendor_VendorID { get; set; }
        public string VendorName { get; set; }


    }
}