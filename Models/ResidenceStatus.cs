﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MandiriCRE.Models
{
    public class ResidenceStatus
    {
        public string ResidenceStatusName { get; set; }
        public string Description { get; set; }
        public string ResidenceStatusID { get; set; }
    }
}