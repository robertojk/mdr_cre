﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MandiriCRE.Models
{
    public class NotificationPengadaanDitolak
    {
        public string ProcurementID { get; set; }
        public string DocumentNo { get; set; }
        public string ProcurementName { get; set; }
        public string DateStart { get; set; }
        public string DateEnd { get; set; }
        public string Workflow_WorkflowID { get; set; }
        public string StepName { get; set; }
        public string RolePending { get; set; }
        public string WFStatusName { get; set; }
    }
}