﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MandiriCRE.Models
{
    public class GL
    {
        public string GLID { get; set; }
        public string GLName { get; set; }
        public string Description { get; set; }
    }
}