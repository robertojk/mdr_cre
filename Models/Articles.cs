﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MandiriCRE.Models
{
    public class Articles
    {
        public string No { get; set; }
        public string UpdatedBy { get; set; }
        public string UpdateBy { get; set; }
        public string UpdatedDate { get; set; }
        public string ValidFrom { get; set; }
        public string Position { get; set; }
        public string CreatedDate { get; set; }
        public string Content { get; set; }
        public string ValidTo { get; set; }
        public string CreatedBy { get; set; }
        public string CreateBy { get; set; }
        public string NewsID { get; set; }
        public string Headline { get; set; }
        
    }
}