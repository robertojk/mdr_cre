//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MandiriCRE.Models
{
    using System;
    
    public partial class sp_NotifPerjanjianTelahJatuhTempo_Result
    {
        public int AgreementID { get; set; }
        public string DocumentNo { get; set; }
        public string AgreementName { get; set; }
        public int Department_DepartmentID { get; set; }
        public string DepartmentName { get; set; }
        public int Vendor_VendorID { get; set; }
        public string Rekanan { get; set; }
        public System.DateTime TglJatuhTempo { get; set; }
    }
}
