﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MandiriCRE.Models
{
    public class BuildingPosition
    {
        public string BuildingPositionID { get; set; }
        public string BuildingPositionName { get; set; }
        public string Description { get; set; }
        
    }
}