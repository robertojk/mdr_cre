﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MandiriCRE.Models
{
    public class AgreementHistory
    {
        public string WorkflowType { get; set; }
        public string TableID { get; set; }
        public string Name { get; set; }
        public string UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public string StepName { get; set; }
        public string WorkflowStatus { get; set; }
        public string RejectReason { get; set; }
        public string WorkflowID { get; set; }
    }
}