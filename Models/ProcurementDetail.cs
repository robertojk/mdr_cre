﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MandiriCRE.Models
{
    public class ProcurementDetail
    {
        public string ProcurementID { get; set; }
        public string ProcurementDetailID { get; set; }
        public string TaskNameDetail { get; set; }
        public string Price { get; set; }
        public string Tax { get; set; }
        public string TotalBeforeTax { get; set; }
        public string TotalAfterTax { get; set; }
        public string UserName { get; set; }
        public string Negotiation { get; set; }
    }
}