﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MandiriCRE.Models
{
    public class PartnerHistory
    {
        public string VendorID { get; set; }
        public string VendorName { get; set; }
        public string ProcurementID { get; set; }
        public string Nomor { get; set; }
        public string Jenis { get; set; }
        public string TanggalMulai { get; set; }
        public string TanggalAkhir { get; set; }
        public string Nilai { get; set; }
        public string Status { get; set; }

    }
} 