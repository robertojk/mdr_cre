﻿#region Using

using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
#endregion

namespace MandiriCRE.Models
{
    public class AccountLoginModel
    {
        //[Required]
        //[EmailAddress]
        public string Email { get; set; }

        [Required]
        public string Username { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        public string ReturnUrl { get; set; }
        public bool RememberMe { get; set; }
    }

    public class AccountForgotPasswordModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }

    public class AccountResetPasswordModel
    {
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Compare("Password")]
        public string PasswordConfirm { get; set; }
    }

    public class AccountRegistrationModel
    {
        [Required]
        public string Username { get; set; }

        [Required]
        [EmailAddress]
        [RegularExpression("^[a-zA-Z0-9_.+-]+@bankmandiri.co.id$", ErrorMessage = "E-mail tidak valid,domain harus @bankmandiri.co.id")]
        public string Email { get; set; }

        //[Required]
        //[EmailAddress]
        //[Compare("Email")]
        //public string EmailConfirm { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Compare("Password")]
        [RegularExpression("^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{8,}$", ErrorMessage = "Password minimal terdiri dari 1 huruf dan 1 angka")]
        public string PasswordConfirm { get; set; }

        //[Required(ErrorMessage = "Silahkan pilih Team")]
        public string Team { get; set; }

        public User _user { get; set; }
        public List<Team> _listTeam { get; set; }
    }
}