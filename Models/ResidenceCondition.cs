﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MandiriCRE.Models
{
    public class ResidenceCondition
    {
        public string ResidenceConditionID { get; set; }
        public string Description { get; set; }
        public string ResidenceConditionName { get; set; }
    }
}