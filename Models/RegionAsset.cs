﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MandiriCRE.Models
{
    public class RegionAssetModels
    {
        public string RegionAssetID { get; set; }
        public string RegionAsset { get; set; }
        public string Description { get; set; }
    }
}