﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MandiriCRE.Models
{
    public class NotificationPerjanjianAkanJatuhTempo
    {
        public string AgreementID { get; set; }
        public string DocumentNo { get; set; }
        public string AgreementName { get; set; }
        public string Department_DepartmentID { get; set; }
        public string DepartmentName { get; set; }
        public string Vendor_VendorID { get; set; }
        public string Rekanan { get; set; }
        public string TglJatuhTempo { get; set; }
    }
}