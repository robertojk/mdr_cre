﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MandiriCRE.Models
{
    public class NotificationSudahUploadKPI
    {
        public string Bulan { get; set; }
        public string Tahun { get; set; }
        public string TeamID { get; set; }
        public string TeamName { get; set; }
        public string UploadBy { get; set; }
 public string TglUpload { get; set; }
    }
}