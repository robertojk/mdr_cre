﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MandiriCRE.Models
{
    public class BuildingOccupants
    {
        public string No { get; set; }
        public string BuildingOccupantsID { get; set; }
        public string BuildingID { get; set; }
        public string BuildingName { get; set; }
        public string BuildingPositionID { get; set; }
        public string BuildingPositionName { get; set; }
        public string BuildingFloorID { get; set; }
        public string BuildingFloor { get; set; }
        public string URCCodeID { get; set; }
        public string URCCode { get; set; }
        public string URCCodeDesc { get; set; }
        public string Description { get; set; }
        public string NumberOfEmployee { get; set; }
        public string BuildingArea { get; set; }
        public string SpaceLocation { get; set; }
        public string PhotoDocID { get; set; }
        public string DocFileName { get; set; }
        public string Path { get; set; }
        public string FolderName { get; set; }
        public string CreatedDate { get; set; }
        public string ActionSave { get; set; }
        public string UserName { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }

    }
}