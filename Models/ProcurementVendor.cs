﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MandiriCRE.Models
{
    public class ProcurementVendor
    {
        public string VendorDetailID { get; set; }
        public string VendorID { get; set; }
        public string VendorName { get; set; }
        public string Industry { get; set; }
        public string Address { get; set; }
        public string PIC { get; set; }
        public string Phone { get; set; }
        public string AssetID { get; set; }
        public string budget { get; set; }
        public string StartingPrice { get; set; }
        public string Negotiation { get; set; }
        public string Final { get; set; }
        public string WinnerStatus { get; set; }
        public string Procurement_ProcurementID { get; set; }
        public string ProcurementID { get; set; }
        public string Username { get; set; }
        public string TotalPenawaran { get; set; }
        public string TotalNegosiasi { get; set; }
    }
}