﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MandiriCRE.Models
{
    public class LaporanKPIGroup
    {
        public string No { get; set; }
        public Int32 KPIGroupDocID { get; set; }
        public Int32 TeamID { get; set; }
        public string TeamName { get; set; }
        public string Description { get; set; }
        public string CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public Int32 DocumentID { get; set; }
        public string DocumentName { get; set; }
        public string DocName { get; set; }
        public string Path { get; set; }
        public Int32 FolderID { get; set; }
        public string FolderName { get; set; }
        public string UserName { get; set; }
    }
}