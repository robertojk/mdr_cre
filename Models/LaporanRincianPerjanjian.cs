﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MandiriCRE.Models
{
    public class LaporanRincianPerjanjian
    {
        public Int32 AgreementID { get; set; }
        public string DocumentNo { get; set; }
        public string MemoLO { get; set; }
        public string AgreementDate { get; set; }
        public string InputDate { get; set; }
        public string AgreementName { get; set; }
        public string EmployeePIC { get; set; }
        public string EmployeePIC_Name { get; set; }
        public Int32 Vendor_VendorID { get; set; }
        public string VendorName { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string SLA { get; set; }
        public string Status { get; set; }
    }
}