﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MandiriCRE.Models
{
    public class ResidenceClass
    {
        public string ClassName { get; set; }
        public string Description { get; set; }
        public string ClassID { get; set; }
    }
}