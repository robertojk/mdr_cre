﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MandiriCRE.Models
{
    public class ATTBStatusModels
    {
        public string ATTBStatusID { get; set; }
        public string ATTBStatus { get; set; }
        public string Description { get; set; }
    }
}