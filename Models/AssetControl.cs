﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MandiriCRE.Models
{

    public class AssetControlModels
    {
        public string AssetControlID { get; set; }
        public string AssetControl { get; set; }
        public string Description { get; set; }

    }
}