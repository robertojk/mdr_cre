﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MandiriCRE.Models
{

    public class PrimaryUserModels
    {
        public string PrimaryUserID { get; set; }
        public string PrimaryUser { get; set; }
        public string Description { get; set; }

    }
}