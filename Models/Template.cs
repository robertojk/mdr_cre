﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MandiriCRE.Models
{
    public class Template
    {
        public string No { get; set; }
        public string TemplateID { get; set; }
        public string TemplateName { get; set; }
        public string Description { get; set; }
        public string Document_DocumentID { get; set; }
        public string DocumentName { get; set; }
        public string Path { get; set; }
        public string Folder_FolderID { get; set; }
        public string FolderName { get; set; }
        public string CreateDate { get; set; }
        public string CreateBy { get; set; }
        public string UpdateDate { get; set; }
        public string UpdateBy { get; set; }

    }
}