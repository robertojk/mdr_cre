﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MandiriCRE.Models
{
    public class NotificationViewAll
    {
        public string NotificationCode { get; set; }
        public string NotificationMessage { get; set; }
    }
}