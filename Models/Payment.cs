﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MandiriCRE.Models
{
    public class Payment
    {
        public string PaymentID { get; set; }
        public string PaymentName { get; set; }
        public string Description { get; set; }
    }
}