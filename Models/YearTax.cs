﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MandiriCRE.Models
{
    public class YearTax
    {
        public string TaxYearID { get; set; }
        public string TaxYear { get; set; }
        public string Description { get; set; }
    }
}