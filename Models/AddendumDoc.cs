﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MandiriCRE.Models
{
    public class AddendumDoc
    {
        public string AddendumDocID { get; set; }
        public string DocName { get; set; }
        public string Description { get; set; }
        public string DescriptionFile { get; set; }
        public string createdDate { get; set; }
        public string UplodedDate { get; set; }
        public string CreatedBy { get; set; }
        public string UplodedBy { get; set; }
        public string DocumentID { get; set; }
        public string FolderID { get; set; }
        public string FolderName { get; set; }
        public string ModulName { get; set; }
        public string TableId { get; set; }
        public string User_Name { get; set; }
        public string DocumentName { get; set; }
        public string UserName { get; set; }
        public Int32 Total { get; set; }
    }
}