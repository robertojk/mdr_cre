﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MandiriCRE.Models
{
    public class User
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string PasswordHash { get; set; }
        public string TeamName { get; set; }
        public string UserID { get; set; }
        public string Status { get; set; }
        public string Department_DepartmentID { get; set; }
        public string Team_TeamID { get; set; }
        public string DepartmentName { get; set; }
    }
}