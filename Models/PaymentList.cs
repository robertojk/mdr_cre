﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MandiriCRE.Models
{
    public class PaymentList
    {
        public Int32 ProcurementID { get; set; }
        public string ProcurementNo { get; set; }
        public Int32 VendorDetailID { get; set; }
        public string ProcurementVendorName { get; set; }
        public Int32 AgreementID { get; set; }
        public string AgreementNo { get; set; }
        public string AgreementName { get; set; }
        public string AgreementValue { get; set; }
        public string Status { get; set; }
        public string ProgressofWork { get; set; }
        public string ProgressofPayment { get; set; }
        public string Description { get; set; }
        public string isPaid { get; set; }
        public string PaymentDate { get; set; }
        public string ProgressofPaymentDoc { get; set; }
        public string ProgressofWorkDoc { get; set; }
        public string DocumentPaymentName { get; set; }
        public string DocumentPaymentPath { get; set; }
        public string DocumentWorkName { get; set; }
        public string DocumentWorkPath { get; set; }
        public Int32 VendorTerminID { get; set; }
        public string TotalBayar { get; set; }
        public string TotalKerja { get; set; }



    }
}