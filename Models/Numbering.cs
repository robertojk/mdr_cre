﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MandiriCRE.Models
{
    public class Numbering
    {
        public string No { get; set; }
        public string NumberingID { get; set; }
        public string NumberingType { get; set; }
        public string NumberingContent { get; set; }
        public string CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public string UpdateBy { get; set; }
        public string UpdatedDate { get; set; }
        public string USERNAME { get; set; }

    }
}