﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MandiriCRE.Models
{
    public class ReportOfficialResident
    {
        public string No { get; set; }
        public string OfficialResidenceID { get; set; }
        public string EmployeePositionID { get; set; }
        public string EmployeePositionName { get; set; }
        public string ResidentID { get; set; }
        public string Address { get; set; }
        public string CityID { get; set; }
        public string CityName { get; set; }
        public string BuildingArea { get; set; }
        public string LandArea { get; set; }
        public string ClassID { get; set; }
        public string ClassName { get; set; }
        public string ResidenceConditionID { get; set; }
        public string ResidenceConditionName { get; set; }
        public string NameOfResident { get; set; }
        public string NIP { get; set; }
        public string DateTo { get; set; }
        public string DateFrom { get; set; }
        public string Status { get; set; }
        public string FileDocID { get; set; }
        public string DocFileName { get; set; }
        public string OrderInformation { get; set; }
        public string EmptyingInformation { get; set; }
        public string CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
    }
}