﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MandiriCRE.Models
{
    public class EmployeePosition
    {
        public string EmployeePositionName { get; set; }
        public string Description { get; set; }
        public string EmployeePositionID { get; set; }
    }
}