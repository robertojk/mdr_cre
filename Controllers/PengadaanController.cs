﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using MandiriCRE.Models;
using System.IO;
using System.Data.SqlClient;
using System.Data;
using System.Web.UI;

namespace MandiriCRE.Controllers
{

    public class PengadaanController : Controller
    {

        string Baseurl = Properties.Settings.Default.BaseURI;

        public Page Page { get; private set; }
        public object Controls { get; private set; }

        // GET: Pengadaan
        public async Task<ActionResult> Index()
        {

            List<Procurements> objInfo = new List<Procurements>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.ProcurementAPI);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<List<Procurements>>(APIResponse);

                }

                //returning the employee list to view  
                objInfo = new List<Procurements>(objInfo.OrderByDescending(Procurements => Procurements.ProcurementID));
                return View(objInfo);
            }
        }

        // GET: Procurement/Details/5
        public async Task<ActionResult> Details(string id)
        {
            ProcurementHeader objInfo = new ProcurementHeader();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllArticles using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.ProcurementHeaderAPI + "/" + id);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<ProcurementHeader>(APIResponse);

                }
                ViewBag.ProcurementID = id;
                return View(objInfo);
            }
        }
        public async Task<ActionResult> Approve(string id, string WF)
        {
            ProcurementHeader objInfo = new ProcurementHeader();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllArticles using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.ProcurementHeaderAPI + "/" + id);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<ProcurementHeader>(APIResponse);

                }
                List<Employee> _listEmployee = await GetListEmployees();
                ViewBag.ListEmployee = _listEmployee;
                List<Numbering> _listTemplates = await GetListDocs();
                ViewBag.ListTemplates = _listTemplates;
                List<ProcurementDoc> _listProcurementDocs = await GetListProcurementDocs(id);
                ViewBag.ListProcurementDoc = _listProcurementDocs;

                List<WorkFlowHistory> _listHistories = await GetListHistory(id);
                ViewBag.ListWorkFlow = _listHistories;

                List<ProcurementDetail> _ListProcurementDetail = await GetListProcurementDetail(id);
                ViewBag.ListProcurementDetail = _ListProcurementDetail;
                #region Tax Calculation
                int sebelumPajak = 0;
                int sesudahPajak = 0;
                int pajak = 0;
                foreach (ProcurementDetail obj in _ListProcurementDetail)
                {
                    sebelumPajak = sebelumPajak + Convert.ToInt32(obj.Price);
                    pajak = Convert.ToInt32(obj.Tax);
                }
                sesudahPajak = sebelumPajak + (sebelumPajak * pajak);
                ViewBag.sebelumPajak = sebelumPajak.ToString("N");
                ViewBag.pajak = pajak.ToString() + "%";
                ViewBag.sesudahPajak = sesudahPajak.ToString("N");
                #endregion

                List<ProcurementVendor> _listVendor = await GetListVendor(id);
                ViewBag.ListProcurementVendor = _listVendor;

                ViewBag.ProcurementID = id;
                ViewBag.objInfo = objInfo;

                ViewBag.WF = WF;
                return View(objInfo);
            }

        }
        // POST: Procurement/Create
        [HttpPost]
        public async Task<ActionResult> Approve(string id, string WF, ProcurementHeader obj, FormCollection collection)
        {
            try
            {
                string Action = "APPROVE";
                if (collection["Approve"] == null)
                {
                    Action = "REJECT";
                }
                obj.AdditionalInformation = collection["description"].ToString();
                obj.DateEnd = collection["finishdate"].ToString();
                obj.DateStart = collection["startdate"].ToString();
                obj.DocumentNo = collection["documentNumber"].ToString();
                obj.EmployeeManagerID = obj.EmployeeManagerID;
                obj.EmployeePICID = obj.EmployeePICID;
                obj.ProcurementName = collection["procTitle"].ToString();
                obj.ProcurementID = id;
                obj.UserName = User.Identity.Name;
                // TODO: Add update logic here
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                // TODO: Add update logic here
                HttpResponseMessage response = await client.PutAsJsonAsync(Properties.Settings.Default.ProcurementHeaderAPI + "/" + id, obj);
                response.EnsureSuccessStatusCode();

                //Approve process
                string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    using (var cmd = new SqlCommand("sp_GetNextApproval", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@WFName", "WF_PENGADAAN");
                        cmd.Parameters.AddWithValue("@Action", Action);
                        cmd.Parameters.AddWithValue("@WorkflowId", WF);
                        cmd.Parameters.AddWithValue("@MainTableId", id);
                        cmd.Parameters.AddWithValue("@RejectReason", "");
                        cmd.Parameters.AddWithValue("@USER_NAME", User.Identity.Name);
                        conn.Open();
                        int k = cmd.ExecuteNonQuery();
                        if (k != 0)
                        {
                            //sukses
                        }
                        conn.Close();
                    }
                }

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }
        //GET: Procurement/Create
        public async Task<ActionResult> Create(string id, string Status, string WF, string ErrorMessage)
        {
            //Session["activeTabPengadaan"] = "s1";

            ProcurementHeader objInfo = new ProcurementHeader();
            if (ErrorMessage != null)
            {
                ViewBag.ErrorMessage = ErrorMessage;
            }

            if (Status == "Edit")
            {
                Session["Action"] = "SIMPAN";
            }
            else
            {
                Session["Action"] = "APPROVE";
            }

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllArticles using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.ProcurementHeaderAPI + "/" + id);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<ProcurementHeader>(APIResponse);

                }
                List<Employee> _listEmployee = await GetListEmployees();
                ViewBag.ListEmployee = _listEmployee;
                List<Numbering> _listTemplates = await GetListDocs();
                ViewBag.ListTemplates = _listTemplates;
                List<ProcurementDoc> _listProcurementDocs = await GetListProcurementDocs(id);
                ViewBag.ListProcurementDoc = _listProcurementDocs;
                ViewBag.Total = 0;
                foreach (ProcurementDoc vardoc in _listProcurementDocs)
                {
                    ViewBag.Total = vardoc.Total;
                }

                List<WorkFlowHistory> _listHistories = await GetListHistory(id);
                ViewBag.ListWorkFlow = _listHistories;

                List<ProcurementDetail> _ListProcurementDetail = await GetListProcurementDetail(id);
                ViewBag.ListProcurementDetail = _ListProcurementDetail;
                #region Tax Calculation
                double Penawaran = 0;
                double Negosiasi = 0;
                double pajak = 0;
                foreach (ProcurementDetail obj in _ListProcurementDetail)
                {
                    Penawaran = Penawaran + Convert.ToDouble(obj.Price);
                    Negosiasi = Negosiasi + Convert.ToDouble(obj.Negotiation);
                    pajak = Convert.ToDouble(obj.Tax);
                }
                ViewBag.Penawaran = Penawaran.ToString("N0");
                ViewBag.Negosiasi = Negosiasi.ToString("N0");

                //sesudahPajak = sebelumPajak + ((sebelumPajak * pajak)/100);
                //ViewBag.sebelumPajak = sebelumPajak.ToString("N");
                //ViewBag.pajak = pajak.ToString() + "%";
                //ViewBag.sesudahPajak = sesudahPajak.ToString("N");
                //sesudahPajak = sebelumPajak + ((sebelumPajak * pajak) / 100);
                //ViewBag.sebelumPajak = sebelumPajak;
                //ViewBag.pajak = pajak.ToString();
                //ViewBag.sesudahPajak = sesudahPajak;
                #endregion

                List<ProcurementVendor> _listVendor = await GetListVendor(id);
                ViewBag.ListProcurementVendor = _listVendor;

                ViewBag.ProcurementID = id;
                ViewBag.objInfo = objInfo;
                ViewBag.WF = WF;
                ViewBag.Status = Status;
                return View(objInfo);
            }

        }
        // POST: Procurement/Create
        [HttpPost]
        public async Task<ActionResult> Create(string id, string WF, ProcurementHeader obj, FormCollection collection)
        {
            try
            {
                ProcurementHeader objInfo = new ProcurementHeader();

                string Action = "SIMPAN";
                if (collection["Approve"] != null)
                {
                    Action = "APPROVE";
                }
                else if (collection["Reject"] != null)
                {
                    Action = "REJECT";
                }
                //Logic to save Tax
                ProcurementDetail objDetail = new ProcurementDetail();
               // objDetail.Tax = collection["pajak"].ToString();
                objDetail.ProcurementID = id;
                //call procedure update pajak dengan parameter Nilai Pajak dan ProcurementID
                //UpdateTax(objDetail);
                //end of the logic to save Tax

                obj.AdditionalInformation = collection["description"].ToString();
                obj.DateEnd = collection["finishdate"].ToString();
                obj.DateStart = collection["startdate"].ToString();
                obj.DocumentNo = collection["documentNumber"].ToString();
                obj.EmployeeManagerID = obj.EmployeeManagerID;
                obj.EmployeePICID = obj.EmployeePICID;
                if (collection["picName"] != null && collection["picName"] != "")
                {
                    obj.EmployeePICID = collection["picName"];
                }
                if (collection["ManName"] != null && collection["ManName"] != "")
                {
                    obj.EmployeeManagerID = collection["ManName"];
                }

                obj.ProcurementName = collection["procTitle"].ToString();
                obj.ProcurementID = id;
                obj.UserName = User.Identity.Name;
                //cek duplicate
                string ErrorMessage = "";
                ProcurementHeader obj2 = new ProcurementHeader();
                string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                using (SqlConnection conn2 = new SqlConnection(connectionstring))
                {
                    using (var cmd2 = new SqlCommand("sp_CheckJudulPengadaan", conn2))
                    {
                        cmd2.CommandType = CommandType.StoredProcedure;
                        cmd2.Parameters.AddWithValue("@Title", obj.ProcurementName);
                        cmd2.Parameters.AddWithValue("@DocumentNo", obj.DocumentNo);
                        cmd2.Parameters.AddWithValue("@ProcurementID", obj.ProcurementID);
                        conn2.Open();

                        using (var reader = cmd2.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                obj2.ProcurementID = reader[0].ToString();
                                ErrorMessage = reader[1].ToString();
                            }
                        }
                        conn2.Close();
                    }
                    //using (var cmd3 = new SqlCommand("sp_UpdateProcurementDetail", conn2))
                    //{
                    //    cmd3.CommandType = CommandType.StoredProcedure;
                    //    cmd3.Parameters.AddWithValue("@ProcurementID", obj.ProcurementID);
                    //    cmd3.Parameters.AddWithValue("@Tax", objDetail.Tax);
                    //    conn2.Open();
                    //    int k = cmd3.ExecuteNonQuery();
                    //    if (k != 0)
                    //    {
                    //        //sukses
                    //    }
                    //    conn2.Close();
                    //}

                }

                if (ErrorMessage != null && ErrorMessage != "")
                {

                    if (Session["Action"] == "SIMPAN")
                    {
                        return RedirectToAction("Create", "Pengadaan", new { id = id, Status = "Edit", WF = WF, ErrorMessage = ErrorMessage });
                    }
                    else
                    {
                        return RedirectToAction("Create", "Pengadaan", new { id = id, Status = "Approve", WF = WF, ErrorMessage = ErrorMessage });
                    }

                }
                // TODO: Add update logic here
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //Get data Procurement
                //Sending request to find web api REST service resource GetAllArticles using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.ProcurementHeaderAPI + "/" + id);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<ProcurementHeader>(APIResponse);

                }

                // TODO: Add update logic here
                HttpResponseMessage response = await client.PutAsJsonAsync(Properties.Settings.Default.ProcurementHeaderAPI + "/" + id, obj);
                response.EnsureSuccessStatusCode();

                //validate approval
                //isWIN and isTermin
                string ErrorMessageApprove = "";
                if (Action == "APPROVE")
                {
                    using (SqlConnection conn = new SqlConnection(connectionstring))
                    {
                        using (var cmd = new SqlCommand("sp_CheckProcApprove", conn))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@ProcurementID", id);
                            conn.Open();

                            using (var reader = cmd.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    ErrorMessageApprove = reader[0].ToString();
                                }
                            }
                        }

                    }

                    if (ErrorMessageApprove != null && ErrorMessageApprove != "")
                    {

                        return RedirectToAction("Create", "Pengadaan", new { id = id, Status = "Approve", WF = WF, ErrorMessage = ErrorMessageApprove });

                    }
                }
                //Approve process
                if (Action != "SIMPAN")
                {
                    WF = objInfo.Workflow_WorkflowID;
                    using (SqlConnection conn = new SqlConnection(connectionstring))
                    {
                        using (var cmd = new SqlCommand("sp_GetNextApproval", conn))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@WFName", "WF_PENGADAAN");
                            cmd.Parameters.AddWithValue("@Action", Action);
                            cmd.Parameters.AddWithValue("@WorkflowId", WF);
                            cmd.Parameters.AddWithValue("@MainTableId", id);
                            cmd.Parameters.AddWithValue("@RejectReason", "");
                            cmd.Parameters.AddWithValue("@USER_NAME", User.Identity.Name);
                            conn.Open();
                            int k = cmd.ExecuteNonQuery();
                            if (k != 0)
                            {
                                //sukses
                            }
                            conn.Close();
                        }

                    }



                }

                Session["Action"] = "";
                Session["activeTabPengadaan"] = null;
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }

        //GET: Procurement/Create
        public async Task<ActionResult> CreatePengadaan(string ErrorMessage, ProcurementHeader objProcurement)
        {
            List<Employee> _listEmployee = await GetListEmployees();
            ViewBag.ListEmployee = _listEmployee;
            ProcurementHeader objInfo = new ProcurementHeader();
            List<Numbering> _listTemplates = await GetListDocs();
            ViewBag.ListTemplates = _listTemplates;
            if (objProcurement != null)
            {
                objInfo = objProcurement;
            }
            if (ErrorMessage != null)
            {
                ViewBag.ErrorMessage = ErrorMessage;
            }
            return View(objInfo);
        }
        // POST: Procurement/Create
        [HttpPost]
        public async Task<ActionResult> CreatePengadaan(ProcurementHeader obj, FormCollection collection)
        {
            try
            {
                string ErrorMessage = "";
                obj.AdditionalInformation = collection["description"].ToString();
                obj.DateEnd = collection["finishdate"].ToString();
                obj.DateStart = collection["startdate"].ToString();
                obj.DocumentNo = collection["DocumentNo"].ToString() + collection["documentNumber"].ToString();
                obj.EmployeeManagerID = obj.EmployeeManagerID;
                obj.EmployeePICID = obj.EmployeePICID;
                if ((collection["picName"] != null && collection["picName"] != "") || (obj.EmployeePICName != null && obj.EmployeePICName != ""))
                {
                    obj.EmployeePICID = collection["picName"];
                    //obj.EmployeePICID = obj.EmployeePICName;
                }
                if ((collection["ManName"] != null && collection["ManName"] != "") || (obj.EmployeeManagerName != null && obj.EmployeeManagerName != ""))
                {
                    obj.EmployeeManagerID = collection["ManName"];
                    //obj.EmployeeManagerID = obj.EmployeeManagerName;
                }

                obj.ProcurementName = collection["procTitle"].ToString();
                obj.UserName = User.Identity.Name;
                ProcurementHeader obj2 = new ProcurementHeader();
                string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    using (var cmd = new SqlCommand("sp_CheckJudulPengadaan", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@Title", obj.ProcurementName);
                        cmd.Parameters.AddWithValue("@DocumentNo", obj.DocumentNo);
                        cmd.Parameters.AddWithValue("@ProcurementID", "");
                        conn.Open();

                        using (var reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                obj2.ProcurementID = reader[0].ToString();
                                ErrorMessage = reader[1].ToString();
                            }
                        }
                    }

                }
                
                if (ErrorMessage != null && ErrorMessage != "")
                {
                    obj.DocumentNo = collection["documentNumber"].ToString();
                    obj.EmployeePICID = "";
                    obj.EmployeeManagerID = "";
                    //return RedirectToAction("CreatePengadaan", new { ErrorMessage = ErrorMessage });
                    return await CreatePengadaan(ErrorMessage, obj);
                }

                // TODO: Add update logic here
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                // TODO: Add update logic here
                HttpResponseMessage response = await client.PostAsJsonAsync(Properties.Settings.Default.ProcurementHeaderAPI + "/" + "0", obj);
                response.EnsureSuccessStatusCode();
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }

        // GET: Procurement/Edit/5
        public async Task<ActionResult> Edit(string id)
        {
            ProcurementHeader objInfo = new ProcurementHeader();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllArticles using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.ProcurementHeaderAPI + "/" + id);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<ProcurementHeader>(APIResponse);

                }
                List<Employee> _listEmployee = await GetListEmployees();
                ViewBag.ListEmployee = _listEmployee;
                //List<Template> _listTemplates = await GetListDocs();
                //ViewBag.ListTemplates = _listTemplates;
                //returning the employee list to view  
                return View(objInfo);
            }
        }
        // POST: Procurement/Edit/5
        [HttpPost]
        public async Task<ActionResult> Edit(string id, ProcurementHeader obj, FormCollection collection)
        {
            try
            {
                obj.AdditionalInformation = collection["description"].ToString();
                obj.DateEnd = collection["finishdate"].ToString();
                obj.DateStart = collection["startdate"].ToString();
                obj.DocumentNo = collection["documentNumber"].ToString();
                obj.EmployeeManagerID = obj.EmployeeManagerID;
                obj.EmployeePICID = obj.EmployeePICID;
                obj.ProcurementName = collection["procTitle"].ToString();
                obj.ProcurementID = id;
                obj.UserName = User.Identity.Name;
                // TODO: Add update logic here
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                // TODO: Add update logic here
                HttpResponseMessage response = await client.PutAsJsonAsync(Properties.Settings.Default.ProcurementHeaderAPI + "/" + id, obj);
                response.EnsureSuccessStatusCode();
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }
        //public async Task<ActionResult> Approve(int id,string WF)
        //{
        //    ProcurementHeader objInfo = new ProcurementHeader();

        //    using (var client = new HttpClient())
        //    {
        //        //Passing service base url  
        //        client.BaseAddress = new Uri(Baseurl);

        //        client.DefaultRequestHeaders.Clear();
        //        //Define request data format  
        //        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

        //        //Sending request to find web api REST service resource GetAllArticles using HttpClient  
        //        HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.ArticleAPI + "/" + id);

        //        //Checking the response is successful or not which is sent using HttpClient  
        //        if (Res.IsSuccessStatusCode)
        //        {
        //            //Storing the response details recieved from web api   
        //            var APIResponse = Res.Content.ReadAsStringAsync().Result;

        //            //Deserializing the response recieved from web api and storing into the Employee list  
        //            objInfo = JsonConvert.DeserializeObject<ProcurementHeader>(APIResponse);

        //        }

        //        ViewBag.WF = WF;
        //        //returning the employee list to view  
        //        return View(objInfo);
        //    }
        //}
        // POST: Procurement/Edit/5
        //[HttpPost]
        //public async Task<ActionResult> Approve(int id,string WF, ProcurementHeader obj, FormCollection collection)
        //{
        //    try
        //    {

        //        // TODO: Add update logic here
        //        HttpClient client = new HttpClient();
        //        client.BaseAddress = new Uri(Baseurl);
        //        client.DefaultRequestHeaders.Accept.Clear();
        //        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        //        // TODO: Add update logic here
        //        HttpResponseMessage response = await client.PutAsJsonAsync(Properties.Settings.Default.ArticleAPI + "/" + id, obj);
        //        response.EnsureSuccessStatusCode();
        //        return RedirectToAction("Index");
        //    }
        //    catch (Exception ex)
        //    {
        //        string err = ex.Message.ToString();
        //        return View();
        //    }
        //}
        public async Task<ActionResult> Duplicate(int id)
        {
            ProcurementHeader objInfo = new ProcurementHeader();

            //using (var client = new HttpClient())
            //{
            //    //Passing service base url  
            //    client.BaseAddress = new Uri(Baseurl);

            //    client.DefaultRequestHeaders.Clear();
            //    //Define request data format  
            //    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            //    //Sending request to find web api REST service resource GetAllArticles using HttpClient  
            //    HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.ArticleAPI + "/" + id);

            //    //Checking the response is successful or not which is sent using HttpClient  
            //    if (Res.IsSuccessStatusCode)
            //    {
            //        //Storing the response details recieved from web api   
            //        var APIResponse = Res.Content.ReadAsStringAsync().Result;

            //        //Deserializing the response recieved from web api and storing into the Employee list  
            //        objInfo = JsonConvert.DeserializeObject<ProcurementHeader>(APIResponse);

            //    }
            //    //returning the employee list to view  
            //    return View(objInfo);
            //}

            // TODO: Add update logic here
            string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(connectionstring))
            {
                using (var cmd = new SqlCommand("sp_DuplicatebyProcurementID", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ProcurementID", id);
                    cmd.Parameters.AddWithValue("@UserName", User.Identity.Name);
                    conn.Open();
                    int k = cmd.ExecuteNonQuery();
                    if (k != 0)
                    {
                        //sukses
                    }
                    conn.Close();
                }
            }

            return RedirectToAction("Index", "Pengadaan");
        }
        // POST: Procurement/Edit/5
        [HttpPost]
        public async Task<ActionResult> Duplicate(int id, ProcurementHeader obj, FormCollection collection)
        {
            try
            {

                // TODO: Add update logic here
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                // TODO: Add update logic here
                HttpResponseMessage response = await client.PutAsJsonAsync(Properties.Settings.Default.ArticleAPI + "/" + id, obj);
                response.EnsureSuccessStatusCode();
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }
        public async Task<List<ProcurementDoc>> Attachments(string id)
        {
            List<ProcurementDoc> objInfo = new List<ProcurementDoc>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.ProcurementDocsAPI + "/" + id);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<List<ProcurementDoc>>(APIResponse);

                }
                //ViewBag.ProcurementID = id;
                //returning the employee list to view  
                //return View(objInfo);
                return objInfo;
            }
        }
        public ActionResult CreateAttachmentPengadaan(string id, int Total, string ErrorMessage)
        {
            Session["activeTabPengadaan"] = "s2";
            ProcurementDoc objInfo = new ProcurementDoc();
            if (ErrorMessage != null)
            {
                ViewBag.ErrorMessage = ErrorMessage;
            }
            ViewBag.Total = Total;
            return View(objInfo);
        }
        // POST: CRUD/Create
        [HttpPost]
        public async Task<ActionResult> CreateAttachmentPengadaan(string id, int Total, ProcurementDoc obj, FormCollection form, HttpPostedFileBase file)
        {
            try
            {
                Session["activeTabPengadaan"] = "s2";
                string ErrorMessage = "";
                string fileName = "";
                string path = "";
                string fileType = "";
                if (Total < 10)
                {
                    if (file != null && file.ContentLength > 0)
                    {
                        fileType = Path.GetExtension(file.FileName);
                        if (fileType.ToUpper().Contains("DOC") || fileType.ToUpper().Contains("XLS") || fileType.ToUpper().Contains("PPT") || fileType.ToUpper().Contains("PDF"))
                        {
                            if (file.ContentLength <= (2000 * 1024))
                            {
                                fileName = Path.GetFileName(file.FileName);
                                path = Path.Combine(Server.MapPath("~/content/document/procurement/"), fileName);
                                file.SaveAs(path);
                            }
                            else
                            {
                                ErrorMessage = "File Upload tidak boleh lebih dari 2MB";
                                return RedirectToAction("CreateAttachmentPengadaan", new { id = id,Total=Total, ErrorMessage = ErrorMessage });
                            }
                        }
                        else
                        {
                            ErrorMessage = "File Upload harus dengan format All Office atau PDF";
                            return RedirectToAction("CreateAttachmentPengadaan", new { id = id, Total = Total, ErrorMessage = ErrorMessage });
                        }
                    }
                }
                else
                {
                    ErrorMessage = "Penambahan gagal, jumlah dokumen maksimal adalah 10";
                    return RedirectToAction("CreateAttachmentPengadaan", new { id = id, Total = Total, ErrorMessage = ErrorMessage });
                }
                obj.DocumentName = fileName;
                obj.createdDate = DateTime.Now.ToString("dd/MM/yyyy");
                obj.CreatedBy = User.Identity.Name;
                obj.UplodedDate = DateTime.Now.ToString("dd/MM/yyyy");
                obj.UplodedBy = User.Identity.Name;
                obj.ModulName = "PENGADAAN";
                obj.TableId = id;
                obj.Description = form["description"].ToString();
                obj.FolderName = "template";
                obj.User_Name = User.Identity.Name;
                //check Duplicate
                ProcurementDoc obj2 = new ProcurementDoc();
                string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    using (var cmd = new SqlCommand("sp_CheckFilePengadaan", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@FileName", obj.DocumentName);
                        cmd.Parameters.AddWithValue("@Description", obj.Description);
                        cmd.Parameters.AddWithValue("@ProcurementID", id);
                        cmd.Parameters.AddWithValue("@ProcurementDocID", "");
                        conn.Open();

                        using (var reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                obj2.ProcurementDocID = reader[0].ToString();
                            }
                        }
                    }

                }

                if (obj2.ProcurementDocID != null)
                {
                    ErrorMessage = "Nama Dokumen atau Keterangan Sudah Ada";
                    return RedirectToAction("CreateAttachmentPengadaan", new { id = id, Total = Total, ErrorMessage = ErrorMessage });
                }

                // TODO: Add update logic here
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                // TODO: Add update logic here
                HttpResponseMessage response = await client.PostAsJsonAsync(Properties.Settings.Default.ProcurementDocsAPI + "/" + id + "/0", obj);
                response.EnsureSuccessStatusCode();
                if (Session["Action"] == "SIMPAN")
                {
                    return RedirectToAction("Create", "Pengadaan", new { id = id, Status = "Edit" });
                }
                else
                {
                    return RedirectToAction("Create", "Pengadaan", new { id = id, Status = "Approve" });
                }


            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }
        public async Task<ActionResult> EditAttachmentPengadaan(string id, string procurementId, string ErrorMessage)
        {
            try
            {
                Session["activeTabPengadaan"] = "s2";
                // TODO: Add delete logic here
                ProcurementDoc objInfo = new ProcurementDoc();
                if (ErrorMessage != null)
                {
                    ViewBag.ErrorMessage = ErrorMessage;
                }
                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(Baseurl);

                    client.DefaultRequestHeaders.Clear();
                    //Define request data format  
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                    HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.ProcurementDocsAPI + "/" + procurementId + "/" + id);

                    //Checking the response is successful or not which is sent using HttpClient  
                    if (Res.IsSuccessStatusCode)
                    {
                        //Storing the response details recieved from web api   
                        var APIResponse = Res.Content.ReadAsStringAsync().Result;

                        //Deserializing the response recieved from web api and storing into the Employee list  
                        objInfo = JsonConvert.DeserializeObject<ProcurementDoc>(APIResponse);

                    }
                    ViewBag.ProcurementID = procurementId;
                    ViewBag.id = id;
                    //returning the employee list to view  
                    return View(objInfo);
                }
            }
            catch
            {
                return View();
            }
        }
        // POST: CRUD/Create
        [HttpPost]
        public async Task<ActionResult> EditAttachmentPengadaan(string id, ProcurementDoc obj, FormCollection form, HttpPostedFileBase file)
        {

            try
            {
                Session["activeTabPengadaan"] = "s2";
                string procurementId = form["procurementId"].ToString();
                string fileName = "";
                string path = "";
                string ErrorMessage = "";
                string fileType = "";
                if (file != null && file.ContentLength > 0)
                {
                    fileType = Path.GetExtension(file.FileName);
                    if (fileType.ToUpper().Contains("DOC") || fileType.ToUpper().Contains("XLS") || fileType.ToUpper().Contains("PPT") || fileType.ToUpper().Contains("PDF"))
                    {
                        if (file.ContentLength <= (2000 * 1024))
                        {
                            fileName = Path.GetFileName(file.FileName);
                            path = Path.Combine(Server.MapPath("~/content/document/procurement/"), fileName);
                            file.SaveAs(path);
                        }
                        else
                        {
                            ErrorMessage = "File Upload tidak boleh lebih dari 2MB";
                            return RedirectToAction("EditAttachmentPengadaan", new { id = id, procurementId = procurementId, ErrorMessage = ErrorMessage });
                        }
                    }
                    else
                    {
                        ErrorMessage = "File Upload harus dengan format All Office atau PDF";
                        return RedirectToAction("EditAttachmentPengadaan", new { id = id, procurementId = procurementId, ErrorMessage = ErrorMessage });
                    }
                }
                if (file == null && (form["filename"].ToString() != null || form["filename"].ToString() != ""))
                {
                    fileName = form["filename"].ToString();
                    path = Path.Combine(Server.MapPath("~/content/document/procurement/"), fileName);
                }
                obj.ProcurementDocID = id;
                obj.DocName = fileName;
                obj.UserName = User.Identity.Name;
                obj.UplodedDate = DateTime.Now.ToString("dd/MM/yyyy");
                obj.UplodedBy = User.Identity.Name;
                obj.ModulName = "PENGADAAN";
                obj.TableId = id;
                obj.Description = form["description"].ToString();
                obj.FolderName = "template";
                obj.User_Name = User.Identity.Name;
                //check Duplicate
                ProcurementDoc obj2 = new ProcurementDoc();
                string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    using (var cmd = new SqlCommand("sp_CheckFilePengadaan", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@FileName", obj.DocName);
                        cmd.Parameters.AddWithValue("@Description", obj.Description);
                        cmd.Parameters.AddWithValue("@ProcurementID", procurementId);
                        cmd.Parameters.AddWithValue("@ProcurementDocID", id);
                        conn.Open();

                        using (var reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                obj2.ProcurementDocID = reader[0].ToString();
                            }
                        }
                    }

                }

                if (obj2.ProcurementDocID != null)
                {
                    ErrorMessage = "Nama Dokumen atau Keterangan Sudah Ada";
                    return RedirectToAction("EditAttachmentPengadaan", new { id = id, procurementId = procurementId, ErrorMessage = ErrorMessage });

                }
                // TODO: Add update logic here
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                // TODO: Add update logic here
                HttpResponseMessage response = await client.PutAsJsonAsync(Properties.Settings.Default.ProcurementDocsAPI + "/" + id + "/0", obj);
                response.EnsureSuccessStatusCode();

                if (Session["Action"] == "SIMPAN")
                {
                    return RedirectToAction("Create", "Pengadaan", new { id = procurementId, Status = "Edit" });
                }
                else
                {
                    return RedirectToAction("Create", "Pengadaan", new { id = id, Status = "Approve" });
                }
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }
        // GET: CRUD/Delete/5
        public async Task<ActionResult> DeleteAttachmentPengadaan(string id, string procurementId)
        {
            try
            {
                Session["activeTabPengadaan"] = "s2";
                // TODO: Add delete logic here
                ProcurementDoc objInfo = new ProcurementDoc();

                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(Baseurl);

                    client.DefaultRequestHeaders.Clear();
                    //Define request data format  
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                    HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.ProcurementDocsAPI + "/" + procurementId + "/" + id);

                    //Checking the response is successful or not which is sent using HttpClient  
                    if (Res.IsSuccessStatusCode)
                    {
                        //Storing the response details recieved from web api   
                        var APIResponse = Res.Content.ReadAsStringAsync().Result;

                        //Deserializing the response recieved from web api and storing into the Employee list  
                        objInfo = JsonConvert.DeserializeObject<ProcurementDoc>(APIResponse);

                    }
                    ViewBag.ProcurementID = procurementId;
                    ViewBag.id = id;
                    //returning the employee list to view  
                    return View(objInfo);
                }
            }
            catch
            {
                return View();
            }
        }
        // POST: CRUD/Delete/5
        [HttpPost]
        public async Task<ActionResult> DeleteAttachmentPengadaan(string id, string procurementId, ProcurementDoc obj, FormCollection collection)
        {
            // TODO: Add delete logic here
            try
            {
                Session["activeTabPengadaan"] = "s2";
                //obj.ProcurementDocID = collection["ProcurementDocID"].ToString();
                procurementId = collection["procurementId"].ToString();
                // TODO: Add update logic here
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                // TODO: Add update logic here
                HttpResponseMessage response = await client.DeleteAsync(Properties.Settings.Default.ProcurementDocsAPI + "/" + procurementId + "/" + id);
                response.EnsureSuccessStatusCode();
                if (Session["Action"] == "SIMPAN")
                {
                    return RedirectToAction("Create", "Pengadaan", new { id = procurementId, Status = "Edit" });
                }
                else
                {
                    return RedirectToAction("Create", "Pengadaan", new { id = procurementId, Status = "Approve" });
                }
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }

        // GET: CRUD/Create
        public ActionResult CreateDetailPengadaan(string id)
        {
            Session["activeTabPengadaan"] = "s4";
            ProcurementDetail objInfo = new ProcurementDetail();
            return View(objInfo);
        }
        // POST: CRUD/Create
        [HttpPost]
        public async Task<ActionResult> CreateDetailPengadaan(string id, ProcurementDetail obj, FormCollection collection)
        {
            try
            {
                Session["activeTabPengadaan"] = "s4";
                obj.UserName = User.Identity.Name;
                obj.ProcurementID = id;
                obj.TaskNameDetail = collection["taskNameDetail"].ToString();
                obj.Price = collection["price"].ToString();
                obj.Negotiation = collection["negotiation"].ToString();
                //obj.Tax = collection["tax"].ToString();
                obj.Tax = "0";
                //obj.CreatedDate = DateTime.Now.ToString("dd/MM/yyyy");
                // TODO: Add update logic here
                //HttpClient client = new HttpClient();
                //client.BaseAddress = new Uri(Baseurl);
                //client.DefaultRequestHeaders.Accept.Clear();
                //client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                // TODO: Add update logic here
                //HttpResponseMessage response = await client.PostAsJsonAsync(Properties.Settings.Default.ProcurementDetailsAPI + "/" + id + "/0", obj);
                //response.EnsureSuccessStatusCode();

                string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    using (var cmd = new SqlCommand("sp_InsertProcurementDetail", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ProcurementID", id);
                        cmd.Parameters.AddWithValue("@TaskNameDetail", obj.TaskNameDetail);
                        cmd.Parameters.AddWithValue("@Price", obj.Price);
                        cmd.Parameters.AddWithValue("@Negotiation", obj.Negotiation);
                        cmd.Parameters.AddWithValue("@UserName", User.Identity.Name);
                        conn.Open();
                        int k = cmd.ExecuteNonQuery();
                        if (k != 0)
                        {
                            //sukses
                        }
                        conn.Close();
                    }
                }

                if (Session["Action"] == "SIMPAN")
                {
                    return RedirectToAction("Create", "Pengadaan", new { id = id, Status = "Edit" });
                }
                else
                {
                    return RedirectToAction("Create", "Pengadaan", new { id = id, Status = "Approve" });
                }
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }
        // GET: CRUD/Delete/5
        public async Task<ActionResult> DeleteDetailPengadaan(string id, string procurementId)
        {
            try
            {
                Session["activeTabPengadaan"] = "s4";
                // TODO: Add delete logic here
                ProcurementDetail objInfo = new ProcurementDetail();

                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(Baseurl);

                    client.DefaultRequestHeaders.Clear();
                    //Define request data format  
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                    HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.ProcurementDetailsAPI + "/" + procurementId + "/" + id);

                    //Checking the response is successful or not which is sent using HttpClient  
                    if (Res.IsSuccessStatusCode)
                    {
                        //Storing the response details recieved from web api   
                        var APIResponse = Res.Content.ReadAsStringAsync().Result;

                        //Deserializing the response recieved from web api and storing into the Employee list  
                        objInfo = JsonConvert.DeserializeObject<ProcurementDetail>(APIResponse);

                    }
                    ViewBag.ProcurementID = procurementId;
                    //returning the employee list to view  
                    return View(objInfo);
                }
            }
            catch
            {
                return View();
            }
        }
        // POST: CRUD/Delete/5
        [HttpPost]
        public async Task<ActionResult> DeleteDetailPengadaan(string id, ProcurementDetail obj, FormCollection collection)
        {
            // TODO: Add delete logic here
            try
            {
                Session["activeTabPengadaan"] = "s4";
                string procurementId = collection["procurementId"].ToString();
                // TODO: Add update logic here
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                // TODO: Add update logic here
                HttpResponseMessage response = await client.DeleteAsync(Properties.Settings.Default.ProcurementDetailsAPI + "/" + procurementId + "/" + id);
                response.EnsureSuccessStatusCode();

                if (Session["Action"] == "SIMPAN")
                {
                    return RedirectToAction("Create", "Pengadaan", new { id = procurementId, Status = "Edit" });
                }
                else
                {
                    return RedirectToAction("Create", "Pengadaan", new { id = procurementId, Status = "Approve" });
                }
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }
        public async Task<ActionResult> Partner(string id)
        {
            List<ProcurementVendor> objInfo = new List<ProcurementVendor>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.ProcurementVendorsAPI + "/" + id);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<List<ProcurementVendor>>(APIResponse);

                }
                ViewBag.ProcurementID = id;
                //returning the employee list to view  
                return View(objInfo);
            }
        }
        // GET: CRUD/Create
        public async Task<ActionResult> CreateVendor(string id)
        {
            Session["activeTabPengadaan"] = "s3";
            ProcurementVendor objInfo = new ProcurementVendor();
            List<Partner> _listPartners = await GetListPartners();
            ViewBag.ListPartners = _listPartners;
            ViewBag.ProcurementID = id;
            return View(objInfo);
        }
        // POST: CRUD/Create
        [HttpPost]
        public async Task<ActionResult> CreateVendor(string id, ProcurementVendor obj, FormCollection collection)
        {
            try
            {
                Session["activeTabPengadaan"] = "s3";
                //obj.UserName = User.Identity.Name;
                //obj.ProcurementID = id;
                obj.VendorID = collection["VendorID"].ToString();
                obj.Procurement_ProcurementID = collection["procurementId"].ToString();
                obj.ProcurementID = collection["procurementId"].ToString();
                obj.Username = User.Identity.Name;
                //obj.CreatedDate = DateTime.Now.ToString("dd/MM/yyyy");
                // TODO: Add update logic here
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                // TODO: Add update logic here
                HttpResponseMessage response = await client.PostAsJsonAsync(Properties.Settings.Default.ProcurementVendorsAPI + "/" + id + "/" + obj.VendorID, obj);
                response.EnsureSuccessStatusCode();
                if (Session["Action"] == "SIMPAN")
                {
                    return RedirectToAction("Create", "Pengadaan", new { id = id, Status = "Edit" });
                }
                else
                {
                    return RedirectToAction("Create", "Pengadaan", new { id = id, Status = "Approve" });
                }
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }
        public async Task<List<Partner>> GetListPartners()
        {
            List<Partner> _list = new List<Partner>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.RekananAPI);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    _list = JsonConvert.DeserializeObject<List<Partner>>(APIResponse);

                }
                //returning the employee list to view  

            }
            return _list;
        }

        public async Task<ActionResult> Negosiation(string id)
        {
            List<ProcurementVendor> objInfo = new List<ProcurementVendor>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.ProcurementVendorsAPI + "/" + id);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<List<ProcurementVendor>>(APIResponse);

                }
                //returning the employee list to view  
                return View(objInfo);
            }
        }
        public async Task<ActionResult> EditVendorNegotiation(string id, string procurementId)
        {
            try
            {
                Session["activeTabPengadaan"] = "s5";
                // TODO: Add delete logic here
                ProcurementVendor objInfo = new ProcurementVendor();

                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(Baseurl);

                    client.DefaultRequestHeaders.Clear();
                    //Define request data format  
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                    HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.ProcurementVendorsAPI + "/" + procurementId + "/" + id);

                    //Checking the response is successful or not which is sent using HttpClient  
                    if (Res.IsSuccessStatusCode)
                    {
                        //Storing the response details recieved from web api   
                        var APIResponse = Res.Content.ReadAsStringAsync().Result;

                        //Deserializing the response recieved from web api and storing into the Employee list  
                        objInfo = JsonConvert.DeserializeObject<ProcurementVendor>(APIResponse);

                    }
                    ViewBag.ProcurementID = procurementId;
                    ViewBag.id = id;
                    //returning the employee list to view  
                    return View(objInfo);
                }
            }
            catch
            {
                return View();
            }
        }
        // POST: CRUD/Create
        [HttpPost]
        public async Task<ActionResult> EditVendorNegotiation(string id, string procurementId, ProcurementVendor obj, FormCollection collection, HttpPostedFileBase file)
        {

            try
            {
                //obj.UserName = User.Identity.Name;
                //obj.ProcurementID = id;
                //obj.VendorID = collection["VendorID"].ToString();
                //obj.Procurement_ProcurementID = collection["procurementId"].ToString();
                //obj.ProcurementID = collection["procurementId"].ToString();
                obj.Username = User.Identity.Name;
                //obj.CreatedDate = DateTime.Now.ToString("dd/MM/yyyy");
                // TODO: Add update logic here
                obj.ProcurementID = procurementId;
                obj.StartingPrice = collection["StartingPrice"].ToString();
                obj.Negotiation = collection["Negotiation"].ToString();
                obj.Final = collection["Final"].ToString();
                obj.WinnerStatus = "";
                if (collection["status"] != null)
                {
                    obj.WinnerStatus = collection["status"].ToString();
                }


                //HttpClient client = new HttpClient();
                //client.BaseAddress = new Uri(Baseurl);
                //client.DefaultRequestHeaders.Accept.Clear();
                //client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //// TODO: Add update logic here
                //HttpResponseMessage response = await client.PostAsJsonAsync(Properties.Settings.Default.ProcurementVendorsAPI + "/" + obj.ProcurementID + "/" + id, obj);
                //response.EnsureSuccessStatusCode();

                string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    using (var cmd = new SqlCommand("sp_UpdateVendorDetail", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@VendorDetailID", id);
                        cmd.Parameters.AddWithValue("@AssetID", "");
                        cmd.Parameters.AddWithValue("@StartingPrice", obj.StartingPrice);
                        cmd.Parameters.AddWithValue("@Negotiation", 0); //diambil dari detailpengadaan 
                        cmd.Parameters.AddWithValue("@Final", 0); //diambil dari detailpengadaan 
                        cmd.Parameters.AddWithValue("@WinnerStatus", obj.WinnerStatus);
                        cmd.Parameters.AddWithValue("@UserName", User.Identity.Name);
                        conn.Open();
                        int k = cmd.ExecuteNonQuery();
                        if (k != 0)
                        {
                            //sukses
                        }
                        conn.Close();
                    }
                }

                if (Session["Action"] == "SIMPAN")
                {
                    return RedirectToAction("Create", "Pengadaan", new { id = procurementId, Status = "Edit" });
                }
                else
                {
                    return RedirectToAction("Create", "Pengadaan", new { id = procurementId, Status = "Approve" });
                }
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }
        public async Task<List<Employee>> GetListEmployees()
        {
            List<Employee> _list = new List<Employee>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.EmployeeAPI);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    _list = JsonConvert.DeserializeObject<List<Employee>>(APIResponse);

                }
                //returning the employee list to view  

            }
            return _list;
        }
        public async Task<List<Numbering>> GetListDocs()
        {
            List<Numbering> _list = new List<Numbering>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.PenomoranAPI);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    _list = JsonConvert.DeserializeObject<List<Numbering>>(APIResponse);

                }
                //returning the employee list to view  

            }
            return _list;
        }

        public PartialViewResult PartialPengadaanHeader(ProcurementHeader obj, List<Employee> _listEmployee, List<Numbering> _listTemplates)
        {

            //List<Employee> _listEmployee = GetListEmployees();
            ViewBag.ListEmployee = _listEmployee;
            //Agreement objInfo = new Agreement();
            //List<Numbering> _listTemplates = GetListDocs();
            ViewBag.ListTemplates = _listTemplates;
            ProcurementHeader objInfo = obj;
            if (obj == null)
            {
                obj = new ProcurementHeader();
            }
            return PartialView("PartialPengadaanHeader", obj);
        }

        public async Task<ActionResult> PartialAttachment(string id)
        {
            List<ProcurementDoc> objInfo = new List<ProcurementDoc>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.ProcurementDocsAPI + "/" + id);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<List<ProcurementDoc>>(APIResponse);

                }
                //ViewBag.ProcurementID = id;
                //returning the employee list to view  
                //ViewBag.objInfo = objInfo;

                //info total dokumen 
               

                return View(objInfo);

            }
            //return PartialView("PartialAttachment", new ProcurementDoc());
        }

        // POST: Procurement/CreatePartialPengadaanHeader
        [HttpPost]
        public async Task<ActionResult> PartialPengadaanHeader(ProcurementHeader obj, FormCollection collection)
        {
            try
            {

                obj.AdditionalInformation = collection["description"].ToString();
                obj.DateEnd = collection["finishdate"].ToString();
                obj.DateStart = collection["startdate"].ToString();
                obj.DocumentNo = collection["documentNumber"].ToString();
                obj.EmployeeManagerID = obj.EmployeeManagerID;
                obj.EmployeePICID = obj.EmployeePICID;
                obj.ProcurementName = collection["procTitle"].ToString();
                obj.UserName = User.Identity.Name;
                // TODO: Add update logic here
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                // TODO: Add update logic here
                HttpResponseMessage response = await client.PostAsJsonAsync(Properties.Settings.Default.ProcurementHeaderAPI + "/" + "0", obj);
                response.EnsureSuccessStatusCode();
                Session["ProcurementHeaderId"] = 1;
                return RedirectToAction("Create");
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }

        public PartialViewResult PartialHistory()
        {
            return PartialView("PartialHistory", new List<WorkFlowHistory>());
        }

        public async Task<ActionResult> PartialPengadaanDetail(string id)
        {
            List<ProcurementDetail> objInfo = new List<ProcurementDetail>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.ProcurementDetailsAPI + "/" + id);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<List<ProcurementDetail>>(APIResponse);

                }
                #region Tax Calculation
                int sebelumPajak = 0;
                int sesudahPajak = 0;
                int pajak = 0;
                foreach (ProcurementDetail obj in objInfo)
                {
                    sebelumPajak = sebelumPajak + Convert.ToInt32(obj.Price);
                    pajak = Convert.ToInt32(obj.Tax);
                }
                sesudahPajak = sebelumPajak + (sebelumPajak * (pajak / 100));
                ViewBag.sebelumPajak = sebelumPajak.ToString("N");
                ViewBag.pajak = pajak.ToString() + "%";
                ViewBag.sesudahPajak = sesudahPajak.ToString("N");

                #endregion
                ViewBag.ProcurementID = id;
                //returning the employee list to view  
                return View(objInfo);
            }
        }

        public PartialViewResult PartialRekananDetail()
        {
            return PartialView("PartialRekananDetail", new List<ProcurementVendor>());
        }

        public PartialViewResult PartialNegosiasi()
        {
            return PartialView("PartialNegosiasi", new List<ProcurementVendor>());
        }

        public async Task<List<ProcurementDoc>> GetListProcurementDocs(string id)
        {
            List<ProcurementDoc> _list = new List<ProcurementDoc>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.ProcurementDocsAPI + "/" + id);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    _list = JsonConvert.DeserializeObject<List<ProcurementDoc>>(APIResponse);

                }
                //returning the employee list to view  

            }
            return _list;
        }

        public async Task<List<WorkFlowHistory>> GetListHistory(string id)
        {
            List<WorkFlowHistory> _list = new List<WorkFlowHistory>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.ProcurementHistoriesAPI + "/" + id);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    _list = JsonConvert.DeserializeObject<List<WorkFlowHistory>>(APIResponse);

                }
                //returning the employee list to view  

            }
            return _list;
        }

        public async Task<List<ProcurementDetail>> GetListProcurementDetail(string id)
        {
            List<ProcurementDetail> _list = new List<ProcurementDetail>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.ProcurementDetailsAPI + "/" + id);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    _list = JsonConvert.DeserializeObject<List<ProcurementDetail>>(APIResponse);

                }

                ViewBag.ProcurementID = id;
                //returning the employee list to view  

            }
            return _list;
        }

        public async Task<List<ProcurementVendor>> GetListVendor(string id)
        {
            List<ProcurementVendor> objInfo = new List<ProcurementVendor>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.ProcurementVendorsAPI + "/" + id);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<List<ProcurementVendor>>(APIResponse);

                }
                ViewBag.ProcurementID = id;

                //returning the employee list to view  
                return objInfo;
            }
        }

        public ActionResult DetailTerminPengadaan(int id, int procurementId)
        {
            Session["activeTabPengadaan"] = "s5";
            List<PaymentList> objInfo = new List<PaymentList>();
            objInfo = GetPaymentDetail(id);
            ViewBag.vendordetailid = id;
            ViewBag.procurementid = procurementId;
            //cari nilai max kerja
            foreach (PaymentList obj in objInfo)
            {
                ViewBag.TotalBayar = obj.TotalBayar;
                ViewBag.TotalKerja = obj.TotalKerja;
            }
            //hitung total bayar
            return View(objInfo);
        }

        public List<PaymentList> GetPaymentDetail(int vendordetailid)
        {
            List<PaymentList> _PaymentList = new List<PaymentList>();

            string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(connectionstring))
            {
                using (var cmd = new SqlCommand("sp_ViewVendorTermin", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@VendorDetailID", vendordetailid);
                    conn.Open();

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            PaymentList obj = new PaymentList();

                            obj.VendorTerminID = Convert.ToInt32(reader[0]);
                            obj.ProgressofWork = reader[1].ToString();
                            obj.ProgressofPayment = reader[2].ToString();
                            obj.Description = reader[3].ToString();
                            obj.TotalBayar = reader[4].ToString();
                            obj.TotalKerja = reader[5].ToString();

                            _PaymentList.Add(obj);

                            ViewBag.VendorTerminID = obj.VendorTerminID;

                        }
                    }
                }

            }
            return _PaymentList;

        }

        public ActionResult CreateTermin(string vendordetailid, int procurementId, string TotalBayar, string TotalKerja, string ErrorMessage)
        {
            Session["activeTabPengadaan"] = "s5";
            PaymentList objInfo = new PaymentList();
            ViewBag.vendordetailid = vendordetailid;
            ViewBag.procurementid = procurementId;
            ViewBag.TotalBayar = TotalBayar;
            ViewBag.TotalKerja = TotalKerja;
            if (ErrorMessage != null)
            {
                ViewBag.ErrorMessage = ErrorMessage;
            }
            return View(objInfo);
        }
        // POST: CRUD/Create
        [HttpPost]
        public async Task<ActionResult> CreateTermin(string vendordetailid, int procurementId, string TotalBayar, string TotalKerja, PaymentList obj, FormCollection form, HttpPostedFileBase file)
        {
            try
            {

                string ErrorMessage = "";
                obj.ProgressofWork = form["ProgressofWork"].ToString();
                obj.ProgressofPayment = form["ProgressofPayment"].ToString();
                obj.Description = form["description"].ToString();
                if (Convert.ToInt32(obj.ProgressofPayment) + Convert.ToInt32(TotalBayar) > 100)
                {
                    ErrorMessage = "Total Progress Pembayaran tidak bisa lebih dari 100 % ";
                    return RedirectToAction("CreateTermin", "Pengadaan", new { vendordetailid = vendordetailid, procurementId = procurementId, TotalBayar = TotalBayar, TotalKerja = TotalKerja, ErrorMessage = ErrorMessage });
                }
                if (Convert.ToInt32(TotalKerja) == 100 || Convert.ToInt32(obj.TotalKerja) > 100)
                {
                    ErrorMessage = "Progress Pekerjaan maksimal 100 % ";
                    return RedirectToAction("CreateTermin", "Pengadaan", new { vendordetailid = vendordetailid, procurementId = procurementId, TotalBayar = TotalBayar, TotalKerja = TotalKerja, ErrorMessage = ErrorMessage });
                }
                // TODO: Add update logic here
                string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    using (var cmd = new SqlCommand("sp_InsertVendorTermin", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@VendorDetailID", vendordetailid);
                        cmd.Parameters.AddWithValue("@ProgressofWork", obj.ProgressofWork);
                        cmd.Parameters.AddWithValue("@ProgressofPayment", obj.ProgressofPayment);
                        cmd.Parameters.AddWithValue("@Description", obj.Description);
                        cmd.Parameters.AddWithValue("@USERNAME", User.Identity.Name);
                        conn.Open();
                        int k = cmd.ExecuteNonQuery();
                        if (k != 0)
                        {
                            //sukses
                        }
                        conn.Close();
                    }
                }

                return RedirectToAction("DetailTerminPengadaan", "Pengadaan", new { id = vendordetailid, procurementId = procurementId });
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }

        public ActionResult DeleteTermin(string id, string vendordetailid, int procurementId)
        {
            string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(connectionstring))
            {
                using (var cmd = new SqlCommand("sp_DeleteVendorTermin", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@VendorTerminID", id);
                    conn.Open();
                    int k = cmd.ExecuteNonQuery();
                    if (k != 0)
                    {
                        //sukses
                    }
                    conn.Close();
                }
            }
            return RedirectToAction("DetailTerminPengadaan", "Pengadaan", new { id = vendordetailid, procurementId = procurementId });

        }


        //for view only
        public async Task<ActionResult> View(string id)
        {
            ProcurementHeader objInfo = new ProcurementHeader();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllArticles using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.ProcurementHeaderAPI + "/" + id);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<ProcurementHeader>(APIResponse);

                }
                List<Employee> _listEmployee = await GetListEmployees();
                ViewBag.ListEmployee = _listEmployee;
                List<Numbering> _listTemplates = await GetListDocs();
                ViewBag.ListTemplates = _listTemplates;
                List<ProcurementDoc> _listProcurementDocs = await GetListProcurementDocs(id);
                ViewBag.ListProcurementDoc = _listProcurementDocs;

                List<WorkFlowHistory> _listHistories = await GetListHistory(id);
                ViewBag.ListWorkFlow = _listHistories;

                List<ProcurementDetail> _ListProcurementDetail = await GetListProcurementDetail(id);
                ViewBag.ListProcurementDetail = _ListProcurementDetail;

                #region Tax Calculation
                double Penawaran = 0;
                double Negosiasi = 0;
                double pajak = 0;
                foreach (ProcurementDetail obj in _ListProcurementDetail)
                {
                    Penawaran = Penawaran + Convert.ToDouble(obj.Price);
                    Negosiasi = Negosiasi + Convert.ToDouble(obj.Negotiation);
                    pajak = Convert.ToDouble(obj.Tax);
                }
                ViewBag.Penawaran = Penawaran.ToString("N0");
                ViewBag.Negosiasi = Negosiasi.ToString("N0");

                //sesudahPajak = sebelumPajak + ((sebelumPajak * pajak)/100);
                //ViewBag.sebelumPajak = sebelumPajak.ToString("N");
                //ViewBag.pajak = pajak.ToString() + "%";
                //ViewBag.sesudahPajak = sesudahPajak.ToString("N");
                //sesudahPajak = sebelumPajak + ((sebelumPajak * pajak) / 100);
                //ViewBag.sebelumPajak = sebelumPajak;
                //ViewBag.pajak = pajak.ToString();
                //ViewBag.sesudahPajak = sesudahPajak;
                #endregion


                List<ProcurementVendor> _listVendor = await GetListVendor(id);
                ViewBag.ListProcurementVendor = _listVendor;

                ViewBag.ProcurementID = id;
                ViewBag.objInfo = objInfo;
                return View(objInfo);
            }

        }

        public PartialViewResult PartialPengadaanHeaderView(ProcurementHeader obj, List<Employee> _listEmployee, List<Numbering> _listTemplates)
        {

            //List<Employee> _listEmployee = GetListEmployees();
            ViewBag.ListEmployee = _listEmployee;
            //Agreement objInfo = new Agreement();
            //List<Numbering> _listTemplates = GetListDocs();
            ViewBag.ListTemplates = _listTemplates;
            ProcurementHeader objInfo = obj;
            if (obj == null)
            {
                obj = new ProcurementHeader();
            }
            return PartialView("PartialPengadaanHeaderView", obj);
        }

        public async Task<ActionResult> PartialAttachmentView(string id)
        {
            List<ProcurementDoc> objInfo = new List<ProcurementDoc>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.ProcurementDocsAPI + "/" + id);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<List<ProcurementDoc>>(APIResponse);

                }
                //ViewBag.ProcurementID = id;
                //returning the employee list to view  
                //ViewBag.objInfo = objInfo;
                return View(objInfo);

            }
            //return PartialView("PartialAttachment", new ProcurementDoc());
        }
        public async Task<ActionResult> PartialPengadaanDetailView(string id)
        {
            List<ProcurementDetail> objInfo = new List<ProcurementDetail>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.ProcurementDetailsAPI + "/" + id);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<List<ProcurementDetail>>(APIResponse);

                }
                #region Tax Calculation
                int sebelumPajak = 0;
                int sesudahPajak = 0;
                int pajak = 0;
                foreach (ProcurementDetail obj in objInfo)
                {
                    sebelumPajak = sebelumPajak + Convert.ToInt32(obj.Price);
                    pajak = Convert.ToInt32(obj.Tax);
                }
                sesudahPajak = sebelumPajak + (sebelumPajak * pajak);
                ViewBag.sebelumPajak = sebelumPajak.ToString("N");
                ViewBag.pajak = pajak.ToString() + "%";
                ViewBag.sesudahPajak = sesudahPajak.ToString("N");

                #endregion
                ViewBag.ProcurementID = id;
                //returning the employee list to view  
                return View(objInfo);
            }
        }

        public PartialViewResult PartialRekananDetailView()
        {
            return PartialView("PartialRekananDetailView", new List<ProcurementVendor>());
        }

        public PartialViewResult PartialNegosiasiView()
        {
            return PartialView("PartialNegosiasiView", new List<ProcurementVendor>());
        }

        public ActionResult DeleteVendor(string id, string ProcurementID)
        {
            Session["activeTabPengadaan"] = "s3";
            string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(connectionstring))
            {
                using (var cmd = new SqlCommand("sp_DeleteVendorDetail", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@VendorDetailID", id);
                    conn.Open();
                    int k = cmd.ExecuteNonQuery();
                    if (k != 0)
                    {
                        //sukses
                    }
                    conn.Close();
                }
            }

            //return RedirectToAction("Create", "Pengadaan", new { id = ProcurementID });
            if (Session["Action"] == "SIMPAN")
            {
                return RedirectToAction("Create", "Pengadaan", new { id = ProcurementID, Status = "Edit" });
            }
            else
            {
                return RedirectToAction("Create", "Pengadaan", new { id = ProcurementID, Status = "Approve" });
            }
        }

        public async Task<ActionResult> Reject(string id, string Status, string WF, string ErrorMessage)
        {
            ProcurementHeader objInfo = new ProcurementHeader();
            if (ErrorMessage != null)
            {
                ViewBag.ErrorMessage = ErrorMessage;
            }

            if (Status == "Edit")
            {
                Session["Action"] = "SIMPAN";
            }
            else
            {
                Session["Action"] = "APPROVE";
            }

            List<ProcurementVendor> _listVendor = await GetListVendor(id);
            ViewBag.ListProcurementVendor = _listVendor;

            ViewBag.ProcurementID = id;
            ViewBag.objInfo = objInfo;
            ViewBag.WF = WF;
            ViewBag.Status = Status;
            return View(objInfo);


        }
        // POST: Procurement/Create
        [HttpPost]
        public async Task<ActionResult> Reject(string id, string WF, ProcurementHeader obj, FormCollection collection)
        {
            try
            {
                ProcurementHeader objInfo = new ProcurementHeader();

                string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    using (var cmd = new SqlCommand("sp_GetNextApproval", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@WFName", "WF_PENGADAAN");
                        cmd.Parameters.AddWithValue("@Action", "REJECT");
                        cmd.Parameters.AddWithValue("@WorkflowId", WF);
                        cmd.Parameters.AddWithValue("@MainTableId", id);
                        cmd.Parameters.AddWithValue("@RejectReason", collection["Reason"].ToString());
                        cmd.Parameters.AddWithValue("@USER_NAME", User.Identity.Name);
                        conn.Open();
                        int k = cmd.ExecuteNonQuery();
                        if (k != 0)
                        {
                            //sukses
                        }
                        conn.Close();
                    }

                }


                Session["Action"] = "";
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }

        //public void Back(string session)
        //{
        //    Session["activeTabPengadaan"] = session;
        //    //ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "goBack", "<script>goBack()</script>", true);
        //    //this.Controls.Add(new LiteralControl("<script type='text/javascript'>window.history.back();</script>"));
            
        //}
    }
}
