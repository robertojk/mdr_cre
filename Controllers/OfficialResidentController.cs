﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using MandiriCRE.Models;
using System.IO;
using System.Data.SqlClient;
using System.Data;
using System.Web.UI;
using Simplexcel;
using System.Text.RegularExpressions;
using MandiriCRE.ViewModel;

namespace MandiriCRE.Controllers
{
    public class OfficialResidentController : Controller
    {
        string Baseurl = Properties.Settings.Default.BaseURI;
        string Baseurl2 = Properties.Settings.Default.BaseURI;
        // GET: OfficialResident
        public async Task<ActionResult> Index()
        {
            var model = new OfficialResidentIndexViewModel();

            model.InitializePermissions(
                Session,
                "20"
            );

            model.OfficialResidentIndexJson = await JsonDownloader.GetJsonOrDefaultAsync(
                Baseurl2,
                Properties.Settings.Default.OfficialResidentAPI,
                EmptyJsonDefault.EmptyArray
            );

            return View(model);
        }
        public async Task<ActionResult> Details(string id)
        {
            OfficialResident objInfo = new OfficialResident();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl2);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllATTB using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.OfficialResidentAPI + "/" + id);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the ATTB list  
                    objInfo = JsonConvert.DeserializeObject<OfficialResident>(APIResponse);

                }
                ViewBag.OfficialResident = id;
                return View(objInfo);
            }
        }
        public async Task<ActionResult> View(string id)
        {
            List<OfficialResident> objInfo = new List<OfficialResident>();
          

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl2);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllArticles using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.OfficialResidentAPI + "/" + id);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<List<OfficialResident>>(APIResponse);

                }

                List<EmployeePosition> _listEmployeePosition = await GetEmployeePosition();
                ViewBag.ListEmployeePosition = _listEmployeePosition;

                List<MasterResidence> _listRumahDinas = await GetMasterResidence();
                ViewBag.ListRumahDinas = _listRumahDinas;

                List<LogHistory> _listHistories = GetListHistory(id);
                ViewBag.ListHistory = _listHistories;

                ViewBag.OfficialResidentID = id;

                OfficialResident objOne = new OfficialResident();

                foreach (OfficialResident Obj in objInfo)
                {
                    objOne = Obj;
                }

                objOne.BuildingArea = String.Format("{0:n0}", Convert.ToDouble(objOne.BuildingArea)).Replace(",", ".");
                objOne.LandArea = String.Format("{0:n0}", Convert.ToDouble(objOne.LandArea)).Replace(",", ".");

                ViewBag.objInfo = objOne;
                return View(objOne);
            }

        }

        public PartialViewResult PartialOfficialResidentView(OfficialResident obj, List<EmployeePosition> _listEmployeePosition, List<MasterResidence> _listRumahDinas)
        {
            ViewBag.ListEmployeePosition = _listEmployeePosition;
            ViewBag.ListRumahDinas = _listRumahDinas;
            
            OfficialResident objInfo = obj;
            if (obj == null)
            {
                obj = new OfficialResident();
            }
            return PartialView("PartialOfficialResidentView", obj);
        }

        public PartialViewResult PartialOfficialResident(OfficialResident obj, List<EmployeePosition> _listEmployeePosition, List<MasterResidence> _listRumahDinas)
        {

            ViewBag.ListEmployeePosition = _listEmployeePosition;
            ViewBag.ListRumahDinas = _listRumahDinas;
           

            OfficialResident objInfo = obj;
            if (obj == null)
            {
                obj = new OfficialResident();
            }
            return PartialView("PartialOfficialResident", obj);
        }
        public PartialViewResult PartialOfficialResidentVacate(OfficialResident obj, List<EmployeePosition> _listEmployeePosition, List<MasterResidence> _listRumahDinas)
        {

            ViewBag.ListEmployeePosition = _listEmployeePosition;
            ViewBag.ListRumahDinas = _listRumahDinas;
           

            OfficialResident objInfo = obj;
            if (obj == null)
            {
                obj = new OfficialResident();
            }
            return PartialView("PartialOfficialResidentVacate", obj);
        }

        //GET: Tax/Create
        public async Task<ActionResult> Create(string ErrorMessage, OfficialResident obj)
        {
            string employeePosition = "0";
            string residentId = "0";
            if (ErrorMessage != null)
            {
                ViewBag.ErrorMessage = ErrorMessage;
            }
            List<EmployeePosition> _listEmployeePosition = await GetEmployeePosition();
            ViewBag.ListEmployeePosition = _listEmployeePosition;

           

            OfficialResident objInfo = new OfficialResident();
            if (obj != null)
            {
                objInfo = obj;
                employeePosition = obj.EmployeePositionID;
                residentId = obj.ResidentID;

            }
            if (ErrorMessage != null)
            {
                ViewBag.ErrorMessage = ErrorMessage;
            }

            List<MasterResidence> _listRumahDinas = await GetMasterResidence(employeePosition, residentId);
            ViewBag.ListRumahDinas = _listRumahDinas;

            //get RunningNumber resident

            String No = "";
            string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(connectionstring))
            {
                using (var cmd = new SqlCommand("sp_NoResident", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    conn.Open();

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            No = "0000" + reader[0].ToString();
                        }

                    }


                }
            }

            ViewBag.No = No.Substring(No.Length - 5, 5);


            return View(objInfo);
        }
        // POST: Procurement/Create
        [HttpPost]
        public async Task<ActionResult> Create(OfficialResident objResidence, FormCollection collection, HttpPostedFileBase file)
        {
            try
            {
                OfficialResident obj = new OfficialResident();

                obj.EmployeePositionID = collection["EmployeePositionID"].ToString();
                obj.ResidentID = collection["ResidentID"].ToString();
                obj.NameOfResident = collection["tbxNameOfResident"].ToString();
                obj.NIP = collection["tbxNIP"].ToString();
                obj.DateFrom = collection["tbxDateFrom"].ToString();
                obj.DateTo = collection["tbxDateTo"].ToString();
                obj.Status = "Berpenghuni";
                obj.OrderInformation = collection["tbxOrderInformation"].ToString();
                obj.EmptyingInformation = "";
                obj.CityName = collection["tbxCityName"].ToString();
                obj.LandArea = collection["tbxLandArea"].ToString();
                obj.BuildingArea = collection["tbxBuildingArea"].ToString();
                obj.ClassName = collection["tbxClassName"].ToString();
                obj.ResidenceConditionName = collection["tbxResidenceConditionName"].ToString();
                obj.UserName = User.Identity.Name;
                

                string ErrorMessage = "";
                string fileName = "";
                string path = "";
                string fileType = "";
                //Upload File
                if (file != null && file.ContentLength > 0)
                {
                    fileType = Path.GetExtension(file.FileName);
                    if (fileType.ToUpper().Contains("PDF") || fileType.ToUpper().Contains("DOC") || fileType.ToUpper().Contains("XLS") || fileType.ToUpper().Contains("XLSX"))// || fileType.ToUpper().Contains("JPEG"))
                    {
                        if (file.ContentLength <= (4000 * 1024))
                        {
                            fileName = Path.GetFileName(file.FileName);
                            path = Path.Combine(Server.MapPath("~/content/document/officialresidence/"), fileName);
                        file.SaveAs(path);

                        //insert file data
                        obj.DocFileName = fileName;
                            //obj.FolderName = "residence";
                            //obj.Path = path;

                        }
                        else
                        {
                            ErrorMessage = "File Upload tidak boleh lebih dari 4MB";
                            //return RedirectToAction("Create", new { ErrorMessage = ErrorMessage });
                            return await Create(ErrorMessage, obj);
                        }
                    }
                    else
                    {
                        ErrorMessage = "File Upload harus dengan format Word, Excel dan PDF";
                        //return RedirectToAction("Create", new { ErrorMessage = ErrorMessage });
                        return await Create(ErrorMessage, obj);
                    }
                }
                

                if (ErrorMessage != null && ErrorMessage != "")
                {
                    return await Create(ErrorMessage, obj);
                }

                // TODO: Add update logic here
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(Baseurl2);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllArticles using HttpClient  
                HttpResponseMessage response = await client.PostAsJsonAsync(Properties.Settings.Default.OfficialResidentAPI + "/" + "0", obj);
                response.EnsureSuccessStatusCode();



                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }


        public async Task<ActionResult> Update(string id, string ErrorMessage)
        {
            List<OfficialResident> objInfo = new List<OfficialResident>();
            if (ErrorMessage != null)
            {
                ViewBag.ErrorMessage = ErrorMessage;
            }

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl2);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllArticles using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.OfficialResidentAPI + "/" + id);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<List<OfficialResident>>(APIResponse);

                }

                List<EmployeePosition> _listEmployeePosition = await GetEmployeePosition();
                ViewBag.ListEmployeePosition = _listEmployeePosition;

                

                List<LogHistory> _listHistories = GetListHistory(id);
                ViewBag.ListHistory = _listHistories;

                ViewBag.OfficialResidentID = id;

                OfficialResident objOne = new OfficialResident();

                foreach (OfficialResident Obj in objInfo)
                {
                    objOne = Obj;
                }

                List<MasterResidence> _listRumahDinas = await GetMasterResidence(objOne.EmployeePositionID,objOne.ResidentID);
                //List<MasterResidence> _listRumahDinas = await GetMasterResidenceByPosition(objOne.EmployeePositionID.ToString(), objOne.ResidentID.ToString());
                ViewBag.ListRumahDinas = _listRumahDinas;

                objOne.BuildingArea = String.Format("{0:n0}", Convert.ToDouble(objOne.BuildingArea)).Replace(",", ".");
                objOne.LandArea = String.Format("{0:n0}", Convert.ToDouble(objOne.LandArea)).Replace(",", ".");
                //objOne.DateFrom = Convert.ToDateTime(objOne.DateFrom).ToShortDateString();
                //objOne.DateTo = Convert.ToDateTime(objOne.DateTo).ToShortDateString();
                ViewBag.objInfo = objOne;
                return View(objOne);
            }

        }

        [HttpPost]
        public async Task<ActionResult> Update(string id, OfficialResident objResidence, FormCollection collection, HttpPostedFileBase file)
        {
            try
            {
                OfficialResident obj = new OfficialResident();

                obj.EmployeePositionID = collection["EmployeePositionID"].ToString();
                obj.ResidentID = collection["ResidentID"].ToString();
                obj.NameOfResident = collection["tbxNameOfResident"].ToString();
                obj.NIP = collection["tbxNIP"].ToString();
                obj.DateFrom = collection["tbxDateFrom"].ToString();
                obj.DateTo = collection["tbxDateTo"].ToString();
                obj.Status = "Berpenghuni";
                obj.OrderInformation = collection["tbxOrderInformation"].ToString();
                obj.EmptyingInformation = "";
                obj.UserName = User.Identity.Name;


                string ErrorMessage = "";
                string fileName = "";
                string path = "";
                string fileType = "";

                if (file == null && (collection["filename"].ToString() != null && collection["filename"].ToString() != ""))
                {
                    fileName = collection["filename"].ToString();
                    obj.DocFileName = fileName;
                    path = Path.Combine(Server.MapPath("~/content/document/officialresidence/"), fileName);
                }

                //Upload File
                if (file != null && file.ContentLength > 0)
                {
                    fileType = Path.GetExtension(file.FileName);
                    if (fileType.ToUpper().Contains("PDF") || fileType.ToUpper().Contains("DOC") || fileType.ToUpper().Contains("XLS") || fileType.ToUpper().Contains("XLSX"))// || fileType.ToUpper().Contains("JPEG"))
                    {
                        if (file.ContentLength <= (4000 * 1024))
                        {
                            fileName = Path.GetFileName(file.FileName);
                            path = Path.Combine(Server.MapPath("~/content/document/officialresidence/"), fileName);
                            file.SaveAs(path);

                            //insert file data
                            obj.DocFileName = fileName;
                            //obj.FolderName = "residence";
                            //obj.Path = path;

                        }
                        else
                        {
                            ErrorMessage = "File Upload tidak boleh lebih dari 4MB";
                            return RedirectToAction("Update", new {id=id, ErrorMessage = ErrorMessage});
                        }
                    }
                    else
                    {
                        ErrorMessage = "File Upload harus dengan format Word, Excel dan PDF";
                        return RedirectToAction("Update", new { id = id, ErrorMessage = ErrorMessage });
                    }
                }


                if (ErrorMessage != null && ErrorMessage != "")
                {
                    return await Create(ErrorMessage, obj);
                }

                // TODO: Add update logic here
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(Baseurl2);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                // TODO: Add update logic here
                HttpResponseMessage response = await client.PutAsJsonAsync(Properties.Settings.Default.OfficialResidentAPI + "/" + id, obj);
                response.EnsureSuccessStatusCode();

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }

        public async Task<ActionResult> Vacate(string id, string ErrorMessage)
        {
            List<OfficialResident> objInfo = new List<OfficialResident>();
            if (ErrorMessage != null)
            {
                ViewBag.ErrorMessage = ErrorMessage;
            }

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl2);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllArticles using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.OfficialResidentAPI + "/" + id);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<List<OfficialResident>>(APIResponse);

                }

                List<EmployeePosition> _listEmployeePosition = await GetEmployeePosition();
                ViewBag.ListEmployeePosition = _listEmployeePosition;

                

                List<LogHistory> _listHistories = GetListHistory(id);
                ViewBag.ListHistory = _listHistories;

                ViewBag.OfficialResidentID = id;

                OfficialResident objOne = new OfficialResident();

                foreach (OfficialResident Obj in objInfo)
                {
                    objOne = Obj;
                }

                List<MasterResidence> _listRumahDinas = await GetMasterResidence(objOne.EmployeePositionID,objOne.ResidentID);
                ViewBag.ListRumahDinas = _listRumahDinas;

                objOne.BuildingArea = String.Format("{0:n0}", Convert.ToDouble(objOne.BuildingArea)).Replace(",", ".");
                objOne.LandArea = String.Format("{0:n0}", Convert.ToDouble(objOne.LandArea)).Replace(",", ".");

                ViewBag.objInfo = objOne;
                return View(objOne);
            }

        }

        [HttpPost]
        public async Task<ActionResult> Vacate(string id, OfficialResident objResidence, FormCollection collection)
        {
            try
            {
                OfficialResident obj = new OfficialResident();

                obj.EmployeePositionID = collection["EmployeePositionID"].ToString();
                obj.ResidentID = collection["ResidentID"].ToString();
                obj.NameOfResident = collection["tbxNameOfResident"].ToString();
                obj.NIP = collection["tbxNIP"].ToString();
                obj.DateFrom = collection["tbxDateFrom"].ToString();
                obj.DateTo = collection["tbxDateTo"].ToString();
                obj.Status = "Dikosongkan";
                obj.OrderInformation = collection["tbxOrderInformation"].ToString();
                obj.EmptyingInformation = collection["tbxEmptyingInformation"].ToString();
                obj.UserName = User.Identity.Name;
                obj.DocFileName = collection["filename"].ToString(); //101018

                string ErrorMessage = "";
              

                if (ErrorMessage != null && ErrorMessage != "")
                {
                    return await Create(ErrorMessage, obj);
                }

                // TODO: Add update logic here
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(Baseurl2);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                // TODO: Add update logic here
                HttpResponseMessage response = await client.PutAsJsonAsync(Properties.Settings.Default.OfficialResidentAPI + "/" + id, obj);
                response.EnsureSuccessStatusCode();

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }
        
        public PartialViewResult PartialHistory()
        {
            return PartialView("PartialHistory", new List<LogHistory>());
        }
        public async Task<List<EmployeePosition>> GetEmployeePosition()
        {
            List<EmployeePosition> _list = new List<EmployeePosition>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl2);
                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.EmployeePositionAPI);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    _list = JsonConvert.DeserializeObject<List<EmployeePosition>>(APIResponse);

                }
                //returning the employee list to view  

            }
            return _list;
        }

        public async Task<List<MasterResidence>> GetMasterResidence(string positionId = "0",string residentId = "0")
        {
            List<MasterResidence> _list = new List<MasterResidence>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl2);
                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.MasterResidenceAPI);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    _list = JsonConvert.DeserializeObject<List<MasterResidence>>(APIResponse);

                }
                //returning the employee list to view  

            }


            List<MasterResidence> _masterResidence = new List<MasterResidence>();
            _masterResidence = new List<MasterResidence>(_list.Where(obj => obj.EmployeePositionID == positionId && (obj.ResidenceStatusID == "2" || obj.ResidentID == residentId)));
            //if (positionId =="0")
            //{
            //    return _list;
            //}
            List<SelectListItem> subjects = new List<SelectListItem>();

            foreach (var item in _masterResidence)
            {
                SelectListItem subject = new SelectListItem
                {
                    Text = item.Address,
                    Value = item.ResidentID
                };
                subjects.Add(subject);
            }
            return _masterResidence;
        }
        public async Task<JsonResult> GetMasterResidenceByPosition(string positionId = "0",string residentId="0")
        {
            List<MasterResidence> _list = new List<MasterResidence>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl2);
                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.MasterResidenceAPI);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    _list = JsonConvert.DeserializeObject<List<MasterResidence>>(APIResponse);

                }
                //returning the employee list to view  

            }


            List<MasterResidence> _masterResidence = new List<MasterResidence>();
            _masterResidence = new List<MasterResidence>(_list.Where(obj => obj.EmployeePositionID == positionId));
            _masterResidence = new List<MasterResidence>(_masterResidence.Where(obj => obj.ResidenceStatusID == "2" || obj.ResidentID == residentId));
            //if (positionId =="0")
            //{
            //    return _list;
            //}
            List<SelectListItem> subjects = new List<SelectListItem>();

            foreach (var item in _masterResidence)
            {
                SelectListItem subject = new SelectListItem
                {
                    Text = item.Address,
                    Value = item.ResidentID
                };
                subjects.Add(subject);
            }
            return Json(subjects, JsonRequestBehavior.AllowGet);
        }
        public async Task<JsonResult> GetMasterResidenceById(string id)
        {
            List<MasterResidence> objInfo = new List<MasterResidence>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl2);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllArticles using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.MasterResidenceAPI + "/" + id);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<List<MasterResidence>>(APIResponse);

                }



                MasterResidence objOne = new MasterResidence();

                foreach (MasterResidence Obj in objInfo)
                {
                    objOne = Obj;
                }

                objOne.BuildingArea = String.Format("{0:n0}", Convert.ToDouble(objOne.BuildingArea)).Replace(",", ".");
                objOne.LandArea = String.Format("{0:n0}", Convert.ToDouble(objOne.LandArea)).Replace(",", ".");
                objOne.AcquisitionValue = String.Format("{0:n0}", Convert.ToDouble(objOne.AcquisitionValue)).Replace(",", ".");
                objOne.RenovationCost = String.Format("{0:n0}", Convert.ToDouble(objOne.RenovationCost)).Replace(",", ".");
                return Json(objOne, JsonRequestBehavior.AllowGet);

            }

        }

        public List<LogHistory> GetListHistory(string id)
        {
            List<LogHistory> _list = new List<LogHistory>();


            string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(connectionstring))
            {
                using (var cmd = new SqlCommand("sp_ViewHistoryLog", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@TableID", id);
                    cmd.Parameters.AddWithValue("@TableName", "OfficialResidence");
                    conn.Open();

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            LogHistory obj = new LogHistory();

                            obj.LogID = reader[0].ToString();
                            obj.TableName = reader[1].ToString();
                            obj.TableID = reader[2].ToString();
                            obj.Action = reader[3].ToString();
                            obj.UpdatedDate = reader[4].ToString();
                            obj.UpdatedBy = reader[5].ToString();

                            _list.Add(obj);
                        }


                    }
                }

            }

            return _list;
        }

        public ActionResult Preview(string docfile)
        {

            ViewBag.DocFile = docfile;

            return View();


        }
    }
}