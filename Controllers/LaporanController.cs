﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using MandiriCRE.Models;
using System.IO;
using MandiriCRE.Models;
using MandiriCRE.ViewModel;
using System.Text.RegularExpressions;
using Simplexcel;

namespace MandiriCRE.Controllers
{
    public class LaporanController : Controller
    {
        // GET: Laporan
        string Baseurl = Properties.Settings.Default.BaseURI;
        string Baseurl2 = Properties.Settings.Default.BaseURI;
        public ActionResult Index1()
        {
            return View();
        }

        public async Task<ActionResult> Index()
        {
            List<Numbering> objInfo = new List<Numbering>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.PenomoranAPI);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<List<Numbering>>(APIResponse);

                }
                //returning the employee list to view  
                return View(objInfo);
            }
        }

        public async Task<ActionResult> LaporanRincianPerjanjian()
        {
            List<LaporanRincianPerjanjian> objInfo = new List<LaporanRincianPerjanjian>();

            string StartDate = DateTime.Now.ToString();
            string EndDate = DateTime.Now.ToString();

            using (var client = new HttpClient())
            {

                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.LaporanRincianPerjanjianAPI + "/" + StartDate + "/" + EndDate);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<List<LaporanRincianPerjanjian>>(APIResponse);

                }
            }
            return View(objInfo);
        }

        [HttpPost]
        public async Task<ActionResult> LaporanRincianPerjanjian(FormCollection form)
        {

            List<LaporanRincianPerjanjian> objInfo = new List<LaporanRincianPerjanjian>();

            string StartDate = form["startdate"].ToString();
            string EndDate = form["enddate"].ToString();
            ViewBag.startdate = StartDate;
            ViewBag.enddate = EndDate;

            if (form["Excel"] != null)
            {
                return RedirectToAction("DownloadExcel", "Laporan", new { startdate=StartDate,enddate=EndDate });
            }

            using (var client = new HttpClient())
            {

                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.LaporanRincianPerjanjianAPI + "/" + StartDate + "/" + EndDate);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<List<LaporanRincianPerjanjian>>(APIResponse);

                }

                //returning the employee list to view  
                return View(objInfo);
            }
        }

        public async Task<ActionResult> LaporanRekapReviewPerjanjian()
        {

            List<LaporanRekapReviewPerjanjian> objInfo = new List<LaporanRekapReviewPerjanjian>();

            using (var client = new HttpClient())
            {
                DateTime StartDate = DateTime.Now;
                DateTime EndDate = DateTime.Now;
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.LaporanRekapReviewPerjanjianAPI + "/" + StartDate + "/" + EndDate);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<List<LaporanRekapReviewPerjanjian>>(APIResponse);

                }

                //returning the employee list to view  
                return View(objInfo);
            }
        }

        [HttpPost]
        public async Task<ActionResult> LaporanRekapReviewPerjanjian(FormCollection form)
        {

            List<LaporanRekapReviewPerjanjian> objInfo = new List<LaporanRekapReviewPerjanjian>();

            string StartDate = form["startdate"].ToString();
            string EndDate = form["enddate"].ToString();

            ViewBag.startdate = StartDate;
            ViewBag.enddate = EndDate;

            if (form["Excel"] != null)
            {
                return RedirectToAction("DownloadExcelReviewPerjanjian", "Laporan", new { startdate = StartDate, enddate = EndDate });
            }

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.LaporanRekapReviewPerjanjianAPI + "/" + StartDate + "/" + EndDate);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<List<LaporanRekapReviewPerjanjian>>(APIResponse);

                }

                //returning the employee list to view  
                return View(objInfo);
            }
        }

        public async Task<ActionResult> LaporanKPIGroup()
        {

            List<LaporanKPIGroup> objInfo = new List<LaporanKPIGroup>();

            using (var client = new HttpClient())
            {
                DateTime StartDate = DateTime.Now;
                DateTime EndDate = DateTime.Now;
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.LaporanKPIGroupAPI);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<List<LaporanKPIGroup>>(APIResponse);

                }

                //returning the employee list to view  
                return View(objInfo);
            }
        }

        public async Task<ActionResult> LaporanSPOATBL()
        {

            List<LaporanSPOATBL> objInfo = new List<LaporanSPOATBL>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.LaporanSPOATBLAPI);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<List<LaporanSPOATBL>>(APIResponse);

                }

                //returning the employee list to view  
                return View(objInfo);
            }
        }
        public async Task<ActionResult> LaporanSPOProcurement()
        {

            List<LaporanSPOProcurement> objInfo = new List<LaporanSPOProcurement>();

            using (var client = new HttpClient())
            {
                DateTime StartDate = DateTime.Now;
                DateTime EndDate = DateTime.Now;
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.LaporanSPOProcurementAPI);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<List<LaporanSPOProcurement>>(APIResponse);

                }

                //returning the employee list to view  
                return View(objInfo);
            }
        }

        public async Task<ActionResult> LaporanPTO()
        {

            List<LaporanPTO> objInfo = new List<LaporanPTO>();

            using (var client = new HttpClient())
            {
                DateTime StartDate = DateTime.Now;
                DateTime EndDate = DateTime.Now;
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.LaporanPTOAPI);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<List<LaporanPTO>>(APIResponse);

                }

                //returning the employee list to view  
                return View(objInfo);
            }
        }
        public async Task<ActionResult> LaporanCeloxis()
        {

            List<LaporanCeloxis> objInfo = new List<LaporanCeloxis>();

            using (var client = new HttpClient())
            {
                DateTime StartDate = DateTime.Now;
                DateTime EndDate = DateTime.Now;
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.LaporanCeloxisAPI);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<List<LaporanCeloxis>>(APIResponse);

                }

                //returning the employee list to view  
                return View(objInfo);
            }
        }
        public async Task<ActionResult> LaporanOpex()
        {

            List<LaporanOpex> objInfo = new List<LaporanOpex>();

            using (var client = new HttpClient())
            {
                DateTime StartDate = DateTime.Now;
                DateTime EndDate = DateTime.Now;
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.LaporanOPEXAPI);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<List<LaporanOpex>>(APIResponse);

                }

                //returning the employee list to view  
                return View(objInfo);
            }
        }
        public async Task<ActionResult> LaporanCapex()
        {

            List<LaporanCapex> objInfo = new List<LaporanCapex>();

            using (var client = new HttpClient())
            {
                DateTime StartDate = DateTime.Now;
                DateTime EndDate = DateTime.Now;
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.LaporanCapexAPI);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<List<LaporanCapex>>(APIResponse);

                }

                //returning the employee list to view  
                return View(objInfo);
            }
        }

        public ActionResult UploadKPIGroup(string ErrorMessage)
        {
            LaporanKPIGroup objInfo = new LaporanKPIGroup();
            if (ErrorMessage != null)
            {
                ViewBag.ErrorMessage = ErrorMessage;
            }
            return View(objInfo);
        }

        // POST: CRUD/Create
        [HttpPost]
        public async Task<ActionResult> UploadKPIGroup(LaporanKPIGroup obj, FormCollection form, HttpPostedFileBase file)
        {
            try
            {
                string fileName = "";
                string path = "";
                string ErrorMessage = "";
                string fileType = "";
                if (file != null && file.ContentLength > 0)
                {
                    fileType = Path.GetExtension(file.FileName);
                    if (fileType.ToUpper().Contains("DOC") || fileType.ToUpper().Contains("XLS") || fileType.ToUpper().Contains("PPT") || fileType.ToUpper().Contains("PDF"))
                    {
                        if (file.ContentLength <= (2000 * 1024))
                        {
                            fileName = Path.GetFileName(file.FileName);
                            path = Path.Combine(Server.MapPath("~/content/document/kpigroup/"), fileName);
                            file.SaveAs(path);
                        }
                        else
                        {
                            ErrorMessage = "File Upload tidak boleh lebih dari 2MB";
                            return RedirectToAction("UploadKPIGroup", new { ErrorMessage = ErrorMessage });
                        }
                    }
                    else
                    {
                        ErrorMessage = "File Upload harus dengan format All Office atau PDF";
                        return RedirectToAction("UploadKPIGroup", new { ErrorMessage = ErrorMessage });
                    }

                }
                obj.UserName = User.Identity.Name;
                obj.UpdatedBy = User.Identity.Name;
                obj.UpdatedDate = DateTime.Now.ToString("dd/MM/yyyy");
                obj.CreatedBy = User.Identity.Name;
                obj.CreatedDate = DateTime.Now.ToString("dd/MM/yyyy");
                obj.DocumentName = fileName;
                obj.DocName = fileName;
                obj.Description = form["description"].ToString();
                obj.FolderName = "kpigroup";
                obj.Path = path;
                // TODO: Add update logic here
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                // TODO: Add update logic here
                HttpResponseMessage response = await client.PostAsJsonAsync(Properties.Settings.Default.LaporanKPIGroupAPI, obj);
                response.EnsureSuccessStatusCode();
                return RedirectToAction("LaporanKPIGroup");
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }

        public ActionResult UploadSPOATBL(string ErrorMessage)
        {
            LaporanSPOATBL objInfo = new LaporanSPOATBL();
            if (ErrorMessage != null)
            {
                ViewBag.ErrorMessage = ErrorMessage;
            }
            return View(objInfo);
        }

        // POST: CRUD/Create
        [HttpPost]
        public async Task<ActionResult> UploadSPOATBL(LaporanSPOATBL obj, FormCollection form, HttpPostedFileBase file)
        {
            try
            {
                string fileName = "";
                string path = "";
                string ErrorMessage = "";
                string fileType = "";
                if (file != null && file.ContentLength > 0)
                {
                    fileType = Path.GetExtension(file.FileName);
                    if (fileType.ToUpper().Contains("DOC") || fileType.ToUpper().Contains("XLS") || fileType.ToUpper().Contains("PPT") || fileType.ToUpper().Contains("PDF"))
                    {
                        if (file.ContentLength <= (2000 * 1024))
                        {
                            fileName = Path.GetFileName(file.FileName);
                            path = Path.Combine(Server.MapPath("~/content/document/spoatbl/"), fileName);
                            file.SaveAs(path);
                        }
                        else
                        {
                            ErrorMessage = "File Upload tidak boleh lebih dari 2MB";
                            return RedirectToAction("UploadSPOATBL", new { ErrorMessage = ErrorMessage });
                        }
                    }
                    else
                    {
                        ErrorMessage = "File Upload harus dengan format All Office atau PDF";
                        return RedirectToAction("UploadSPOATBL", new { ErrorMessage = ErrorMessage });
                    }
                }
                obj.UserName = User.Identity.Name;
                obj.UpdatedBy = User.Identity.Name;
                obj.UpdatedDate = DateTime.Now.ToString("dd/MM/yyyy");
                obj.CreatedBy = User.Identity.Name;
                obj.CreatedDate = DateTime.Now.ToString("dd/MM/yyyy");
                obj.DocumentName = fileName;
                obj.DocFileName = fileName;
                obj.Description = form["description"].ToString();
                obj.RevisionDate = form["revisiondate"].ToString();
                obj.FolderName = "spoatbl";
                obj.Path = path;
                // TODO: Add update logic here
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                // TODO: Add update logic here
                HttpResponseMessage response = await client.PostAsJsonAsync(Properties.Settings.Default.LaporanSPOATBLAPI, obj);
                response.EnsureSuccessStatusCode();
                return RedirectToAction("LaporanSPOATBL");
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }

        public ActionResult UploadSPOProcurement(string ErrorMessage)
        {
            LaporanSPOProcurement objInfo = new Models.LaporanSPOProcurement();
            if (ErrorMessage != null)
            {
                ViewBag.ErrorMessage = ErrorMessage;
            }
            return View(objInfo);
        }

        // POST: CRUD/Create
        [HttpPost]
        public async Task<ActionResult> UploadSPOProcurement(LaporanSPOProcurement obj, FormCollection form, HttpPostedFileBase file)
        {
            try
            {
                string fileName = "";
                string path = "";
                string ErrorMessage = "";
                string fileType = "";
                if (file != null && file.ContentLength > 0)
                {
                    fileType = Path.GetExtension(file.FileName);
                    if (fileType.ToUpper().Contains("DOC") || fileType.ToUpper().Contains("XLS") || fileType.ToUpper().Contains("PPT") || fileType.ToUpper().Contains("PDF"))
                    {
                        if (file.ContentLength <= (2000 * 1024))
                        {
                            fileName = Path.GetFileName(file.FileName);
                            path = Path.Combine(Server.MapPath("~/content/document/spoprocurement/"), fileName);
                            file.SaveAs(path);
                        }
                        else
                        {
                            ErrorMessage = "File Upload tidak boleh lebih dari 2MB";
                            return RedirectToAction("UploadSPOProcurement", new { ErrorMessage = ErrorMessage });
                        }
                    }
                    else
                    {
                        ErrorMessage = "File Upload harus dengan format All Office atau PDF";
                        return RedirectToAction("UploadSPOProcurement", new { ErrorMessage = ErrorMessage });
                    }
                }
                obj.UserName = User.Identity.Name;
                obj.UpdatedBy = User.Identity.Name;
                obj.UpdatedDate = DateTime.Now.ToString("dd/MM/yyyy");
                obj.CreatedBy = User.Identity.Name;
                obj.CreatedDate = DateTime.Now.ToString("dd/MM/yyyy");
                obj.DocumentName = fileName;
                obj.DocFileName = fileName;
                obj.Description = form["description"].ToString();
                obj.RevisionDate = form["revisiondate"].ToString();
                obj.FolderName = "spoprocurement";
                obj.Path = path;
                // TODO: Add update logic here
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                // TODO: Add update logic here
                HttpResponseMessage response = await client.PostAsJsonAsync(Properties.Settings.Default.LaporanSPOProcurementAPI, obj);
                response.EnsureSuccessStatusCode();
                return RedirectToAction("LaporanSPOProcurement");
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }

        public ActionResult UploadPTO(string ErrorMessage)
        {
            LaporanPTO objInfo = new Models.LaporanPTO();
            if (ErrorMessage != null)
            {
                ViewBag.ErrorMessage = ErrorMessage;
            }
            return View(objInfo);
        }

        // POST: CRUD/Create
        [HttpPost]
        public async Task<ActionResult> UploadPTO(LaporanPTO obj, FormCollection form, HttpPostedFileBase file)
        {
            try
            {
                string fileName = "";
                string path = "";
                string ErrorMessage = "";
                string fileType = "";
                if (file != null && file.ContentLength > 0)
                {
                    fileType = Path.GetExtension(file.FileName);
                    if (fileType.ToUpper().Contains("DOC") || fileType.ToUpper().Contains("XLS") || fileType.ToUpper().Contains("PPT") || fileType.ToUpper().Contains("PDF"))
                    {
                        if (file.ContentLength <= (2000 * 1024))
                        {
                            fileName = Path.GetFileName(file.FileName);
                            path = Path.Combine(Server.MapPath("~/content/document/pto/"), fileName);
                            file.SaveAs(path);
                        }
                        else
                        {
                            ErrorMessage = "File Upload tidak boleh lebih dari 2MB";
                            return RedirectToAction("UploadPTO", new { ErrorMessage = ErrorMessage });
                        }
                    }
                    else
                    {
                        ErrorMessage = "File Upload harus dengan format All Office atau PDF";
                        return RedirectToAction("UploadPTO", new { ErrorMessage = ErrorMessage });
                    }
                }
                obj.UserName = User.Identity.Name;
                obj.UpdatedBy = User.Identity.Name;
                obj.UpdatedDate = DateTime.Now.ToString("dd/MM/yyyy");
                obj.CreatedBy = User.Identity.Name;
                obj.CreatedDate = DateTime.Now.ToString("dd/MM/yyyy");
                obj.DocumentName = fileName;
                obj.DocFileName = fileName;
                obj.Description = form["description"].ToString();
                obj.RevisionDate = form["revisiondate"].ToString();
                obj.FolderName = "pto";
                obj.Path = path;
                // TODO: Add update logic here
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                // TODO: Add update logic here
                HttpResponseMessage response = await client.PostAsJsonAsync(Properties.Settings.Default.LaporanPTOAPI, obj);
                response.EnsureSuccessStatusCode();
                return RedirectToAction("LaporanPTO");
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }

        public ActionResult UploadCeloxis(string ErrorMessage)
        {
            LaporanCeloxis objInfo = new Models.LaporanCeloxis();
            if (ErrorMessage != null)
            {
                ViewBag.ErrorMessage = ErrorMessage;
            }
            return View(objInfo);
        }

        // POST: CRUD/Create
        [HttpPost]
        public async Task<ActionResult> UploadCeloxis(LaporanCeloxis obj, FormCollection form, HttpPostedFileBase file, HttpPostedFileBase picfile)
        {
            try
            {
                string fileName = "";
                string path = "";
                string ErrorMessage = "";
                string fileType = "";
                if (file != null && file.ContentLength > 0)
                {
                    fileType = Path.GetExtension(file.FileName);
                    if (fileType.ToUpper().Contains("DOC") || fileType.ToUpper().Contains("XLS") || fileType.ToUpper().Contains("PPT") || fileType.ToUpper().Contains("PDF") || fileType.ToUpper().Contains("PNG") || fileType.ToUpper().Contains("JPG"))
                    {
                        if (file.ContentLength <= (2000 * 1024))
                        {
                            fileName = Path.GetFileName(file.FileName);
                            path = Path.Combine(Server.MapPath("~/content/document/celoxis/"), fileName);
                            file.SaveAs(path);
                        }
                        else
                        {
                            ErrorMessage = "File Upload tidak boleh lebih dari 2MB";
                            return RedirectToAction("UploadCeloxis", new { ErrorMessage = ErrorMessage });
                        }
                    }
                    else
                    {
                        ErrorMessage = "File Upload harus dengan format All Office atau PDF atau JPG atau PNG";
                        return RedirectToAction("UploadCeloxis", new { ErrorMessage = ErrorMessage });
                    }
                }
                string picName = "";
                string pathPic = "";
                string picType = "";
                if (picfile != null && picfile.ContentLength > 0)
                {
                    picType = Path.GetExtension(picfile.FileName);
                    if (picType.ToUpper().Contains("DOC") || picType.ToUpper().Contains("XLS") || picType.ToUpper().Contains("PPT") || picType.ToUpper().Contains("PDF") || picType.ToUpper().Contains("PNG") || picType.ToUpper().Contains("JPG"))
                    {
                        if (picfile.ContentLength <= (2000 * 1024))
                        {
                            picName = Path.GetFileName(picfile.FileName);
                            pathPic = Path.Combine(Server.MapPath("~/content/document/celoxis/"), picName);
                            picfile.SaveAs(pathPic);
                        }
                        else
                        {
                            ErrorMessage = "Gambar Upload tidak boleh lebih dari 2MB";
                            return RedirectToAction("UploadCeloxis", new { ErrorMessage = ErrorMessage });
                        }
                    }
                    else
                    {
                        ErrorMessage = "Gambar Upload harus dengan format All Office atau PDF atau JPG atau PNG";
                        return RedirectToAction("UploadCeloxis", new { ErrorMessage = ErrorMessage });
                    }
                }
                obj.UserName = User.Identity.Name;
                obj.UpdatedBy = User.Identity.Name;
                obj.UpdatedDate = DateTime.Now.ToString("dd/MM/yyyy");
                obj.CreatedBy = User.Identity.Name;
                obj.CreatedDate = DateTime.Now.ToString("dd/MM/yyyy");
                obj.DocFileName = fileName;
                obj.DocPicName = picName;
                obj.DocumentName = fileName;
                obj.DocumentPicName = picName;
                obj.Description = form["description"].ToString();
                obj.FolderName = "celoxis";
                obj.Path = path;
                obj.FolderPicName = "celoxis";
                obj.PathPic = pathPic;
                // TODO: Add update logic here
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                // TODO: Add update logic here
                HttpResponseMessage response = await client.PostAsJsonAsync(Properties.Settings.Default.LaporanCeloxisAPI, obj);
                response.EnsureSuccessStatusCode();
                return RedirectToAction("LaporanCeloxis");
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }

        public ActionResult UploadOpex(string ErrorMessage)
        {
            LaporanOpex objInfo = new Models.LaporanOpex();
            if (ErrorMessage != null)
            {
                ViewBag.ErrorMessage = ErrorMessage;
            }
            return View(objInfo);
        }

        // POST: CRUD/Create
        [HttpPost]
        public async Task<ActionResult> UploadOpex(LaporanOpex obj, FormCollection form, HttpPostedFileBase file, HttpPostedFileBase picfile)
        {
            try
            {
                string fileName = "";
                string path = "";
                string ErrorMessage = "";
                string fileType = "";
                if (file != null && file.ContentLength > 0)
                {
                    fileType = Path.GetExtension(file.FileName);
                    if (fileType.ToUpper().Contains("DOC") || fileType.ToUpper().Contains("XLS") || fileType.ToUpper().Contains("PPT") || fileType.ToUpper().Contains("PDF") || fileType.ToUpper().Contains("PNG") || fileType.ToUpper().Contains("JPG"))
                    {
                        if (file.ContentLength <= (2000 * 1024))
                        {
                            fileName = Path.GetFileName(file.FileName);
                            path = Path.Combine(Server.MapPath("~/content/document/opex/"), fileName);
                            file.SaveAs(path);
                        }
                        else
                        {
                            ErrorMessage = "File Upload tidak boleh lebih dari 2MB";
                            return RedirectToAction("UploadOpex", new { ErrorMessage = ErrorMessage });
                        }
                    }
                    else
                    {
                        ErrorMessage = "File Upload harus dengan format All Office atau PDF atau JPG atau PNG";
                        return RedirectToAction("UploadOpex", new { ErrorMessage = ErrorMessage });
                    }

                }
                string picName = "";
                string pathPic = "";
                string picType = "";
                if (picfile != null && picfile.ContentLength > 0)
                {
                    picType = Path.GetExtension(picfile.FileName);
                    if (picType.ToUpper().Contains("DOC") || picType.ToUpper().Contains("XLS") || picType.ToUpper().Contains("PPT") || picType.ToUpper().Contains("PDF") || picType.ToUpper().Contains("PNG") || picType.ToUpper().Contains("JPG"))
                    {
                        if (picfile.ContentLength <= (2000 * 1024))
                        {
                            picName = Path.GetFileName(picfile.FileName);
                            pathPic = Path.Combine(Server.MapPath("~/content/document/opex/"), picName);
                            picfile.SaveAs(pathPic);
                        }
                        else
                        {
                            ErrorMessage = "Gambar Upload tidak boleh lebih dari 2MB";
                            return RedirectToAction("UploadOpex", new { ErrorMessage = ErrorMessage });
                        }
                    }
                    else
                    {
                        ErrorMessage = "Gambar Upload harus dengan format All Office atau PDF atau JPG atau PNG";
                        return RedirectToAction("UploadOpex", new { ErrorMessage = ErrorMessage });
                    }
                }
                obj.UserName = User.Identity.Name;
                obj.UpdatedBy = User.Identity.Name;
                obj.UpdatedDate = DateTime.Now.ToString("dd/MM/yyyy");
                obj.CreatedBy = User.Identity.Name;
                obj.CreatedDate = DateTime.Now.ToString("dd/MM/yyyy");
                obj.DocFileName = fileName;
                obj.DocPicName = picName;
                obj.DocumentName = fileName;
                obj.DocumentPicName = picName;
                obj.Description = form["description"].ToString();
                obj.FolderName = "opex";
                obj.Path = path;
                obj.FolderPicName = "opex";
                obj.PathPic = pathPic;
                // TODO: Add update logic here
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                // TODO: Add update logic here
                HttpResponseMessage response = await client.PostAsJsonAsync(Properties.Settings.Default.LaporanOPEXAPI, obj);
                response.EnsureSuccessStatusCode();
                return RedirectToAction("LaporanOpex");
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }

        public ActionResult UploadCapex(string ErrorMessage)
        {
            LaporanCapex objInfo = new Models.LaporanCapex();
            if (ErrorMessage != null)
            {
                ViewBag.ErrorMessage = ErrorMessage;
            }
            return View(objInfo);
        }

        // POST: CRUD/Create
        [HttpPost]
        public async Task<ActionResult> UploadCapex(LaporanCapex obj, FormCollection form, HttpPostedFileBase file, HttpPostedFileBase picfile)
        {
            try
            {
                string fileName = "";
                string path = "";
                string ErrorMessage = "";
                string fileType = "";
                if (file != null && file.ContentLength > 0)
                {
                    fileType = Path.GetExtension(file.FileName);
                    if (fileType.ToUpper().Contains("DOC") || fileType.ToUpper().Contains("XLS") || fileType.ToUpper().Contains("PPT") || fileType.ToUpper().Contains("PDF") || fileType.ToUpper().Contains("PNG") || fileType.ToUpper().Contains("JPG"))
                    {
                        if (file.ContentLength <= (2000 * 1024))
                        {
                            fileName = Path.GetFileName(file.FileName);
                            path = Path.Combine(Server.MapPath("~/content/document/capex/"), fileName);
                            file.SaveAs(path);
                        }
                        else
                        {
                            ErrorMessage = "File Upload tidak boleh lebih dari 2MB";
                            return RedirectToAction("UploadCapex", new { ErrorMessage = ErrorMessage });
                        }
                    }
                    else
                    {
                        ErrorMessage = "File Upload harus dengan format All Office atau PDF atau JPG atau PNG";
                        return RedirectToAction("UploadCapex", new { ErrorMessage = ErrorMessage });
                    }
                }
                string picName = "";
                string pathPic = "";
                string picType = "";
                if (picfile != null && picfile.ContentLength > 0)
                {
                    picType = Path.GetExtension(picfile.FileName);
                    if (picType.ToUpper().Contains("DOC") || picType.ToUpper().Contains("XLS") || picType.ToUpper().Contains("PPT") || picType.ToUpper().Contains("PDF") || picType.ToUpper().Contains("PNG") || picType.ToUpper().Contains("JPG"))
                    {
                        if (picfile.ContentLength <= (2000 * 1024))
                        {
                            picName = Path.GetFileName(picfile.FileName);
                            pathPic = Path.Combine(Server.MapPath("~/content/document/capex/"), picName);
                            picfile.SaveAs(pathPic);
                        }
                        else
                        {
                            ErrorMessage = "Gambar Upload tidak boleh lebih dari 2MB";
                            return RedirectToAction("UploadCapex", new { ErrorMessage = ErrorMessage });
                        }
                    }
                    else
                    {
                        ErrorMessage = "Gambar Upload harus dengan format All Office atau PDF atau JPG atau PNG";
                        return RedirectToAction("UploadCapex", new { ErrorMessage = ErrorMessage });
                    }
                }
                obj.UserName = User.Identity.Name;
                obj.UpdatedBy = User.Identity.Name;
                obj.UpdatedDate = DateTime.Now.ToString("dd/MM/yyyy");
                obj.CreatedBy = User.Identity.Name;
                obj.CreatedDate = DateTime.Now.ToString("dd/MM/yyyy");
                obj.DocFileName = fileName;
                obj.DocPicName = picName;
                obj.UDocPicName = picName;
                obj.DocumentName = fileName;
                obj.DocumentPicName = picName;
                obj.Description = form["description"].ToString();
                obj.FolderName = "capex";
                obj.Path = path;
                obj.FolderPicName = "capex";
                obj.PathPic = pathPic;
                // TODO: Add update logic here
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                // TODO: Add update logic here
                HttpResponseMessage response = await client.PostAsJsonAsync(Properties.Settings.Default.LaporanCapexAPI, obj);
                response.EnsureSuccessStatusCode();
                return RedirectToAction("LaporanCapex");
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }

        public static string MakeValidFileName(string name)
        {
            string invalidChars = Regex.Escape(new string(System.IO.Path.GetInvalidFileNameChars()));
            string invalidReStr = string.Format(@"[{0}]+", invalidChars);
            string replace = Regex.Replace(name, invalidReStr, "_").Replace(";", "").Replace(",", "");
            return replace;
        }


        public async Task<FileResult> DownloadExcel(string startdate, string enddate)
        {
            string filename = "LaporanRincianPerjanjian.xls";
            string path = Server.MapPath("~/Content/Download/");
            string fullPath = Path.Combine(Server.MapPath("~/Content/Download/"), filename);


            Workbook wb = new Workbook();
            Worksheet ws = new Worksheet("Rincian Perjanjian");
            ws.ColumnWidths[0] = 17;
            ws.ColumnWidths[1] = 9;
            ws.ColumnWidths[2] = 44;
            ws.ColumnWidths[3] = 15;
            ws.ColumnWidths[4] = 15;
            ws.ColumnWidths[5] = 60;
            ws.ColumnWidths[6] = 15;
            ws.ColumnWidths[7] = 15;
            ws.ColumnWidths[8] = 20;
            ws.ColumnWidths[9] = 20;
            ws.ColumnWidths[10] = 10;

            ws.Cells[0, 0] = "No. Perjanjian";
            ws.Cells[0, 1] = "Memo LO";
            ws.Cells[0, 2] = "Tanggal Perjanjian";
            ws.Cells[0, 3] = "Judul Perjanjian";
            ws.Cells[0, 4] = "Rekanan";
            ws.Cells[0, 5] = "Tanggal Terima";
            ws.Cells[0, 6] = "Tanggal Selesai";
            ws.Cells[0, 7] = "PIC";
            ws.Cells[0, 8] = "Realisasi SLA";
            ws.Cells[0, 9] = "Status";

            ws.Cells[0, 0].Bold = true;
            ws.Cells[0, 1].Bold = true;
            ws.Cells[0, 2].Bold = true;
            ws.Cells[0, 3].Bold = true;
            ws.Cells[0, 4].Bold = true;
            ws.Cells[0, 5].Bold = true;
            ws.Cells[0, 6].Bold = true;
            ws.Cells[0, 7].Bold = true;
            ws.Cells[0, 8].Bold = true;
            ws.Cells[0, 9].Bold = true;


            List<LaporanRincianPerjanjian> objInfo = new List<LaporanRincianPerjanjian>();


            string StartDate = startdate;
            string EndDate = enddate;

            using (var client = new HttpClient())
            {

                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.LaporanRincianPerjanjianAPI + "/" + StartDate + "/" + EndDate);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<List<LaporanRincianPerjanjian>>(APIResponse);



                    for (int i = 0; i < objInfo.Count; i++)
                    {
                        ws.Cells[i + 1, 0] = objInfo[i].DocumentNo;
                        ws.Cells[i + 1, 1] = objInfo[i].MemoLO;
                        ws.Cells[i + 1, 2] = objInfo[i].AgreementDate;
                        ws.Cells[i + 1, 3] = objInfo[i].AgreementName;
                        ws.Cells[i + 1, 4] = objInfo[i].VendorName;
                        ws.Cells[i + 1, 5] = objInfo[i].StartDate;
                        ws.Cells[i + 1, 6] = objInfo[i].EndDate;
                        ws.Cells[i + 1, 7] = objInfo[i].EmployeePIC_Name;
                        ws.Cells[i + 1, 8] = objInfo[i].SLA;
                        ws.Cells[i + 1, 9] = objInfo[i].Status;

                    }
                    wb.Add(ws);
                    wb.Save(fullPath);
                    filename = MakeValidFileName(filename);




                }

            }

            return File(fullPath, "application/vnd.ms-excel", filename);
            var fileStream = new MemoryStream();
            wb.Save(fileStream, Simplexcel.CompressionLevel.NoCompression);



        }

        public async Task<FileResult> DownloadExcelSatuan(string No, string MemoLO, string TglPerjanjian, string Judul, string Rekanan, string Terima, string Selesai, string PIC, string SLA,string Status)
        {
            string filename = "LaporanRincianPerjanjian.xls";
            string path = Server.MapPath("~/Content/Download/");
            string fullPath = Path.Combine(Server.MapPath("~/Content/Download/"), filename);


            Workbook wb = new Workbook();
            Worksheet ws = new Worksheet("Rincian Perjanjian");
            ws.ColumnWidths[0] = 17;
            ws.ColumnWidths[1] = 9;
            ws.ColumnWidths[2] = 44;
            ws.ColumnWidths[3] = 15;
            ws.ColumnWidths[4] = 15;
            ws.ColumnWidths[5] = 60;
            ws.ColumnWidths[6] = 15;
            ws.ColumnWidths[7] = 15;
            ws.ColumnWidths[8] = 20;
            ws.ColumnWidths[9] = 10;

            ws.Cells[0, 0] = "No. Perjanjian";
            ws.Cells[0, 1] = "Memo LO";
            ws.Cells[0, 2] = "Tanggal Perjanjian";
            ws.Cells[0, 3] = "Judul Perjanjian";
            ws.Cells[0, 4] = "Rekanan";
            ws.Cells[0, 5] = "Tanggal Terima";
            ws.Cells[0, 6] = "Tanggal Selesai";
            ws.Cells[0, 7] = "PIC";
            ws.Cells[0, 8] = "Realisasi SLA";
            ws.Cells[0, 9] = "Status";

            ws.Cells[0, 0].Bold = true;
            ws.Cells[0, 1].Bold = true;
            ws.Cells[0, 2].Bold = true;
            ws.Cells[0, 3].Bold = true;
            ws.Cells[0, 4].Bold = true;
            ws.Cells[0, 5].Bold = true;
            ws.Cells[0, 6].Bold = true;
            ws.Cells[0, 7].Bold = true;
            ws.Cells[0, 8].Bold = true;
            ws.Cells[0, 9].Bold = true;

            ws.Cells[1, 0] = No;
            ws.Cells[1, 1] = MemoLO;
            ws.Cells[1, 2] = TglPerjanjian;
            ws.Cells[1, 3] = Judul;
            ws.Cells[1, 4] = Rekanan;
            ws.Cells[1, 5] = Terima;
            ws.Cells[1, 6] = Selesai;
            ws.Cells[1, 7] = PIC;
            ws.Cells[1, 8] = SLA;
            ws.Cells[1, 9] = Status;


            wb.Add(ws);
            wb.Save(fullPath);
            filename = MakeValidFileName(filename);

            return File(fullPath, "application/vnd.ms-excel", filename);
            var fileStream = new MemoryStream();
            wb.Save(fileStream, Simplexcel.CompressionLevel.NoCompression);



        }

        public async Task<FileResult> DownloadExcelReviewPerjanjian(string startdate, string enddate)
        {
            string filename = "LaporanRekapReviewPerjanjian.xls";
            string path = Server.MapPath("~/Content/Download/");
            string fullPath = Path.Combine(Server.MapPath("~/Content/Download/"), filename);


            Workbook wb = new Workbook();
            Worksheet ws = new Worksheet("Rekap Perjanjian");
            ws.ColumnWidths[0] = 17;
            ws.ColumnWidths[1] = 9;
            ws.ColumnWidths[2] = 44;
            ws.ColumnWidths[3] = 15;
            ws.ColumnWidths[4] = 15;
            ws.ColumnWidths[5] = 60;
            ws.ColumnWidths[6] = 15;
            ws.ColumnWidths[7] = 15;
            ws.ColumnWidths[8] = 20;
            ws.ColumnWidths[9] = 20;

            ws.Cells[0, 0] = "No. Perjanjian";
            ws.Cells[0, 1] = "Tanggal Perjanjian";
            ws.Cells[0, 2] = "Judul Perjanjian";
            ws.Cells[0, 3] = "SLA";
            ws.Cells[0, 4] = "Base SLA";
            ws.Cells[0, 5] = "Keterangan";
            ws.Cells[0, 6] = "Status";

            ws.Cells[0, 0].Bold = true;
            ws.Cells[0, 1].Bold = true;
            ws.Cells[0, 2].Bold = true;
            ws.Cells[0, 3].Bold = true;
            ws.Cells[0, 4].Bold = true;
            ws.Cells[0, 5].Bold = true;
            ws.Cells[0, 6].Bold = true;


            List<LaporanRekapReviewPerjanjian> objInfo = new List<LaporanRekapReviewPerjanjian>();


            string StartDate = startdate;
            string EndDate = enddate;

            using (var client = new HttpClient())
            {

                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.LaporanRekapReviewPerjanjianAPI + "/" + StartDate + "/" + EndDate);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<List<LaporanRekapReviewPerjanjian>>(APIResponse);



                    for (int i = 0; i < objInfo.Count; i++)
                    {
                        ws.Cells[i + 1, 0] = objInfo[i].DocumentNo;
                        ws.Cells[i + 1, 1] = objInfo[i].AgreementDate;
                        ws.Cells[i + 1, 2] = objInfo[i].AgreementName;
                        ws.Cells[i + 1, 3] = objInfo[i].SLA;
                        ws.Cells[i + 1, 4] = objInfo[i].Base_SLA;
                        ws.Cells[i + 1, 5] = objInfo[i].AditionalInformation;
                        ws.Cells[i + 1, 6] = objInfo[i].Status;

                    }
                    wb.Add(ws);
                    wb.Save(fullPath);
                    filename = MakeValidFileName(filename);




                }

            }

            return File(fullPath, "application/vnd.ms-excel", filename);
            var fileStream = new MemoryStream();
            wb.Save(fileStream, Simplexcel.CompressionLevel.NoCompression);



        }

        public async Task<FileResult> DownloadExcelReviewPerjanjianSatuan(string No, string Tgl, string Judul, string SLA, string BaseSLA, string Ket,string Status)
        {
            string filename = "LaporanRekapReviewPerjanjian.xls";
            string path = Server.MapPath("~/Content/Download/");
            string fullPath = Path.Combine(Server.MapPath("~/Content/Download/"), filename);


            Workbook wb = new Workbook();
            Worksheet ws = new Worksheet("Rincian Perjanjian");
            ws.ColumnWidths[0] = 17;
            ws.ColumnWidths[1] = 9;
            ws.ColumnWidths[2] = 44;
            ws.ColumnWidths[3] = 15;
            ws.ColumnWidths[4] = 15;
            ws.ColumnWidths[5] = 60;
            ws.ColumnWidths[6] = 15;
            ws.ColumnWidths[7] = 15;
            ws.ColumnWidths[8] = 20;
            ws.ColumnWidths[9] = 20;

            ws.Cells[0, 0] = "No. Perjanjian";
            ws.Cells[0, 1] = "Tanggal Perjanjian";
            ws.Cells[0, 2] = "Judul Perjanjian";
            ws.Cells[0, 3] = "SLA";
            ws.Cells[0, 4] = "Base SLA";
            ws.Cells[0, 5] = "Keterangan";
            ws.Cells[0, 6] = "Status";

            ws.Cells[0, 0].Bold = true;
            ws.Cells[0, 1].Bold = true;
            ws.Cells[0, 2].Bold = true;
            ws.Cells[0, 3].Bold = true;
            ws.Cells[0, 4].Bold = true;
            ws.Cells[0, 5].Bold = true;
            ws.Cells[0, 6].Bold = true;


            ws.Cells[1, 0] = No;
            ws.Cells[1, 1] = Tgl;
            ws.Cells[1, 2] = Judul;
            ws.Cells[1, 3] = SLA;
            ws.Cells[1, 4] = BaseSLA;
            ws.Cells[1, 5] = Ket;
            ws.Cells[1, 6] = Status;


            wb.Add(ws);
            wb.Save(fullPath);
            filename = MakeValidFileName(filename);

            return File(fullPath, "application/vnd.ms-excel", filename);
            var fileStream = new MemoryStream();
            wb.Save(fileStream, Simplexcel.CompressionLevel.NoCompression);



        }

        public async Task<ActionResult> EditKPIGroup(int id, string doc, string des,string ErrorMessage)
        {
            LaporanKPIGroup objInfo = new LaporanKPIGroup();

            //using (var client = new HttpClient())
            //{
            //    //Passing service base url  
            //    client.BaseAddress = new Uri(Baseurl);

            //    client.DefaultRequestHeaders.Clear();
            //    //Define request data format  
            //    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            //    //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
            //    HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.LaporanKPIGroupAPI + "/" + id);

            //    //Checking the response is successful or not which is sent using HttpClient  
            //    if (Res.IsSuccessStatusCode)
            //    {
            //        //Storing the response details recieved from web api   
            //        var APIResponse = Res.Content.ReadAsStringAsync().Result;

            //        //Deserializing the response recieved from web api and storing into the Employee list  
            //        objInfo = JsonConvert.DeserializeObject<LaporanKPIGroup>(APIResponse);

            //    }
            //    //returning the employee list to view  
            //   return View(objInfo);
            //}
            if (ErrorMessage != null)
            {
                ViewBag.ErrorMessage = ErrorMessage;
            }
            objInfo.KPIGroupDocID = id;
            objInfo.DocName = doc;
            objInfo.DocumentName = doc;
            objInfo.Description = des;
            return View(objInfo);

        }

        // POST: CRUD/Create
        [HttpPost]
        public async Task<ActionResult> EditKPIGroup(int id, LaporanKPIGroup obj, FormCollection form, HttpPostedFileBase file)
        {
            try
            {

                string ErrorMessage = "";
                string fileType = "";
                string fileName = "";
                string path = "";
                obj.Description = form["description"].ToString();
                if (file != null && file.ContentLength > 0)
                {
                    fileType = Path.GetExtension(file.FileName);
                    fileName = Path.GetFileName(file.FileName);
                    if (fileType.ToUpper().Contains("DOC") || fileType.ToUpper().Contains("XLS") || fileType.ToUpper().Contains("PPT") || fileType.ToUpper().Contains("PDF"))
                    {
                        if (file.ContentLength <= (2000 * 1024))
                        {
                            
                            path = Path.Combine(Server.MapPath("~/content/document/kpigroup/"), fileName);
                            file.SaveAs(path);
                        }
                        else
                        {
                            ErrorMessage = "File Upload tidak boleh lebih dari 2MB";
                            return RedirectToAction("EditKPIGroup", new { id=id,doc=obj.DocName,des=obj.Description,ErrorMessage = ErrorMessage });
                        }
                    }
                    else
                    {
                        ErrorMessage = "File Upload harus dengan format All Office atau PDF";
                        return RedirectToAction("EditKPIGroup", new { id = id, doc = obj.DocName, des = obj.Description, ErrorMessage = ErrorMessage });
                    }
                }
                if (file == null && (form["filename"].ToString() != null || form["filename"].ToString() != ""))
                {
                    fileName = form["filename"].ToString();
                    path = Path.Combine(Server.MapPath("~/content/document/kpigroup/"), fileName);
                }
                obj.UserName = User.Identity.Name;
                obj.UpdatedBy = User.Identity.Name;
                obj.UpdatedDate = DateTime.Now.ToString("dd/MM/yyyy");
                obj.CreatedBy = User.Identity.Name;
                obj.CreatedDate = DateTime.Now.ToString("dd/MM/yyyy");
                obj.DocumentName = fileName;
                obj.DocName = fileName;
                obj.Description = form["description"].ToString();
                obj.FolderName = "kpigroup";
                obj.Path = path;
                obj.KPIGroupDocID = id;
                // TODO: Add update logic here
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                // TODO: Add update logic here
                HttpResponseMessage response = await client.PutAsJsonAsync(Properties.Settings.Default.LaporanKPIGroupAPI + "/" + id, obj);
                response.EnsureSuccessStatusCode();
                return RedirectToAction("LaporanKPIGroup");
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }
        public async Task<ActionResult> EditSPOATBL(int id, string doc, string des,string revisiondate,string ErrorMessage)
        {
            LaporanSPOATBL objInfo = new LaporanSPOATBL();
            
            if (ErrorMessage != null)
            {
                ViewBag.ErrorMessage = ErrorMessage;
            }
            objInfo.SPOATBLDocID = id;
            objInfo.DocName = doc;
            objInfo.DocumentName = doc;
            objInfo.Description = des;
            objInfo.RevisionDate = revisiondate;
            return View(objInfo);

        }
        // POST: CRUD/Create
        [HttpPost]
        public async Task<ActionResult> EditSPOATBL(int id, LaporanSPOATBL obj, FormCollection form, HttpPostedFileBase file)
        {
            try
            {
                string fileName = "";
                string path = "";
                string ErrorMessage = "";
                string fileType = "";
                obj.Description = form["description"].ToString();
                obj.RevisionDate = form["revisiondate"].ToString();
                if (file != null && file.ContentLength > 0)
                {
                    fileType = Path.GetExtension(file.FileName);
                    fileName = Path.GetFileName(file.FileName);
                    if (fileType.ToUpper().Contains("DOC") || fileType.ToUpper().Contains("XLS") || fileType.ToUpper().Contains("PPT") || fileType.ToUpper().Contains("PDF"))
                    {
                        if (file.ContentLength <= (2000 * 1024))
                        {
                            fileName = Path.GetFileName(file.FileName);
                            path = Path.Combine(Server.MapPath("~/content/document/spoatbl/"), fileName);
                            file.SaveAs(path);
                        }
                        else
                        {
                            ErrorMessage = "File Upload tidak boleh lebih dari 2MB";
                            return RedirectToAction("EditSPOATBL", new { id = id, doc = obj.DocName, des = obj.Description, revisiondate=obj.RevisionDate, ErrorMessage = ErrorMessage });
                        }
                    }
                    else
                    {
                        ErrorMessage = "File Upload harus dengan format All Office atau PDF";
                        return RedirectToAction("EditSPOATBL", new { id = id, doc = obj.DocName, des = obj.Description, revisiondate = obj.RevisionDate, ErrorMessage = ErrorMessage });
                    }
                }
                if (file == null && (form["filename"].ToString() != null || form["filename"].ToString() != ""))
                {
                    fileName = form["filename"].ToString();
                    path = Path.Combine(Server.MapPath("~/content/document/spoatbl/"), fileName);
                }
                obj.UserName = User.Identity.Name;
                obj.UpdatedBy = User.Identity.Name;
                obj.UpdatedDate = DateTime.Now.ToString("dd/MM/yyyy");
                obj.CreatedBy = User.Identity.Name;
                obj.CreatedDate = DateTime.Now.ToString("dd/MM/yyyy");
                obj.DocumentName = fileName;
                obj.DocName = fileName;
                obj.DocFileName = fileName;
                obj.FolderName = "kpigroup";
                obj.Path = path;
                obj.SPOATBLDocID = id;
                
                // TODO: Add update logic here
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                // TODO: Add update logic here
                HttpResponseMessage response = await client.PutAsJsonAsync(Properties.Settings.Default.LaporanSPOATBLAPI + "/" + id, obj);
                response.EnsureSuccessStatusCode();
                return RedirectToAction("LaporanSPOATBL");
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }
        public async Task<ActionResult> EditSPOProcurement(int id, string doc, string des, string revisiondate,string ErrorMessage)
        {
            LaporanSPOProcurement objInfo = new LaporanSPOProcurement();
            if (ErrorMessage != null)
            {
                ViewBag.ErrorMessage = ErrorMessage;
            }
            objInfo.SPOProcurementDocID = id;
            objInfo.DocName = doc;
            objInfo.DocumentName = doc;
            objInfo.Description = des;
            objInfo.RevisionDate = revisiondate;
            return View(objInfo);

        }
        // POST: CRUD/Create
        [HttpPost]
        public async Task<ActionResult> EditSPOProcurement(int id, LaporanSPOProcurement obj, FormCollection form, HttpPostedFileBase file)
        {
            try
            {
                string fileName = "";
                string path = "";
                string ErrorMessage = "";
                string fileType = "";
                obj.Description = form["description"].ToString();
                obj.RevisionDate = form["revisiondate"].ToString();
                if (file != null && file.ContentLength > 0)
                {
                    fileType = Path.GetExtension(file.FileName);
                    fileName = Path.GetFileName(file.FileName);
                    if (fileType.ToUpper().Contains("DOC") || fileType.ToUpper().Contains("XLS") || fileType.ToUpper().Contains("PPT") || fileType.ToUpper().Contains("PDF"))
                    {
                        if (file.ContentLength <= (2000 * 1024))
                        {
                            fileName = Path.GetFileName(file.FileName);
                            path = Path.Combine(Server.MapPath("~/content/document/spoprocurement/"), fileName);
                            file.SaveAs(path);
                        }
                        else
                        {
                            ErrorMessage = "File Upload tidak boleh lebih dari 2MB";
                            return RedirectToAction("EditSPOProcurement", new { id = id, doc = obj.DocName, des = obj.Description, revisiondate = obj.RevisionDate, ErrorMessage = ErrorMessage });
                        }
                    }
                    else
                    {
                        ErrorMessage = "File Upload harus dengan format All Office atau PDF";
                        return RedirectToAction("EditSPOProcurement", new { id = id, doc = obj.DocName, des = obj.Description, revisiondate = obj.RevisionDate, ErrorMessage = ErrorMessage });
                    }
                }
                if (file == null && (form["filename"].ToString() != null || form["filename"].ToString() != ""))
                {
                    fileName = form["filename"].ToString();
                    path = Path.Combine(Server.MapPath("~/content/document/spoprocurement/"), fileName);
                }
                obj.UserName = User.Identity.Name;
                obj.UpdatedBy = User.Identity.Name;
                obj.UpdatedDate = DateTime.Now.ToString("dd/MM/yyyy");
                obj.CreatedBy = User.Identity.Name;
                obj.CreatedDate = DateTime.Now.ToString("dd/MM/yyyy");
                obj.DocFileName = fileName;
                obj.DocumentName = fileName;
                obj.DocName = fileName;
                obj.Description = form["description"].ToString();
                obj.FolderName = "spoprocurement";
                obj.Path = path;
                obj.SPOProcurementDocID = id;
               
                // TODO: Add update logic here
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                // TODO: Add update logic here
                HttpResponseMessage response = await client.PutAsJsonAsync(Properties.Settings.Default.LaporanSPOProcurementAPI + "/" + id, obj);
                response.EnsureSuccessStatusCode();
                return RedirectToAction("LaporanSPOProcurement");
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }

        public async Task<ActionResult> EditPTO(int id, string doc, string des, string revisiondate,string ErrorMessage)
        {
            LaporanPTO objInfo = new LaporanPTO();
            if (ErrorMessage != null)
            {
                ViewBag.ErrorMessage = ErrorMessage;
            }
            objInfo.PTODocID = id;
            objInfo.DocName = doc;
            objInfo.DocumentName = doc;
            objInfo.Description = des;
            objInfo.RevisionDate = revisiondate;
            return View(objInfo);

        }
        // POST: CRUD/Create
        [HttpPost]
        public async Task<ActionResult> EditPTO(int id, LaporanPTO obj, FormCollection form, HttpPostedFileBase file)
        {
            try
            {
                string fileName = "";
                string path = "";
                string ErrorMessage = "";
                string fileType = "";
                obj.Description = form["description"].ToString();
                obj.RevisionDate = form["revisiondate"].ToString();
                if (file != null && file.ContentLength > 0)
                {
                    fileType = Path.GetExtension(file.FileName);
                    fileName = Path.GetFileName(file.FileName);
                    if (fileType.ToUpper().Contains("DOC") || fileType.ToUpper().Contains("XLS") || fileType.ToUpper().Contains("PPT") || fileType.ToUpper().Contains("PDF"))
                    {
                        if (file.ContentLength <= (2000 * 1024))
                        {
                            fileName = Path.GetFileName(file.FileName);
                            path = Path.Combine(Server.MapPath("~/content/document/pto/"), fileName);
                            file.SaveAs(path);
                        }
                        else
                        {
                            ErrorMessage = "File Upload tidak boleh lebih dari 2MB";
                            return RedirectToAction("EditPTO", new { id = id, doc = obj.DocName, des = obj.Description, revisiondate = obj.RevisionDate, ErrorMessage = ErrorMessage });
                        }
                    }
                    else
                    {
                        ErrorMessage = "File Upload harus dengan format All Office atau PDF";
                        return RedirectToAction("EditPTO", new { id = id, doc = obj.DocName, des = obj.Description, revisiondate = obj.RevisionDate, ErrorMessage = ErrorMessage });
                    }
                }
                if (file == null && (form["filename"].ToString() != null || form["filename"].ToString() != ""))
                {
                    fileName = form["filename"].ToString();
                    path = Path.Combine(Server.MapPath("~/content/document/pto/"), fileName);
                }
                obj.UserName = User.Identity.Name;
                obj.UpdatedBy = User.Identity.Name;
                obj.UpdatedDate = DateTime.Now.ToString("dd/MM/yyyy");
                obj.CreatedBy = User.Identity.Name;
                obj.CreatedDate = DateTime.Now.ToString("dd/MM/yyyy");
                obj.DocFileName = fileName;
                obj.DocumentName = fileName;
                obj.DocName = fileName;
                obj.Description = form["description"].ToString();
                obj.FolderName = "pto";
                obj.Path = path;
                obj.PTODocID = id;
                
                // TODO: Add update logic here
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                // TODO: Add update logic here
                HttpResponseMessage response = await client.PutAsJsonAsync(Properties.Settings.Default.LaporanPTOAPI + "/" + id, obj);
                response.EnsureSuccessStatusCode();
                return RedirectToAction("LaporanPTO");
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }
        public async Task<ActionResult> EditCeloxis(int id, string doc, string des, string picdoc,string ErrorMessage)
        {
            LaporanCeloxis objInfo = new LaporanCeloxis();
            if (ErrorMessage != null)
            {
                ViewBag.ErrorMessage = ErrorMessage;
            }
            objInfo.CeloxisDocID = id;
            objInfo.DocFileName = doc;
            objInfo.DocumentName = doc;
            objInfo.Description = des;
            objInfo.DocPicName = picdoc;
            return View(objInfo);

        }
        // POST: CRUD/Create
        [HttpPost]
        public async Task<ActionResult> EditCeloxis(int id, LaporanCeloxis obj, FormCollection form, HttpPostedFileBase file, HttpPostedFileBase picfile)
        {
            try
            {
                string fileName = "";
                string picName = "";
                string path = "";
                string ErrorMessage = "";
                string fileType = "";
                string picType = "";
                obj.Description = form["description"].ToString();
                fileName = form["filename"].ToString();
                picName = form["filenamepic"].ToString();
                if (file != null && file.ContentLength > 0)
                {

                    fileType = Path.GetExtension(file.FileName);
                    fileName = Path.GetFileName(file.FileName);
                    if (fileType.ToUpper().Contains("DOC") || fileType.ToUpper().Contains("XLS") || fileType.ToUpper().Contains("PPT") || fileType.ToUpper().Contains("PDF") || fileType.ToUpper().Contains("PNG") || fileType.ToUpper().Contains("JPG"))
                    {
                        if (file.ContentLength <= (2000 * 1024))
                        {
                            fileName = Path.GetFileName(file.FileName);
                            path = Path.Combine(Server.MapPath("~/content/document/celoxis/"), fileName);
                            file.SaveAs(path);
                        }
                        else
                        {
                            ErrorMessage = "File Upload tidak boleh lebih dari 2MB";
                            return RedirectToAction("EditCeloxis", new { id = id,  des = obj.Description,doc=fileName,picdoc=picName, ErrorMessage = ErrorMessage });
                        }
                    }
                    else
                    {
                        ErrorMessage = "File Upload harus dengan format All Office atau PDF atau PNG atau JPG";
                        return RedirectToAction("EditCeloxis", new { id = id, des = obj.Description, doc = fileName, picdoc = picName, ErrorMessage = ErrorMessage });
                    }
                }
                if (file == null && (form["filename"].ToString() != null || form["filename"].ToString() != ""))
                {
                    fileName = form["filename"].ToString();
                    path = Path.Combine(Server.MapPath("~/content/document/celoxis/"), fileName);
                }
                
                string pathPic = "";
                if (picfile != null && picfile.ContentLength > 0)
                {
                    picType = Path.GetExtension(picfile.FileName);
                    picName = Path.GetFileName(picfile.FileName);
                    if (picType.ToUpper().Contains("DOC") || picType.ToUpper().Contains("XLS") || picType.ToUpper().Contains("PPT") || picType.ToUpper().Contains("PDF") || picType.ToUpper().Contains("PNG") || picType.ToUpper().Contains("JPG"))
                    {
                        if (picfile.ContentLength <= (2000 * 1024))
                        {
                            picName = Path.GetFileName(picfile.FileName);
                            pathPic = Path.Combine(Server.MapPath("~/content/document/celoxis/"), picName);
                            picfile.SaveAs(pathPic);
                        }
                        else
                        {
                            ErrorMessage = "File Upload tidak boleh lebih dari 2MB";
                            return RedirectToAction("EditCeloxis", new { id = id, des = obj.Description, doc = fileName, picdoc = picName, ErrorMessage = ErrorMessage });
                        }
                    }
                    else
                    {
                        ErrorMessage = "File Upload harus dengan format All Office atau PDF atau PNG atau JPG";
                        return RedirectToAction("EditCeloxis", new { id = id, des = obj.Description, doc = fileName, picdoc = picName, ErrorMessage = ErrorMessage });
                    }
                }
                if (picfile == null && (form["filenamepic"].ToString() != null || form["filenamepic"].ToString() != ""))
                {
                    picName = form["filenamepic"].ToString();
                    path = Path.Combine(Server.MapPath("~/content/document/celoxis/"), picName);
                }
                obj.UserName = User.Identity.Name;
                obj.UpdatedBy = User.Identity.Name;
                obj.UpdatedDate = DateTime.Now.ToString("dd/MM/yyyy");
                obj.CreatedBy = User.Identity.Name;
                obj.CreatedDate = DateTime.Now.ToString("dd/MM/yyyy");
                obj.DocumentName = fileName;
                obj.Description = form["description"].ToString();
                obj.FolderName = "celoxis";
                obj.Path = path;
                obj.CeloxisDocID = id;
                obj.DocFileName = fileName;
                obj.DocPicName = picName;
                // TODO: Add update logic here
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                // TODO: Add update logic here
                HttpResponseMessage response = await client.PutAsJsonAsync(Properties.Settings.Default.LaporanCeloxisAPI + "/" + id, obj);
                response.EnsureSuccessStatusCode();
                return RedirectToAction("LaporanCeloxis");
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }

        public async Task<ActionResult> EditOpex(int id, string doc, string des, string picdoc,string ErrorMessage)
        {
            LaporanOpex objInfo = new LaporanOpex();
            if (ErrorMessage != null)
            {
                ViewBag.ErrorMessage = ErrorMessage;
            }
            objInfo.OpexDocID = id;
            objInfo.DocFileName = doc;
            objInfo.DocumentName = doc;
            objInfo.Description = des;
            objInfo.DocPicName = picdoc;
            return View(objInfo);

        }
        // POST: CRUD/Create
        [HttpPost]
        public async Task<ActionResult> EditOpex(int id, LaporanOpex obj, FormCollection form, HttpPostedFileBase file, HttpPostedFileBase picfile)
        {
            try
            {
                string fileName = "";
                string picName = "";
                string path = "";
                string ErrorMessage = "";
                string fileType = "";
                string picType = "";
                obj.Description = form["description"].ToString();
                fileName = form["filename"].ToString();
                picName = form["filenamepic"].ToString();
                if (file != null && file.ContentLength > 0)
                {
                    picType = Path.GetExtension(picfile.FileName);
                    picName = Path.GetFileName(picfile.FileName);
                    if (picType.ToUpper().Contains("DOC") || picType.ToUpper().Contains("XLS") || picType.ToUpper().Contains("PPT") || picType.ToUpper().Contains("PDF") || picType.ToUpper().Contains("PNG") || picType.ToUpper().Contains("JPG"))
                    {
                        if (picfile.ContentLength <= (2000 * 1024))
                        {
                            fileName = Path.GetFileName(file.FileName);
                            path = Path.Combine(Server.MapPath("~/content/document/opex/"), fileName);
                            file.SaveAs(path);
                        }
                        else
                        {
                            ErrorMessage = "File Upload tidak boleh lebih dari 2MB";
                            return RedirectToAction("EditOpex", new { id = id, des = obj.Description, doc = fileName, picdoc = picName, ErrorMessage = ErrorMessage });
                        }
                    }
                    else
                    {
                        ErrorMessage = "File Upload harus dengan format All Office atau PDF atau PNG atau JPG";
                        return RedirectToAction("EditOpex", new { id = id, des = obj.Description, doc = fileName, picdoc = picName, ErrorMessage = ErrorMessage });
                    }
                }
                if (file == null && (form["filename"].ToString() != null || form["filename"].ToString() != ""))
                {
                    fileName = form["filename"].ToString();
                    path = Path.Combine(Server.MapPath("~/content/document/opex/"), fileName);
                }
                
                string pathPic = "";
                if (picfile != null && picfile.ContentLength > 0)
                {
                    picType = Path.GetExtension(picfile.FileName);
                    picName = Path.GetFileName(picfile.FileName);
                    if (picType.ToUpper().Contains("DOC") || picType.ToUpper().Contains("XLS") || picType.ToUpper().Contains("PPT") || picType.ToUpper().Contains("PDF") || picType.ToUpper().Contains("PNG") || picType.ToUpper().Contains("JPG"))
                    {
                        if (picfile.ContentLength <= (2000 * 1024))
                        {
                            picName = Path.GetFileName(picfile.FileName);
                            pathPic = Path.Combine(Server.MapPath("~/content/document/opex/"), picName);
                            picfile.SaveAs(pathPic);
                        }
                        else
                        {
                            ErrorMessage = "File Upload tidak boleh lebih dari 2MB";
                            return RedirectToAction("EditOpex", new { id = id, des = obj.Description, doc = fileName, picdoc = picName, ErrorMessage = ErrorMessage });
                        }
                    }
                    else
                    {
                        ErrorMessage = "File Upload harus dengan format All Office atau PDF atau PNG atau JPG";
                        return RedirectToAction("EditOpex", new { id = id, des = obj.Description, doc = fileName, picdoc = picName, ErrorMessage = ErrorMessage });
                    }
                }
                if (picfile == null && (form["filenamepic"].ToString() != null || form["filenamepic"].ToString() != ""))
                {
                    picName = form["filenamepic"].ToString();
                    path = Path.Combine(Server.MapPath("~/content/document/opex/"), picName);
                }
                obj.UserName = User.Identity.Name;
                obj.UpdatedBy = User.Identity.Name;
                obj.UpdatedDate = DateTime.Now.ToString("dd/MM/yyyy");
                obj.CreatedBy = User.Identity.Name;
                obj.CreatedDate = DateTime.Now.ToString("dd/MM/yyyy");
                obj.DocumentName = fileName;
                obj.Description = form["description"].ToString();
                obj.FolderName = "capex";
                obj.Path = path;
                obj.OpexDocID = id;
                obj.DocFileName = fileName;
                obj.DocPicName = picName;
                // TODO: Add update logic here
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                // TODO: Add update logic here
                HttpResponseMessage response = await client.PutAsJsonAsync(Properties.Settings.Default.LaporanOPEXAPI + "/" + id, obj);
                response.EnsureSuccessStatusCode();
                return RedirectToAction("LaporanOpex");
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }
        public async Task<ActionResult> EditCapex(int id, string doc, string des, string picdoc,string ErrorMessage)
        {
            LaporanCapex objInfo = new LaporanCapex();
            if (ErrorMessage != null)
            {
                ViewBag.ErrorMessage = ErrorMessage;
            }
            objInfo.CapexDocID = id;
            objInfo.DocFileName = doc;
            objInfo.DocumentName = doc;
            objInfo.Description = des;
            objInfo.DocPicName = picdoc;
            return View(objInfo);

        }
        // POST: CRUD/Create
        [HttpPost]
        public async Task<ActionResult> EditCapex(int id, LaporanCapex obj, FormCollection form, HttpPostedFileBase file, HttpPostedFileBase picfile)
        {
            try
            {
                string fileName = "";
                string picName = "";
                string path = "";
                string ErrorMessage = "";
                string fileType = "";
                string picType = "";
                obj.Description = form["description"].ToString();
                fileName = form["filename"].ToString();
                picName = form["filenamepic"].ToString();
                if (file != null && file.ContentLength > 0)
                {
                    picType = Path.GetExtension(picfile.FileName);
                    picName = Path.GetFileName(picfile.FileName);
                    if (picType.ToUpper().Contains("DOC") || picType.ToUpper().Contains("XLS") || picType.ToUpper().Contains("PPT") || picType.ToUpper().Contains("PDF") || picType.ToUpper().Contains("PNG") || picType.ToUpper().Contains("JPG"))
                    {
                        if (picfile.ContentLength <= (2000 * 1024))
                        {
                            fileName = Path.GetFileName(file.FileName);
                            path = Path.Combine(Server.MapPath("~/content/document/capex/"), fileName);
                            picfile.SaveAs(path);
                        }
                        else
                        {
                            ErrorMessage = "File Upload tidak boleh lebih dari 2MB";
                            return RedirectToAction("EditCapex", new { id = id, des = obj.Description, doc = fileName, picdoc = picName, ErrorMessage = ErrorMessage });
                        }
                    }
                    else
                    {
                        ErrorMessage = "File Upload harus dengan format All Office atau PDF atau PNG atau JPG";
                        return RedirectToAction("EditCapex", new { id = id, des = obj.Description, doc = fileName, picdoc = picName, ErrorMessage = ErrorMessage });
                    }
                }
                if (file == null && (form["filename"].ToString() != null || form["filename"].ToString() != ""))
                {
                    fileName = form["filename"].ToString();
                    path = Path.Combine(Server.MapPath("~/content/document/capex/"), fileName);
                }
                
                string pathPic = "";
                if (picfile != null && picfile.ContentLength > 0)
                {
                    picType = Path.GetExtension(picfile.FileName);
                    picName = Path.GetFileName(picfile.FileName);
                    if (picType.ToUpper().Contains("DOC") || picType.ToUpper().Contains("XLS") || picType.ToUpper().Contains("PPT") || picType.ToUpper().Contains("PDF") || picType.ToUpper().Contains("PNG") || picType.ToUpper().Contains("JPG"))
                    {
                        if (picfile.ContentLength <= (2000 * 1024))
                        {
                            picName = Path.GetFileName(picfile.FileName);
                            pathPic = Path.Combine(Server.MapPath("~/content/document/capex/"), picName);
                            picfile.SaveAs(pathPic);
                        }
                        else
                        {
                            ErrorMessage = "File Upload tidak boleh lebih dari 2MB";
                            return RedirectToAction("EditCapex", new { id = id, des = obj.Description, doc = fileName, picdoc = picName, ErrorMessage = ErrorMessage });
                        }
                    }
                    else
                    {
                        ErrorMessage = "File Upload harus dengan format All Office atau PDF atau PNG atau JPG";
                        return RedirectToAction("EditCapex", new { id = id, des = obj.Description, doc = fileName, picdoc = picName, ErrorMessage = ErrorMessage });
                    }

                }
                if (picfile == null && (form["filenamepic"].ToString() != null || form["filenamepic"].ToString() != ""))
                {
                    picName = form["filenamepic"].ToString();
                    path = Path.Combine(Server.MapPath("~/content/document/capex/"), picName);
                }
                obj.UserName = User.Identity.Name;
                obj.UpdatedBy = User.Identity.Name;
                obj.UpdatedDate = DateTime.Now.ToString("dd/MM/yyyy");
                obj.CreatedBy = User.Identity.Name;
                obj.CreatedDate = DateTime.Now.ToString("dd/MM/yyyy");
                obj.DocumentName = fileName;
                obj.Description = form["description"].ToString();
                obj.FolderName = "capex";
                obj.Path = path;
                obj.CapexDocID = id;
                obj.DocFileName = fileName;
                obj.DocPicName = picName;
                // TODO: Add update logic here
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                // TODO: Add update logic here
                HttpResponseMessage response = await client.PutAsJsonAsync(Properties.Settings.Default.LaporanCapexAPI + "/" + id, obj);
                response.EnsureSuccessStatusCode();
                return RedirectToAction("LaporanCapex");
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }

        public async Task<ActionResult> LaporanResidence()
        {
            var objInfo = new ReportResidenceViewModel();
            objInfo.InitializePermissions(
                Session,
                "20"
            );

            objInfo.ReportResidenceJson = await JsonDownloader.GetJsonOrDefaultAsync(
                Baseurl2,
                null,
                EmptyJsonDefault.EmptyArray
            );

            string id = "0";
            List<EmployeePosition> _listEmployeePosition = await GetEmployeePosition();
            ViewBag.ListEmployeePosition = _listEmployeePosition;

            //using (var client = new HttpClient())
            //{

            //    //Passing service base url  
            //    client.BaseAddress = new Uri(Baseurl2);

            //    client.DefaultRequestHeaders.Clear();
            //    //Define request data format  
            //    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            //    //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
            //    //HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.ResidenceReportAPI + "/" + id);
            //    HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.MasterResidenceAPI);
            //    //Checking the response is successful or not which is sent using HttpClient  
            //    if (Res.IsSuccessStatusCode)
            //    {
            //        //Storing the response details recieved from web api   
            //        var APIResponse = Res.Content.ReadAsStringAsync().Result;

            //        //Deserializing the response recieved from web api and storing into the Employee list  
            //        objInfo = JsonConvert.DeserializeObject<List<ReportResidence>>(APIResponse);

            //    }
            //}
            return View(objInfo);
        }

        [HttpPost]
        public async Task<ActionResult> LaporanResidence(FormCollection form)
        {
            List<ReportResidence> objInfo = new List<ReportResidence>();
            string EmployeePositionID = form["EmployeePositionID"].ToString();
            if (EmployeePositionID == "")
            {
                EmployeePositionID = "0";
            }
            ViewBag.EmployeePositionID = EmployeePositionID;
            List<EmployeePosition> _listEmployeePosition = await GetEmployeePosition();
            ViewBag.ListEmployeePosition = _listEmployeePosition;
            if (form["Excel"] != null)
            {
                return RedirectToAction("DownloadExcelResidenceReport", "Laporan", new { EmployeePoisitionId = EmployeePositionID });
            }

            var model = new ReportResidenceViewModel();

            model.InitializePermissions(
                Session,
                "20"
            );

            model.ReportResidenceJson = await JsonDownloader.GetJsonOrDefaultAsync(
                Baseurl2,
                Properties.Settings.Default.ReportResidenceAPI + "/" + EmployeePositionID,
                EmptyJsonDefault.EmptyArray
            );

            //returning the employee list to view  
            return View(model);
            
        }

        public async Task<FileResult> DownloadExcelResidenceReport(string EmployeePoisitionId)
        {
            string filename = "LaporanResident.xls";
            string path = Server.MapPath("~/Content/Download/");
            string fullPath = Path.Combine(Server.MapPath("~/Content/Download/"), filename);


            Workbook wb = new Workbook();
            Worksheet ws = new Worksheet("Laporan Resident");
            ws.ColumnWidths[0] = 5;
            ws.ColumnWidths[1] = 30;
            ws.ColumnWidths[2] = 15;
            ws.ColumnWidths[3] = 15;
            ws.ColumnWidths[4] = 15;
            ws.ColumnWidths[5] = 30;
            ws.ColumnWidths[6] = 15;
            ws.ColumnWidths[7] = 15;
            ws.ColumnWidths[8] = 15;
            ws.ColumnWidths[9] = 15;
            ws.ColumnWidths[10] = 15;
            ws.ColumnWidths[11] = 15;
            ws.ColumnWidths[12] = 15;
            ws.ColumnWidths[13] = 15;
            ws.ColumnWidths[14] = 15;
            ws.ColumnWidths[15] = 25;
            ws.ColumnWidths[16] = 20;
            ws.ColumnWidths[17] = 20;
            ws.ColumnWidths[18] = 20;
            ws.ColumnWidths[19] = 20;
            ws.ColumnWidths[20] = 15;

            ws.Cells[0, 0] = "No";
            ws.Cells[0, 1] = "Alamat Rumah Dinas";
            ws.Cells[0, 2] = "Kota";
            ws.Cells[0, 3] = "Luas Tanah";
            ws.Cells[0, 4] = "Luas Bangunan";
            ws.Cells[0, 5] = "Nama Penghuni / Calon";
            ws.Cells[0, 6] = "Jabatan";
            ws.Cells[0, 7] = "Kelas";
            ws.Cells[0, 8] = "Status Rumah Dinas";
            ws.Cells[0, 9] = "Kondisi";
            ws.Cells[0, 10] = "Jenis Perbaikan";
            ws.Cells[0, 11] = "Keterangan";
            ws.Cells[0, 12] = "No. ID PLN";
            ws.Cells[0, 13] = "No. ID PDAM";
            ws.Cells[0, 14] = "No. ID Telepon";
            ws.Cells[0, 15] = "Vendor Residential Management";
            ws.Cells[0, 16] = "Nilai Perolehan (Rp)";
            ws.Cells[0, 17] = "Tahun Perolehan";
            ws.Cells[0, 18] = "Renovasi Terakhir";
            ws.Cells[0, 19] = "Biaya Renovasi (Rp)";
            ws.Cells[0, 20] = "Foto";


            ws.Cells[0, 0].Bold = true;
            ws.Cells[0, 1].Bold = true;
            ws.Cells[0, 2].Bold = true;
            ws.Cells[0, 3].Bold = true;
            ws.Cells[0, 4].Bold = true;
            ws.Cells[0, 5].Bold = true;
            ws.Cells[0, 6].Bold = true;
            ws.Cells[0, 7].Bold = true;
            ws.Cells[0, 8].Bold = true;
            ws.Cells[0, 9].Bold = true;
            ws.Cells[0, 10].Bold = true;
            ws.Cells[0, 11].Bold = true;
            ws.Cells[0, 12].Bold = true;
            ws.Cells[0, 13].Bold = true;
            ws.Cells[0, 14].Bold = true;
            ws.Cells[0, 15].Bold = true;
            ws.Cells[0, 16].Bold = true;
            ws.Cells[0, 17].Bold = true;
            ws.Cells[0, 18].Bold = true;
            ws.Cells[0, 19].Bold = true;
            ws.Cells[0, 20].Bold = true;



            List<ReportResidence> objInfo = new List<ReportResidence>();


            //string StartDate = startdate;
            //string EndDate = enddate;

            using (var client = new HttpClient())
            {

                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.ReportResidenceAPI + "/" + EmployeePoisitionId);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<List<ReportResidence>>(APIResponse);



                    for (int i = 0; i < objInfo.Count; i++)
                    {
                        ws.Cells[i + 1, 0] = objInfo[i].ListNo;
                        ws.Cells[i + 1, 1] = objInfo[i].Address;
                        ws.Cells[i + 1, 2] = objInfo[i].CityName;
                        ws.Cells[i + 1, 3] = String.Format("{0:n0}", Convert.ToDouble(objInfo[i].LandArea)).Replace(",", ".");
                        //ws.Cells[i + 1, 3] = objInfo[i].LandArea;
                        ws.Cells[i + 1, 4] = String.Format("{0:n0}", Convert.ToDouble(objInfo[i].BuildingArea)).Replace(",", ".");
                        //ws.Cells[i + 1, 4] = objInfo[i].BuildingArea;
                        ws.Cells[i + 1, 5] = objInfo[i].ResidentName;
                        ws.Cells[i + 1, 6] = objInfo[i].EmployeePositionName;
                        ws.Cells[i + 1, 7] = objInfo[i].ClassName;
                        ws.Cells[i + 1, 8] = objInfo[i].ResidenceStatusName;
                        ws.Cells[i + 1, 9] = objInfo[i].ResidenceConditionName;
                        ws.Cells[i + 1, 10] = objInfo[i].RepairTypeName;
                        ws.Cells[i + 1, 11] = objInfo[i].Description;
                        ws.Cells[i + 1, 12] = objInfo[i].PLNID;
                        ws.Cells[i + 1, 13] = objInfo[i].PDAMID;
                        ws.Cells[i + 1, 14] = objInfo[i].TelephoneID;
                        ws.Cells[i + 1, 15] = objInfo[i].VendorResidentialManagement;
                        ws.Cells[i + 1, 16] = String.Format("{0:n0}", Convert.ToDouble(objInfo[i].AcquisitionValue)).Replace(",", ".");
                        //ws.Cells[i + 1, 16] = objInfo[i].AcquisitionValue;
                        ws.Cells[i + 1, 17] = objInfo[i].AcquisitionYear;
                        ws.Cells[i + 1, 18] = objInfo[i].RenovationDate;
                        ws.Cells[i + 1, 19] = String.Format("{0:n0}", Convert.ToDouble(objInfo[i].RenovationCost)).Replace(",", ".");
                        //ws.Cells[i + 1, 19] = objInfo[i].RenovationCost;
                        ws.Cells[i + 1, 20] = objInfo[i].DocFileName;

                    }
                    wb.Add(ws);
                    wb.Save(fullPath);
                    filename = MakeValidFileName(filename);




                }

            }

            return File(fullPath, "application/vnd.ms-excel", filename);
            var fileStream = new MemoryStream();
            wb.Save(fileStream, Simplexcel.CompressionLevel.NoCompression);



        }

        public async Task<List<EmployeePosition>> GetEmployeePosition()
        {
            List<EmployeePosition> _list = new List<EmployeePosition>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.EmployeePositionAPI);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    _list = JsonConvert.DeserializeObject<List<EmployeePosition>>(APIResponse);

                }
                //returning the employee list to view  

            }
            return _list;
        }

        public async Task<ActionResult> LaporanOfficialResident()
        {
            var model = new ReportOfficialResidentViewModel();

            model.InitializePermissions(
                Session,
                "20"
            );

            model.ReportOfficialResidentIndexJson = await JsonDownloader.GetJsonOrDefaultAsync(
                Baseurl2,
                null,
                EmptyJsonDefault.EmptyArray
            );

            string id = "0";
            List<EmployeePosition> _listEmployeePosition = await GetEmployeePosition();
            ViewBag.ListEmployeePosition = _listEmployeePosition;

            //using (var client = new HttpClient())
            //{

            //    //Passing service base url  
            //    client.BaseAddress = new Uri(Baseurl2);

            //    client.DefaultRequestHeaders.Clear();
            //    //Define request data format  
            //    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            //    //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
            //    //HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.ResidenceReportAPI + "/" + id);
            //    HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.MasterResidenceAPI);
            //    //Checking the response is successful or not which is sent using HttpClient  
            //    if (Res.IsSuccessStatusCode)
            //    {
            //        //Storing the response details recieved from web api   
            //        var APIResponse = Res.Content.ReadAsStringAsync().Result;

            //        //Deserializing the response recieved from web api and storing into the Employee list  
            //        objInfo = JsonConvert.DeserializeObject<List<ReportResidence>>(APIResponse);

            //    }
            //}
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> LaporanOfficialResident(FormCollection form)
        {

            var model = new ReportOfficialResidentViewModel();
            string EmployeePositionID = form["EmployeePositionID"].ToString();
            string DateFrom = form["DateFrom"].ToString();
            string DateTo = form["DateTo"].ToString();

            ViewBag.DateFrom = form["DateFrom"].ToString();
            ViewBag.DateTo = form["DateTo"].ToString();

            if (EmployeePositionID == "")
            {
                EmployeePositionID = "0";
            }
            ViewBag.EmployeePositionID = EmployeePositionID;
            List<EmployeePosition> _listEmployeePosition = await GetEmployeePosition();
            ViewBag.ListEmployeePosition = _listEmployeePosition;
            //if (form["Excel"] != null)
            //{
            //    return RedirectToAction("DownloadExcelOfficialResidentReport", "Laporan", new { EmployeePositionID = EmployeePositionID, DateFrom = DateFrom , DateTo = DateTo });
            //}

            model.InitializePermissions(
                Session,
                "20"
            );

            model.ReportOfficialResidentIndexJson = await JsonDownloader.GetJsonOrDefaultAsync(
                Baseurl2,
                Properties.Settings.Default.OfficialResidentReportAPI + "?EmployeePositionID=" + EmployeePositionID.ToString() + "&DateFrom=" + DateFrom.ToString() + "&DateTo=" + DateTo.ToString(),
                EmptyJsonDefault.EmptyArray
            );

            return View(model);
            
        }

        public async Task<FileResult> DownloadExcelOfficialResidentReport(string EmployeePositionID,string DateFrom,string DateTo)
        {
            string filename = "LaporanRumahDinas.xls";
            string path = Server.MapPath("~/Content/Download/");
            string fullPath = Path.Combine(Server.MapPath("~/Content/Download/"), filename);
            if (DateFrom == null)
            {
                DateFrom = "";
            }

            if (DateTo == null)
            {
                DateTo = "";
            }

            Workbook wb = new Workbook();
            Worksheet ws = new Worksheet("Laporan Rumah Dinas");
            ws.ColumnWidths[0] = 20;
            ws.ColumnWidths[1] = 30;
            ws.ColumnWidths[2] = 15;
            ws.ColumnWidths[3] = 15;
            ws.ColumnWidths[4] = 15;
            ws.ColumnWidths[5] = 30;
            ws.ColumnWidths[6] = 15;
            ws.ColumnWidths[7] = 15;
            ws.ColumnWidths[8] = 15;
            ws.ColumnWidths[9] =20;
            ws.ColumnWidths[10] = 20;
            ws.ColumnWidths[11] = 15;
            ws.ColumnWidths[12] = 25;
            ws.ColumnWidths[13] = 25;
            ws.ColumnWidths[14] = 15;

            ws.Cells[0, 0] = "Jabatan";
            ws.Cells[0, 1] = "Alamat Rumah Dinas";
            ws.Cells[0, 2] = "Kota";
            ws.Cells[0, 3] = "Luas Tanah";
            ws.Cells[0, 4] = "Luas Bangunan";
            ws.Cells[0, 5] = "Kelas";
            ws.Cells[0, 6] = "Kondisi";
            ws.Cells[0, 7] = "Nama Penghuni";
            ws.Cells[0, 8] = "NIP";
            ws.Cells[0, 9] = "Tanggal Dihuni (dari)";
            ws.Cells[0, 10] = "Tanggal Dihuni (sampai)";
            ws.Cells[0, 11] = "Lampiran";
            ws.Cells[0, 12] = "Keterangan Pemesanan";
            ws.Cells[0, 13] = "Keterangan Pengosongan";
            ws.Cells[0, 14] = "Status";



            ws.Cells[0, 0].Bold = true;
            ws.Cells[0, 1].Bold = true;
            ws.Cells[0, 2].Bold = true;
            ws.Cells[0, 3].Bold = true;
            ws.Cells[0, 4].Bold = true;
            ws.Cells[0, 5].Bold = true;
            ws.Cells[0, 6].Bold = true;
            ws.Cells[0, 7].Bold = true;
            ws.Cells[0, 8].Bold = true;
            ws.Cells[0, 9].Bold = true;
            ws.Cells[0, 10].Bold = true;
            ws.Cells[0, 11].Bold = true;
            ws.Cells[0, 12].Bold = true;
            ws.Cells[0, 13].Bold = true;
            ws.Cells[0, 14].Bold = true;



            List<ReportOfficialResident> objInfo = new List<ReportOfficialResident>();


            //string StartDate = startdate;
            //string EndDate = enddate;

            using (var client = new HttpClient())
            {

                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.OfficialResidentReportAPI + "?EmployeePositionID=" + EmployeePositionID.ToString() + "&DateFrom=" + DateFrom.ToString() + "&DateTo=" + DateTo.ToString());

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<List<ReportOfficialResident>>(APIResponse);



                    for (int i = 0; i < objInfo.Count; i++)
                    {
                        ws.Cells[i + 1, 0] = objInfo[i].EmployeePositionName;
                        ws.Cells[i + 1, 1] = objInfo[i].Address;
                        ws.Cells[i + 1, 2] = objInfo[i].CityName;
                        ws.Cells[i + 1, 3] = objInfo[i].LandArea;
                        ws.Cells[i + 1, 4] = objInfo[i].BuildingArea;
                        ws.Cells[i + 1, 5] = objInfo[i].ClassName;
                        ws.Cells[i + 1, 6] = objInfo[i].ResidenceConditionName;
                        ws.Cells[i + 1, 7] = objInfo[i].NameOfResident;
                        ws.Cells[i + 1, 8] = objInfo[i].NIP;
                        ws.Cells[i + 1, 9] = objInfo[i].DateFrom;
                        ws.Cells[i + 1, 10] = objInfo[i].DateTo;
                        ws.Cells[i + 1, 11] = objInfo[i].DocFileName;
                        ws.Cells[i + 1, 12] = objInfo[i].OrderInformation;
                        ws.Cells[i + 1, 13] = objInfo[i].EmptyingInformation;
                        ws.Cells[i + 1, 14] = objInfo[i].Status;

                    }
                    wb.Add(ws);
                    wb.Save(fullPath);
                    filename = MakeValidFileName(filename);




                }

            }

            return File(fullPath, "application/vnd.ms-excel", filename);
            var fileStream = new MemoryStream();
            wb.Save(fileStream, Simplexcel.CompressionLevel.NoCompression);



        }
    }
}