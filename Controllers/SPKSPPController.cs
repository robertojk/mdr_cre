﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using MandiriCRE.Models;
using MandiriCRE.ViewModel;
using System.IO;
using System.Data.SqlClient;
using System.Data;

namespace MandiriCRE.Controllers
{
    public class SPKSPPController : Controller
    {
        // GET: SPKSPP
        string Baseurl = Properties.Settings.Default.BaseURI;

        public ActionResult Index1()
        {
            return View();
        }

        public async Task<ActionResult> Index()
        {
            List<SPKSPP> objInfo = new List<SPKSPP>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.SPKSPPAPI);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<List<SPKSPP>>(APIResponse);

                }
                //returning the employee list to view  
                return View(objInfo);
            }
        }

        public FileResult Downlod(string id,string doc)
        {
            string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
             using (SqlConnection conn = new SqlConnection(connectionstring))
            {
                using (var cmd = new SqlCommand("sp_UpdatePrintStatus", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@AgreementDocID", id);
                    conn.Open();
                    int k = cmd.ExecuteNonQuery();
                    if (k != 0)
                    {
                        //sukses
                    }
                    conn.Close();
                }
            }
            //byte[] fileBytes = System.IO.File.ReadAllBytes(@"~/content/document/agreement/"+doc);
            //string fileName =doc;
            //return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
            // return "~/content/document/agreement/"+doc;
            string path= "~/content/document/perjanjian/"+ doc;
            return File(path, "multipart/form-data",doc);
        }
    }
}