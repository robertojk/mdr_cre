﻿#region Using

using System;
using System.Data.Entity.Validation;
using System.Linq;
using System.Security.Principal;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Security;
using Microsoft.Ajax.Utilities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using MandiriCRE.Models;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using MandiriCRE.ViewModel;
using System.Web;
using System.Web.Configuration;
using System.Net;

#endregion

namespace MandiriCRE.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        // TODO: This should be moved to the constructor of the controller in combination with a DependencyResolver setup
        // NOTE: You can use NuGet to find a strategy for the various IoC packages out there (i.e. StructureMap.MVC5)
        private readonly UserManager _manager = UserManager.Create();
        string Baseurl = Properties.Settings.Default.BaseURI;
        private Task _signInManager;


        // GET: /account/forgotpassword
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            // We do not want to use any existing identity information
            EnsureLoggedOut();

            return View();
        }

        // GET: /account/login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl,string message)
        {
            // We do not want to use any existing identity information
            EnsureLoggedOut();

            // Store the originating URL so we can attach it to a form field
            var viewModel = new AccountLoginModel { ReturnUrl = returnUrl };
            ViewBag.Message = message;

            return View(viewModel);
        }

        // POST: /account/login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(AccountLoginModel viewModel)
        {
            // Ensure we have a valid viewModel to work with
            //if (!ModelState.IsValid)
              //  return View(viewModel);

            // Verify if a user exists with the provided identity information
            //var user = await _manager.FindByEmailAsync(viewModel.Email);
            var user = await _manager.FindByNameAsync(viewModel.Username);

            // If a user was found
            if (user != null)
            {
                var userVal = await _manager.FindAsync(user.UserName, viewModel.Password);
                if (userVal != null)
                {
                    //check status
                    string status = _manager.CheckStatus(viewModel.Username.ToString());
                    
                    if (status=="Y" || status == "")
                    {
                        //Session["MasterPenomoran"] = null;
                        //get menu
                        #region cek menu
                        string CheckMasterPenomoran = _manager.defineMenu(user.UserName, "MasterPenomoran");
                        if (CheckMasterPenomoran != null)
                        {
                            Session["MasterPenomoran"] = "Y";
                        }
                        else
                        {
                            Session["MasterPenomoran"] = null;
                        }
                        string CheckMasterRekanan = _manager.defineMenu(user.UserName, "MasterRekanan");
                        if (CheckMasterRekanan != null)
                        {
                            Session["MasterRekanan"] = "Y";
                        }
                        else
                        {
                            Session["MasterRekanan"] = null;
                        }
                        string CheckMasterTemplate = _manager.defineMenu(user.UserName, "MasterTemplate");
                        if (CheckMasterTemplate != null)
                        {
                            Session["MasterTemplate"] = "Y";
                        }
                        else
                        {
                            Session["MasterTemplate"] = null;
                        }
                        string CheckMasterFotoBeranda = _manager.defineMenu(user.UserName, "MasterFotoBeranda");
                        if (CheckMasterFotoBeranda != null)
                        {
                            Session["MasterFotoBeranda"] = "Y";
                        }
                        else
                        {
                            Session["MasterFotoBeranda"] = null;
                        }
                        string CheckMasterArtikel = _manager.defineMenu(user.UserName, "MasterArtikel");
                        if (CheckMasterArtikel != null)
                        {
                            Session["MasterArtikel"] = "Y";
                        }
                        else
                        {
                            Session["MasterArtikel"] = null;
                        }
                        string CheckMasterUser = _manager.defineMenu(user.UserName, "MasterUser");
                        if (CheckMasterUser != null)
                        {
                            Session["MasterUser"] = "Y";
                        }
                        else
                        {
                            Session["MasterUser"] = null;
                        }
                        string CheckPengadaan = _manager.defineMenu(user.UserName, "Pengadaan");
                        if (CheckPengadaan != null)
                        {
                            Session["Pengadaan"] = "Y";
                        }
                        else
                        {
                            Session["Pengadaan"] = null;
                        }
                        string CheckPerjanjian = _manager.defineMenu(user.UserName, "Perjanjian");
                        if (CheckPerjanjian != null)
                        {
                            Session["Perjanjian"] = "Y";
                        }
                        else
                        {
                            Session["Perjanjian"] = null;
                        }
                        string CheckSPKSPP = _manager.defineMenu(user.UserName, "SPKSPP");
                        if (CheckSPKSPP != null)
                        {
                            Session["SPKSPP"] = "Y";
                        }
                        else
                        {
                            Session["SPKSPP"] = null;
                        }
                        string CheckAddendum = _manager.defineMenu(user.UserName, "Addendum");
                        if (CheckAddendum != null)
                        {
                            Session["Addendum"] = "Y";
                        }
                        else
                        {
                            Session["Addendum"] = null;
                        }
                        string CheckTimeline = _manager.defineMenu(user.UserName, "Timeline");
                        if (CheckTimeline != null)
                        {
                            Session["Timeline"] = "Y";
                        }
                        else
                        {
                            Session["Timeline"] = null;
                        }
                        string CheckPayment = _manager.defineMenu(user.UserName, "Payment");
                        if (CheckPayment != null)
                        {
                            Session["Payment"] = "Y";
                        }
                        else
                        {
                            Session["Payment"] = null;
                        }
                        string CheckLaporanRincianPerjanjian = _manager.defineMenu(user.UserName, "LaporanRincianPerjanjian");
                        if (CheckLaporanRincianPerjanjian != null)
                        {
                            Session["LaporanRincianPerjanjian"] = "Y";
                        }
                        else
                        {
                            Session["LaporanRincianPerjanjian"] = null;
                        }
                        string CheckLaporanRekapReviewPerjanjian = _manager.defineMenu(user.UserName, "LaporanRekapReviewPerjanjian");
                        if (CheckLaporanRekapReviewPerjanjian != null)
                        {
                            Session["LaporanRekapReviewPerjanjian"] = "Y";
                        }
                        else
                        {
                            Session["LaporanRekapReviewPerjanjian"] = null;
                        }
                        string CheckLaporanKPIGroup = _manager.defineMenu(user.UserName, "LaporanKPIGroup");
                        if (CheckLaporanKPIGroup != null)
                        {
                            Session["LaporanKPIGroup"] = "Y";
                        }
                        else
                        {
                            Session["LaporanKPIGroup"] = null;
                        }
                        string CheckLaporanPTO = _manager.defineMenu(user.UserName, "LaporanPTO");
                        if (CheckLaporanPTO != null)
                        {
                            Session["LaporanPTO"] = "Y";
                        }
                        else
                        {
                            Session["LaporanPTO"] = null;
                        }
                        string CheckLaporanCeloxis = _manager.defineMenu(user.UserName, "LaporanCeloxis");
                        if (CheckLaporanCeloxis != null)
                        {
                            Session["LaporanCeloxis"] = "Y";
                        }
                        else
                        {
                            Session["LaporanCeloxis"] = null;
                        }
                        string CheckLaporanOpex = _manager.defineMenu(user.UserName, "LaporanOpex");
                        if (CheckLaporanOpex != null)
                        {
                            Session["LaporanOpex"] = "Y";
                        }
                        else
                        {
                            Session["LaporanOpex"] = null;
                        }
                        string CheckLaporanCapex = _manager.defineMenu(user.UserName, "LaporanCapex");
                        if (CheckLaporanCapex != null)
                        {
                            Session["LaporanCapex"] = "Y";
                        }
                        else
                        {
                            Session["LaporanCapex"] = null;
                        }
                        string CheckLaporanSPOATBL = _manager.defineMenu(user.UserName, "LaporanSPOATBL");
                        if (CheckLaporanSPOATBL != null)
                        {
                            Session["LaporanSPOATBL"] = "Y";
                        }
                        else
                        {
                            Session["LaporanSPOATBL"] = null;
                        }
                        string CheckPropertyManagement = _manager.defineMenu(user.UserName, "PropertyManagement");
                        if (CheckPropertyManagement != null)
                        {
                            Session["PropertyManagement"] = "Y";
                        }
                        else
                        {
                            Session["PropertyManagement"] = null;
                        }
                        string CheckRealEstateDelivery = _manager.defineMenu(user.UserName, "RealEstateDelivery");
                        if (CheckRealEstateDelivery != null)
                        {
                            Session["RealEstateDelivery"] = "Y";
                        }
                        else
                        {
                            Session["RealEstateDelivery"] = null;
                        }
                        string CheckRealEstateDocument = _manager.defineMenu(user.UserName, "RealEstateDocument");
                        if (CheckRealEstateDocument != null)
                        {
                            Session["RealEstateDocument"] = "Y";
                        }
                        else
                        {
                            Session["RealEstateDocument"] = null;
                        }
                        string CheckPersuratan = _manager.defineMenu(user.UserName, "Persuratan");
                        if (CheckPersuratan != null)
                        {
                            Session["Persuratan"] = "Y";
                        }
                        else
                        {
                            Session["Persuratan"] = null;
                        }
                        //new
                        string CheckPBB = _manager.defineMenu(user.UserName, "PBB");
                        if (CheckPBB != null)
                        {
                            Session["PBB"] = "Y";
                        }
                        else
                        {
                            Session["PBB"] = null;
                        }
                        string CheckATTB = _manager.defineMenu(user.UserName, "ATTB");
                        if (CheckATTB != null)
                        {
                            Session["ATTB"] = "Y";
                        }
                        else
                        {
                            Session["ATTB"] = null;
                        }
                        string CheckMasterPenghuni = _manager.defineMenu(user.UserName, "MasterPenghuni");
                        if (CheckMasterPenghuni != null)
                        {
                            Session["MasterPenghuni"] = "Y";
                        }
                        else
                        {
                            Session["MasterPenghuni"] = null;
                        }
                        string CheckUtilitasGedung = _manager.defineMenu(user.UserName, "UtilitasGedung");
                        if (CheckUtilitasGedung != null)
                        {
                            Session["UtilitasGedung"] = "Y";
                        }
                        else
                        {
                            Session["UtilitasGedung"] = null;
                        }
                        string CheckViewGedung = _manager.defineMenu(user.UserName, "ViewGedung");
                        if (CheckViewGedung != null)
                        {
                            Session["ViewGedung"] = "Y";
                        }
                        else
                        {
                            Session["ViewGedung"] = null;
                        }
                        string CheckLaporanUtilitas = _manager.defineMenu(user.UserName, "LaporanUtilitas");
                        if (CheckLaporanUtilitas != null)
                        {
                            Session["LaporanUtilitas"] = "Y";
                        }
                        else
                        {
                            Session["LaporanUtilitas"] = null;
                        }
                        string CheckMasterResident = _manager.defineMenu(user.UserName, "MasterResident");
                        if (CheckMasterResident != null)
                        {
                            Session["MasterResident"] = "Y";
                        }
                        else
                        {
                            Session["MasterResident"] = null;
                        }
                        string CheckLaporanResident = _manager.defineMenu(user.UserName, "LaporanResident");
                        if (CheckLaporanResident != null)
                        {
                            Session["LaporanResident"] = "Y";
                        }
                        else
                        {
                            Session["LaporanResident"] = null;
                        }
                        string CheckPemesananRumahDinas = _manager.defineMenu(user.UserName, "PemesananRumahDinas");
                        if (CheckPemesananRumahDinas != null)
                        {
                            Session["PemesananRumahDinas"] = "Y";
                        }
                        else
                        {
                            Session["PemesananRumahDinas"] = null;
                        }
                        string CheckLaporanRumahDinas = _manager.defineMenu(user.UserName, "LaporanRumahDinas");
                        if (CheckLaporanRumahDinas != null)
                        {
                            Session["LaporanRumahDinas"] = "Y";
                        }
                        else
                        {
                            Session["LaporanRumahDinas"] = null;
                        }
                        #endregion
                        #region cek allaccess
                        string CheckAccess = _manager.defineAccess(user.UserName);
                        Session["Access"] = CheckAccess;
                        Session["userName"] = viewModel.Username.ToString();
                        #endregion
                        // Then create an identity for it and sign it in
                        await SignInAsync(user,false);

                        // If the user came from a specific page, redirect back to it
                        return RedirectToLocal(viewModel.ReturnUrl);
                    }
                    else
                    {
                        return RedirectToAction("Login", "Account", new { message = "Login Gagal, User Sudah Tidak Aktif" });
                    }

                    
                }
                
            }

            // No existing user was found that matched the given criteria
            ModelState.AddModelError("", "Username atau Password tidak valid.");

            // If we got this far, something failed, redisplay form
            return View(viewModel);
        }

        // GET: /account/error
        [AllowAnonymous]
        public ActionResult Error()
        {
            // We do not want to use any existing identity information
            EnsureLoggedOut();

            return View();
        }

        // GET: /account/register
        [AllowAnonymous]
        public async Task<ActionResult> Register()
        {
            AccountRegistrationModel obj = new AccountRegistrationModel();
            // We do not want to use any existing identity information
            EnsureLoggedOut();
            obj._listTeam = await getTeamList();
            return View(obj);
        }

        // POST: /account/register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(AccountRegistrationModel viewModel, FormCollection collection)
        {
            // Ensure we have a valid viewModel to work with
            viewModel._listTeam = await getTeamList();
            if (!ModelState.IsValid)
                return View(viewModel);

            // Prepare the identity with the provided information
            var user = new IdentityUser
            {
                UserName = viewModel.Username ?? viewModel.Email,
                Email = viewModel.Email
            };

            // Try to create a user with the given identity
            try
            {
                var result = await _manager.CreateAsync(user, viewModel.Password);
              
                // If the user could not be created
                if (!result.Succeeded) {
                    // Add all errors to the page so they can be used to display what went wrong
                    AddErrors(result);

                    return View(viewModel);
                }

                //update team
                //viewModel.Team =   collection["_user.Team_TeamID"].ToString().Replace(",","");
                viewModel.Team = collection["TeamHidden"].ToString();
                if (viewModel.Team != null && viewModel.Team != "0")
                {
                    UserManager.UpdateTeam(viewModel.Username, viewModel.Team);
                }

                //get menu
                #region cek menu
                string CheckMasterPenomoran = _manager.defineMenu(user.UserName, "MasterPenomoran");
                if (CheckMasterPenomoran != null)
                {
                    Session["MasterPenomoran"] = "Y";
                }
                else
                {
                    Session["MasterPenomoran"] = null;
                }
                string CheckMasterRekanan = _manager.defineMenu(user.UserName, "MasterRekanan");
                if (CheckMasterRekanan != null)
                {
                    Session["MasterRekanan"] = "Y";
                }
                else
                {
                    Session["MasterRekanan"] = null;
                }
                string CheckMasterTemplate = _manager.defineMenu(user.UserName, "MasterTemplate");
                if (CheckMasterTemplate != null)
                {
                    Session["MasterTemplate"] = "Y";
                }
                else
                {
                    Session["MasterTemplate"] = null;
                }
                string CheckMasterFotoBeranda = _manager.defineMenu(user.UserName, "MasterFotoBeranda");
                if (CheckMasterFotoBeranda != null)
                {
                    Session["MasterFotoBeranda"] = "Y";
                }
                else
                {
                    Session["MasterFotoBeranda"] = null;
                }
                string CheckMasterArtikel = _manager.defineMenu(user.UserName, "MasterArtikel");
                if (CheckMasterArtikel != null)
                {
                    Session["MasterArtikel"] = "Y";
                }
                else
                {
                    Session["MasterArtikel"] = null;
                }
                string CheckMasterUser = _manager.defineMenu(user.UserName, "MasterUser");
                if (CheckMasterUser != null)
                {
                    Session["MasterUser"] = "Y";
                }
                else
                {
                    Session["MasterUser"] = null;
                }
                string CheckPengadaan = _manager.defineMenu(user.UserName, "Pengadaan");
                if (CheckPengadaan != null)
                {
                    Session["Pengadaan"] = "Y";
                }
                else
                {
                    Session["Pengadaan"] = null;
                }
                string CheckPerjanjian = _manager.defineMenu(user.UserName, "Perjanjian");
                if (CheckPerjanjian != null)
                {
                    Session["Perjanjian"] = "Y";
                }
                else
                {
                    Session["Perjanjian"] = null;
                }
                string CheckSPKSPP = _manager.defineMenu(user.UserName, "SPKSPP");
                if (CheckSPKSPP != null)
                {
                    Session["SPKSPP"] = "Y";
                }
                else
                {
                    Session["SPKSPP"] = null;
                }
                string CheckAddendum = _manager.defineMenu(user.UserName, "Addendum");
                if (CheckAddendum != null)
                {
                    Session["Addendum"] = "Y";
                }
                else
                {
                    Session["Addendum"] = null;
                }
                string CheckTimeline = _manager.defineMenu(user.UserName, "Timeline");
                if (CheckTimeline != null)
                {
                    Session["Timeline"] = "Y";
                }
                else
                {
                    Session["Timeline"] = null;
                }
                string CheckPayment = _manager.defineMenu(user.UserName, "Payment");
                if (CheckPayment != null)
                {
                    Session["Payment"] = "Y";
                }
                else
                {
                    Session["Payment"] = null;
                }
                string CheckLaporanRincianPerjanjian = _manager.defineMenu(user.UserName, "LaporanRincianPerjanjian");
                if (CheckLaporanRincianPerjanjian != null)
                {
                    Session["LaporanRincianPerjanjian"] = "Y";
                }
                else
                {
                    Session["LaporanRincianPerjanjian"] = null;
                }
                string CheckLaporanRekapReviewPerjanjian = _manager.defineMenu(user.UserName, "LaporanRekapReviewPerjanjian");
                if (CheckLaporanRekapReviewPerjanjian != null)
                {
                    Session["LaporanRekapReviewPerjanjian"] = "Y";
                }
                else
                {
                    Session["LaporanRekapReviewPerjanjian"] = null;
                }
                string CheckLaporanKPIGroup = _manager.defineMenu(user.UserName, "LaporanKPIGroup");
                if (CheckLaporanKPIGroup != null)
                {
                    Session["LaporanKPIGroup"] = "Y";
                }
                else
                {
                    Session["LaporanKPIGroup"] = null;
                }
                string CheckLaporanPTO = _manager.defineMenu(user.UserName, "LaporanPTO");
                if (CheckLaporanPTO != null)
                {
                    Session["LaporanPTO"] = "Y";
                }
                else
                {
                    Session["LaporanPTO"] = null;
                }
                string CheckLaporanCeloxis = _manager.defineMenu(user.UserName, "LaporanCeloxis");
                if (CheckLaporanCeloxis != null)
                {
                    Session["LaporanCeloxis"] = "Y";
                }
                else
                {
                    Session["LaporanCeloxis"] = null;
                }
                string CheckLaporanOpex = _manager.defineMenu(user.UserName, "LaporanOpex");
                if (CheckLaporanOpex != null)
                {
                    Session["LaporanOpex"] = "Y";
                }
                else
                {
                    Session["LaporanOpex"] = null;
                }
                string CheckLaporanCapex = _manager.defineMenu(user.UserName, "LaporanCapex");
                if (CheckLaporanCapex != null)
                {
                    Session["LaporanCapex"] = "Y";
                }
                else
                {
                    Session["LaporanCapex"] = null;
                }
                string CheckLaporanSPOATBL = _manager.defineMenu(user.UserName, "LaporanSPOATBL");
                if (CheckLaporanSPOATBL != null)
                {
                    Session["LaporanSPOATBL"] = "Y";
                }
                else
                {
                    Session["LaporanSPOATBL"] = null;
                }
                string CheckPropertyManagement = _manager.defineMenu(user.UserName, "PropertyManagement");
                if (CheckPropertyManagement != null)
                {
                    Session["PropertyManagement"] = "Y";
                }
                else
                {
                    Session["PropertyManagement"] = null;
                }
                string CheckRealEstateDelivery = _manager.defineMenu(user.UserName, "RealEstateDelivery");
                if (CheckRealEstateDelivery != null)
                {
                    Session["RealEstateDelivery"] = "Y";
                }
                else
                {
                    Session["RealEstateDelivery"] = null;
                }
                string CheckRealEstateDocument = _manager.defineMenu(user.UserName, "RealEstateDocument");
                if (CheckRealEstateDocument != null)
                {
                    Session["RealEstateDocument"] = "Y";
                }
                else
                {
                    Session["RealEstateDocument"] = null;
                }
                string CheckPersuratan = _manager.defineMenu(user.UserName, "Persuratan");
                if (CheckPersuratan != null)
                {
                    Session["Persuratan"] = "Y";
                }
                else
                {
                    Session["Persuratan"] = null;
                }
                #endregion
                #region cek allaccess
                string CheckAccess = _manager.defineAccess(user.UserName);
                Session["Access"] = CheckAccess;
                #endregion

                // If the user was able to be created we can sign it in immediately
                // Note: Consider using the email verification proces
                //await SignInAsync(user, false);


                //return RedirectToLocal();
                return RedirectToAction("Login", "Account",new {message="Registrasi Berhasil, silahkan Lanjutkan untuk Login" });
            }
            catch (DbEntityValidationException ex)
            {
                // Add all errors to the page so they can be used to display what went wrong
                AddErrors(ex);

                return View(viewModel);
            }
        }
        
        // POST: /account/Logout
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Logout()
        {
            // First we clean the authentication ticket like always
            FormsAuthentication.SignOut();

            Session.Clear();
            Session.Abandon();

            Response.Cookies.Clear();
            // Clear authentication cookie
            HttpCookie cookie1 = new HttpCookie(FormsAuthentication.FormsCookieName, "");
            cookie1.Expires = DateTime.Now.AddYears(-1);
            Response.Cookies.Add(cookie1);

            //// Clear session cookie 
            HttpCookie rSessionCookie = new HttpCookie("ASP.NET_SessionId", "");
            rSessionCookie.Expires = DateTime.Now.AddYears(-1);
            Response.Cookies.Add(rSessionCookie);

            #region delete session
            Session["MasterPenomoran"] = null;
            Session["MasterRekanan"] = null;
            Session["MasterTemplate"] = null;
            Session["MasterFotoBeranda"] = null;
            Session["MasterArtikel"] = null;
            Session["MasterUser"] = null;
            Session["Pengadaan"] = null;
            Session["Perjanjian"] = null;
            Session["SPKSPP"] = null;
            Session["Addendum"] = null;
            Session["Timeline"] = null;
            Session["Payment"] = null;
            Session["LaporanRincianPerjanjian"] = null;
            Session["LaporanRekapReviewPerjanjian"] = null;
            Session["LaporanKPIGroup"] = null;
            Session["LaporanPTO"] = null;
            Session["LaporanCeloxis"] = null;
            Session["LaporanOpex"] = null;
            Session["LaporanCapex"] = null;
            Session["LaporanSPOATBL"] = null;
            Session["LaporanSPOProcurement"] = null;
            Session["PropertyManagement"] = null;
            Session["RealEstateDelivery"] = null;
            Session["RealEstateDocument"] = null;
            Session["Persuratan"] = null;
            #endregion
            // Second we clear the principal to ensure the user does not retain any authentication
            HttpContext.User = new GenericPrincipal(new GenericIdentity(string.Empty), null);

            // Last we redirect to a controller/action that requires authentication to ensure a redirect takes place
            // this clears the Request.IsAuthenticated flag since this triggers a new request
            return RedirectToLocal();
        }

        private ActionResult RedirectToLocal(string returnUrl = "")
        {
            // If the return url starts with a slash "/" we assume it belongs to our site
            // so we will redirect to this "action"
            if (!returnUrl.IsNullOrWhiteSpace() && Url.IsLocalUrl(returnUrl))
                return Redirect(returnUrl);

            // If we cannot verify if the url is local to our host we redirect to a default location
            return RedirectToAction("index", "beranda");
        }

        private void AddErrors(DbEntityValidationException exc)
        {
            foreach (var error in exc.EntityValidationErrors.SelectMany(validationErrors => validationErrors.ValidationErrors.Select(validationError => validationError.ErrorMessage)))
            {
                ModelState.AddModelError("", error);
            }
        }

        private void AddErrors(IdentityResult result)
        {
            // Add all errors that were returned to the page error collection
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private void EnsureLoggedOut()
        {
            // If the request is (still) marked as authenticated we send the user to the logout action
            if (Request.IsAuthenticated)
                Logout();
        }

        private async Task SignInAsync(IdentityUser user, bool isPersistent)
        {
            // Clear any lingering authencation data
            FormsAuthentication.SignOut();

            // Create a claims based identity for the current user
            var identity = await _manager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);

            // Write the authentication cookie
            FormsAuthentication.SetAuthCookie(identity.Name, isPersistent);
        }

        // GET: /account/lock
        [AllowAnonymous]
        public ActionResult Lock()
        {
            return View();
        }

        public async Task<List<Team>> getTeamList()
        {
            List<Team> retVal = new List<Team>();
            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllArticles using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.TeamAPI);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    retVal = JsonConvert.DeserializeObject<List<Team>>(APIResponse);

                }
                //returning the employee list to view  

            }

            return retVal;
        }

        //[HttpPost]
        //public ActionResult getTeamListbyDepartment(string department)
        //{

        //    List<Team> objTeam = new List<Team>();
        //    objTeam = _manager.getListTeam().Where(m => m.Department_DepartmentID == department).ToList();
        //    //objTeam = getTeamList().Where(m => m.StateId == stateid).ToList();
        //    SelectList obgTeam = new SelectList(objTeam, "TeamID", "TeamName", 0);
        //    return Json(obgTeam);
        //}

        

    }
}