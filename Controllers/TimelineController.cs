﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using MandiriCRE.Models;
using System.IO;
using System.Data.SqlClient;
using System.Data;

namespace MandiriCRE.Controllers
{
    public class TimelineController : Controller
    {
        string Baseurl = Properties.Settings.Default.BaseURI;
        // GET: Timeline
        public async Task<ActionResult> Index()
        {
            List<TimelineList> objInfo = new List<TimelineList>();
            objInfo = GetTimelineList();


            return View(objInfo);
        }

        public List<TimelineList> GetTimelineList()
        {
            List<TimelineList> _TimelineList = new List<TimelineList>();

            string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(connectionstring))
            {
                using (var cmd = new SqlCommand("sp_TimelineList", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    //cmd.Parameters.AddWithValue("@pMyParamater", myParamaterValue);
                    conn.Open();

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            TimelineList obj = new TimelineList();

                            obj.ProcurementID = Convert.ToInt32(reader[0]);
                            obj.ProcurementNo = reader[1].ToString();
                            obj.ProcurementName = reader[2].ToString();
                            obj.ProcurementDateStart = reader[3].ToString();
                            obj.ProcurementDateEnd = reader[4].ToString();
                            obj.VendorDetailID = Convert.ToInt32(reader[5]);
                            obj.ProcurementVendorID = Convert.ToInt32(reader[6]);
                            obj.ProcurementVendorName = reader[7].ToString();
                            obj.AssetID = reader[8].ToString();
                            obj.AgreementID = Convert.ToInt32(reader[9]);
                            obj.AgreementNo = reader[10].ToString();
                            obj.AgreementDateStart = reader[11].ToString();
                            obj.AgreementDateEnd = reader[12].ToString();
                            obj.AgreementVendorID = Convert.ToInt32(reader[13]);
                            obj.AgreementVendorName = reader[14].ToString();

                            _TimelineList.Add(obj);
                        }
                    }
                }

            }
            return _TimelineList;



        }

        public ActionResult Detail(int prcid,int id, string No, string Judul, string Aset, string Mulai, string Akhir, int vendordetailid)
        {
            //ViewBag.NoPerjanjian = No;
            //ViewBag.JudulPerjanjian = Judul;
            //ViewBag.IDAset = Aset;
            //ViewBag.TglMulai = Mulai;
            //ViewBag.TglAkhir = Akhir;

            TimelineList obj = new TimelineList();
            obj.ProcurementID = prcid;
            obj.AgreementID = id;
            obj.AgreementNo = No;
            obj.ProcurementName = Judul;
            obj.AssetID = Aset;
            obj.AgreementDateStart = Mulai;
            obj.AgreementDateEnd = Akhir;
            obj.VendorDetailID = vendordetailid;
            return View(obj);
        }

        public ActionResult DetailView(int prcid, int id, string No, string Judul, string Aset, string Mulai, string Akhir, int vendordetailid)
        {
            //ViewBag.NoPerjanjian = No;
            //ViewBag.JudulPerjanjian = Judul;
            //ViewBag.IDAset = Aset;
            //ViewBag.TglMulai = Mulai;
            //ViewBag.TglAkhir = Akhir;

            TimelineList obj = new TimelineList();
            obj.ProcurementID = prcid;
            obj.AgreementID = id;
            obj.AgreementNo = No;
            obj.ProcurementName = Judul;
            obj.AssetID = Aset;
            obj.AgreementDateStart = Mulai;
            obj.AgreementDateEnd = Akhir;
            obj.VendorDetailID = vendordetailid;
            return View(obj);
        }

        [HttpPost]
        public ActionResult DetailAset(int vendordetailid, FormCollection form, TimelineList obj)
        {

            try
            {
                obj.VendorDetailID = vendordetailid;
                obj.AssetID = form["idasset"].ToString();
                // TODO: Add update logic here
                string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    using (var cmd = new SqlCommand("sp_UpdateAsetID", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@VendorDetailID", obj.VendorDetailID);
                        cmd.Parameters.AddWithValue("@AssetID", obj.AssetID);
                        cmd.Parameters.AddWithValue("@UserName", User.Identity.Name);
                        conn.Open();
                        int k = cmd.ExecuteNonQuery();
                        if (k != 0)
                        {
                            //sukses
                        }
                        conn.Close();
                    }
                }
                return RedirectToAction("Index", "Timeline");
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }

        }

        public PartialViewResult PartialTaskDetail(int prcid)
        {
            //EXEC sp_ViewProcurementDetail @ProcurementID=?
            List<ProcurementDetail> objInfo = new List<ProcurementDetail>();

            //using (var client = new HttpClient())
            //{
            //    //Passing service base url  
            //    client.BaseAddress = new Uri(Baseurl);

            //    client.DefaultRequestHeaders.Clear();
            //    //Define request data format  
            //    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            //    //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
            //    HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.ProcurementDetailsAPI + "/" + id);

            //    //Checking the response is successful or not which is sent using HttpClient  
            //    if (Res.IsSuccessStatusCode)
            //    {
            //        //Storing the response details recieved from web api   
            //        var APIResponse = Res.Content.ReadAsStringAsync().Result;

            //        //Deserializing the response recieved from web api and storing into the Employee list  
            //        objInfo = JsonConvert.DeserializeObject<List<ProcurementDetail>>(APIResponse);

            //    }
            objInfo = GetProcurementDetails(prcid);
            ViewBag.ProcurementID = prcid;
            //returning the employee list to view  
            return PartialView(objInfo);
            //}
        }

        public List<ProcurementDetail> GetProcurementDetails(int id)
        {
            List<ProcurementDetail> _ProcurementDetail = new List<ProcurementDetail>();

            string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(connectionstring))
            {
                using (var cmd = new SqlCommand("sp_ViewProcurementDetail", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ProcurementID", id);
                    conn.Open();

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            ProcurementDetail obj = new ProcurementDetail();

                            obj.ProcurementID = reader[0].ToString();
                            obj.TaskNameDetail = reader[1].ToString();
                            obj.Price = reader[2].ToString();
                            obj.Negotiation= reader[6].ToString();

                            _ProcurementDetail.Add(obj);
                        }
                    }
                }

            }
            return _ProcurementDetail;
        }

        public PartialViewResult PartialTimelineList(int id,int prcid, string No, string Judul, string Aset, string Mulai, string Akhir, int vendordetailid)
        {
            //EXEC sp_viewTimelineDetail @VendorDetailID=1
            List<TimelineDetail> objInfo = new List<TimelineDetail>();

            //using (var client = new HttpClient())
            //{
            //    //Passing service base url  
            //    client.BaseAddress = new Uri(Baseurl);

            //    client.DefaultRequestHeaders.Clear();
            //    //Define request data format  
            //    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            //    //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
            //    HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.ProcurementDetailsAPI + "/" + id);

            //    //Checking the response is successful or not which is sent using HttpClient  
            //    if (Res.IsSuccessStatusCode)
            //    {
            //        //Storing the response details recieved from web api   
            //        var APIResponse = Res.Content.ReadAsStringAsync().Result;

            //        //Deserializing the response recieved from web api and storing into the Employee list  
            //        objInfo = JsonConvert.DeserializeObject<List<ProcurementDetail>>(APIResponse);

            //    }
            objInfo = GetTimelineDetails(vendordetailid);
            ViewBag.prcid = prcid;
            ViewBag.id = id;
            ViewBag.No = No;
            ViewBag.judul = Judul;
            ViewBag.Aset = Aset;
            ViewBag.Mulai = Mulai;
            ViewBag.Akhir = Akhir;
            ViewBag.VendorDetailID = vendordetailid;
            //returning the employee list to view  
            return PartialView(objInfo);
            //}
        }

        public List<TimelineDetail> GetTimelineDetails(int id)
        {
            List<TimelineDetail> _ProcurementDetail = new List<TimelineDetail>();

            string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(connectionstring))
            {
                using (var cmd = new SqlCommand("sp_viewTimelineDetail", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@VendorDetailID", id);
                    conn.Open();

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            TimelineDetail obj = new TimelineDetail();

                            obj.VendorTimelineID = Convert.ToInt32(reader[0]);
                            obj.Taskname = reader[1].ToString();
                            obj.PlanStartDate = reader[2].ToString();
                            obj.PlanEndDate = reader[3].ToString();
                            obj.DurationPlan = reader[4].ToString();
                            obj.ActualStartDate = reader[5].ToString();
                            obj.ActualEndDate = reader[6].ToString();
                            obj.DurationActual = reader[7].ToString();

                            _ProcurementDetail.Add(obj);
                        }
                    }
                }

            }
            return _ProcurementDetail;
        }

        public ActionResult CreateTimeline(int prcid,int id, string No, string Judul, string Aset, string Mulai, string Akhir, int vendordetailid)
        {
            TimelineDetail objInfo = new TimelineDetail();
            ViewBag.prcid = prcid;
            ViewBag.id = id;
            ViewBag.No = No;
            ViewBag.judul = Judul;
            ViewBag.Aset = Aset;
            ViewBag.Mulai = Mulai;
            ViewBag.Akhir = Akhir;
            ViewBag.VendorDetailID = vendordetailid;
            return View(objInfo);
        }

        [HttpPost]
        public ActionResult CreateTimeline(int prcid, int id, string No, string Judul, string Aset, string Mulai, string Akhir, int vendordetailid, TimelineDetail obj, FormCollection form)
        {
            try
            {
                obj.VendorDetailID = vendordetailid;
                obj.Taskname = form["taskNameDetail"].ToString();
                obj.PlanStartDate = form["planstart"].ToString();
                obj.PlanEndDate = form["planend"].ToString();
                obj.DurationPlan = form["durationplan"].ToString();
                obj.ActualStartDate = form["Actualstart"].ToString();
                obj.ActualEndDate = form["Actualend"].ToString();
                obj.DurationActual = form["durationActual"].ToString();
                // TODO: Add update logic here
                string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    using (var cmd = new SqlCommand("sp_InsertVendorTimeline", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@VendorDetailID", obj.VendorDetailID);
                        cmd.Parameters.AddWithValue("@TaskName", obj.Taskname);
                        cmd.Parameters.AddWithValue("@PlanStartDate", obj.PlanStartDate);
                        cmd.Parameters.AddWithValue("@PlanEndDate", obj.PlanEndDate);
                        cmd.Parameters.AddWithValue("@DurationPlan", obj.DurationPlan);
                        cmd.Parameters.AddWithValue("@ActualStartDate", obj.ActualStartDate);
                        cmd.Parameters.AddWithValue("@ActualEndDate", obj.ActualEndDate);
                        cmd.Parameters.AddWithValue("@DurationActual", obj.DurationActual);
                        cmd.Parameters.AddWithValue("@USERNAME", User.Identity.Name);
                        conn.Open();
                        int k = cmd.ExecuteNonQuery();
                        if (k != 0)
                        {
                            //sukses
                        }
                        conn.Close();
                    }
                }
                return RedirectToAction("Detail", "Timeline", new {prcid=prcid, id = id, No = No, Judul = Judul, Aset = Aset, Mulai = Mulai, Akhir = Akhir, vendordetailid = vendordetailid });
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }

        public PartialViewResult PartialTimelineListView(int id, int prcid, string No, string Judul, string Aset, string Mulai, string Akhir, int vendordetailid)
        {
            //EXEC sp_viewTimelineDetail @VendorDetailID=1
            List<TimelineDetail> objInfo = new List<TimelineDetail>();
            
            objInfo = GetTimelineDetails(vendordetailid);
            ViewBag.prcid = prcid;
            ViewBag.id = id;
            ViewBag.No = No;
            ViewBag.judul = Judul;
            ViewBag.Aset = Aset;
            ViewBag.Mulai = Mulai;
            ViewBag.Akhir = Akhir;
            ViewBag.VendorDetailID = vendordetailid;
            //returning the employee list to view  
            return PartialView(objInfo);
            //}
        }
    }
}