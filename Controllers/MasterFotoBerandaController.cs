﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using MandiriCRE.Models;
using System.IO;

namespace MandiriCRE.Controllers
{
    public class MasterFotoBerandaController : Controller
    {
        // GET: MasterFotoBeranda
        public ActionResult Index1()
        {
            return View();
        }
        string Baseurl = Properties.Settings.Default.BaseURI;
        public async Task<ActionResult> Index()
        {
            List<Photo> objInfo = new List<Photo>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.FotoBerandaAPI);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<List<Photo>>(APIResponse);

                }
                //returning the employee list to view 
                ViewBag.Total = 0;
                foreach (Photo varphoto in objInfo)
                {
                    ViewBag.Total = varphoto.Total;
                }
                return View(objInfo);
            }
        }
        // GET: CRUD/Details/5
        public async Task<ActionResult> Details(int id)
        {
            Photo objInfo = new Photo();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.FotoBerandaAPI + "/" + id);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<Photo>(APIResponse);

                }
                //returning the employee list to view  
                return View(objInfo);
            }
        }

        // GET: CRUD/Create
        public ActionResult Create(string ErrorMessage,int Total)
        {
            Photo objInfo = new Photo();
            if (ErrorMessage != null)
            {
                ViewBag.ErrorMessage = ErrorMessage;
            }
            ViewBag.Total = Total;
            return View(objInfo);
        }

        // POST: CRUD/Create
        [HttpPost]
        public async Task<ActionResult> Create(Photo obj,int Total, HttpPostedFileBase file)
        {
            string filePath = Properties.Settings.Default.UploadFilePath;
            try
            {
                //var file = Request.Files[0];
                string ErrorMessage = "";
                string fileName = "";
                string path = "";
                string fileType = "";
                if (Total < 6)
                {
                    if (file != null && file.ContentLength > 0)
                    {
                        fileType = Path.GetExtension(file.FileName);
                        if (fileType.ToUpper().Contains("JPG") || fileType.ToUpper().Contains("PNG"))
                        {
                            if (file.ContentLength <= (1000 * 1024))
                            {
                                fileName = Path.GetFileName(file.FileName);
                                path = Path.Combine(Server.MapPath("~/content/img/slider/"), fileName);
                                file.SaveAs(path);
                            }
                            else
                            {
                                ErrorMessage = "File Upload tidak boleh lebih dari 1 MB";
                                return RedirectToAction("Create", new {Total=Total, ErrorMessage = ErrorMessage });
                            }

                        }
                        else
                        {
                            ErrorMessage = "File Upload harus dengan format .jpg atau .png";
                            return RedirectToAction("Create", new { Total = Total, ErrorMessage = ErrorMessage });
                        }
                    }
                }
                else
                {
                    ErrorMessage = "Penambahan gagal, jumlah foto maksimal adalah 6";
                    return RedirectToAction("Create", new { Total = Total, ErrorMessage = ErrorMessage });
                }

                // TODO: Add update logic here
                obj.DocumentName = fileName;
                obj.FolderName = "img";
                obj.Name = fileName;
                obj.Path = path;
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                // TODO: Add update logic here
                HttpResponseMessage response = await client.PostAsJsonAsync(Properties.Settings.Default.FotoBerandaAPI, obj);
                //response.EnsureSuccessStatusCode();
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }

        // GET: CRUD/Edit/5
        public async Task<ActionResult> Edit(int id)
        {
            Photo objInfo = new Photo();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.FotoBerandaAPI + "/" + id);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<Photo>(APIResponse);

                }
                //returning the employee list to view  
                return View(objInfo);
            }
        }

        // POST: CRUD/Edit/5
        [HttpPost]
        public async Task<ActionResult> Edit(int id, Photo obj)
        {
            try
            {
                // TODO: Add update logic here
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                // TODO: Add update logic here
                HttpResponseMessage response = await client.PutAsJsonAsync(Properties.Settings.Default.FotoBerandaAPI + "/" + id, obj);
                //response.EnsureSuccessStatusCode();
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }

        // GET: CRUD/Delete/5
        public async Task<ActionResult> Delete(int id)
        {
            Photo objInfo = new Photo();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.FotoBerandaAPI + "/" + id);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<Photo>(APIResponse);

                }
                //returning the employee list to view  
                return View(objInfo);
            }
        }

        // POST: CRUD/Delete/5
        [HttpPost]
        public async Task<ActionResult> Delete(int id, Photo obj)
        {

                // TODO: Add delete logic here
                try
                {
                    // TODO: Add update logic here
                    HttpClient client = new HttpClient();
                    client.BaseAddress = new Uri(Baseurl);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    // TODO: Add update logic here
                    HttpResponseMessage response = await client.DeleteAsync(Properties.Settings.Default.FotoBerandaAPI + "/" + id);
                    response.EnsureSuccessStatusCode();
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    string err = ex.Message.ToString();
                    return View();
                }
                //return RedirectToAction("Index");

        }
    }
}
