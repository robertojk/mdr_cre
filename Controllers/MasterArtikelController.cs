﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using MandiriCRE.Models;
using MandiriCRE.Properties;
using System.Configuration;

namespace MandiriCRE.Controllers
{
    public class MasterArtikelController : Controller
    {
        // GET: MasterArtikel
        public ActionResult Index1()
        {
            return View();
        }
        string Baseurl = Properties.Settings.Default.BaseURI;
        public async Task<ActionResult> Index()
        {
            List<Articles> objInfo = new List<Articles>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.ArticleAPI);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<List<Articles>>(APIResponse);

                }
                //returning the employee list to view  
                return View(objInfo);
            }
        }
        // GET: MasterArtikel/Details/5
        public async Task<ActionResult> Details(int id)
        {
            Articles objInfo = new Articles();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllArticles using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.ArticleAPI + "/" + id);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<Articles>(APIResponse);

                }
                //returning the employee list to view  
                return View(objInfo);
            }
        }

        // GET: MasterArtikel/Create
        public ActionResult Create()
        {
            Articles objInfo = new Articles();
            return View(objInfo);
        }

        // POST: MasterArtikel/Create
        [HttpPost]
        public async Task<ActionResult> Create(Articles obj, FormCollection form)
        {
            try
            {
                obj.Headline = form["headline"].ToString();
                obj.Content = form["content"].ToString();
                obj.Position =  form["position"].ToString();
                obj.ValidFrom = form["startdate"].ToString();
                obj.ValidTo = form["finishdate"].ToString();
                obj.UpdatedBy = User.Identity.Name;
                obj.UpdatedDate = DateTime.Now.ToString("dd/MM/yyyy");
                obj.CreatedBy = User.Identity.Name;
                obj.CreatedDate = DateTime.Now.ToString("dd/MM/yyyy");
                obj.UpdateBy = User.Identity.Name;
                obj.CreateBy = User.Identity.Name;
                // TODO: Add update logic here
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                // TODO: Add update logic here
                HttpResponseMessage response = await client.PostAsJsonAsync(Properties.Settings.Default.ArticleAPI, obj);
                response.EnsureSuccessStatusCode();
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }

        // GET: MasterArtikel/Edit/5
        public async Task<ActionResult> Edit(int id)
        {
            Articles objInfo = new Articles();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllArticles using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.ArticleAPI + "/" + id);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<Articles>(APIResponse);

                }
                //returning the employee list to view  
                return View(objInfo);
            }
        }

        // POST: MasterArtikel/Edit/5
        [HttpPost]
        public async Task<ActionResult> Edit(int id, Articles obj, FormCollection form)
        {
            try
            {
                obj.Headline = form["headline"].ToString();
                obj.Content = form["content"].ToString();
                obj.Position =  form["position"].ToString();
                obj.ValidFrom = form["startdate"].ToString();
                obj.ValidTo = form["finishdate"].ToString();
                obj.UpdatedBy = User.Identity.Name;
                obj.UpdatedDate = DateTime.Now.ToString("dd/MM/yyyy");
                obj.NewsID = id.ToString();
                obj.UpdateBy = User.Identity.Name;
                // TODO: Add update logic here
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                // TODO: Add update logic here
                HttpResponseMessage response = await client.PutAsJsonAsync(Properties.Settings.Default.ArticleAPI + "/" + id, obj);
                response.EnsureSuccessStatusCode();
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }

        // GET: MasterArtikel/Delete/5
        public async Task<ActionResult> Delete(int id)
        {
            Articles objInfo = new Articles();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllArticles using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.ArticleAPI + "/" + id);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<Articles>(APIResponse);

                }
                //returning the employee list to view  
                return View(objInfo);
            }
        }

        // POST: MasterArtikel/Delete/5
        [HttpPost]
        public async Task<ActionResult> Delete(int id, Articles obj)
        {
            // TODO: Add delete logic here
            try
            {
                // TODO: Add update logic here
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                // TODO: Add update logic here
                HttpResponseMessage response = await client.DeleteAsync(Properties.Settings.Default.ArticleAPI + "/" + id);
                response.EnsureSuccessStatusCode();
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }
    }
}
