﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using MandiriCRE.Models;
using System.IO;
using System.Data.SqlClient;
using System.Data;
using MandiriCRE.ViewModel;
using System.Web.UI;
using Simplexcel;
using System.Text.RegularExpressions;

namespace MandiriCRE.Controllers
{
    public class BuildingUtilityController : Controller
    {
        string Baseurl = Properties.Settings.Default.BaseURI;
        string Baseurl2 = Properties.Settings.Default.BaseURI;
        // GET: Building Utility
        public async Task<ActionResult> Index()
        {
            var model = new BuildingUtilityIndexViewModel();

            model.InitializePermissions(
                Session,
                "20"
            );

            model.BuildingUtilityJson = await JsonDownloader.GetJsonOrDefaultAsync(
                Baseurl2,
                Properties.Settings.Default.BuildingUtilityAPI,
                EmptyJsonDefault.EmptyArray
            );

            return View(model);
        }

        public async Task<ActionResult> Create(string ErrorMessage, BuildingUtility objBuilding)
        {
            if (ErrorMessage != null)
            {
                ViewBag.ErrorMessage = ErrorMessage;
            }

            List<Building> _listBuilding = await GetListBuilding();
            ViewBag.ListBuilding = _listBuilding;

            List<URCCode> _listURCCode = await GetListURCCode();
            ViewBag.ListURCCode = _listURCCode;

            List<Subject> _listSubject = await GetListSubject();
            ViewBag.ListSubject = _listSubject;

            BuildingUtility objInfo = new BuildingUtility();
            if (objBuilding != null)
            {
                objInfo = objBuilding;
            }
            if (ErrorMessage != null)
            {
                ViewBag.ErrorMessage = ErrorMessage;
            }
            return View(objInfo);
        }

        // POST: BuildingOccupants/Create
        [HttpPost]
        //public async Task<ActionResult> Create(string id, string WF, ProcurementHeader obj, FormCollection collection)
        public async Task<ActionResult> Create(BuildingUtility objBuilding, FormCollection collection, HttpPostedFileBase file)
        {
            try
            {
                BuildingUtility obj = new BuildingUtility();

                obj.BuildingID = collection["BuildingID"].ToString();
                obj.YearID = collection["YearID"].ToString();
                obj.MonthID = collection["MonthID"].ToString();
                obj.SubjectID = collection["SubjectID"].ToString();
                obj.URCCodeID = ""; //req 231018
                obj.URCCodeDesc = ""; //req 231018
                if (collection["URCCodeID"] != null && collection["URCCodeID"] != "")
                {
                    obj.URCCodeID = collection["URCCodeID"].ToString();
                    obj.URCCodeDesc = collection["URCCodeDesc"].ToString();
                }
                obj.Invoice = collection["Invoice"].ToString();
                obj.SetOff = "";
                if (collection["SetOff"] != null && collection["SetOff"] != "")
                {
                    obj.SetOff = collection["SetOff"].ToString();
                }
                obj.ActionSave = "INSERT";
                obj.BuildingUtilityID = "0";
                obj.UserName = User.Identity.Name;

                string ErrorMessage = "";
                string fileName = "";
                string path = "";
                string fileType = "";

                /*DOK 1*/
                if (file == null && (collection["DocumentName"].ToString() != null && collection["DocumentName"].ToString() != ""))
                {
                    fileName = collection["DocumentName"].ToString();
                    obj.DocFileName = fileName;
                    path = Path.Combine(Server.MapPath("~/content/document/buildingutility/"), fileName);
                }
                //Upload File
                /*DOK 1*/
                if (file != null && file.ContentLength > 0)
                {
                    fileType = Path.GetExtension(file.FileName);
                    if (fileType.ToUpper().Contains("PDF"))
                    {
                        if (file.ContentLength <= (5000 * 1024))
                        {
                            fileName = Path.GetFileName(file.FileName);
                            path = Path.Combine(Server.MapPath("~/content/document/buildingutility/"), fileName);
                            file.SaveAs(path);

                            //insert file data
                            obj.DocFileName = fileName;
                            obj.FolderName = "buildingutility";
                            obj.Path = path;
                        }
                        else
                        {
                            ErrorMessage = "File Upload tidak boleh lebih dari 5 MB";
                            return await Create(ErrorMessage, obj);
                        }

                    }
                    else
                    {
                        ErrorMessage = "File Upload harus dengan format PDF";
                        return await Create(ErrorMessage, obj);
                    }
                }


                #region cekduplicate
                BuildingOccupants obj2 = new BuildingOccupants();

                string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    using (var cmd = new SqlCommand("sp_CheckDuplicateBuildingUtility", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@BuildingUtilityID", 0);
                        cmd.Parameters.AddWithValue("@BuildingID", obj.BuildingID);
                        cmd.Parameters.AddWithValue("@YearID", obj.YearID);
                        cmd.Parameters.AddWithValue("@MonthID", obj.MonthID);
                        cmd.Parameters.AddWithValue("@URCCodeID", obj.URCCodeID);
                        cmd.Parameters.AddWithValue("@SubjectID", obj.SubjectID);
                        cmd.Parameters.AddWithValue("@Invoice", obj.Invoice);
                        cmd.Parameters.AddWithValue("@SetOff", obj.SetOff);
                        conn.Open();

                        using (var reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                obj2.BuildingOccupantsID = reader[0].ToString();
                                ErrorMessage = reader[1].ToString();
                            }
                        }
                    }

                }

                if (ErrorMessage != null && ErrorMessage != "")
                {
                    return await Create(ErrorMessage, obj);
                }
                #endregion


                // TODO: Add update logic here
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(Baseurl2);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllArticles using HttpClient  
                HttpResponseMessage response = await client.PostAsJsonAsync(Properties.Settings.Default.BuildingUtilityAPI + "/" + "0", obj);
                response.EnsureSuccessStatusCode();



                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }

        public async Task<ActionResult> Update(string id, string ErrorMessage)
        {
            List<BuildingUtility> objInfo = new List<BuildingUtility>();
            if (ErrorMessage != null)
            {
                ViewBag.ErrorMessage = ErrorMessage;
            }

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl2);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllArticles using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.BuildingUtilityAPI + "/" + id);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<List<BuildingUtility>>(APIResponse);

                }

                List<Building> _listBuilding = await GetListBuilding();
                ViewBag.ListBuilding = _listBuilding;

                List<URCCode> _listURCCode = await GetListURCCode();
                ViewBag.ListURCCode = _listURCCode;

                

                ViewBag.BuildingUtilityID = id;

                BuildingUtility objOne = new BuildingUtility();

                foreach (BuildingUtility Obj in objInfo)
                {
                    objOne = Obj;

                }

                //List<Subject> _listSubject = await GetListSubject();
                List<Subject> _listSubject = await GetListSubjectbyBuildingName(objOne.BuildingID);
                ViewBag.ListSubject = _listSubject;

                objOne.Invoice = String.Format("{0:n0}", Convert.ToDouble(objOne.Invoice)).Replace(",", ".");
                objOne.SetOff = String.Format("{0:n0}", Convert.ToDouble(objOne.SetOff)).Replace(",", ".");
                ViewBag.objInfo = objOne;

                List<LogHistory> _listHistories = GetListHistory(id);
                ViewBag.ListHistory = _listHistories;

                return View(objOne);
            }

        }

        public PartialViewResult PartialBuildingUtility(BuildingUtility obj, List<Building> _listBuilding, List<URCCode> _listURCCode, List<Subject> _listSubject)
        {

            ViewBag.ListBuilding = _listBuilding;
            ViewBag.ListURCCode = _listURCCode;
            ViewBag.ListSubject = _listSubject;

            BuildingUtility objInfo = obj;
            if (obj == null)
            {
                obj = new BuildingUtility();
            }
            return PartialView("PartialBuildingUtility", obj);
        }

        [HttpPost]
        public async Task<ActionResult> Update(string id, BuildingUtility obj, FormCollection collection, HttpPostedFileBase file)
        {
            try
            {
                BuildingUtility objInfo = new BuildingUtility();


                obj.BuildingID = collection["BuildingID"].ToString();
                obj.YearID = collection["YearID"].ToString();
                obj.MonthID = collection["MonthID"].ToString();
                obj.SubjectID = collection["SubjectID"].ToString();
                obj.URCCodeID = ""; //req 231018
                if (collection["URCCodeID"] != null && collection["URCCodeID"] != "")
                {
                    obj.URCCodeID = collection["URCCodeID"].ToString();
                }
               
                obj.Invoice = collection["Invoice"].ToString();
                obj.SetOff = "";
                if (collection["SetOff"] != null && collection["SetOff"] != "")
                {
                    obj.SetOff = collection["SetOff"].ToString();
                }
                obj.ActionSave = "UPDATE";
                obj.BuildingUtilityID = id;
                obj.UserName = User.Identity.Name;


                string ErrorMessage = "";
                string fileName = "";
                string path = "";
                string fileType = "";
                
                /*DOK 1*/
                if (file == null && (collection["DocumentName"].ToString() != null && collection["DocumentName"].ToString() != ""))
                {
                    fileName = collection["DocumentName"].ToString();
                    obj.DocFileName = fileName;
                    path = Path.Combine(Server.MapPath("~/content/document/buildingoccupants/"), fileName);
                }
                //Upload File
                /*DOK 1*/
                if (file != null && file.ContentLength > 0)
                {
                    fileType = Path.GetExtension(file.FileName);
                    if (fileType.ToUpper().Contains("PDF"))
                    {
                        if (file.ContentLength <= (5000 * 1024))
                        {
                            fileName = Path.GetFileName(file.FileName);
                            path = Path.Combine(Server.MapPath("~/content/document/buildingoccupants/"), fileName);
                            file.SaveAs(path);

                            //insert file data
                            obj.DocFileName = fileName;
                            obj.FolderName = "buildingoccupants";
                            obj.Path = path;
                        }
                        else
                        {
                            ErrorMessage = "File Upload tidak boleh lebih dari 5 MB";
                            return RedirectToAction("Update", new { id, ErrorMessage = ErrorMessage });
                        }

                    }
                    else
                    {
                        ErrorMessage = "File Upload harus dengan format PDF";
                        return RedirectToAction("Update", new { id, ErrorMessage = ErrorMessage });
                    }
                }


                #region cekduplicate
                BuildingOccupants obj2 = new BuildingOccupants();

                string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    using (var cmd = new SqlCommand("sp_CheckDuplicateBuildingUtility", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@BuildingUtilityID", id);
                        cmd.Parameters.AddWithValue("@BuildingID", obj.BuildingID);
                        cmd.Parameters.AddWithValue("@YearID", obj.YearID);
                        cmd.Parameters.AddWithValue("@MonthID", obj.MonthID);
                        cmd.Parameters.AddWithValue("@URCCodeID", obj.URCCodeID);
                        cmd.Parameters.AddWithValue("@SubjectID", obj.SubjectID);
                        cmd.Parameters.AddWithValue("@Invoice", obj.Invoice);
                        cmd.Parameters.AddWithValue("@SetOff", obj.SetOff);
                        conn.Open();

                        using (var reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                obj2.BuildingOccupantsID = reader[0].ToString();
                                ErrorMessage = reader[1].ToString();
                            }
                        }
                    }

                }

                if (ErrorMessage != null && ErrorMessage != "")
                {
                    return await Update(id, ErrorMessage);
                }
                #endregion


                // TODO: Add update logic here
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(Baseurl2);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                

                // TODO: Add update logic here
                HttpResponseMessage response = await client.PutAsJsonAsync(Properties.Settings.Default.BuildingUtilityAPI + "/" + id, obj);
                response.EnsureSuccessStatusCode();

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }

        public async Task<ActionResult> View(string id)
        {
            List<BuildingUtility> objInfo = new List<BuildingUtility>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl2);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllArticles using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.BuildingUtilityAPI + "/" + id);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<List<BuildingUtility>>(APIResponse);

                }
                List<Building> _listBuilding = await GetListBuilding();
                ViewBag.ListBuilding = _listBuilding;

                List<Subject> _listSubject = await GetListSubject();
                ViewBag.ListSubject = _listSubject;

                List<URCCode> _listURCCode = await GetListURCCode();
                ViewBag.ListURCCode = _listURCCode;

                List<LogHistory> _listHistories = GetListHistory(id);
                ViewBag.ListHistory = _listHistories;

                BuildingUtility objOne = new BuildingUtility();
                foreach (BuildingUtility Obj in objInfo)
                {
                    objOne = Obj;
                }

                objOne.Invoice = String.Format("{0:n0}", Convert.ToDouble(objOne.Invoice)).Replace(",", ".");
                objOne.SetOff = String.Format("{0:n0}", Convert.ToDouble(objOne.SetOff)).Replace(",", ".");

                ViewBag.BuildingUtilityID = id;
                ViewBag.objInfo = objOne;
                return View(objOne);
            }

        }

        public PartialViewResult PartialBuildingUtilityView(BuildingUtility obj, List<Building> _listBuilding, List<URCCode> _listURCCode, List<Subject> _listSubject)
        {

            ViewBag.ListBuilding = _listBuilding;
            ViewBag.ListSubject = _listSubject;
            ViewBag.ListURCCode = _listURCCode;

            BuildingUtility objInfo = obj;
            if (obj == null)
            {
                obj = new BuildingUtility();
                obj.Invoice = String.Format("{0:n0}", Convert.ToDouble(obj.Invoice)).Replace(",", ".");
                obj.SetOff = String.Format("{0:n0}", Convert.ToDouble(obj.SetOff)).Replace(",", ".");
            }
            return PartialView("PartialBuildingUtilityView", obj);
        }

        public async Task<List<Building>> GetListBuilding()
        {
            List<Building> _list = new List<Building>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl2);
                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.MasterBuildingAPI);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    _list = JsonConvert.DeserializeObject<List<Building>>(APIResponse);

                }
                //returning the employee list to view  

            }
            return _list;
        }

        public async Task<List<BuildingPosition>> GetListBuildingPosition()
        {
            List<BuildingPosition> _list = new List<BuildingPosition>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl2);
                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.BuildingPositionAPI);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    _list = JsonConvert.DeserializeObject<List<BuildingPosition>>(APIResponse);

                }
                //returning the employee list to view  

            }
            return _list;
        }

        public async Task<List<BuildingFloor>> GetListBuildingFloor()
        {
            List<BuildingFloor> _list = new List<BuildingFloor>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl2);
                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.BuildingFloorAPI);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    _list = JsonConvert.DeserializeObject<List<BuildingFloor>>(APIResponse);

                }
                //returning the employee list to view  

            }
            return _list;
        }

        public async Task<List<URCCode>> GetListURCCode()
        {
            List<URCCode> _list = new List<URCCode>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl2);
                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.URCCodeAPI);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    _list = JsonConvert.DeserializeObject<List<URCCode>>(APIResponse);

                }
                //returning the employee list to view  

            }
            return _list;
        }

        public async Task<List<Subject>> GetListSubject()
        {
            List<Subject> _list = new List<Subject>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl2);
                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.SubjectAPI);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Subject list  
                    _list = JsonConvert.DeserializeObject<List<Subject>>(APIResponse);

                }
                //returning the Subject list to view  

            }
            return _list;
        }

        public async Task<List<Subject>> GetListSubjectbyBuildingName(String BuildingName)
        {
            List<Subject> _list = new List<Subject>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl2);
                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.SubjectAPI);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Subject list  
                    _list = JsonConvert.DeserializeObject<List<Subject>>(APIResponse);

                }
                //returning the Subject list to view  

            }


            List<Subject> _list2 = new List<Subject>();
             _list2 = new List<Subject>(_list.Where(Subject => Subject.Description.Contains("-"+BuildingName.ToString()+"-")));

            return _list2;
        }

        public List<LogHistory> GetListHistory(string id)
        {
            List<LogHistory> _list = new List<LogHistory>();
            //LogHistory obj = new LogHistory();

            string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(connectionstring))
            {
                using (var cmd = new SqlCommand("sp_ViewHistoryLog", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@TableID", id);
                    cmd.Parameters.AddWithValue("@TableName", "BuildingUtility");
                    conn.Open();

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            LogHistory obj = new LogHistory();

                            obj.LogID = reader[0].ToString();
                            obj.TableName = reader[1].ToString();
                            obj.TableID = reader[2].ToString();
                            obj.Action = reader[3].ToString();
                            obj.UpdatedDate = reader[4].ToString();
                            obj.UpdatedBy = reader[5].ToString();

                            _list.Add(obj);
                        }

                       
                    }
                }

            }

            return _list;
        }

        public PartialViewResult PartialHistory()
        {
            return PartialView("PartialHistory", new List<LogHistory>());
        }

        public ActionResult Preview(string docfile)
        {

            ViewBag.DocFile = docfile;

            return View();


        }

        public async Task<String> GetURCDesc(String URCID)
        {
            List<URCCode> _list = new List<URCCode>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl2);
                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.URCCodeAPI);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Subject list  
                    _list = JsonConvert.DeserializeObject<List<URCCode>>(APIResponse);

                }
                //returning the Subject list to view  

            }

            List<URCCode> URCList = new List<URCCode>(_list.Where(Subject => Subject.URCCodeID.Equals(URCID.ToString())));

            URCCode objOne = new URCCode();
            string URCDescription = "";

            foreach (URCCode Obj in URCList)
            {
                URCDescription = Obj.Description.ToString();

            }

            Response.Write(JsonConvert.SerializeObject(URCDescription));
            Response.End();

            return JsonConvert.SerializeObject(URCDescription);

        }

        public async Task<JsonResult> GetSubjectbyBuilding(String BuildingID)
        {
            List<Subject> _list = new List<Subject>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl2);
                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.SubjectAPI);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Subject list  
                    _list = JsonConvert.DeserializeObject<List<Subject>>(APIResponse);

                }
                //returning the Subject list to view  

            }

            List<Subject> SubjectList = new List<Subject>(_list.Where(Subject => Subject.Description.Contains(BuildingID.ToString())));
            
            //Response.Write(JsonConvert.SerializeObject(SubjectList));
            //Response.End();

            //var json = JsonConvert.SerializeObject(SubjectList);

            //return JsonConvert.SerializeObject(SubjectList);
            List<SelectListItem> subjects = new List<SelectListItem>();

            foreach (var item in SubjectList)
            {
                SelectListItem subject = new SelectListItem
                {
                    Text = item.SubjectName,
                    Value = item.SubjectID
                };
                subjects.Add(subject);
            }
            return Json(subjects, JsonRequestBehavior.AllowGet);

        }
    }
}