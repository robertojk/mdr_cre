﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using MandiriCRE.Models;
using System.IO;


namespace MandiriCRE.Controllers
{
    public class ProcurementController : Controller
    {
        string Baseurl = Properties.Settings.Default.BaseURI;
        // GET: Procurement
        public async Task<ActionResult> Index()
        {
            List<Procurements> objInfo = new List<Procurements>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.ProcurementAPI);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<List<Procurements>>(APIResponse);

                }

                //returning the employee list to view  
                return View(objInfo);
            }
        }
        // GET: Procurement/Details/5
        public async Task<ActionResult> Details(string id)
        {
            ProcurementHeader objInfo = new ProcurementHeader();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllArticles using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.ProcurementHeaderAPI + "/" + id);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<ProcurementHeader>(APIResponse);

                }
                ViewBag.ProcurementID = id;
                return View(objInfo);
            }
        }
        // GET: Procurement/Create
        public async Task<ActionResult> Create()
        {
            List<Employee> _listEmployee = await GetListEmployees();
            ViewBag.ListEmployee = _listEmployee;
            ProcurementHeader objInfo = new ProcurementHeader();
            List<Numbering> _listTemplates = await GetListDocs();
            ViewBag.ListTemplates = _listTemplates;
            return View(objInfo);
        }
        // POST: Procurement/Create
        [HttpPost]
        public async Task<ActionResult> Create(ProcurementHeader obj, FormCollection collection)
        {
            try
            {
                obj.AdditionalInformation = collection["description"].ToString();
                obj.DateEnd = collection["finishdate"].ToString();
                obj.DateStart = collection["startdate"].ToString();
                obj.DocumentNo = collection["DocumentNo"].ToString() + collection["documentNumber"].ToString();
                obj.EmployeeManagerID = obj.EmployeeManagerID;
                obj.EmployeePICID = obj.EmployeePICID;
                obj.ProcurementName = collection["procTitle"].ToString();
                obj.UserName = User.Identity.Name;
                // TODO: Add update logic here
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                // TODO: Add update logic here
                HttpResponseMessage response = await client.PostAsJsonAsync(Properties.Settings.Default.ProcurementHeaderAPI + "/" + "0", obj);
                response.EnsureSuccessStatusCode();
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }
        // GET: Procurement/Edit/5
        public async Task<ActionResult> Edit(string id)
        {
            ProcurementHeader objInfo = new ProcurementHeader();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllArticles using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.ProcurementHeaderAPI + "/" + id);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<ProcurementHeader>(APIResponse);

                }
                List<Employee> _listEmployee = await GetListEmployees();
                ViewBag.ListEmployee = _listEmployee;
                //List<Template> _listTemplates = await GetListDocs();
                //ViewBag.ListTemplates = _listTemplates;
                //returning the employee list to view  
                return View(objInfo);
            }
        }
        // POST: Procurement/Edit/5
        [HttpPost]
        public async Task<ActionResult> Edit(string id, ProcurementHeader obj, FormCollection collection)
        {
            try
            {
                obj.AdditionalInformation = collection["description"].ToString();
                obj.DateEnd = collection["finishdate"].ToString();
                obj.DateStart = collection["startdate"].ToString();
                obj.DocumentNo = collection["documentNumber"].ToString();
                obj.EmployeeManagerID = obj.EmployeeManagerID;
                obj.EmployeePICID = obj.EmployeePICID;
                obj.ProcurementName = collection["procTitle"].ToString();
                obj.ProcurementID = id;
                obj.UserName = User.Identity.Name;
                // TODO: Add update logic here
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                // TODO: Add update logic here
                HttpResponseMessage response = await client.PutAsJsonAsync(Properties.Settings.Default.ProcurementHeaderAPI + "/" + id, obj);
                response.EnsureSuccessStatusCode();
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }
        public async Task<ActionResult> Approve(int id)
        {
            ProcurementHeader objInfo = new ProcurementHeader();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllArticles using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.ArticleAPI + "/" + id);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<ProcurementHeader>(APIResponse);

                }
                //returning the employee list to view  
                return View(objInfo);
            }
        }
        // POST: Procurement/Edit/5
        [HttpPost]
        public async Task<ActionResult> Approve(int id, ProcurementHeader obj, FormCollection collection)
        {
            try
            {

                // TODO: Add update logic here
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                // TODO: Add update logic here
                HttpResponseMessage response = await client.PutAsJsonAsync(Properties.Settings.Default.ArticleAPI + "/" + id, obj);
                response.EnsureSuccessStatusCode();
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }
        public async Task<ActionResult> Duplicate(int id)
        {
            ProcurementHeader objInfo = new ProcurementHeader();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllArticles using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.ArticleAPI + "/" + id);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<ProcurementHeader>(APIResponse);

                }
                //returning the employee list to view  
                return View(objInfo);
            }
        }
        // POST: Procurement/Edit/5
        [HttpPost]
        public async Task<ActionResult> Duplicate(int id, ProcurementHeader obj, FormCollection collection)
        {
            try
            {

                // TODO: Add update logic here
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                // TODO: Add update logic here
                HttpResponseMessage response = await client.PutAsJsonAsync(Properties.Settings.Default.ArticleAPI + "/" + id, obj);
                response.EnsureSuccessStatusCode();
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }
        public async Task<ActionResult> Attachments(string id)
        {
            List<ProcurementDoc> objInfo = new List<ProcurementDoc>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.ProcurementDocsAPI + "/" + id);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<List<ProcurementDoc>>(APIResponse);

                }
                ViewBag.ProcurementID = id;
                //returning the employee list to view  
                return View(objInfo);
            }
        }
        public ActionResult CreateAttachmentPengadaan(string id)
        {
            ProcurementDoc objInfo = new ProcurementDoc();
            return View(objInfo);
        }
        // POST: CRUD/Create
        [HttpPost]
        public async Task<ActionResult> CreateAttachmentPengadaan(string id, ProcurementDoc obj, FormCollection form, HttpPostedFileBase file)
        {
            try
            {
                string fileName = "";
                string path = "";
                if (file != null && file.ContentLength > 0)
                {
                    fileName = Path.GetFileName(file.FileName);
                    path = Path.Combine(Server.MapPath("~/content/document/procurement/"), fileName);
                    file.SaveAs(path);
                }
                obj.DocumentName = fileName;
                obj.createdDate = DateTime.Now.ToString("dd/MM/yyyy");
                obj.CreatedBy = User.Identity.Name;
                obj.UplodedDate = DateTime.Now.ToString("dd/MM/yyyy");
                obj.UplodedBy = User.Identity.Name;
                obj.ModulName = "PENGADAAN";
                obj.TableId = id;
                obj.Description = form["description"].ToString();
                obj.FolderName = "template";
                obj.User_Name = User.Identity.Name;
                // TODO: Add update logic here
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                // TODO: Add update logic here
                HttpResponseMessage response = await client.PostAsJsonAsync(Properties.Settings.Default.ProcurementDocsAPI + "/" + id + "/0", obj);
                response.EnsureSuccessStatusCode();
                return RedirectToAction("Attachments", "Procurement", new { id = id });
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }
        public async Task<ActionResult> EditAttachmentPengadaan(string id, string procurementId)
        {
            try
            {
                // TODO: Add delete logic here
                ProcurementDoc objInfo = new ProcurementDoc();

                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(Baseurl);

                    client.DefaultRequestHeaders.Clear();
                    //Define request data format  
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                    HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.ProcurementDocsAPI + "/" + procurementId + "/" + id);

                    //Checking the response is successful or not which is sent using HttpClient  
                    if (Res.IsSuccessStatusCode)
                    {
                        //Storing the response details recieved from web api   
                        var APIResponse = Res.Content.ReadAsStringAsync().Result;

                        //Deserializing the response recieved from web api and storing into the Employee list  
                        objInfo = JsonConvert.DeserializeObject<ProcurementDoc>(APIResponse);

                    }
                    ViewBag.ProcurementID = procurementId;
                    ViewBag.id = id;
                    //returning the employee list to view  
                    return View(objInfo);
                }
            }
            catch
            {
                return View();
            }
        }
        // POST: CRUD/Create
        [HttpPost]
        public async Task<ActionResult> EditAttachmentPengadaan(string id, ProcurementDoc obj, FormCollection form, HttpPostedFileBase file)
        {

            try
            {
                string procurementId = form["procurementId"].ToString();
                string fileName = "";
                string path = "";
                if (file != null && file.ContentLength > 0)
                {
                    fileName = Path.GetFileName(file.FileName);
                    path = Path.Combine(Server.MapPath("~/content/document/procurement/"), fileName);
                    file.SaveAs(path);
                }
                obj.ProcurementDocID = id;
                obj.DocName = fileName;
                obj.UserName = User.Identity.Name;
                obj.UplodedDate = DateTime.Now.ToString("dd/MM/yyyy");
                obj.UplodedBy = User.Identity.Name;
                obj.ModulName = "PENGADAAN";
                obj.TableId = procurementId;
                obj.Description = form["description"].ToString();
                obj.FolderName = "template";
                obj.User_Name = User.Identity.Name;
                // TODO: Add update logic here
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                // TODO: Add update logic here
                HttpResponseMessage response = await client.PutAsJsonAsync(Properties.Settings.Default.ProcurementDocsAPI + "/" + id + "/0", obj);
                response.EnsureSuccessStatusCode();
                return RedirectToAction("Attachments", "Procurement", new { id = procurementId });
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }
        // GET: CRUD/Delete/5
        public async Task<ActionResult> DeleteAttachmentPengadaan(string id, string procurementId)
        {
            try
            {
                // TODO: Add delete logic here
                ProcurementDoc objInfo = new ProcurementDoc();

                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(Baseurl);

                    client.DefaultRequestHeaders.Clear();
                    //Define request data format  
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                    HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.ProcurementDocsAPI + "/" + procurementId + "/" + id);

                    //Checking the response is successful or not which is sent using HttpClient  
                    if (Res.IsSuccessStatusCode)
                    {
                        //Storing the response details recieved from web api   
                        var APIResponse = Res.Content.ReadAsStringAsync().Result;

                        //Deserializing the response recieved from web api and storing into the Employee list  
                        objInfo = JsonConvert.DeserializeObject<ProcurementDoc>(APIResponse);

                    }
                    ViewBag.ProcurementID = procurementId;
                    ViewBag.id = id;
                    //returning the employee list to view  
                    return View(objInfo);
                }
            }
            catch
            {
                return View();
            }
        }
        // POST: CRUD/Delete/5
        [HttpPost]
        public async Task<ActionResult> DeleteAttachmentPengadaan(string id, string procurementId, ProcurementDoc obj, FormCollection collection)
        {
            // TODO: Add delete logic here
            try
            {
                //obj.ProcurementDocID = collection["ProcurementDocID"].ToString();
                procurementId = collection["procurementId"].ToString();
                // TODO: Add update logic here
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                // TODO: Add update logic here
                HttpResponseMessage response = await client.DeleteAsync(Properties.Settings.Default.ProcurementDocsAPI + "/" + procurementId + "/" + id);
                response.EnsureSuccessStatusCode();
                return RedirectToAction("Attachments", "Procurement", new { id = procurementId });
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }
        public async Task<ActionResult> DetailPengadaan(string id)
        {
            List<ProcurementDetail> objInfo = new List<ProcurementDetail>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.ProcurementDetailsAPI + "/" + id);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<List<ProcurementDetail>>(APIResponse);

                }
                #region Tax Calculation
                int sebelumPajak = 0;
                int sesudahPajak = 0;
                int pajak = 0;
                foreach (ProcurementDetail obj in objInfo)
                {
                    sebelumPajak = sebelumPajak + Convert.ToInt32(obj.Price);
                    pajak = Convert.ToInt32(obj.Tax);
                }
                sesudahPajak = sebelumPajak + (sebelumPajak * pajak);
                ViewBag.sebelumPajak = sebelumPajak.ToString("N");
                ViewBag.pajak = pajak.ToString() + "%";
                ViewBag.sesudahPajak = sesudahPajak.ToString("N");

                #endregion
                ViewBag.ProcurementID = id;
                //returning the employee list to view  
                return View(objInfo);
            }
        }
        // GET: CRUD/Create
        public ActionResult CreateDetailPengadaan(string id)
        {
            ProcurementDetail objInfo = new ProcurementDetail();
            return View(objInfo);
        }
        // POST: CRUD/Create
        [HttpPost]
        public async Task<ActionResult> CreateDetailPengadaan(string id, ProcurementDetail obj, FormCollection collection)
        {
            try
            {
                obj.UserName = User.Identity.Name;
                obj.ProcurementID = id;
                obj.TaskNameDetail = collection["taskNameDetail"].ToString();
                obj.Price = collection["price"].ToString();
                obj.Tax = collection["tax"].ToString();
                //obj.CreatedDate = DateTime.Now.ToString("dd/MM/yyyy");
                // TODO: Add update logic here
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                // TODO: Add update logic here
                HttpResponseMessage response = await client.PostAsJsonAsync(Properties.Settings.Default.ProcurementDetailsAPI + "/" + id + "/0", obj);
                response.EnsureSuccessStatusCode();
                return RedirectToAction("DetailPengadaan", "Procurement", new { id = id });
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }
        // GET: CRUD/Delete/5
        public async Task<ActionResult> DeleteDetailPengadaan(string id, string procurementId)
        {
            try
            {
                // TODO: Add delete logic here
                ProcurementDetail objInfo = new ProcurementDetail();

                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(Baseurl);

                    client.DefaultRequestHeaders.Clear();
                    //Define request data format  
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                    HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.ProcurementDetailsAPI + "/" + procurementId + "/" + id);

                    //Checking the response is successful or not which is sent using HttpClient  
                    if (Res.IsSuccessStatusCode)
                    {
                        //Storing the response details recieved from web api   
                        var APIResponse = Res.Content.ReadAsStringAsync().Result;

                        //Deserializing the response recieved from web api and storing into the Employee list  
                        objInfo = JsonConvert.DeserializeObject<ProcurementDetail>(APIResponse);

                    }
                    ViewBag.ProcurementID = procurementId;
                    //returning the employee list to view  
                    return View(objInfo);
                }
            }
            catch
            {
                return View();
            }
        }
        // POST: CRUD/Delete/5
        [HttpPost]
        public async Task<ActionResult> DeleteDetailPengadaan(string id, ProcurementDetail obj, FormCollection collection)
        {
            // TODO: Add delete logic here
            try
            {
                string procurementId = collection["procurementId"].ToString();
                // TODO: Add update logic here
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                // TODO: Add update logic here
                HttpResponseMessage response = await client.DeleteAsync(Properties.Settings.Default.ProcurementDetailsAPI + "/" + procurementId + "/" + id);
                response.EnsureSuccessStatusCode();
                return RedirectToAction("DetailPengadaan", "Procurement", new { id = procurementId });
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }
        public async Task<ActionResult> Partner(string id)
        {
            List<ProcurementVendor> objInfo = new List<ProcurementVendor>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.ProcurementVendorsAPI + "/" + id);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<List<ProcurementVendor>>(APIResponse);

                }
                ViewBag.ProcurementID = id;
                //returning the employee list to view  
                return View(objInfo);
            }
        }
        // GET: CRUD/Create
        public async Task<ActionResult> CreateVendor(string id)
        {
            ProcurementVendor objInfo = new ProcurementVendor();
            List<Partner> _listPartners = await GetListPartners();
            ViewBag.ListPartners = _listPartners;
            ViewBag.ProcurementID = id;
            return View(objInfo);
        }
        // POST: CRUD/Create
        [HttpPost]
        public async Task<ActionResult> CreateVendor(string id, ProcurementVendor obj, FormCollection collection)
        {
            try
            {
                //obj.UserName = User.Identity.Name;
                //obj.ProcurementID = id;
                obj.VendorID = collection["VendorID"].ToString();
                obj.Procurement_ProcurementID = collection["procurementId"].ToString();
                obj.ProcurementID = collection["procurementId"].ToString();
                obj.Username = User.Identity.Name;
                //obj.CreatedDate = DateTime.Now.ToString("dd/MM/yyyy");
                // TODO: Add update logic here
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                // TODO: Add update logic here
                HttpResponseMessage response = await client.PostAsJsonAsync(Properties.Settings.Default.ProcurementVendorsAPI + "/" + id + "/" + obj.VendorID, obj);
                response.EnsureSuccessStatusCode();
                return RedirectToAction("Partner", "Procurement", new { id = id });
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }
        public async Task<List<Partner>> GetListPartners()
        {
            List<Partner> _list = new List<Partner>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.RekananAPI);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    _list = JsonConvert.DeserializeObject<List<Partner>>(APIResponse);

                }
                //returning the employee list to view  

            }
            return _list;
        }
        public async Task<ActionResult> History(string id)
        {
            List<WorkFlowHistory> objInfo = new List<WorkFlowHistory>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.ProcurementHistoriesAPI + "/" + id);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<List<WorkFlowHistory>>(APIResponse);

                }
                //returning the employee list to view  
                return View(objInfo);
            }
        }
        public async Task<ActionResult> Negosiation(string id)
        {
            List<ProcurementVendor> objInfo = new List<ProcurementVendor>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.ProcurementVendorsAPI + "/" + id);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<List<ProcurementVendor>>(APIResponse);

                }
                //returning the employee list to view  
                return View(objInfo);
            }
        }
        public async Task<ActionResult> EditVendorNegotiation(string id, string procurementId)
        {
            try
            {
                // TODO: Add delete logic here
                ProcurementVendor objInfo = new ProcurementVendor();

                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(Baseurl);

                    client.DefaultRequestHeaders.Clear();
                    //Define request data format  
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                    HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.ProcurementVendorsAPI + "/" + procurementId + "/" + id);

                    //Checking the response is successful or not which is sent using HttpClient  
                    if (Res.IsSuccessStatusCode)
                    {
                        //Storing the response details recieved from web api   
                        var APIResponse = Res.Content.ReadAsStringAsync().Result;

                        //Deserializing the response recieved from web api and storing into the Employee list  
                        objInfo = JsonConvert.DeserializeObject<ProcurementVendor>(APIResponse);

                    }
                    ViewBag.ProcurementID = procurementId;
                    ViewBag.id = id;
                    //returning the employee list to view  
                    return View(objInfo);
                }
            }
            catch
            {
                return View();
            }
        }
        // POST: CRUD/Create
        [HttpPost]
        public async Task<ActionResult> EditVendorNegotiation(string id, string procurementId, ProcurementVendor obj, FormCollection collection, HttpPostedFileBase file)
        {

            try
            {
                //obj.UserName = User.Identity.Name;
                //obj.ProcurementID = id;
                //obj.VendorID = collection["VendorID"].ToString();
                //obj.Procurement_ProcurementID = collection["procurementId"].ToString();
                //obj.ProcurementID = collection["procurementId"].ToString();
                obj.Username = User.Identity.Name;
                //obj.CreatedDate = DateTime.Now.ToString("dd/MM/yyyy");
                // TODO: Add update logic here
                obj.ProcurementID = "9";
                obj.StartingPrice= collection["StartingPrice"].ToString();
                obj.Negotiation= collection["Negotiation"].ToString();
                obj.Final= collection["Final"].ToString();
                obj.WinnerStatus= collection["status"].ToString();
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                // TODO: Add update logic here
                HttpResponseMessage response = await client.PostAsJsonAsync(Properties.Settings.Default.ProcurementVendorsAPI + "/" + obj.ProcurementID + "/" + id, obj);
                response.EnsureSuccessStatusCode();
                return RedirectToAction("Negosiation", "Procurement", new { id = obj.ProcurementID });
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }
        public async Task<List<Employee>> GetListEmployees()
        {
            List<Employee> _list = new List<Employee>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.EmployeeAPI);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    _list = JsonConvert.DeserializeObject<List<Employee>>(APIResponse);

                }
                //returning the employee list to view  

            }
            return _list;
        }
        public async Task<List<Numbering>> GetListDocs()
        {
            List<Numbering> _list = new List<Numbering>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.PenomoranAPI);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    _list = JsonConvert.DeserializeObject<List<Numbering>>(APIResponse);

                }
                //returning the employee list to view  

            }
            return _list;
        }
    }
}
