﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using MandiriCRE.Models;
using System.IO;
using System.Data.SqlClient;
using System.Data;

namespace MandiriCRE.Controllers
{
    public class PaymentController : Controller
    {
        string Baseurl = Properties.Settings.Default.BaseURI;
        // GET: Payment
        public ActionResult Index()
        {
            List<PaymentList> objInfo = new List<PaymentList>();
            objInfo = GetPaymentList();


            return View(objInfo);
        }

        public List<PaymentList> GetPaymentList()
        {
            List<PaymentList> _PaymentList = new List<PaymentList>();

            string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(connectionstring))
            {
                using (var cmd = new SqlCommand("sp_PaymentList", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    //cmd.Parameters.AddWithValue("@pMyParamater", myParamaterValue);
                    conn.Open();

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            PaymentList obj = new PaymentList();

                            obj.ProcurementID = Convert.ToInt32(reader[3]);
                            obj.ProcurementNo = reader[4].ToString();
                            obj.VendorDetailID = Convert.ToInt32(reader[0]);
                            obj.ProcurementVendorName = reader[5].ToString();
                            obj.AgreementID = Convert.ToInt32(reader[1]);
                            obj.AgreementNo = reader[2].ToString();
                            obj.AgreementName = reader[6].ToString();
                            obj.Status = reader[7].ToString();

                            _PaymentList.Add(obj);
                        }
                    }
                }

            }
            return _PaymentList;
            
        }

        public ActionResult Detail(int vendordetailid,int agreementid)
        {
            List<PaymentList> objInfo = new List<PaymentList>();
            objInfo = GetPaymentDetail(vendordetailid, agreementid);


            return View(objInfo);
        }

        public List<PaymentList> GetPaymentDetail(int vendordetailid, int agreementid)
        {
            List<PaymentList> _PaymentList = new List<PaymentList>();

            string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(connectionstring))
            {
                using (var cmd = new SqlCommand("sp_PaymentListDetail", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@VendorDetailID", vendordetailid);
                    cmd.Parameters.AddWithValue("@AgreementID", agreementid);
                    conn.Open();

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            PaymentList obj = new PaymentList();

                            obj.ProcurementID = Convert.ToInt32(reader[0]);
                            obj.ProcurementNo = reader[1].ToString();
                            obj.VendorDetailID = Convert.ToInt32(reader[5]);
                            obj.ProcurementVendorName = reader[7].ToString();
                            obj.AgreementID = Convert.ToInt32(reader[9]);
                            obj.AgreementNo = reader[10].ToString();
                            obj.AgreementName = reader[14].ToString();
                            obj.AgreementValue = reader[11].ToString();
                            obj.ProgressofWork = reader[16].ToString();
                            obj.ProgressofPayment = reader[17].ToString();
                            obj.Description = reader[18].ToString();
                            obj.isPaid = reader[19].ToString();
                            obj.PaymentDate = reader[20].ToString();
                            obj.ProgressofPaymentDoc = reader[21].ToString();
                            obj.ProgressofWorkDoc = reader[22].ToString();
                            obj.DocumentPaymentName = reader[23].ToString();
                            obj.DocumentPaymentPath = reader[24].ToString();
                            obj.DocumentWorkName = reader[25].ToString();
                            obj.DocumentWorkPath = reader[26].ToString();
                            obj.VendorTerminID= Convert.ToInt32(reader[15]);

                            _PaymentList.Add(obj);

                            ViewBag.AgreementNo = obj.AgreementNo;
                            ViewBag.AgreementName = obj.AgreementName;
                            ViewBag.AgreementValue = Convert.ToDouble(obj.AgreementValue).ToString("N0");

                        }
                    }
                }

            }
            return _PaymentList;
            
        }

        public ActionResult Bayar(int vendordetailid,int agreementid, int terminid,string ErrorMessage)
        {
            PaymentList objInfo = new PaymentList();
            if (ErrorMessage != null)
            {
                ViewBag.ErrorMessage = ErrorMessage;
            }
            objInfo.VendorTerminID = terminid;
            objInfo.VendorDetailID = vendordetailid;
            objInfo.AgreementID = agreementid;
            return View(objInfo);
        }

        // POST: CRUD/Create
        [HttpPost]
        public ActionResult Bayar(int vendordetailid,int terminid,int agreementid,PaymentList obj, FormCollection form, HttpPostedFileBase file, HttpPostedFileBase picfile)
        {
            try
            {
                string ErrorMessage = "";
                string fileType = "";
                string fileName = "";
                string path = "";
                if (file != null && file.ContentLength > 0)
                {
                    fileType = Path.GetExtension(file.FileName);
                    if (fileType.ToUpper().Contains("DOC") || fileType.ToUpper().Contains("XLS") || fileType.ToUpper().Contains("PPT") || fileType.ToUpper().Contains("PDF"))
                    {
                        if (file.ContentLength <= (2000 * 1024))
                        {

                            fileName = Path.GetFileName(file.FileName);
                            path = Path.Combine(Server.MapPath("~/content/document/payment/"), fileName);
                            file.SaveAs(path);
                        }
                        else
                        {
                            ErrorMessage = "File Upload tidak boleh lebih dari 2MB";
                            return RedirectToAction("Bayar", "Payment", new { vendordetailid = vendordetailid, terminid= terminid, agreementid= agreementid,ErrorMessage = ErrorMessage });
                        }
                    }
                    else
                    {
                        ErrorMessage = "File Upload harus dengan format All Office atau PDF";
                        return RedirectToAction("Bayar", "Payment", new { vendordetailid = vendordetailid, terminid = terminid, agreementid = agreementid, ErrorMessage = ErrorMessage });
                    }
                }
                string picName = "";
                string pathPic = "";
                if (picfile != null && picfile.ContentLength > 0)
                {
                    picName = Path.GetFileName(picfile.FileName);
                    pathPic = Path.Combine(Server.MapPath("~/content/document/payment/"), picName);
                    file.SaveAs(pathPic);
                }
                obj.VendorDetailID = vendordetailid;
                obj.VendorTerminID = terminid;
                obj.AgreementID = agreementid;
                obj.DocumentPaymentName = fileName;
                obj.DocumentWorkName = picName;
                obj.DocumentPaymentPath = path;
                obj.DocumentWorkPath = pathPic;
                obj.Description = "";
                obj.PaymentDate = form["paymentdate"].ToString();
                // TODO: Add update logic here
                string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    using (var cmd = new SqlCommand("sp_PaymentProcess", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@VendorTerminID", obj.VendorTerminID);
                        cmd.Parameters.AddWithValue("@PaymentDtae", obj.PaymentDate);
                        cmd.Parameters.AddWithValue("@ProgressofWorkDocName", obj.DocumentWorkName);
                        cmd.Parameters.AddWithValue("@ProgressofPaymentDocName", obj.DocumentPaymentName);
                        cmd.Parameters.AddWithValue("@Descriotion", obj.Description);
                        cmd.Parameters.AddWithValue("@UserName", User.Identity.Name);
                        conn.Open();
                        int k = cmd.ExecuteNonQuery();
                        if (k != 0)
                        {
                            //sukses
                        }
                        conn.Close();
                    }
                }

                return RedirectToAction("Detail", "Payment", new { vendordetailid = obj.VendorDetailID, agreementid = obj.AgreementID});
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }

        public ActionResult PaymentTerminDetail(int vendordetailid, int agreementid, int terminid)
        {
            PaymentList objInfo = new PaymentList();
            objInfo = GetPaymentTerminDetail(vendordetailid, agreementid,terminid);

            return View(objInfo);
        }

        public PaymentList GetPaymentTerminDetail(int vendordetailid, int agreementid,int terminid)
        {
            PaymentList obj = new PaymentList();

            string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(connectionstring))
            {
                using (var cmd = new SqlCommand("sp_PaymentTerminDetail", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@VendorDetailID", vendordetailid);
                    cmd.Parameters.AddWithValue("@AgreementID", agreementid);
                    cmd.Parameters.AddWithValue("@VendorTerminID", terminid);
                    conn.Open();

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            obj.ProcurementID = Convert.ToInt32(reader[0]);
                            obj.ProcurementNo = reader[1].ToString();
                            obj.VendorDetailID = Convert.ToInt32(reader[5]);
                            obj.ProcurementVendorName = reader[7].ToString();
                            obj.AgreementID = Convert.ToInt32(reader[9]);
                            obj.AgreementNo = reader[10].ToString();
                            obj.AgreementName = reader[14].ToString();
                            obj.AgreementValue = reader[11].ToString();
                            obj.ProgressofWork = reader[16].ToString();
                            obj.ProgressofPayment = reader[17].ToString();
                            obj.Description = reader[18].ToString();
                            obj.isPaid = reader[19].ToString();
                            obj.PaymentDate = reader[20].ToString();
                            obj.ProgressofPaymentDoc = reader[21].ToString();
                            obj.ProgressofWorkDoc = reader[22].ToString();
                            obj.DocumentPaymentName = reader[23].ToString();
                            obj.DocumentPaymentPath = reader[24].ToString();
                            obj.DocumentWorkName = reader[25].ToString();
                            obj.DocumentWorkPath = reader[26].ToString();
                            obj.VendorTerminID = Convert.ToInt32(reader[15]);
                        }
                    }
                }

            }
            return obj;
        }

    }
}