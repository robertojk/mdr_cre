﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using MandiriCRE.Models;
using MandiriCRE.ViewModel;

namespace MandiriCRE.Controllers
{
    public class ManageRoleMemberController : Controller
    {
        // GET: ManageRoleMember
        string Baseurl = Properties.Settings.Default.BaseURI;
        // GET: MasterHakAkses
        
        public async Task<ActionResult> Index()
        {
            List<UserRole> objInfo = new List<UserRole>();
   
            string roleId = Request.QueryString["roleId"];
            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.UserRoleAPI + "/" + roleId);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    var res = JsonConvert.DeserializeObject<List<UserRole>>(APIResponse);
                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<List<UserRole>>(APIResponse);

                }
                ViewBag.RoleId = roleId;
                //returning the employee list to view  
                return View(objInfo);
            }
        }
        // GET: CRUD/Create
        public async Task<ActionResult> Create()
        {
            string roleId = Request.QueryString["roleId"];
            UserRole objInfo = new UserRole();
            List<User> _listUsers = await GetListUsers();
            ViewBag.ListUsers = _listUsers;
            ViewBag.roleId = roleId;
            return View(objInfo);
        }
        public async Task<List<User>> GetListUsers()
        {
            List<User> _list = new List<User>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.UserAPI);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    _list = JsonConvert.DeserializeObject<List<User>>(APIResponse);

                }
                //returning the employee list to view  
                
            }
            return _list;
        }
        // POST: CRUD/Create
        [HttpPost]
        public async Task<ActionResult> Create( UserRole obj, FormCollection collection)
        {
            try
            {
                string roleId = collection["roleId"].ToString();
                // TODO: Add update logic here
                obj.UserId = collection["UserId"].ToString();
                obj.RoleId = roleId;
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                // TODO: Add update logic here
                HttpResponseMessage response = await client.PostAsJsonAsync(Properties.Settings.Default.UserRoleAPI + "/" + roleId, obj);
                //response.EnsureSuccessStatusCode();
                return RedirectToAction("Index","ManageRoleMember", new { roleId = roleId});
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }

        // GET: CRUD/Delete/5
        public async Task<ActionResult> Delete(string userId, string roleId)
        {
            User objUser = new User();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.UserAPI + "/" + userId);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result; 

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objUser = JsonConvert.DeserializeObject<User>(APIResponse);

                }
                ViewBag.roleId = roleId;
                //returning the employee list to view  
                return View(objUser);
            }
        }
   
        // POST: CRUD/Delete/5
        [HttpPost]
        public async Task<ActionResult> Delete(string userId, string roleId, UserRole obj)
        {
            // TODO: Add delete logic here
            try
            {
                string userId2 = Request.QueryString["userId"];
                string roleId2 = Request.QueryString["roleId"];
                // TODO: Add update logic here 
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                // TODO: Add update logic here
                HttpResponseMessage response = await client.PostAsJsonAsync(Properties.Settings.Default.UserRoleAPI + "/delete/" + userId + "/" + roleId, obj);
                response.EnsureSuccessStatusCode();
                return RedirectToAction("Index", "ManageRoleMember", new { roleId = roleId });
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }
    }
}