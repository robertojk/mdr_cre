﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using MandiriCRE.Models;
using System.IO;
using System.Data.SqlClient;
using System.Data;
using System.Web.UI;
using Simplexcel;
using System.Text.RegularExpressions;
using System.Globalization;
using MandiriCRE.ViewModel;
//using Microsoft.Office.Interop.Excel;

namespace MandiriCRE.Controllers
{

    public class ATTBController : Controller
    {

        string Baseurl = Properties.Settings.Default.BaseURI;
        string Baseurl2 = Properties.Settings.Default.BaseURI;

        //public Microsoft.Office.Interop.Excel.Page Page { get; private set; }
        public object Controls { get; private set; }

        // GET: ATTB
        public async Task<ActionResult> Index()
        {
            var model = new ATTBIndexViewModel();

            model.InitializePermissions(
                Session,
                "20"
            );

            model.ATTBJson = await JsonDownloader.GetJsonOrDefaultAsync(
                Baseurl2,
                Properties.Settings.Default.ATTBAPI,
                EmptyJsonDefault.EmptyArray
            );

            return View(model);
        }        
        
        // GET: ATTB/ATTBID
        public async Task<ActionResult> Details(string id)
        {
            ATTB objInfo = new ATTB();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl2);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllATTB using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.ATTBAPI + "/" + id);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the ATTB list  
                    objInfo = JsonConvert.DeserializeObject<ATTB>(APIResponse);

                }
                ViewBag.ATTBID = id;
                return View(objInfo);
            }
        }

        //GET: Tax/Create
        public async Task<ActionResult> Create(string ErrorMessage, ATTB objTax)
        {
            if (ErrorMessage != null)
            {
                ViewBag.ErrorMessage = ErrorMessage;
            }

            List<ATTBStatusModels> _listATTBStatus = await GetListATTBStatus();
            ViewBag.ListATTBStatus = _listATTBStatus;

            List<OwnershipModels> _listOwnership = await GetListOwnership();
            ViewBag.ListOwnership = _listOwnership;

            List<ATTBTypeModels> _listATTBType = await GetListATTBType();
            ViewBag.ListATTBType = _listATTBType;

            List<RegionAssetModels> _listRegionAsset = await GetListRegionAsset();
            ViewBag.ListRegionAsset = _listRegionAsset;

            List<AssetControlModels> _listAssetControl = await GetListAssetControl();
            ViewBag.ListAssetControl = _listAssetControl;

            List<PrimaryUserModels> _listPrimaryUser = await GetListPrimaryUser();
            ViewBag.ListPrimaryUser = _listPrimaryUser;

            ATTB objInfo = new ATTB();
            if (objTax != null)
            {
                objInfo = objTax;
            }
            if (ErrorMessage != null)
            {
                ViewBag.ErrorMessage = ErrorMessage;
            }
            return View(objInfo);
        }
        // POST: Procurement/Create
        [HttpPost]
        public async Task<ActionResult> Create(ATTB objTax, FormCollection collection, HttpPostedFileBase file, HttpPostedFileBase file2, HttpPostedFileBase file3, HttpPostedFileBase file4, HttpPostedFileBase file5, HttpPostedFileBase file6, HttpPostedFileBase file7, HttpPostedFileBase file8, HttpPostedFileBase file9, HttpPostedFileBase file10, HttpPostedFileBase file11, HttpPostedFileBase file12)
        {
            try
            {
                ATTB obj = new ATTB();

                obj.ListNo = collection["ListNo"].ToString();
                obj.AssetNo = collection["AssetNo"].ToString();
                obj.IDSAP = collection["IDSAP"].ToString();
                obj.ActualAddress = collection["ActualAddress"].ToString();
                obj.BrachCode = collection["BrachCode"].ToString();
                obj.NJOPPBB2017 = collection["NJOPPBB2017"].ToString();
                obj.NJOPPBB2018 = collection["NJOPPBB2018"].ToString();
                obj.ATTBTypeID = collection["ATTBTypeID"].ToString();
                obj.ATTBStatusID = collection["ATTBStatusID"].ToString();
                obj.RegionAssetID = collection["RegionAssetID"].ToString();
                obj.City = collection["City"].ToString();
                obj.OwnershipID = collection["OwnershipID"].ToString();
                obj.OwnershipNo = collection["OwnershipNo"].ToString();
                obj.OwnerName = collection["OwnerName"].ToString();
                obj.ReleaseDateCertificate = collection["ReleaseDateCertificate"].ToString();
                obj.DueDateCertificate = collection["DueDateCertificate"].ToString();
                obj.LandArea = collection["LandArea"].ToString();
                obj.BuildingArea = collection["BuildingArea"].ToString();
                obj.NilaiBukuTanah = collection["NilaiBukuTanah"].ToString();
                obj.NilaiBukuBangunan = collection["NilaiBukuBangunan"].ToString();
                obj.AssetControlID = collection["AssetControl"].ToString();
                if ((collection["AssetControlOther"] != null && collection["AssetControlOther"] != "") )
                {
                    obj.AssetControlID = collection["AssetControlOther"];
                }
                obj.PrimaryUserID = collection["PrimaryUser"].ToString();
                if ((collection["PrimaryUserOther"] != null && collection["PrimaryUserOther"] != ""))
                {
                    obj.PrimaryUserID = collection["PrimaryUserOther"];
                }
                obj.AcquisitionValue = collection["AcquisitionValue"].ToString();
                if (collection["docFileName"] != null && collection["docFileName"] != "")
                {
                    obj.DocFileName = collection["docFileName"];
                }
                obj.ActionSave = "INSERT";
                obj.ATTBID = "0";
                obj.UserName = User.Identity.Name;
                objTax = obj;

                string ErrorMessage = "";
                string fileName = "";
                string path = "";
                string fileType = "";

                #region UploadFile
                //Upload File
                if (file != null && file.ContentLength > 0)
                {
                    fileType = Path.GetExtension(file.FileName);
                    if (fileType.ToUpper().Contains("DOC") || fileType.ToUpper().Contains("XLS") || fileType.ToUpper().Contains("PDF"))
                    {
                            fileName = Path.GetFileName(file.FileName);
                            path = Path.Combine(Server.MapPath("~/content/document/attb/"), fileName);
                            file.SaveAs(path);

                            //insert file data
                            obj.DocFileName = fileName;
                            obj.FolderName = "ATTB";
                            obj.Path = path;
                    }
                    else
                    {
                        ErrorMessage = "File Upload harus dengan format Word / Excel / PDF";
                        // return RedirectToAction("Create", new { ErrorMessage = ErrorMessage, objTax });
                        return await Create(ErrorMessage, objTax);
                    }
                }
                if (file2 != null && file2.ContentLength > 0)
                {
                    fileType = Path.GetExtension(file2.FileName);
                    if (fileType.ToUpper().Contains("DOC") || fileType.ToUpper().Contains("XLS") || fileType.ToUpper().Contains("PDF"))
                    {
                        fileName = Path.GetFileName(file2.FileName);
                        path = Path.Combine(Server.MapPath("~/content/document/attb/"), fileName);
                        file2.SaveAs(path);

                        //insert file data
                        obj.DocFileName2 = fileName;
                        obj.FolderName = "ATTB";
                        obj.Path = path;
                    }
                    else
                    {
                        ErrorMessage = "File Upload harus dengan format Word / Excel / PDF";
                        //return RedirectToAction("Create", new { ErrorMessage = ErrorMessage, objTax = obj });
                        return await Create(ErrorMessage, objTax);
                    }
                }
                if (file3 != null && file3.ContentLength > 0)
                {
                    fileType = Path.GetExtension(file3.FileName);
                    if (fileType.ToUpper().Contains("DOC") || fileType.ToUpper().Contains("XLS") || fileType.ToUpper().Contains("PDF"))
                    {
                        fileName = Path.GetFileName(file3.FileName);
                        path = Path.Combine(Server.MapPath("~/content/document/attb/"), fileName);
                        file3.SaveAs(path);

                        //insert file data
                        obj.DocFileName3 = fileName;
                        obj.FolderName = "ATTB";
                        obj.Path = path;
                    }
                    else
                    {
                        ErrorMessage = "File Upload harus dengan format Word / Excel / PDF";
                        //return RedirectToAction("Create", new { ErrorMessage = ErrorMessage, objTax= obj });
                        return await Create(ErrorMessage, objTax);
                    }
                }
                if (file4 != null && file4.ContentLength > 0)
                {
                    fileType = Path.GetExtension(file4.FileName);
                    if (fileType.ToUpper().Contains("DOC") || fileType.ToUpper().Contains("XLS") || fileType.ToUpper().Contains("PDF"))
                    {
                        fileName = Path.GetFileName(file4.FileName);
                        path = Path.Combine(Server.MapPath("~/content/document/attb/"), fileName);
                        file4.SaveAs(path);

                        //insert file data
                        obj.DocFileName4 = fileName;
                        obj.FolderName = "ATTB";
                        obj.Path = path;
                    }
                    else
                    {
                        ErrorMessage = "File Upload harus dengan format Word / Excel / PDF";
                        //return RedirectToAction("Create", new { ErrorMessage = ErrorMessage,obj });
                        return await Create(ErrorMessage, objTax);
                    }
                }
                if (file5 != null && file5.ContentLength > 0)
                {
                    fileType = Path.GetExtension(file5.FileName);
                    if (fileType.ToUpper().Contains("DOC") || fileType.ToUpper().Contains("XLS") || fileType.ToUpper().Contains("PDF"))
                    {
                        fileName = Path.GetFileName(file5.FileName);
                        path = Path.Combine(Server.MapPath("~/content/document/attb/"), fileName);
                        file5.SaveAs(path);

                        //insert file data
                        obj.DocFileName5 = fileName;
                        obj.FolderName = "ATTB";
                        obj.Path = path;
                    }
                    else
                    {
                        ErrorMessage = "File Upload harus dengan format Word / Excel / PDF";
                        //return RedirectToAction("Create", new { ErrorMessage = ErrorMessage, obj });
                        return await Create(ErrorMessage, objTax);
                    }
                }
                if (file6 != null && file6.ContentLength > 0)
                {
                    fileType = Path.GetExtension(file6.FileName);
                    if (fileType.ToUpper().Contains("DOC") || fileType.ToUpper().Contains("XLS") || fileType.ToUpper().Contains("PDF"))
                    {
                        fileName = Path.GetFileName(file6.FileName);
                        path = Path.Combine(Server.MapPath("~/content/document/attb/"), fileName);
                        file6.SaveAs(path);

                        //insert file data
                        obj.DocFileName6 = fileName;
                        obj.FolderName = "ATTB";
                        obj.Path = path;
                    }
                    else
                    {
                        ErrorMessage = "File Upload harus dengan format Word / Excel / PDF";
                        //return RedirectToAction("Create", new { ErrorMessage = ErrorMessage, obj });
                        return await Create(ErrorMessage, objTax);
                    }
                }
                if (file7 != null && file7.ContentLength > 0)
                {
                    fileType = Path.GetExtension(file7.FileName);
                    if (fileType.ToUpper().Contains("DOC") || fileType.ToUpper().Contains("XLS") || fileType.ToUpper().Contains("PDF"))
                    {
                        fileName = Path.GetFileName(file7.FileName);
                        path = Path.Combine(Server.MapPath("~/content/document/attb/"), fileName);
                        file7.SaveAs(path);

                        //insert file data
                        obj.DocFileName7 = fileName;
                        obj.FolderName = "ATTB";
                        obj.Path = path;
                    }
                    else
                    {
                        ErrorMessage = "File Upload harus dengan format Word / Excel / PDF";
                        //return RedirectToAction("Create", new { ErrorMessage = ErrorMessage, obj });
                        return await Create(ErrorMessage, objTax);
                    }
                }
                if (file8 != null && file8.ContentLength > 0)
                {
                    fileType = Path.GetExtension(file8.FileName);
                    if (fileType.ToUpper().Contains("DOC") || fileType.ToUpper().Contains("XLS") || fileType.ToUpper().Contains("PDF"))
                    {
                        fileName = Path.GetFileName(file8.FileName);
                        path = Path.Combine(Server.MapPath("~/content/document/attb/"), fileName);
                        file8.SaveAs(path);

                        //insert file data
                        obj.DocFileName8 = fileName;
                        obj.FolderName = "ATTB";
                        obj.Path = path;
                    }
                    else
                    {
                        ErrorMessage = "File Upload harus dengan format Word / Excel / PDF";
                        //return RedirectToAction("Create", new { ErrorMessage = ErrorMessage, obj });
                        return await Create(ErrorMessage, objTax);
                    }
                }
                if (file9 != null && file9.ContentLength > 0)
                {
                    fileType = Path.GetExtension(file9.FileName);
                    if (fileType.ToUpper().Contains("DOC") || fileType.ToUpper().Contains("XLS") || fileType.ToUpper().Contains("PDF"))
                    {
                        fileName = Path.GetFileName(file9.FileName);
                        path = Path.Combine(Server.MapPath("~/content/document/attb/"), fileName);
                        file9.SaveAs(path);

                        //insert file data
                        obj.DocFileName9 = fileName;
                        obj.FolderName = "ATTB";
                        obj.Path = path;
                    }
                    else
                    {
                        ErrorMessage = "File Upload harus dengan format Word / Excel / PDF";
                        //return RedirectToAction("Create", new { ErrorMessage = ErrorMessage, obj });
                        return await Create(ErrorMessage, objTax);
                    }
                }
                if (file10 != null && file10.ContentLength > 0)
                {
                    fileType = Path.GetExtension(file10.FileName);
                    if (fileType.ToUpper().Contains("DOC") || fileType.ToUpper().Contains("XLS") || fileType.ToUpper().Contains("PDF"))
                    {
                        fileName = Path.GetFileName(file10.FileName);
                        path = Path.Combine(Server.MapPath("~/content/document/attb/"), fileName);
                        file10.SaveAs(path);

                        //insert file data
                        obj.DocFileName10 = fileName;
                        obj.FolderName = "ATTB";
                        obj.Path = path;
                    }
                    else
                    {
                        ErrorMessage = "File Upload harus dengan format Word / Excel / PDF";
                        // return RedirectToAction("Create", new { ErrorMessage = ErrorMessage, obj });
                        return await Create(ErrorMessage, objTax);
                    }
                }
                if (file11 != null && file11.ContentLength > 0)
                {
                    fileType = Path.GetExtension(file11.FileName);
                    if (fileType.ToUpper().Contains("DOC") || fileType.ToUpper().Contains("XLS") || fileType.ToUpper().Contains("PDF"))
                    {
                        fileName = Path.GetFileName(file11.FileName);
                        path = Path.Combine(Server.MapPath("~/content/document/attb/"), fileName);
                        file11.SaveAs(path);

                        //insert file data
                        obj.DocFileName11 = fileName;
                        obj.FolderName = "ATTB";
                        obj.Path = path;
                    }
                    else
                    {
                        ErrorMessage = "File Upload harus dengan format Word / Excel / PDF";
                        //return RedirectToAction("Create", new { ErrorMessage = ErrorMessage, obj });
                        return await Create(ErrorMessage, objTax);
                    }
                }
                if (file12 != null && file12.ContentLength > 0)
                {
                    fileType = Path.GetExtension(file12.FileName);
                    if (fileType.ToUpper().Contains("DOC") || fileType.ToUpper().Contains("XLS") || fileType.ToUpper().Contains("PDF"))
                    {
                        fileName = Path.GetFileName(file12.FileName);
                        path = Path.Combine(Server.MapPath("~/content/document/attb/"), fileName);
                        file12.SaveAs(path);

                        //insert file data
                        obj.DocFileName12 = fileName;
                        obj.FolderName = "ATTB";
                        obj.Path = path;
                    }
                    else
                    {
                        ErrorMessage = "File Upload harus dengan format Word / Excel / PDF";
                        //return RedirectToAction("Create", new { ErrorMessage = ErrorMessage, obj });
                        return await Create(ErrorMessage, objTax);
                    }
                }
                #endregion

                ATTB obj2 = new ATTB();

                #region cekduplicate
                string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    using (var cmd = new SqlCommand("sp_CheckDuplicateATTB", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ATTBID", "0");
                        cmd.Parameters.AddWithValue("@AssetNo", obj.AssetNo);
                        cmd.Parameters.AddWithValue("@IDSAP", obj.IDSAP);
                        cmd.Parameters.AddWithValue("@ActualAddress", obj.ActualAddress);
                        cmd.Parameters.AddWithValue("@BrachCode", obj.BrachCode);
                        cmd.Parameters.AddWithValue("@NJOPPBB2017", obj.NJOPPBB2017);
                        cmd.Parameters.AddWithValue("@NJOPPBB2018", obj.NJOPPBB2018);
                        cmd.Parameters.AddWithValue("@ATTBTypeID", obj.ATTBTypeID);
                        cmd.Parameters.AddWithValue("@ATTBStatusID", obj.ATTBStatusID);
                        cmd.Parameters.AddWithValue("@RegionAssetID", obj.RegionAssetID);
                        cmd.Parameters.AddWithValue("@City", obj.City);
                        cmd.Parameters.AddWithValue("@OwnershipID", obj.OwnershipID);
                        cmd.Parameters.AddWithValue("@OwnershipNo", obj.OwnershipNo);
                        cmd.Parameters.AddWithValue("@OwnerName", obj.OwnerName);
                        cmd.Parameters.AddWithValue("@ReleaseDateCertificate", obj.ReleaseDateCertificate);
                        cmd.Parameters.AddWithValue("@DueDateCertificate", obj.DueDateCertificate);
                        cmd.Parameters.AddWithValue("@LandArea", obj.LandArea);
                        cmd.Parameters.AddWithValue("@BuildingArea", obj.BuildingArea);
                        cmd.Parameters.AddWithValue("@NilaiBukuTanah", obj.NilaiBukuTanah);
                        cmd.Parameters.AddWithValue("@NilaiBukuBangunan", obj.NilaiBukuBangunan);
                        cmd.Parameters.AddWithValue("@AssetControlID", obj.AssetControlID);
                        cmd.Parameters.AddWithValue("@PrimaryUserID", obj.PrimaryUserID);
                        cmd.Parameters.AddWithValue("@AcquisitionValue", obj.AcquisitionValue);
                        conn.Open();

                        using (var reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                obj2.ATTBID = reader[0].ToString();
                                ErrorMessage = reader[1].ToString();
                            }
                        }
                    }

                }

                if (ErrorMessage != null && ErrorMessage != "")
                {
                    return await Create(ErrorMessage, obj);
                }
                #endregion


                // TODO: Add update logic here
                HttpClient client = new HttpClient();
                    client.BaseAddress = new Uri(Baseurl2);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    //Sending request to find web api REST service resource GetAllArticles using HttpClient  
                    HttpResponseMessage response = await client.PostAsJsonAsync(Properties.Settings.Default.ATTBAPI + "/" + "0", obj);
                    response.EnsureSuccessStatusCode();
                
                
                
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }

        public async Task<List<ATTBStatusModels>> GetListATTBStatus()
        {
            List<ATTBStatusModels> _list = new List<ATTBStatusModels>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl2);
                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.ATTBStatusAPI);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    _list = JsonConvert.DeserializeObject<List<ATTBStatusModels>>(APIResponse);

                }
                //returning the employee list to view  

            }
            return _list;
        }
        public async Task<List<RegionAssetModels>> GetListRegionAsset()
        {
            List<RegionAssetModels> _list = new List<RegionAssetModels>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl2);
                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.RegionAssetAPI);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    _list = JsonConvert.DeserializeObject<List<RegionAssetModels>>(APIResponse);

                }
                //returning the employee list to view  

            }
            List<RegionAssetModels> objInfo = new List<RegionAssetModels>(_list.OrderBy(x => x.RegionAsset));
            //return _list;
            return objInfo;
        }

        public async Task<List<ATTBTypeModels>> GetListATTBType()
        {
            List<ATTBTypeModels> _list = new List<ATTBTypeModels>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl2);
                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.ATTBTypeAPI);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    _list = JsonConvert.DeserializeObject<List<ATTBTypeModels>>(APIResponse);

                }
                //returning the employee list to view  

            }
            return _list;
        }

        public async Task<List<OwnershipModels>> GetListOwnership()
        {
            List<OwnershipModels> _list = new List<OwnershipModels>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl2);
                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.OwnershipAPI);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    _list = JsonConvert.DeserializeObject<List<OwnershipModels>>(APIResponse);

                }
                //returning the employee list to view  

            }
            return _list;
        }

        public async Task<List<AssetControlModels>> GetListAssetControl()
        {
            List<AssetControlModels> _list = new List<AssetControlModels>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl2);
                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.AssetControlAPI);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    _list = JsonConvert.DeserializeObject<List<AssetControlModels>>(APIResponse);

                }
                //returning the list to view  

            }
            return _list;
        }
        public async Task<List<PrimaryUserModels>> GetListPrimaryUser()
        {
            List<PrimaryUserModels> _list = new List<PrimaryUserModels>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl2);
                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.PrimaryUserAPI);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    _list = JsonConvert.DeserializeObject<List<PrimaryUserModels>>(APIResponse);

                }
                //returning the employee list to view  

            }
            return _list;
        }

        public async Task<ActionResult> Update(string id, string ErrorMessage)
        {
            List<ATTB> objInfo = new List<ATTB>();
            if (ErrorMessage != null)
            {
                ViewBag.ErrorMessage = ErrorMessage;
            }
            
            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl2);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllArticles using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.ATTBAPI + "/" + id);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<List<ATTB>>(APIResponse);

                }

                List<ATTBStatusModels> _listATTBStatus = await GetListATTBStatus();
                ViewBag.ListATTBStatus = _listATTBStatus;

                List<OwnershipModels> _listOwnership = await GetListOwnership();
                ViewBag.ListOwnership = _listOwnership;

                List<ATTBTypeModels> _listATTBType = await GetListATTBType();
                ViewBag.ListATTBType = _listATTBType;

                List<RegionAssetModels> _listRegionAsset = await GetListRegionAsset();
                ViewBag.ListRegionAsset = _listRegionAsset;

                List<AssetControlModels> _listAssetControl = await GetListAssetControl();
                ViewBag.ListAssetControl = _listAssetControl;

                List<PrimaryUserModels> _listPrimaryUser = await GetListPrimaryUser();
                ViewBag.ListPrimaryUser = _listPrimaryUser;

                List<LogHistory> _listHistories = GetListHistory(id);
                ViewBag.ListHistory = _listHistories;

                ViewBag.ATTBID = id;

                ATTB objOne = new ATTB();

                foreach (ATTB Obj in objInfo)
                {
                    objOne = Obj;
                }
                objOne.AcquisitionValue = String.Format("{0:n0}", Convert.ToDouble(objOne.AcquisitionValue)).Replace(",", ".");
                //objOne.AssetNo = String.Format("{0:n0}", Convert.ToDouble(objOne.AssetNo)).Replace(",", ".");
                //objOne.BrachCode = String.Format("{0:n0}", Convert.ToDouble(objOne.BrachCode)).Replace(",", ".");
                objOne.BuildingArea = String.Format("{0:n0}", Convert.ToDouble(objOne.BuildingArea)).Replace(",", ".");
                objOne.LandArea = String.Format("{0:n0}", Convert.ToDouble(objOne.LandArea)).Replace(",", ".");
                //objOne.ListNo = String.Format("{0:n0}", Convert.ToDouble(objOne.ListNo)).Replace(",", ".");
                objOne.NilaiBukuBangunan = String.Format("{0:n0}", Convert.ToDouble(objOne.NilaiBukuBangunan)).Replace(",", ".");
                objOne.NilaiBukuTanah = String.Format("{0:n0}", Convert.ToDouble(objOne.NilaiBukuTanah)).Replace(",", ".");
                objOne.NJOPPBB2017 = String.Format("{0:n0}", Convert.ToDouble(objOne.NJOPPBB2017)).Replace(",", ".");
                objOne.NJOPPBB2018 = String.Format("{0:n0}", Convert.ToDouble(objOne.NJOPPBB2018)).Replace(",", ".");
                ViewBag.objInfo = objOne;
                return View(objOne);
            }

        }
        
        [HttpPost]
        public async Task<ActionResult> Update(string id, ATTB obj, FormCollection collection, HttpPostedFileBase file, HttpPostedFileBase file2, HttpPostedFileBase file3, HttpPostedFileBase file4, HttpPostedFileBase file5, HttpPostedFileBase file6, HttpPostedFileBase file7, HttpPostedFileBase file8, HttpPostedFileBase file9, HttpPostedFileBase file10, HttpPostedFileBase file11, HttpPostedFileBase file12)
        {
            try
            {
                ATTB objInfo = new ATTB();

                //Logic to save ATTB
                obj.ListNo = collection["ListNo"].ToString();
                obj.AssetNo = collection["AssetNo"].ToString();
                obj.IDSAP = collection["IDSAP"].ToString();
                obj.ActualAddress = collection["ActualAddress"].ToString();
                obj.BrachCode = collection["BrachCode"].ToString();
                obj.NJOPPBB2017 = collection["NJOPPBB2017"].ToString();
                obj.NJOPPBB2018 = collection["NJOPPBB2018"].ToString();
                obj.ATTBTypeID = collection["ATTBTypeID"].ToString();
                obj.ATTBStatusID = collection["ATTBStatusID"].ToString();
                obj.RegionAssetID = collection["RegionAssetID"].ToString();
                obj.City = collection["City"].ToString();
                obj.OwnershipID = collection["OwnershipID"].ToString();
                obj.OwnershipNo = collection["OwnershipNo"].ToString();
                obj.OwnerName = collection["OwnerName"].ToString();
                obj.ReleaseDateCertificate = collection["ReleaseDateCertificate"].ToString();
                obj.DueDateCertificate = collection["DueDateCertificate"].ToString();
                obj.LandArea = collection["LandArea"].ToString();
                obj.BuildingArea = collection["BuildingArea"].ToString();
                obj.NilaiBukuTanah = collection["NilaiBukuTanah"].ToString();
                obj.NilaiBukuBangunan = collection["NilaiBukuBangunan"].ToString();
                obj.AssetControlID = collection["AssetControl"].ToString();
                if ((collection["AssetControlOther"] != null && collection["AssetControlOther"] != ""))
                {
                    obj.AssetControlID = collection["AssetControlOther"];
                }
                obj.PrimaryUserID = collection["PrimaryUser"].ToString();
                if ((collection["PrimaryUserOther"] != null && collection["PrimaryUserOther"] != ""))
                {
                    obj.PrimaryUserID = collection["PrimaryUserOther"];
                }
                obj.AcquisitionValue = collection["AcquisitionValue"].ToString();
                if (collection["docFileName"] != null && collection["docFileName"] != "")
                {
                    obj.DocFileName = collection["docFileName"];
                }
                obj.ActionSave = "UPDATE";
                obj.ATTBID = id;
                obj.UserName = User.Identity.Name;


                string ErrorMessage = "";
                string fileName = "";
                string path = "";
                string fileType = "";

                #region uploadfile
                //Upload File
                if (file != null && file.ContentLength > 0)
                {
                    fileType = Path.GetExtension(file.FileName);
                    if (fileType.ToUpper().Contains("DOC") || fileType.ToUpper().Contains("XLS") || fileType.ToUpper().Contains("PDF"))
                    {
                        fileName = Path.GetFileName(file.FileName);
                        path = Path.Combine(Server.MapPath("~/content/document/attb/"), fileName);
                        file.SaveAs(path);

                        //insert file data
                        obj.DocFileName = fileName;
                        obj.FolderName = "ATTB";
                        obj.Path = path;
                    }
                    else
                    {
                        ErrorMessage = "File Upload harus dengan format Word / Excel / PDF";
                        return RedirectToAction("Update", new {id, ErrorMessage = ErrorMessage });
                    }
                }

                if (file == null && (collection["filename"].ToString() != null || collection["filename"].ToString() != ""))
                {
                    obj.DocFileName = collection["filename"].ToString();
                    path = Path.Combine(Server.MapPath("~/content/document/attb/"), fileName);
                }

                if (file2 != null && file2.ContentLength > 0)
                {
                    fileType = Path.GetExtension(file2.FileName);
                    if (fileType.ToUpper().Contains("DOC") || fileType.ToUpper().Contains("XLS") || fileType.ToUpper().Contains("PDF"))
                    {
                        fileName = Path.GetFileName(file2.FileName);
                        path = Path.Combine(Server.MapPath("~/content/document/attb/"), fileName);
                        file2.SaveAs(path);

                        //insert file data
                        obj.DocFileName2 = fileName;
                        obj.FolderName = "ATTB";
                        obj.Path = path;
                    }
                    else
                    {
                        ErrorMessage = "File Upload harus dengan format Word / Excel / PDF";
                        return RedirectToAction("Update", new { id, ErrorMessage = ErrorMessage });
                    }
                }
                if (file2 == null && (collection["filename2"].ToString() != null || collection["filename2"].ToString() != ""))
                {
                    obj.DocFileName2 = collection["filename2"].ToString();
                    path = Path.Combine(Server.MapPath("~/content/document/attb/"), fileName);
                }

                if (file3 != null && file3.ContentLength > 0)
                {
                    fileType = Path.GetExtension(file3.FileName);
                    if (fileType.ToUpper().Contains("DOC") || fileType.ToUpper().Contains("XLS") || fileType.ToUpper().Contains("PDF"))
                    {
                        fileName = Path.GetFileName(file3.FileName);
                        path = Path.Combine(Server.MapPath("~/content/document/attb/"), fileName);
                        file3.SaveAs(path);

                        //insert file data
                        obj.DocFileName3 = fileName;
                        obj.FolderName = "ATTB";
                        obj.Path = path;
                    }
                    else
                    {
                        ErrorMessage = "File Upload harus dengan format Word / Excel / PDF";
                        return RedirectToAction("Update", new { id, ErrorMessage = ErrorMessage });
                    }
                }
                if (file3 == null && (collection["filename3"].ToString() != null || collection["filename3"].ToString() != ""))
                {
                    obj.DocFileName3 = collection["filename3"].ToString();
                    path = Path.Combine(Server.MapPath("~/content/document/attb/"), fileName);
                }
                if (file4 != null && file4.ContentLength > 0)
                {
                    fileType = Path.GetExtension(file4.FileName);
                    if (fileType.ToUpper().Contains("DOC") || fileType.ToUpper().Contains("XLS") || fileType.ToUpper().Contains("PDF"))
                    {
                        fileName = Path.GetFileName(file4.FileName);
                        path = Path.Combine(Server.MapPath("~/content/document/attb/"), fileName);
                        file4.SaveAs(path);

                        //insert file data
                        obj.DocFileName4 = fileName;
                        obj.FolderName = "ATTB";
                        obj.Path = path;
                    }
                    else
                    {
                        ErrorMessage = "File Upload harus dengan format Word / Excel / PDF";
                        return RedirectToAction("Update", new { id, ErrorMessage = ErrorMessage });
                    }
                }
                if (file4 == null && (collection["filename4"].ToString() != null || collection["filename4"].ToString() != ""))
                {
                    obj.DocFileName4 = collection["filename4"].ToString();
                    path = Path.Combine(Server.MapPath("~/content/document/attb/"), fileName);
                }
                if (file5 != null && file5.ContentLength > 0)
                {
                    fileType = Path.GetExtension(file5.FileName);
                    if (fileType.ToUpper().Contains("DOC") || fileType.ToUpper().Contains("XLS") || fileType.ToUpper().Contains("PDF"))
                    {
                        fileName = Path.GetFileName(file5.FileName);
                        path = Path.Combine(Server.MapPath("~/content/document/attb/"), fileName);
                        file5.SaveAs(path);

                        //insert file data
                        obj.DocFileName5 = fileName;
                        obj.FolderName = "ATTB";
                        obj.Path = path;
                    }
                    else
                    {
                        ErrorMessage = "File Upload harus dengan format Word / Excel / PDF";
                        return RedirectToAction("Update", new { id, ErrorMessage = ErrorMessage });
                    }
                }
                if (file5 == null && (collection["filename5"].ToString() != null || collection["filename5"].ToString() != ""))
                {
                    obj.DocFileName5 = collection["filename5"].ToString();
                    path = Path.Combine(Server.MapPath("~/content/document/attb/"), fileName);
                }
                if (file6 != null && file6.ContentLength > 0)
                {
                    fileType = Path.GetExtension(file6.FileName);
                    if (fileType.ToUpper().Contains("DOC") || fileType.ToUpper().Contains("XLS") || fileType.ToUpper().Contains("PDF"))
                    {
                        fileName = Path.GetFileName(file6.FileName);
                        path = Path.Combine(Server.MapPath("~/content/document/attb/"), fileName);
                        file6.SaveAs(path);

                        //insert file data
                        obj.DocFileName6 = fileName;
                        obj.FolderName = "ATTB";
                        obj.Path = path;
                    }
                    else
                    {
                        ErrorMessage = "File Upload harus dengan format Word / Excel / PDF";
                        return RedirectToAction("Update", new { id, ErrorMessage = ErrorMessage });
                    }
                }
                if (file6 == null && (collection["filename6"].ToString() != null || collection["filename6"].ToString() != ""))
                {
                    obj.DocFileName6 = collection["filename6"].ToString();
                    path = Path.Combine(Server.MapPath("~/content/document/attb/"), fileName);
                }
                if (file7 != null && file7.ContentLength > 0)
                {
                    fileType = Path.GetExtension(file7.FileName);
                    if (fileType.ToUpper().Contains("DOC") || fileType.ToUpper().Contains("XLS") || fileType.ToUpper().Contains("PDF"))
                    {
                        fileName = Path.GetFileName(file7.FileName);
                        path = Path.Combine(Server.MapPath("~/content/document/attb/"), fileName);
                        file7.SaveAs(path);

                        //insert file data
                        obj.DocFileName7 = fileName;
                        obj.FolderName = "ATTB";
                        obj.Path = path;
                    }
                    else
                    {
                        ErrorMessage = "File Upload harus dengan format Word / Excel / PDF";
                        return RedirectToAction("Update", new { id, ErrorMessage = ErrorMessage });
                    }
                }
                if (file7 == null && (collection["filename7"].ToString() != null || collection["filename7"].ToString() != ""))
                {
                    obj.DocFileName7 = collection["filename7"].ToString();
                    path = Path.Combine(Server.MapPath("~/content/document/attb/"), fileName);
                }
                if (file8 != null && file8.ContentLength > 0)
                {
                    fileType = Path.GetExtension(file8.FileName);
                    if (fileType.ToUpper().Contains("DOC") || fileType.ToUpper().Contains("XLS") || fileType.ToUpper().Contains("PDF"))
                    {
                        fileName = Path.GetFileName(file8.FileName);
                        path = Path.Combine(Server.MapPath("~/content/document/attb/"), fileName);
                        file8.SaveAs(path);

                        //insert file data
                        obj.DocFileName8 = fileName;
                        obj.FolderName = "ATTB";
                        obj.Path = path;
                    }
                    else
                    {
                        ErrorMessage = "File Upload harus dengan format Word / Excel / PDF";
                        return RedirectToAction("Update", new { id, ErrorMessage = ErrorMessage });
                    }
                }
                if (file8 == null && (collection["filename8"].ToString() != null || collection["filename8"].ToString() != ""))
                {
                    obj.DocFileName8 = collection["filename8"].ToString();
                    path = Path.Combine(Server.MapPath("~/content/document/attb/"), fileName);
                }
                if (file9 != null && file9.ContentLength > 0)
                {
                    fileType = Path.GetExtension(file9.FileName);
                    if (fileType.ToUpper().Contains("DOC") || fileType.ToUpper().Contains("XLS") || fileType.ToUpper().Contains("PDF"))
                    {
                        fileName = Path.GetFileName(file9.FileName);
                        path = Path.Combine(Server.MapPath("~/content/document/attb/"), fileName);
                        file9.SaveAs(path);

                        //insert file data
                        obj.DocFileName9 = fileName;
                        obj.FolderName = "ATTB";
                        obj.Path = path;
                    }
                    else
                    {
                        ErrorMessage = "File Upload harus dengan format Word / Excel / PDF";
                        return RedirectToAction("Update", new { id, ErrorMessage = ErrorMessage });
                    }
                }
                if (file9 == null && (collection["filename9"].ToString() != null || collection["filename9"].ToString() != ""))
                {
                    obj.DocFileName9 = collection["filename9"].ToString();
                    path = Path.Combine(Server.MapPath("~/content/document/attb/"), fileName);
                }
                if (file10 != null && file10.ContentLength > 0)
                {
                    fileType = Path.GetExtension(file10.FileName);
                    if (fileType.ToUpper().Contains("DOC") || fileType.ToUpper().Contains("XLS") || fileType.ToUpper().Contains("PDF"))
                    {
                        fileName = Path.GetFileName(file10.FileName);
                        path = Path.Combine(Server.MapPath("~/content/document/attb/"), fileName);
                        file10.SaveAs(path);

                        //insert file data
                        obj.DocFileName10 = fileName;
                        obj.FolderName = "ATTB";
                        obj.Path = path;
                    }
                    else
                    {
                        ErrorMessage = "File Upload harus dengan format Word / Excel / PDF";
                        return RedirectToAction("Update", new { id, ErrorMessage = ErrorMessage });
                    }
                }
                if (file10 == null && (collection["filename10"].ToString() != null || collection["filename10"].ToString() != ""))
                {
                    obj.DocFileName10 = collection["filename10"].ToString();
                    path = Path.Combine(Server.MapPath("~/content/document/attb/"), fileName);
                }
                if (file11 != null && file11.ContentLength > 0)
                {
                    fileType = Path.GetExtension(file11.FileName);
                    if (fileType.ToUpper().Contains("DOC") || fileType.ToUpper().Contains("XLS") || fileType.ToUpper().Contains("PDF"))
                    {
                        fileName = Path.GetFileName(file11.FileName);
                        path = Path.Combine(Server.MapPath("~/content/document/attb/"), fileName);
                        file11.SaveAs(path);

                        //insert file data
                        obj.DocFileName11 = fileName;
                        obj.FolderName = "ATTB";
                        obj.Path = path;
                    }
                    else
                    {
                        ErrorMessage = "File Upload harus dengan format Word / Excel / PDF";
                        return RedirectToAction("Update", new { id, ErrorMessage = ErrorMessage });
                    }
                }
                if (file11 == null && (collection["filename11"].ToString() != null || collection["filename11"].ToString() != ""))
                {
                    obj.DocFileName11 = collection["filename11"].ToString();
                    path = Path.Combine(Server.MapPath("~/content/document/attb/"), fileName);
                }
                if (file12 != null && file12.ContentLength > 0)
                {
                    fileType = Path.GetExtension(file12.FileName);
                    if (fileType.ToUpper().Contains("DOC") || fileType.ToUpper().Contains("XLS") || fileType.ToUpper().Contains("PDF"))
                    {
                        fileName = Path.GetFileName(file12.FileName);
                        path = Path.Combine(Server.MapPath("~/content/document/attb/"), fileName);
                        file12.SaveAs(path);

                        //insert file data
                        obj.DocFileName12 = fileName;
                        obj.FolderName = "ATTB";
                        obj.Path = path;
                    }
                    else
                    {
                        ErrorMessage = "File Upload harus dengan format Word / Excel / PDF";
                        return RedirectToAction("Update", new { id, ErrorMessage = ErrorMessage });
                    }
                }
                if (file12 == null && (collection["filename12"].ToString() != null || collection["filename12"].ToString() != ""))
                {
                    obj.DocFileName12 = collection["filename12"].ToString();
                    path = Path.Combine(Server.MapPath("~/content/document/attb/"), fileName);
                }


                #endregion

                #region cekduplicate
                ATTB obj2 = new ATTB();

                string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    using (var cmd = new SqlCommand("sp_CheckDuplicateATTB", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ATTBID", id);
                        cmd.Parameters.AddWithValue("@AssetNo", obj.AssetNo);
                        cmd.Parameters.AddWithValue("@IDSAP", obj.IDSAP);
                        cmd.Parameters.AddWithValue("@ActualAddress", obj.ActualAddress);
                        cmd.Parameters.AddWithValue("@BrachCode", obj.BrachCode);
                        cmd.Parameters.AddWithValue("@NJOPPBB2017", obj.NJOPPBB2017);
                        cmd.Parameters.AddWithValue("@NJOPPBB2018", obj.NJOPPBB2018);
                        cmd.Parameters.AddWithValue("@ATTBTypeID", obj.ATTBTypeID);
                        cmd.Parameters.AddWithValue("@ATTBStatusID", obj.ATTBStatusID);
                        cmd.Parameters.AddWithValue("@RegionAssetID", obj.RegionAssetID);
                        cmd.Parameters.AddWithValue("@City", obj.City);
                        cmd.Parameters.AddWithValue("@OwnershipID", obj.OwnershipID);
                        cmd.Parameters.AddWithValue("@OwnershipNo", obj.OwnershipNo);
                        cmd.Parameters.AddWithValue("@OwnerName", obj.OwnerName);
                        cmd.Parameters.AddWithValue("@ReleaseDateCertificate", obj.ReleaseDateCertificate);
                        cmd.Parameters.AddWithValue("@DueDateCertificate", obj.DueDateCertificate);
                        cmd.Parameters.AddWithValue("@LandArea", obj.LandArea);
                        cmd.Parameters.AddWithValue("@BuildingArea", obj.BuildingArea);
                        cmd.Parameters.AddWithValue("@NilaiBukuTanah", obj.NilaiBukuTanah);
                        cmd.Parameters.AddWithValue("@NilaiBukuBangunan", obj.NilaiBukuBangunan);
                        cmd.Parameters.AddWithValue("@AssetControlID", obj.AssetControlID);
                        cmd.Parameters.AddWithValue("@PrimaryUserID", obj.PrimaryUserID);
                        cmd.Parameters.AddWithValue("@AcquisitionValue", obj.AcquisitionValue);
                        conn.Open();

                        using (var reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                obj2.ATTBID = reader[0].ToString();
                                ErrorMessage = reader[1].ToString();
                            }
                        }
                    }

                }

                if (ErrorMessage != null && ErrorMessage != "")
                {
                    return await Update(id,ErrorMessage);
                }
                #endregion


                // TODO: Add update logic here
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(Baseurl2);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                
                
                //Get data Tax
                //Sending request to find web api REST service resource GetAllArticles using HttpClient  
                //HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.TaxAPI + "/" + id);

                //Checking the response is successful or not which is sent using HttpClient  
                //if (Res.IsSuccessStatusCode)
                //{
                //    //Storing the response details recieved from web api   
                //    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                //    //Deserializing the response recieved from web api and storing into the Employee list  
                //    objInfo = JsonConvert.DeserializeObject<Tax>(APIResponse);

                //}

                // TODO: Add update logic here
                HttpResponseMessage response = await client.PutAsJsonAsync(Properties.Settings.Default.ATTBAPI + "/" + id, obj);
                response.EnsureSuccessStatusCode();
                
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }

        public List<LogHistory> GetListHistory(string id)
        {
            List<LogHistory> _list = new List<LogHistory>();
            

            string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(connectionstring))
            {
                using (var cmd = new SqlCommand("sp_ViewHistoryLog", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@TableID", id);
                    cmd.Parameters.AddWithValue("@TableName", "ATTB");
                    conn.Open();

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            LogHistory obj = new LogHistory();
                            obj.LogID = reader[0].ToString();
                            obj.TableName = reader[1].ToString();
                            obj.TableID = reader[2].ToString();
                            obj.Action = reader[3].ToString();
                            obj.UpdatedDate = reader[4].ToString();
                            obj.UpdatedBy = reader[5].ToString();
                            _list.Add(obj);
                        }

                        
                    }
                }

            }

            return _list;
        }

        public PartialViewResult PartialATTB(ATTB obj, List<ATTBStatusModels> _listATTBStatus, List<OwnershipModels> _listOwnership, List<ATTBTypeModels> _listATTBType, List<RegionAssetModels> _listRegionAsset, List<AssetControlModels> _listAssetControl, List<PrimaryUserModels> _listPrimaryUser)
        {
            
            ViewBag.ListATTBStatus = _listATTBStatus;
            ViewBag.ListOwnership = _listOwnership;
            ViewBag.ListATTBType = _listATTBType;
            ViewBag.ListRegionAsset = _listRegionAsset;
            ViewBag.ListAssetControl = _listAssetControl;
            ViewBag.ListPrimaryUser = _listPrimaryUser;

            ATTB objInfo = obj;
            if (obj == null)
            {
                obj = new ATTB();
            }
            return PartialView("PartialATTB", obj);
        }

        public PartialViewResult PartialHistory()
        {
            return PartialView("PartialHistory", new List<LogHistory>());
        }


        //for view only
        public async Task<ActionResult> View(string id)
        {
            List<ATTB> objInfo = new List<ATTB>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl2);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllArticles using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.ATTBAPI + "/" + id);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<List<ATTB>>(APIResponse);

                }
                List<ATTBStatusModels> _listATTBStatus = await GetListATTBStatus();
                ViewBag.ListATTBStatus = _listATTBStatus;

                List<OwnershipModels> _listOwnership = await GetListOwnership();
                ViewBag.ListOwnership = _listOwnership;

                List<ATTBTypeModels> _listATTBType = await GetListATTBType();
                ViewBag.ListATTBType = _listATTBType;

                List<RegionAssetModels> _listRegionAsset = await GetListRegionAsset();
                ViewBag.ListRegionAsset = _listRegionAsset;

                List<AssetControlModels> _listAssetControl = await GetListAssetControl();
                ViewBag.ListAssetControl = _listAssetControl;

                List<PrimaryUserModels> _listPrimaryUser = await GetListPrimaryUser();
                ViewBag.LisPrimaryUser = _listPrimaryUser;

                List<LogHistory> _listHistories = GetListHistory(id);
                ViewBag.ListHistory = _listHistories;

                ATTB objOne = new ATTB();
                foreach (ATTB Obj in objInfo)
                {
                    objOne = Obj;
                }
                objOne.AcquisitionValue = String.Format("{0:n0}", Convert.ToDouble(objOne.AcquisitionValue)).Replace(",", ".");
                //objOne.AssetNo = String.Format("{0:n0}", Convert.ToDouble(objOne.AssetNo)).Replace(",", ".");
                //objOne.BrachCode = String.Format("{0:n0}", Convert.ToDouble(objOne.BrachCode)).Replace(",", ".");
                objOne.BuildingArea = String.Format("{0:n0}", Convert.ToDouble(objOne.BuildingArea)).Replace(",", ".");
                objOne.LandArea = String.Format("{0:n0}", Convert.ToDouble(objOne.LandArea)).Replace(",", ".");
                //objOne.ListNo = String.Format("{0:n0}", Convert.ToDouble(objOne.ListNo)).Replace(",", ".");
                objOne.NilaiBukuBangunan = String.Format("{0:n0}", Convert.ToDouble(objOne.NilaiBukuBangunan)).Replace(",", ".");
                objOne.NilaiBukuTanah = String.Format("{0:n0}", Convert.ToDouble(objOne.NilaiBukuTanah)).Replace(",", ".");
                objOne.NJOPPBB2017 = String.Format("{0:n0}", Convert.ToDouble(objOne.NJOPPBB2017)).Replace(",", ".");
                objOne.NJOPPBB2018 = String.Format("{0:n0}", Convert.ToDouble(objOne.NJOPPBB2018)).Replace(",", ".");
                ViewBag.ATTBID = id;
                ViewBag.objInfo = objOne;
                return View(objOne);
            }

        }

        public PartialViewResult PartialATTBView(ATTB obj, List<ATTBStatusModels> _listATTBStatus, List<OwnershipModels> _listOwnership, List<ATTBTypeModels> _listATTBType, List<RegionAssetModels> _listRegionAsset, List<AssetControlModels> _listAssetControl, List<PrimaryUserModels> _listPrimaryUser)
        {

            ViewBag.ListATTBStatus = _listATTBStatus;
            ViewBag.ListOwnership = _listOwnership;
            ViewBag.ListATTBType = _listATTBType;
            ViewBag.ListRegionAsset = _listRegionAsset;
            ViewBag.ListAssetControl = _listAssetControl;
            ViewBag.ListPrimaryUser = _listPrimaryUser;

            ATTB objInfo = obj;
            if (obj == null)
            {
                obj = new ATTB();
            }
            return PartialView("PartialATTBView", obj);
        }

        public async Task<ActionResult> Duplicate(int id)
        {
            try
            {

                // TODO: Add update logic here
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(Baseurl2);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                // TODO: Add update logic here
                HttpResponseMessage response = await client.PostAsJsonAsync(Properties.Settings.Default.DuplicateATTBAPI + "/" + id+ "?UserName=" + User.Identity.Name.ToString(),id);
                response.EnsureSuccessStatusCode();
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }

        public async Task<ActionResult> Duplicate2(string id, string ErrorMessage)
        {
            List<ATTB> objInfo = new List<ATTB>();
            if (ErrorMessage != null)
            {
                ViewBag.ErrorMessage = ErrorMessage;
            }

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl2);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllArticles using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.ATTBAPI + "/" + id);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<List<ATTB>>(APIResponse);

                }

                List<ATTBStatusModels> _listATTBStatus = await GetListATTBStatus();
                ViewBag.ListATTBStatus = _listATTBStatus;

                List<OwnershipModels> _listOwnership = await GetListOwnership();
                ViewBag.ListOwnership = _listOwnership;

                List<ATTBTypeModels> _listATTBType = await GetListATTBType();
                ViewBag.ListATTBType = _listATTBType;

                List<RegionAssetModels> _listRegionAsset = await GetListRegionAsset();
                ViewBag.ListRegionAsset = _listRegionAsset;

                List<AssetControlModels> _listAssetControl = await GetListAssetControl();
                ViewBag.ListAssetControl = _listAssetControl;

                List<PrimaryUserModels> _listPrimaryUser = await GetListPrimaryUser();
                ViewBag.ListPrimaryUser = _listPrimaryUser;

                List<LogHistory> _listHistories = GetListHistory(id);
                ViewBag.ListHistory = _listHistories;

                ViewBag.ATTBID = id;

                ATTB objOne = new ATTB();

                foreach (ATTB Obj in objInfo)
                {
                    objOne = Obj;
                }
                objOne.AcquisitionValue = String.Format("{0:n0}", Convert.ToDouble(objOne.AcquisitionValue)).Replace(",", ".");
                //objOne.AssetNo = String.Format("{0:n0}", Convert.ToDouble(objOne.AssetNo)).Replace(",", ".");
                //objOne.BrachCode = String.Format("{0:n0}", Convert.ToDouble(objOne.BrachCode)).Replace(",", ".");
                objOne.BuildingArea = String.Format("{0:n0}", Convert.ToDouble(objOne.BuildingArea)).Replace(",", ".");
                objOne.LandArea = String.Format("{0:n0}", Convert.ToDouble(objOne.LandArea)).Replace(",", ".");
                //objOne.ListNo = String.Format("{0:n0}", Convert.ToDouble(objOne.ListNo)).Replace(",", ".");
                objOne.NilaiBukuBangunan = String.Format("{0:n0}", Convert.ToDouble(objOne.NilaiBukuBangunan)).Replace(",", ".");
                objOne.NilaiBukuTanah = String.Format("{0:n0}", Convert.ToDouble(objOne.NilaiBukuTanah)).Replace(",", ".");
                objOne.NJOPPBB2017 = String.Format("{0:n0}", Convert.ToDouble(objOne.NJOPPBB2017)).Replace(",", ".");
                objOne.NJOPPBB2018 = String.Format("{0:n0}", Convert.ToDouble(objOne.NJOPPBB2018)).Replace(",", ".");
                objOne.DocFileName = "";
                objOne.DocFileName2 = "";
                objOne.DocFileName3 = "";
                objOne.DocFileName4 = "";
                objOne.DocFileName5 = "";
                objOne.DocFileName6 = "";
                objOne.DocFileName7 = "";
                objOne.DocFileName8 = "";
                objOne.DocFileName9 = "";
                objOne.DocFileName10 = "";
                objOne.DocFileName11 = "";
                objOne.DocFileName12 = "";
                ViewBag.objInfo = objOne;
                return View(objOne);
            }

        }

        [HttpPost]
        public async Task<ActionResult> Duplicate2(ATTB objATTB, FormCollection collection, HttpPostedFileBase file, HttpPostedFileBase file2, HttpPostedFileBase file3, HttpPostedFileBase file4, HttpPostedFileBase file5, HttpPostedFileBase file6, HttpPostedFileBase file7, HttpPostedFileBase file8, HttpPostedFileBase file9, HttpPostedFileBase file10, HttpPostedFileBase file11, HttpPostedFileBase file12)
        {
            try
            {
                ATTB obj = new ATTB();

                obj.ListNo = collection["ListNo"].ToString();
                obj.AssetNo = collection["AssetNo"].ToString();
                obj.IDSAP = collection["IDSAP"].ToString();
                obj.ActualAddress = collection["ActualAddress"].ToString();
                obj.BrachCode = collection["BrachCode"].ToString();
                obj.NJOPPBB2017 = collection["NJOPPBB2017"].ToString();
                obj.NJOPPBB2018 = collection["NJOPPBB2018"].ToString();
                obj.ATTBTypeID = collection["ATTBTypeID"].ToString();
                obj.ATTBStatusID = collection["ATTBStatusID"].ToString();
                obj.RegionAssetID = collection["RegionAssetID"].ToString();
                obj.City = collection["City"].ToString();
                obj.OwnershipID = collection["OwnershipID"].ToString();
                obj.OwnershipNo = collection["OwnershipNo"].ToString();
                obj.OwnerName = collection["OwnerName"].ToString();
                obj.ReleaseDateCertificate = collection["ReleaseDateCertificate"].ToString();
                obj.DueDateCertificate = collection["DueDateCertificate"].ToString();
                obj.LandArea = collection["LandArea"].ToString();
                obj.BuildingArea = collection["BuildingArea"].ToString();
                obj.NilaiBukuTanah = collection["NilaiBukuTanah"].ToString();
                obj.NilaiBukuBangunan = collection["NilaiBukuBangunan"].ToString();
                obj.AssetControlID = collection["AssetControl"].ToString();
                if ((collection["AssetControlOther"] != null && collection["AssetControlOther"] != ""))
                {
                    obj.AssetControlID = collection["AssetControlOther"];
                }
                obj.PrimaryUserID = collection["PrimaryUser"].ToString();
                if ((collection["PrimaryUserOther"] != null && collection["PrimaryUserOther"] != ""))
                {
                    obj.PrimaryUserID = collection["PrimaryUserOther"];
                }
                obj.AcquisitionValue = collection["AcquisitionValue"].ToString();
                if (collection["docFileName"] != null && collection["docFileName"] != "")
                {
                    obj.DocFileName = collection["docFileName"];
                }
                obj.ActionSave = "INSERT";
                obj.ATTBID = "0";
                obj.UserName = User.Identity.Name;

                string ErrorMessage = "";
                string fileName = "";
                string path = "";
                string fileType = "";

                #region UploadFile
                //Upload File
                if (file != null && file.ContentLength > 0)
                {
                    fileType = Path.GetExtension(file.FileName);
                    if (fileType.ToUpper().Contains("DOC") || fileType.ToUpper().Contains("XLS") || fileType.ToUpper().Contains("PDF"))
                    {
                        fileName = Path.GetFileName(file.FileName);
                        path = Path.Combine(Server.MapPath("~/content/document/attb/"), fileName);
                        file.SaveAs(path);

                        //insert file data
                        obj.DocFileName = fileName;
                        obj.FolderName = "ATTB";
                        obj.Path = path;
                    }
                    else
                    {
                        ErrorMessage = "File Upload harus dengan format Word / Excel / PDF";
                        return RedirectToAction("Duplicate2", new { id= objATTB.ATTBID, ErrorMessage = ErrorMessage });
                    }
                }
                if (file2 != null && file2.ContentLength > 0)
                {
                    fileType = Path.GetExtension(file2.FileName);
                    if (fileType.ToUpper().Contains("DOC") || fileType.ToUpper().Contains("XLS") || fileType.ToUpper().Contains("PDF"))
                    {
                        fileName = Path.GetFileName(file2.FileName);
                        path = Path.Combine(Server.MapPath("~/content/document/attb/"), fileName);
                        file2.SaveAs(path);

                        //insert file data
                        obj.DocFileName2 = fileName;
                        obj.FolderName = "ATTB";
                        obj.Path = path;
                    }
                    else
                    {
                        ErrorMessage = "File Upload harus dengan format Word / Excel / PDF";
                        return RedirectToAction("Duplicate2", new { id = objATTB.ATTBID, ErrorMessage = ErrorMessage });
                    }
                }
                if (file3 != null && file3.ContentLength > 0)
                {
                    fileType = Path.GetExtension(file3.FileName);
                    if (fileType.ToUpper().Contains("DOC") || fileType.ToUpper().Contains("XLS") || fileType.ToUpper().Contains("PDF"))
                    {
                        fileName = Path.GetFileName(file3.FileName);
                        path = Path.Combine(Server.MapPath("~/content/document/attb/"), fileName);
                        file3.SaveAs(path);

                        //insert file data
                        obj.DocFileName3 = fileName;
                        obj.FolderName = "ATTB";
                        obj.Path = path;
                    }
                    else
                    {
                        ErrorMessage = "File Upload harus dengan format Word / Excel / PDF";
                        return RedirectToAction("Duplicate2", new { id = objATTB.ATTBID, ErrorMessage = ErrorMessage });
                    }
                }
                if (file4 != null && file4.ContentLength > 0)
                {
                    fileType = Path.GetExtension(file4.FileName);
                    if (fileType.ToUpper().Contains("DOC") || fileType.ToUpper().Contains("XLS") || fileType.ToUpper().Contains("PDF"))
                    {
                        fileName = Path.GetFileName(file4.FileName);
                        path = Path.Combine(Server.MapPath("~/content/document/attb/"), fileName);
                        file4.SaveAs(path);

                        //insert file data
                        obj.DocFileName4 = fileName;
                        obj.FolderName = "ATTB";
                        obj.Path = path;
                    }
                    else
                    {
                        ErrorMessage = "File Upload harus dengan format Word / Excel / PDF";
                        return RedirectToAction("Duplicate2", new { id = objATTB.ATTBID, ErrorMessage = ErrorMessage });
                    }
                }
                if (file5 != null && file5.ContentLength > 0)
                {
                    fileType = Path.GetExtension(file5.FileName);
                    if (fileType.ToUpper().Contains("DOC") || fileType.ToUpper().Contains("XLS") || fileType.ToUpper().Contains("PDF"))
                    {
                        fileName = Path.GetFileName(file5.FileName);
                        path = Path.Combine(Server.MapPath("~/content/document/attb/"), fileName);
                        file5.SaveAs(path);

                        //insert file data
                        obj.DocFileName5 = fileName;
                        obj.FolderName = "ATTB";
                        obj.Path = path;
                    }
                    else
                    {
                        ErrorMessage = "File Upload harus dengan format Word / Excel / PDF";
                        return RedirectToAction("Duplicate2", new { id = objATTB.ATTBID, ErrorMessage = ErrorMessage });
                    }
                }
                if (file6 != null && file6.ContentLength > 0)
                {
                    fileType = Path.GetExtension(file6.FileName);
                    if (fileType.ToUpper().Contains("DOC") || fileType.ToUpper().Contains("XLS") || fileType.ToUpper().Contains("PDF"))
                    {
                        fileName = Path.GetFileName(file6.FileName);
                        path = Path.Combine(Server.MapPath("~/content/document/attb/"), fileName);
                        file6.SaveAs(path);

                        //insert file data
                        obj.DocFileName6 = fileName;
                        obj.FolderName = "ATTB";
                        obj.Path = path;
                    }
                    else
                    {
                        ErrorMessage = "File Upload harus dengan format Word / Excel / PDF";
                        return RedirectToAction("Duplicate2", new { id = objATTB.ATTBID, ErrorMessage = ErrorMessage });
                    }
                }
                if (file7 != null && file7.ContentLength > 0)
                {
                    fileType = Path.GetExtension(file7.FileName);
                    if (fileType.ToUpper().Contains("DOC") || fileType.ToUpper().Contains("XLS") || fileType.ToUpper().Contains("PDF"))
                    {
                        fileName = Path.GetFileName(file7.FileName);
                        path = Path.Combine(Server.MapPath("~/content/document/attb/"), fileName);
                        file7.SaveAs(path);

                        //insert file data
                        obj.DocFileName7 = fileName;
                        obj.FolderName = "ATTB";
                        obj.Path = path;
                    }
                    else
                    {
                        ErrorMessage = "File Upload harus dengan format Word / Excel / PDF";
                        return RedirectToAction("Duplicate2", new { id = objATTB.ATTBID, ErrorMessage = ErrorMessage });
                    }
                }
                if (file8 != null && file8.ContentLength > 0)
                {
                    fileType = Path.GetExtension(file8.FileName);
                    if (fileType.ToUpper().Contains("DOC") || fileType.ToUpper().Contains("XLS") || fileType.ToUpper().Contains("PDF"))
                    {
                        fileName = Path.GetFileName(file8.FileName);
                        path = Path.Combine(Server.MapPath("~/content/document/attb/"), fileName);
                        file8.SaveAs(path);

                        //insert file data
                        obj.DocFileName8 = fileName;
                        obj.FolderName = "ATTB";
                        obj.Path = path;
                    }
                    else
                    {
                        ErrorMessage = "File Upload harus dengan format Word / Excel / PDF";
                        return RedirectToAction("Duplicate2", new { id = objATTB.ATTBID, ErrorMessage = ErrorMessage });
                    }
                }
                if (file9 != null && file9.ContentLength > 0)
                {
                    fileType = Path.GetExtension(file9.FileName);
                    if (fileType.ToUpper().Contains("DOC") || fileType.ToUpper().Contains("XLS") || fileType.ToUpper().Contains("PDF"))
                    {
                        fileName = Path.GetFileName(file9.FileName);
                        path = Path.Combine(Server.MapPath("~/content/document/attb/"), fileName);
                        file9.SaveAs(path);

                        //insert file data
                        obj.DocFileName9 = fileName;
                        obj.FolderName = "ATTB";
                        obj.Path = path;
                    }
                    else
                    {
                        ErrorMessage = "File Upload harus dengan format Word / Excel / PDF";
                        return RedirectToAction("Duplicate2", new { id = objATTB.ATTBID, ErrorMessage = ErrorMessage });
                    }
                }
                if (file10 != null && file10.ContentLength > 0)
                {
                    fileType = Path.GetExtension(file10.FileName);
                    if (fileType.ToUpper().Contains("DOC") || fileType.ToUpper().Contains("XLS") || fileType.ToUpper().Contains("PDF"))
                    {
                        fileName = Path.GetFileName(file10.FileName);
                        path = Path.Combine(Server.MapPath("~/content/document/attb/"), fileName);
                        file10.SaveAs(path);

                        //insert file data
                        obj.DocFileName10 = fileName;
                        obj.FolderName = "ATTB";
                        obj.Path = path;
                    }
                    else
                    {
                        ErrorMessage = "File Upload harus dengan format Word / Excel / PDF";
                        return RedirectToAction("Duplicate2", new { id = objATTB.ATTBID, ErrorMessage = ErrorMessage });
                    }
                }
                if (file11 != null && file11.ContentLength > 0)
                {
                    fileType = Path.GetExtension(file11.FileName);
                    if (fileType.ToUpper().Contains("DOC") || fileType.ToUpper().Contains("XLS") || fileType.ToUpper().Contains("PDF"))
                    {
                        fileName = Path.GetFileName(file11.FileName);
                        path = Path.Combine(Server.MapPath("~/content/document/attb/"), fileName);
                        file11.SaveAs(path);

                        //insert file data
                        obj.DocFileName11 = fileName;
                        obj.FolderName = "ATTB";
                        obj.Path = path;
                    }
                    else
                    {
                        ErrorMessage = "File Upload harus dengan format Word / Excel / PDF";
                        return RedirectToAction("Duplicate2", new { id = objATTB.ATTBID, ErrorMessage = ErrorMessage });
                    }
                }
                if (file12 != null && file12.ContentLength > 0)
                {
                    fileType = Path.GetExtension(file12.FileName);
                    if (fileType.ToUpper().Contains("DOC") || fileType.ToUpper().Contains("XLS") || fileType.ToUpper().Contains("PDF"))
                    {
                        fileName = Path.GetFileName(file12.FileName);
                        path = Path.Combine(Server.MapPath("~/content/document/attb/"), fileName);
                        file12.SaveAs(path);

                        //insert file data
                        obj.DocFileName12 = fileName;
                        obj.FolderName = "ATTB";
                        obj.Path = path;
                    }
                    else
                    {
                        ErrorMessage = "File Upload harus dengan format Word / Excel / PDF";
                        return RedirectToAction("Duplicate2", new { id = objATTB.ATTBID, ErrorMessage = ErrorMessage });
                    }
                }
                #endregion

                ATTB obj2 = new ATTB();

                #region cekduplicate
                string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    using (var cmd = new SqlCommand("sp_CheckDuplicateATTB", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ATTBID", "0");
                        cmd.Parameters.AddWithValue("@AssetNo", obj.AssetNo);
                        cmd.Parameters.AddWithValue("@IDSAP", obj.IDSAP);
                        cmd.Parameters.AddWithValue("@ActualAddress", obj.ActualAddress);
                        cmd.Parameters.AddWithValue("@BrachCode", obj.BrachCode);
                        cmd.Parameters.AddWithValue("@NJOPPBB2017", obj.NJOPPBB2017);
                        cmd.Parameters.AddWithValue("@NJOPPBB2018", obj.NJOPPBB2018);
                        cmd.Parameters.AddWithValue("@ATTBTypeID", obj.ATTBTypeID);
                        cmd.Parameters.AddWithValue("@ATTBStatusID", obj.ATTBStatusID);
                        cmd.Parameters.AddWithValue("@RegionAssetID", obj.RegionAssetID);
                        cmd.Parameters.AddWithValue("@City", obj.City);
                        cmd.Parameters.AddWithValue("@OwnershipID", obj.OwnershipID);
                        cmd.Parameters.AddWithValue("@OwnershipNo", obj.OwnershipNo);
                        cmd.Parameters.AddWithValue("@OwnerName", obj.OwnerName);
                        cmd.Parameters.AddWithValue("@ReleaseDateCertificate", obj.ReleaseDateCertificate);
                        cmd.Parameters.AddWithValue("@DueDateCertificate", obj.DueDateCertificate);
                        cmd.Parameters.AddWithValue("@LandArea", obj.LandArea);
                        cmd.Parameters.AddWithValue("@BuildingArea", obj.BuildingArea);
                        cmd.Parameters.AddWithValue("@NilaiBukuTanah", obj.NilaiBukuTanah);
                        cmd.Parameters.AddWithValue("@NilaiBukuBangunan", obj.NilaiBukuBangunan);
                        cmd.Parameters.AddWithValue("@AssetControlID", obj.AssetControlID);
                        cmd.Parameters.AddWithValue("@PrimaryUserID", obj.PrimaryUserID);
                        cmd.Parameters.AddWithValue("@AcquisitionValue", obj.AcquisitionValue);
                        conn.Open();

                        using (var reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                obj2.ATTBID = reader[0].ToString();
                                ErrorMessage = reader[1].ToString();
                            }
                        }
                    }

                }

                if (ErrorMessage != null && ErrorMessage != "")
                {
                    return await Duplicate2(objATTB.ATTBID, ErrorMessage);
                }
                #endregion


                // TODO: Add update logic here
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(Baseurl2);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllArticles using HttpClient  
                HttpResponseMessage response = await client.PostAsJsonAsync(Properties.Settings.Default.ATTBAPI + "/" + "0", obj);
                response.EnsureSuccessStatusCode();



                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }
        public async Task<ActionResult> Delete(int id)
        {
            List<ATTB> objInfo = new List<ATTB>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl2);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllArticles using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.ATTBAPI + "/" + id);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<List<ATTB>>(APIResponse);

                }
                List<ATTBStatusModels> _listATTBStatus = await GetListATTBStatus();
                ViewBag.ListATTBStatus = _listATTBStatus;

                List<OwnershipModels> _listOwnership = await GetListOwnership();
                ViewBag.ListOwnership = _listOwnership;

                List<ATTBTypeModels> _listATTBType = await GetListATTBType();
                ViewBag.ListATTBType = _listATTBType;

                List<RegionAssetModels> _listRegionAsset = await GetListRegionAsset();
                ViewBag.ListRegionAsset = _listRegionAsset;

                List<AssetControlModels> _listAssetControl = await GetListAssetControl();
                ViewBag.ListAssetControl = _listAssetControl;

                List<PrimaryUserModels> _listPrimaryUser = await GetListPrimaryUser();
                ViewBag.ListPrimaryUser = _listPrimaryUser;

                List<LogHistory> _listHistories = GetListHistory(id.ToString());
                ViewBag.ListHistory = _listHistories;

                ATTB objOne = new ATTB();
                foreach (ATTB Obj in objInfo)
                {
                    objOne = Obj;
                }

                ViewBag.ATTBID = id;
                ViewBag.objInfo = objOne;
                return View(objOne);
            }
        }

        // POST: CRUD/Delete/5
        [HttpPost]
        public async Task<ActionResult> Delete(int id, ATTB obj)
        {
            // TODO: Add delete logic here
            try
            {

                // TODO: Add update logic here
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(Baseurl2);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                // TODO: Add update logic here
                HttpResponseMessage response = await client.DeleteAsync(Properties.Settings.Default.ATTBAPI + "/" + id + "?UserName=" + User.Identity.Name.ToString());
                response.EnsureSuccessStatusCode();
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }

        public static string MakeValidFileName(string name)
        {
            string invalidChars = Regex.Escape(new string(System.IO.Path.GetInvalidFileNameChars()));
            string invalidReStr = string.Format(@"[{0}]+", invalidChars);
            string replace = Regex.Replace(name, invalidReStr, "_").Replace(";", "").Replace(",", "");
            return replace;
        }

        public async Task<FileResult> DownloadExcel2()
        {

            string filename = "ATTBData.xls";
            string path = Server.MapPath("~/Content/Download/");
            string fullPath = Path.Combine(Server.MapPath("~/Content/Download/"), filename);
            

            Workbook wb = new Workbook();
            Worksheet ws = new Worksheet("Data ATTB");
            for (int c = 0; c <= 30; c++) // this will aply it form col 1 to 10
            {
                ws.ColumnWidths[c] = 22;
                ws.Cells[1, c].Bold = true;

            }
            ws.Cells[0, 0] = "SUMMARY";
            ws.Cells[1, 0] = "No Urut";
            ws.Cells[1, 1] = "No Titik Aset";
            ws.Cells[1, 2] = "ID SAP";
            ws.Cells[1, 3] = "Alamat Aktual";
            ws.Cells[1, 4] = "Kode Cabang";
            ws.Cells[1, 5] = "NJOP  PBB 2017 (Rp)";
            ws.Cells[1, 6] = "NJOP PBB 2018 (Rp)";
            ws.Cells[1, 7] = "Jenis";
            ws.Cells[1, 8] = "Status";
            ws.Cells[1, 9] = "Region";
            ws.Cells[1, 10] = "Kota";
            ws.Cells[1, 11] = "Bukti Kepemilikan";
            ws.Cells[1, 12] = "No Bukti";
            ws.Cells[1, 13] = "Nama Pemegang Hak ";
            ws.Cells[1, 14] = "Tgl Penerbitan Sertifikat";
            ws.Cells[1, 15] = "Tgl Jatuh Tempo Sertifikat";
            ws.Cells[1, 16] = "Luas Tanah Sertifikat";
            ws.Cells[1, 17] = "Luas Bangunan (PBB)";
            ws.Cells[1, 18] = "Nilai Buku Tanah (Rp)";
            ws.Cells[1, 19] = "Nilai Buku Bangunan (Rp)";
            ws.Cells[1, 20] = "Penguasaan";
            ws.Cells[1, 21] = "Pengguna Utama";
            ws.Cells[1, 22] = "Nilai Perolehan (Rp)";
            ws.Cells[1, 23] = "Updated By";
            ws.Cells[1, 24] = "Updated Date";

            ws.Cells[0, 0].Bold = true;
            for (int c = 0; c <= 24; c++) // this will aply it form col 1 to 25
            {
                ws.Cells[1, c].Bold = true;
                ws.Cells[1, c].Border =  CellBorder.All;
                

            }
            /*
            ws.Cells[1, 0].Bold = true;
            ws.Cells[1, 1].Bold = true;
            ws.Cells[1, 2].Bold = true;
            ws.Cells[1, 3].Bold = true;
            ws.Cells[1, 4].Bold = true;
            ws.Cells[1, 5].Bold = true;
            ws.Cells[1, 6].Bold = true;
            ws.Cells[1, 7].Bold = true;
            ws.Cells[1, 8].Bold = true;
            ws.Cells[1, 9].Bold = true;
            ws.Cells[1, 10].Bold = true;
            ws.Cells[1, 11].Bold = true;
            ws.Cells[1, 12].Bold = true;
            ws.Cells[1, 13].Bold = true;
            ws.Cells[1, 14].Bold = true;
            ws.Cells[1, 15].Bold = true;
            ws.Cells[1, 16].Bold = true;
            ws.Cells[1, 17].Bold = true;
            ws.Cells[1, 18].Bold = true;
            ws.Cells[1, 19].Bold = true;
            ws.Cells[1, 20].Bold = true;
            ws.Cells[1, 21].Bold = true;
            ws.Cells[1, 22].Bold = true;
            ws.Cells[1, 23].Bold = true;
            ws.Cells[1, 24].Bold = true;
            ws.Cells[1, 25].Bold = true;
            ws.Cells[1, 26].Bold = true;
            ws.Cells[1, 27].Bold = true;
            ws.Cells[1, 28].Bold = true;
            ws.Cells[1, 29].Bold = true;
            ws.Cells[1, 30].Bold = true;
            */

            List<ATTB> objInfo = new List<ATTB>();
            double NJOPPBB2017 = 0;
            double NJOPPBB2018 = 0;
            double luasTanah = 0;
            double luasBangunan = 0;
            double NilaiBukuTanah = 0;
            double NilaiBukuBangunan = 0;
            double NilaiPerolehan = 0;

            using (var client = new HttpClient())
            {

                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl2);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.ATTBAPI);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<List<ATTB>>(APIResponse);


                    int ii = 1;
                    for (int i = 0; i < objInfo.Count; i++)
                    {
                        ws.Cells[ii + 1, 0] = String.Format("{0:n0}", Convert.ToDouble(objInfo[i].ListNo)).Replace(",", ".");
                        ws.Cells[ii + 1, 1] = String.Format("{0:n0}", Convert.ToDouble(objInfo[i].AssetNo)).Replace(",", ".");
                        ws.Cells[ii + 1, 2] = objInfo[i].IDSAP;
                        ws.Cells[ii + 1, 3] = objInfo[i].ActualAddress;
                        ws.Cells[ii + 1, 4] = objInfo[i].BrachCode;
                        ws.Cells[ii + 1, 5] = String.Format("{0:n0}", Convert.ToDouble(objInfo[i].NJOPPBB2017)).Replace(",", ".");
                        ws.Cells[ii + 1, 6] = String.Format("{0:n0}", Convert.ToDouble(objInfo[i].NJOPPBB2018)).Replace(",", ".");
                        ws.Cells[ii + 1, 7] = objInfo[i].ATTBType;
                        ws.Cells[ii + 1, 8] = objInfo[i].ATTBStatus;
                        ws.Cells[ii + 1, 9] = objInfo[i].RegionAsset;
                        ws.Cells[ii + 1, 10] = objInfo[i].City;
                        ws.Cells[ii + 1, 11] = objInfo[i].Ownership;
                        ws.Cells[ii + 1, 12] = objInfo[i].OwnershipNo;
                        ws.Cells[ii + 1, 13] = objInfo[i].OwnerName;
                        ws.Cells[ii + 1, 14] = objInfo[i].ReleaseDateCertificate;
                        ws.Cells[ii + 1, 15] = objInfo[i].DueDateCertificate;
                        ws.Cells[ii + 1, 16] = String.Format("{0:n0}", Convert.ToDouble(objInfo[i].LandArea)).Replace(",", ".");
                        ws.Cells[ii + 1, 17] = String.Format("{0:n0}", Convert.ToDouble(objInfo[i].BuildingArea)).Replace(",", ".");
                        ws.Cells[ii + 1, 18] = String.Format("{0:n0}", Convert.ToDouble(objInfo[i].NilaiBukuTanah)).Replace(",", ".");
                        ws.Cells[ii + 1, 19] = String.Format("{0:n0}", Convert.ToDouble(objInfo[i].NilaiBukuBangunan)).Replace(",", ".");
                        ws.Cells[ii + 1, 20] = objInfo[i].AssetControl;
                        ws.Cells[ii + 1, 21] = objInfo[i].PrimaryUser;
                        ws.Cells[ii + 1, 22] = String.Format("{0:n0}", Convert.ToDouble(objInfo[i].AcquisitionValue)).Replace(",", ".");
                        ws.Cells[ii + 1, 23] = objInfo[i].UpdatedBy;
                        ws.Cells[ii + 1, 24] = objInfo[i].UpdatedDate;
                        ii = ii + 1;


                        NJOPPBB2017 = NJOPPBB2017 + Convert.ToDouble(objInfo[i].NJOPPBB2017);
                        NJOPPBB2018 = NJOPPBB2018 + Convert.ToDouble(objInfo[i].NJOPPBB2018);
                        luasTanah = luasTanah + Convert.ToDouble(objInfo[i].LandArea);
                        luasBangunan = luasBangunan + Convert.ToDouble(objInfo[i].BuildingArea);
                        NilaiBukuTanah = NilaiBukuTanah + Convert.ToDouble(objInfo[i].NilaiBukuTanah);
                        NilaiBukuBangunan = NilaiBukuBangunan + Convert.ToDouble(objInfo[i].NilaiBukuBangunan);
                        NilaiPerolehan = NilaiPerolehan + Convert.ToDouble(objInfo[i].AcquisitionValue);



                    }


                    ws.Cells[0, 5] = String.Format("{0:n0}", NJOPPBB2017).Replace(",", ".");
                    ws.Cells[0, 6] = String.Format("{0:n0}", NJOPPBB2018).Replace(",", ".");
                    ws.Cells[0, 16] = String.Format("{0:n0}", luasTanah).Replace(",", ".");
                    ws.Cells[0, 17] = String.Format("{0:n0}", luasBangunan).Replace(",", ".");
                    ws.Cells[0, 18] = String.Format("{0:n0}", NilaiBukuTanah).Replace(",", ".");
                    ws.Cells[0, 19] = String.Format("{0:n0}", NilaiBukuBangunan).Replace(",", ".");
                    ws.Cells[0, 22] = String.Format("{0:n0}", NilaiPerolehan).Replace(",", ".");

                    wb.Add(ws);
                    wb.Save(fullPath);
                    filename = MakeValidFileName(filename);




                }

            }

            return File(fullPath, "application/vnd.ms-excel", filename);
            var fileStream = new MemoryStream();
            wb.Save(fileStream, Simplexcel.CompressionLevel.NoCompression);



        }
        public ActionResult Preview(string docfile)
        {

            ViewBag.DocFile = docfile;

            return View();


        }

        //public async Task<FileResult> DownloadExcel2()
        //{

        //    Microsoft.Office.Interop.Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();

        //    Microsoft.Office.Interop.Excel.Workbook xlWorkBook;
        //    Microsoft.Office.Interop.Excel.Worksheet xlWorkSheet;

        //    object misValue = System.Reflection.Missing.Value;

        //    xlWorkBook = xlApp.Workbooks.Add(misValue);
        //    xlWorkSheet = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);

        //    xlWorkSheet.Cells[1, 1] = "SUMMARY";
        //    xlWorkSheet.Cells[2, 1] = "No Urut";
        //    xlWorkSheet.Cells[2, 2] = "No Titik Aset";
        //    xlWorkSheet.Cells[2, 3] = "ID SAP";
        //    xlWorkSheet.Cells[2, 4] = "Alamat Aktual";
        //    xlWorkSheet.Cells[2, 5] = "Kode Cabang";
        //    xlWorkSheet.Cells[2, 6] = "NJOP  PBB 2017 (Rp)";
        //    xlWorkSheet.Cells[2, 7] = "NJOP PBB 2018 (Rp)";
        //    xlWorkSheet.Cells[2, 8] = "Jenis";
        //    xlWorkSheet.Cells[2, 9] = "Status";
        //    xlWorkSheet.Cells[2, 10] = "Region";
        //    xlWorkSheet.Cells[2, 11] = "Kota";
        //    xlWorkSheet.Cells[2, 12] = "Bukti Kepemilikan";
        //    xlWorkSheet.Cells[2, 13] = "No Bukti";
        //    xlWorkSheet.Cells[2, 14] = "Nama Pemegang Hak ";
        //    xlWorkSheet.Cells[2, 15] = "Tgl Penerbitan Sertifikat";
        //    xlWorkSheet.Cells[2, 16] = "Tgl Jatuh Tempo Sertifikat";
        //    xlWorkSheet.Cells[2, 17] = "Luas Tanah Sertifikat";
        //    xlWorkSheet.Cells[2, 18] = "Luas Bangunan (PBB)";
        //    xlWorkSheet.Cells[2, 19] = "Nilai Buku Tanah (Rp)";
        //    xlWorkSheet.Cells[2, 20] = "Nilai Buku Bangunan (Rp)";
        //    xlWorkSheet.Cells[2, 21] = "Penguasaan";
        //    xlWorkSheet.Cells[2, 22] = "Pengguna Utama";
        //    xlWorkSheet.Cells[2, 23] = "Nilai Perolehan (Rp)";
        //    xlWorkSheet.Cells[2, 24] = "Updated By";
        //    xlWorkSheet.Cells[2, 25] = "Updated Date";

        //    for (int c = 1; c <= 25; c++) // this will aply it form col 1 to 10
        //    {
        //        xlWorkSheet.Columns[c].ColumnWidth = 20;
        //    }

        //    for (int aa = 1; aa <= 25; aa++) // this will aply it form col 1 to 10
        //    {
        //        xlWorkSheet.Cells[1, aa].Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Silver);
        //        xlWorkSheet.Cells[2, aa].Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Silver);
        //    }

        //     xlWorkSheet.Range[xlWorkSheet.Cells[1, 1], xlWorkSheet.Cells[1, 6]].Merge();

        //    xlWorkSheet.get_Range("A1", "AD2").Cells.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;


        //    List<ATTB> objInfo = new List<ATTB>();


        //    double NJOPPBB2017 = 0;
        //    double NJOPPBB2018 = 0;
        //    double luasTanah = 0;
        //    double luasBangunan = 0;
        //    double NilaiBukuTanah = 0;
        //    double NilaiBukuBangunan = 0;
        //    double NilaiPerolehan = 0;

        //    using (var client = new HttpClient())
        //    {

        //        //Passing service base url  
        //        client.BaseAddress = new Uri(Baseurl2);

        //        client.DefaultRequestHeaders.Clear();
        //        //Define request data format  
        //        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

        //        //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
        //        HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.ATTBAPI);

        //        //Checking the response is successful or not which is sent using HttpClient  
        //        if (Res.IsSuccessStatusCode)
        //        {
        //            //Storing the response details recieved from web api   
        //            var APIResponse = Res.Content.ReadAsStringAsync().Result;

        //            //Deserializing the response recieved from web api and storing into the Employee list  
        //            objInfo = JsonConvert.DeserializeObject<List<ATTB>>(APIResponse);


        //            int ii = 2;
        //            for (int i = 0; i < objInfo.Count; i++)
        //            {
        //                xlWorkSheet.Cells[ii + 1, 1] = String.Format("{0:n0}", objInfo[i].ListNo).Replace(",", "."); 
        //                xlWorkSheet.Cells[ii + 1, 2] = String.Format("{0:n0}", objInfo[i].AssetNo).Replace(",", "."); 
        //                xlWorkSheet.Cells[ii + 1, 3] = objInfo[i].IDSAP;
        //                xlWorkSheet.Cells[ii + 1, 4] = objInfo[i].ActualAddress;
        //                xlWorkSheet.Cells[ii + 1, 5] = objInfo[i].BrachCode;
        //                xlWorkSheet.Cells[ii + 1, 6] = String.Format("{0:n0}", objInfo[i].NJOPPBB2017).Replace(",", ".");
        //                xlWorkSheet.Cells[ii + 1, 7] = String.Format("{0:n0}", objInfo[i].NJOPPBB2018).Replace(",", ".");
        //                xlWorkSheet.Cells[ii + 1, 8] = objInfo[i].ATTBType;
        //                xlWorkSheet.Cells[ii + 1, 9] = objInfo[i].ATTBStatus;
        //                xlWorkSheet.Cells[ii + 1, 10] = objInfo[i].RegionAsset;
        //                xlWorkSheet.Cells[ii + 1, 11] = objInfo[i].City;
        //                xlWorkSheet.Cells[ii + 1, 12] = objInfo[i].Ownership;
        //                xlWorkSheet.Cells[ii + 1, 13] = objInfo[i].OwnershipNo;
        //                xlWorkSheet.Cells[ii + 1, 14] = objInfo[i].OwnerName;
        //                xlWorkSheet.Cells[ii + 1, 15] = objInfo[i].ReleaseDateCertificate;
        //                xlWorkSheet.Cells[ii + 1, 16] = objInfo[i].DueDateCertificate;
        //                xlWorkSheet.Cells[ii + 1, 17] = String.Format("{0:n0}", objInfo[i].LandArea).Replace(",", "."); 
        //                xlWorkSheet.Cells[ii + 1, 18] = String.Format("{0:n0}", objInfo[i].BuildingArea).Replace(",", "."); 
        //                xlWorkSheet.Cells[ii + 1, 19] = String.Format("{0:n0}", objInfo[i].NilaiBukuTanah).Replace(",", ".");
        //                xlWorkSheet.Cells[ii + 1, 20] = String.Format("{0:n0}", objInfo[i].NilaiBukuBangunan).Replace(",", ".");
        //                xlWorkSheet.Cells[ii + 1, 21] = objInfo[i].AssetControl;
        //                xlWorkSheet.Cells[ii + 1, 22] = objInfo[i].PrimaryUser;
        //                xlWorkSheet.Cells[ii + 1, 23] = String.Format("{0:n0}", objInfo[i].AcquisitionValue).Replace(",", ".");
        //                xlWorkSheet.Cells[ii + 1, 24] = objInfo[i].UpdatedBy;
        //                xlWorkSheet.Cells[ii + 1, 25] = objInfo[i].UpdatedDate;
        //                ii = ii + 1;


        //                NJOPPBB2017 = NJOPPBB2017 + Convert.ToDouble(objInfo[i].NJOPPBB2017);
        //                NJOPPBB2018 = NJOPPBB2018 + Convert.ToDouble(objInfo[i].NJOPPBB2018);
        //                luasTanah = luasTanah + Convert.ToDouble(objInfo[i].LandArea);
        //                luasBangunan = luasBangunan + Convert.ToDouble(objInfo[i].BuildingArea);
        //                NilaiBukuTanah = NilaiBukuTanah + Convert.ToDouble(objInfo[i].NilaiBukuTanah);
        //                NilaiBukuBangunan = NilaiBukuBangunan + Convert.ToDouble(objInfo[i].NilaiBukuBangunan);
        //                NilaiPerolehan = NilaiPerolehan + Convert.ToDouble(objInfo[i].AcquisitionValue);





        //            }


        //            xlWorkSheet.Cells[1, 6] = String.Format("{0:n0}", NJOPPBB2017).Replace(",", ".");
        //            xlWorkSheet.Cells[1, 7] = String.Format("{0:n0}", NJOPPBB2018).Replace(",", ".");
        //            xlWorkSheet.Cells[1, 17] = String.Format("{0:n0}", luasTanah).Replace(",", ".");
        //            xlWorkSheet.Cells[1, 18] = String.Format("{0:n0}", luasBangunan).Replace(",", ".");
        //            xlWorkSheet.Cells[1, 19] = String.Format("{0:n0}", NilaiBukuTanah).Replace(",", ".");
        //            xlWorkSheet.Cells[1, 20] = String.Format("{0:n0}", NilaiBukuBangunan).Replace(",", ".");
        //            xlWorkSheet.Cells[1, 23] = String.Format("{0:n0}", NilaiPerolehan).Replace(",", ".");




        //        }

        //    }



        //    //var path = Server.MapPath("~/Content/Download/TaxData2.xls");
        //    var path = Server.MapPath("~/Content/Download/ATTBData.xls");
        //    //string fullPath = Path.Combine(Server.MapPath("~/Content/Download/"), filename);
        //    xlApp.DisplayAlerts = false;
        //    //var path = "C:\\Users\\Bu-e Thipa\\Desktop\\Mandiri-Visionet-Fase2\\Website\\Versi GIT\\Mandiri.CRE.Web-master\\Mandiri.CRE.Web-master\\Source\\Content\\Download\\TaxData2.xls";
        //    //xlWorkBook.SaveAs(path);
        //    xlWorkBook.SaveAs(path, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookDefault, Type.Missing, Type.Missing, true, false, XlSaveAsAccessMode.xlNoChange, XlSaveConflictResolution.xlLocalSessionChanges, Type.Missing, Type.Missing);
        //    xlWorkBook.Close(true, misValue, misValue);
        //    xlApp.Quit();


        //    //Marshal.ReleaseComObject(xlWorkSheet);
        //    //Marshal.ReleaseComObject(xlWorkBook);
        //    //Marshal.ReleaseComObject(xlApp);
        //    return File(path, "application/vnd.ms-excel", "ATTBData-CRE Mandiri.xlsx");
        //}


    }
}
