﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using MandiriCRE.Models;
using System.IO;
using System.Data.SqlClient;
using System.Data;
using System.Web.UI;
using Simplexcel;
using System.Text.RegularExpressions;
using System.Globalization;
using MandiriCRE.ViewModel;
//using Microsoft.Office.Interop.Excel;

namespace MandiriCRE.Controllers
{

    public class TaxController : Controller
    {

        string Baseurl = Properties.Settings.Default.BaseURI;
        string Baseurl2 = Properties.Settings.Default.BaseURI;

        //public Microsoft.Office.Interop.Excel.Page Page { get; private set; }
        public object Controls { get; private set; }

        // GET: Tax
        public async Task<ActionResult> Index()
        {
            var model = new TaxIndexViewModel();

            var sessionAccessString = Session["Access"]?.ToString();

            var isSuperAdmin = sessionAccessString == "superadmin";

            if (isSuperAdmin)
            {
                model.IsAllowedToView = true;
                model.IsAllowedToUpdate = true;
                model.IsAllowedToDelete = true;
                model.IsAllowedToDuplicate = true;
            }

            if (!string.IsNullOrWhiteSpace(sessionAccessString))
            {
                var index = sessionAccessString.IndexOf("19");
                var allowedToViewString = sessionAccessString.Substring(index + 2, 1);
                if (allowedToViewString == "Y")
                {
                    model.IsAllowedToView = true;
                }
                var allowedToUpdateString = sessionAccessString.Substring(index + 4, 1);
                if (allowedToUpdateString == "Y")
                {
                    model.IsAllowedToUpdate = true;
                }
                var allowedToDeleteString = sessionAccessString.Substring(index + 5, 1);
                if (allowedToDeleteString == "Y")
                {
                    model.IsAllowedToDelete = true;
                }
                var allowedToDuplicateString = sessionAccessString.Substring(index + 6, 1);
                if (allowedToDuplicateString == "Y")
                {
                    model.IsAllowedToDuplicate = true;
                }
            }

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Baseurl2);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var res = await client.GetAsync(Properties.Settings.Default.TaxAPI);

                if (res.IsSuccessStatusCode)
                {
                    model.TaxesJson = res.Content.ReadAsStringAsync().Result;
                }
                return View(model);
            }
        }

        // GET: Tax/TaxID
        public async Task<ActionResult> Details(string id)
        {
            Tax objInfo = new Tax();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl2);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllArticles using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.TaxAPI + "/" + id);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<Tax>(APIResponse);

                }
                ViewBag.TaxID = id;
                return View(objInfo);
            }
        }

        //GET: Procurement/Create
        //public async Task<ActionResult> Create(string id, string Status, string WF, string ErrorMessage)
        public async Task<ActionResult> Create(string ErrorMessage, Tax objTax)
        {
            if (ErrorMessage != null)
            {
                ViewBag.ErrorMessage = ErrorMessage;
            }

            List<GL> _listGL = await GetListGL();
            ViewBag.ListGL = _listGL;

            List<Payment> _listPayments = await GetListPayment();
            ViewBag.ListPayments = _listPayments;

            List<ATTBTypeModels> _listATTBType = await GetListATTBType();
            ViewBag.ListATTBType = _listATTBType;

            List<RegionAssetModels> _listRegionAsset = await GetListRegionAsset();
            ViewBag.ListRegionAsset = _listRegionAsset;

            List<YearTax> _listTaxYear = await GetListTaxYear();
            ViewBag.ListTaxYear = _listTaxYear;

            Tax objInfo = new Tax();
            if (objTax != null)
            {
                objInfo = objTax;
            }
            if (ErrorMessage != null)
            {
                ViewBag.ErrorMessage = ErrorMessage;
            }
            return View(objInfo);
        }
        // POST: Procurement/Create
        [HttpPost]
        //public async Task<ActionResult> Create(string id, string WF, ProcurementHeader obj, FormCollection collection)
        public async Task<ActionResult> Create(Tax objTax, FormCollection collection, HttpPostedFileBase file, HttpPostedFileBase file2, HttpPostedFileBase file3)
        {
            try
            {
                String ListFileName = "";
                Tax obj = new Tax();

                obj.GLID = collection["GLID"].ToString();
                //obj.GLName = collection["gLName"].ToString();
                obj.PaymentID = collection["PaymentName"].ToString();
                if ((collection["OtherPayment"] != null && collection["OtherPayment"] != "") || (obj.OtherPayment != null && obj.OtherPayment != "") || (ViewBag.OtherPayment != null && ViewBag.OtherPayment != ""))
                {
                    obj.PaymentID = collection["OtherPayment"];
                    obj.OtherPayment = collection["OtherPayment"];
                    ViewBag.OtherPayment = collection["OtherPayment"];
                }
                obj.TaxYearID = collection["TaxYearID"].ToString();
                //obj.TaxYear = collection["taxYear"].ToString();
                obj.IDSAP = collection["iDSAP"].ToString();
                obj.FinanceReport = collection["financeReport"].ToString();
                obj.ATTBTypeID = collection["ATTBTypeID"].ToString();
                //obj.ATTBType = collection["aTTBType"].ToString();
                obj.ATTBName = collection["aTTBName"].ToString();
                if ((collection["branch"] != null && collection["branch"] != ""))
                {
                    obj.Branch = collection["branch"].ToString();
                }  
                obj.RegionAssetID = collection["RegionAssetID"].ToString();
                //obj.RegionAsset = collection["regionAsset"].ToString();
                obj.NOP = collection["nOP"].ToString();
                obj.ObjectLocation = collection["objectLocation"].ToString();
                obj.Kelurahan = collection["kelurahan"].ToString();
                obj.Kecamatan = collection["kecamatan"].ToString();
                obj.City = collection["city"].ToString();
                obj.TaxPayerName = collection["taxPayerName"].ToString();
                obj.TaxPayerAddress = collection["taxPayerAddress"].ToString();
                obj.TaxPaid = collection["taxPaid"].ToString();
                obj.PaidBy = collection["paidBy"].ToString();
                obj.Desciption = collection["desciption"].ToString();
                obj.BuildingArea = collection["buildingArea"].ToString();
                obj.BuildingNJOP = collection["buildingNJOP"].ToString();
                obj.BuildingClass = collection["buildingClass"].ToString();
                obj.LandArea = collection["landArea"].ToString();
                obj.LandNJOP = collection["landNJOP"].ToString();
                obj.LandClass = collection["landClass"].ToString();
                if (collection["docFileName"] != null && collection["docFileName"] != "")
                {
                    obj.DocFileName = collection["docFileName"];
                }
                if (collection["docFileName2"] != null && collection["docFileName2"] != "")
                {
                    obj.DocFileName2 = collection["docFileName2"];
                }
                if (collection["docFileName3"] != null && collection["docFileName3"] != "")
                {
                    obj.DocFileName3 = collection["docFileName3"];
                }
                obj.ActionSave = "INSERT";
                obj.TaxID = "0";
                obj.UserName = User.Identity.Name;

                string ErrorMessage = "";
                string fileName = "";
                string path = "";
                string fileType = "";
                //Upload File -1-
                if (file != null && file.ContentLength > 0)
                {
                    fileType = Path.GetExtension(file.FileName);
                    if (fileType.ToUpper().Contains("DOC") || fileType.ToUpper().Contains("XLS") || fileType.ToUpper().Contains("PDF"))
                    {
                            fileName = Path.GetFileName(file.FileName);
                            path = Path.Combine(Server.MapPath("~/content/document/tax/"), fileName);
                            file.SaveAs(path);

                            //insert file data
                            obj.DocFileName = "-1-"+fileName;
                            obj.FolderName = "Tax";
                            obj.Path = path;
                        
                    }
                    else
                    {
                        ErrorMessage = "File Upload harus dengan format Word / Excel / PDF";
                        // return RedirectToAction("Create", new { ErrorMessage = ErrorMessage,obj });
                        return await Create(ErrorMessage, obj);
                    }
                }
                else
                {
                    obj.DocFileName = "-1-" + "";
                }

                //Upload File -2-
                if (file2 != null && file2.ContentLength > 0)
                {
                    fileType = Path.GetExtension(file2.FileName);
                    if (fileType.ToUpper().Contains("DOC") || fileType.ToUpper().Contains("XLS") || fileType.ToUpper().Contains("PDF"))
                    {
                        fileName = Path.GetFileName(file2.FileName);
                        path = Path.Combine(Server.MapPath("~/content/document/tax/"), fileName);
                        file2.SaveAs(path);

                        //insert file data
                        obj.DocFileName = obj.DocFileName+"-2-"+fileName;
                        obj.FolderName = "Tax";
                        obj.Path = path;

                    }
                    else
                    {
                        ErrorMessage = "File Upload harus dengan format Word / Excel / PDF";
                        //return RedirectToAction("Create", new { ErrorMessage = ErrorMessage,obj });
                        return await Create(ErrorMessage, obj);
                    }
                }
                else
                {
                    obj.DocFileName = obj.DocFileName + "-2-" + "";
                }

                //Upload File -2-
                if (file3 != null && file3.ContentLength > 0)
                {
                    fileType = Path.GetExtension(file3.FileName);
                    if (fileType.ToUpper().Contains("DOC") || fileType.ToUpper().Contains("XLS") || fileType.ToUpper().Contains("PDF"))
                    {
                        fileName = Path.GetFileName(file3.FileName);
                        path = Path.Combine(Server.MapPath("~/content/document/tax/"), fileName);
                        file3.SaveAs(path);

                        //insert file data
                        obj.DocFileName = obj.DocFileName+"-3-"+fileName+"-4-";
                        obj.FolderName = "Tax";
                        obj.Path = path;

                    }
                    else
                    {
                        ErrorMessage = "File Upload harus dengan format Word / Excel / PDF";
                        //return RedirectToAction("Create", new { ErrorMessage = ErrorMessage,obj });
                        return await Create(ErrorMessage, obj);
                    }
                }
                else
                {
                    obj.DocFileName = obj.DocFileName + "-3-" + ""+"-4-";
                }

                Tax obj2 = new Tax();

                #region cekduplicate
                string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    using (var cmd = new SqlCommand("sp_CheckDuplicateTax", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@TaxID", "0");
                        cmd.Parameters.AddWithValue("@GLID", obj.GLID);
                        cmd.Parameters.AddWithValue("@PaymentID", obj.PaymentID);
                        cmd.Parameters.AddWithValue("@TaxYearID", obj.TaxYearID);
                        cmd.Parameters.AddWithValue("@IDSAP", obj.IDSAP);
                        cmd.Parameters.AddWithValue("@FinanceReport", obj.FinanceReport);
                        cmd.Parameters.AddWithValue("@ATTBTypeID", obj.ATTBTypeID);
                        cmd.Parameters.AddWithValue("@ATTBName", obj.ATTBName);
                        cmd.Parameters.AddWithValue("@Branch", obj.Branch);
                        cmd.Parameters.AddWithValue("@RegionAssetID", obj.RegionAssetID);
                        cmd.Parameters.AddWithValue("@NOP", obj.NOP);
                        cmd.Parameters.AddWithValue("@ObjectLocation", obj.ObjectLocation);
                        cmd.Parameters.AddWithValue("@Kelurahan", obj.Kelurahan);
                        cmd.Parameters.AddWithValue("@Kecamatan", obj.Kecamatan);
                        cmd.Parameters.AddWithValue("@City", obj.City);
                        cmd.Parameters.AddWithValue("@TaxPayerName", obj.TaxPayerName);
                        cmd.Parameters.AddWithValue("@TaxPayerAddress", obj.TaxPayerAddress);
                        cmd.Parameters.AddWithValue("@TaxPaid", obj.TaxPaid);
                        cmd.Parameters.AddWithValue("@PaidBy", obj.PaidBy);
                        cmd.Parameters.AddWithValue("@BuildingArea", obj.BuildingArea);
                        cmd.Parameters.AddWithValue("@BuildingClass", obj.BuildingClass);
                        cmd.Parameters.AddWithValue("@BuildingNJOP", obj.BuildingNJOP);
                        cmd.Parameters.AddWithValue("@LandClass", obj.LandClass);
                        cmd.Parameters.AddWithValue("@LandArea", obj.LandArea);
                        cmd.Parameters.AddWithValue("@LandNJOP", obj.LandNJOP);
                        conn.Open();

                        using (var reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                obj2.TaxID = reader[0].ToString();
                                ErrorMessage = reader[1].ToString();
                            }
                        }
                    }

                }

                if (ErrorMessage != null && ErrorMessage != "")
                {
                    return await Create(ErrorMessage, obj);
                }
                #endregion


                // TODO: Add update logic here
                HttpClient client = new HttpClient();
                    client.BaseAddress = new Uri(Baseurl2);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    //Sending request to find web api REST service resource GetAllArticles using HttpClient  
                    HttpResponseMessage response = await client.PostAsJsonAsync(Properties.Settings.Default.TaxAPI + "/" + "0", obj);
                    response.EnsureSuccessStatusCode();
                
                
                
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }

        public async Task<List<GL>> GetListGL()
        {
            List<GL> _list = new List<GL>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl2);
                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.GLAPI);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    _list = JsonConvert.DeserializeObject<List<GL>>(APIResponse);

                }
                //returning the employee list to view  

            }
            return _list;
        }
        public async Task<List<RegionAssetModels>> GetListRegionAsset()
        {
            List<RegionAssetModels> _list = new List<RegionAssetModels>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl2);
                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.RegionAssetAPI);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    _list = JsonConvert.DeserializeObject<List<RegionAssetModels>>(APIResponse);

                }
                //returning the employee list to view  

            }
            return _list;
        }

        public async Task<List<ATTBTypeModels>> GetListATTBType()
        {
            List<ATTBTypeModels> _list = new List<ATTBTypeModels>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl2);
                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.ATTBTypeAPI);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    _list = JsonConvert.DeserializeObject<List<ATTBTypeModels>>(APIResponse);

                }
                //returning the employee list to view  

            }
            return _list;
        }

        public async Task<List<Payment>> GetListPayment()
        {
            List<Payment> _list = new List<Payment>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl2);
                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.PaymentAPI);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    _list = JsonConvert.DeserializeObject<List<Payment>>(APIResponse);

                }
                //returning the employee list to view  

            }
            return _list;
        }
        public async Task<List<YearTax>> GetListTaxYear()
        {
            List<YearTax> _list = new List<YearTax>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl2);
                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.TaxYearAPI);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    _list = JsonConvert.DeserializeObject<List<YearTax>>(APIResponse);

                }
                //returning the employee list to view  

            }
            return _list;
        }


        public async Task<ActionResult> Update(string id, string ErrorMessage)
        {
            List<Tax> objInfo = new List<Tax>();
            if (ErrorMessage != null)
            {
                ViewBag.ErrorMessage = ErrorMessage;
            }
            
            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl2);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllArticles using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.TaxAPI + "/" + id);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<List<Tax>>(APIResponse);

                }

                List<GL> _listGL = await GetListGL();
                ViewBag.ListGL = _listGL;

                List<Payment> _listPayments = await GetListPayment();
                ViewBag.ListPayments = _listPayments;

                List<ATTBTypeModels> _listATTBType = await GetListATTBType();
                ViewBag.ListATTBType = _listATTBType;

                List<RegionAssetModels> _listRegionAsset = await GetListRegionAsset();
                ViewBag.ListRegionAsset = _listRegionAsset;

                List<YearTax> _listTaxYear = await GetListTaxYear();
                ViewBag.ListTaxYear = _listTaxYear;

                List<LogHistory> _listHistories = GetListHistory(id);
                ViewBag.ListHistory = _listHistories;

                ViewBag.TaxID = id;

                Tax objOne = new Tax();

                foreach (Tax Obj in objInfo)
                {
                    objOne = Obj;

                }
                objOne.BuildingClass = String.Format("{0:n0}", Convert.ToDouble(objOne.BuildingClass)).Replace(",", ".");
                objOne.BuildingNJOP = String.Format("{0:n0}", Convert.ToDouble(objOne.BuildingNJOP)).Replace(",", ".");
                objOne.LandArea = String.Format("{0:n0}", Convert.ToDouble(objOne.LandArea)).Replace(",", ".");
                objOne.LandClass = String.Format("{0:n0}", Convert.ToDouble(objOne.LandClass)).Replace(",", ".");
                objOne.LandNJOP = String.Format("{0:n0}", Convert.ToDouble(objOne.LandNJOP)).Replace(",", ".");
                objOne.BuildingArea = String.Format("{0:n0}", Convert.ToDouble(objOne.BuildingArea)).Replace(",", ".");
                objOne.TaxPaid = String.Format("{0:n0}", Convert.ToDouble(objOne.TaxPaid)).Replace(",", ".");
                ViewBag.objInfo = objOne;
                return View(objOne);
            }

        }
        
        [HttpPost]
        public async Task<ActionResult> Update(string id, Tax obj, FormCollection collection, HttpPostedFileBase file, HttpPostedFileBase file2, HttpPostedFileBase file3)
        {
            try
            {
                Tax objInfo = new Tax();
               
                //Logic to save Tax
                //ProcurementDetail objDetail = new ProcurementDetail();\
                //objDetail.ProcurementID = id;

                obj.GLID = collection["GLID"].ToString();
                //obj.GLName = collection["gLName"].ToString();
                obj.PaymentID = collection["PaymentName"].ToString();
                if ((collection["OtherPayment"] != null && collection["OtherPayment"] != ""))
                {
                    obj.PaymentID = collection["OtherPayment"];
                }
                obj.TaxYearID = collection["TaxYearID"].ToString();
                //obj.TaxYear = collection["taxYear"].ToString();
                obj.IDSAP = collection["iDSAP"].ToString();
                obj.FinanceReport = collection["financeReport"].ToString();
                obj.ATTBTypeID = collection["ATTBTypeID"].ToString();
                //obj.ATTBType = collection["aTTBType"].ToString();
                obj.ATTBName = collection["aTTBName"].ToString();
                if ((collection["branch"] != null && collection["branch"] != ""))
                {
                    obj.Branch = collection["branch"].ToString();
                }
                obj.RegionAssetID = collection["RegionAssetID"].ToString();
                //obj.RegionAsset = collection["regionAsset"].ToString();
                obj.NOP = collection["nOP"].ToString();
                obj.ObjectLocation = collection["objectLocation"].ToString();
                obj.Kelurahan = collection["kelurahan"].ToString();
                obj.Kecamatan = collection["kecamatan"].ToString();
                obj.City = collection["city"].ToString();
                obj.TaxPayerName = collection["taxPayerName"].ToString();
                obj.TaxPayerAddress = collection["taxPayerAddress"].ToString();
                obj.TaxPaid = collection["taxPaid"].ToString();
                obj.PaidBy = collection["paidBy"].ToString();
                obj.Desciption = collection["desciption"].ToString();
                obj.BuildingArea = collection["buildingArea"].ToString();
                obj.BuildingNJOP = collection["buildingNJOP"].ToString();
                obj.BuildingClass = collection["buildingClass"].ToString();
                obj.LandArea = collection["landArea"].ToString();
                obj.LandNJOP = collection["landNJOP"].ToString();
                obj.LandClass = collection["landClass"].ToString();
                //if (collection["docFileName"] != null && collection["docFileName"] != "")
                //{
                //    obj.DocFileName = collection["docFileName"];
                //}
                //if (collection["docFileName2"] != null && collection["docFileName2"] != "")
                //{
                //    obj.DocFileName2 = collection["docFileName2"];
                //}
                //if (collection["docFileName3"] != null && collection["docFileName3"] != "")
                //{
                //    obj.DocFileName3 = collection["docFileName3"];
                //}
                obj.ActionSave = "UPDATE";
                obj.TaxID = id;
                obj.UserName = User.Identity.Name;


                string ErrorMessage = "";
                string fileName = "";
                string path = "";
                string fileType = "";
                //Upload File
                /*DOK 1*/
                if (file == null && (collection["docfilename"].ToString() != null && collection["docfilename"].ToString() != ""))
                {
                    fileName = collection["docfilename"].ToString();
                    obj.DocFileName = "-1-" + fileName;
                    path = Path.Combine(Server.MapPath("~/content/document/tax/"), fileName);
                }
                /*DOK 1*/
                if (file != null && file.ContentLength > 0)
                {
                    fileType = Path.GetExtension(file.FileName);
                    if (fileType.ToUpper().Contains("DOC") || fileType.ToUpper().Contains("XLS") || fileType.ToUpper().Contains("PDF"))
                    {

                        fileName = Path.GetFileName(file.FileName);
                        path = Path.Combine(Server.MapPath("~/content/document/tax/"), fileName);
                        file.SaveAs(path);

                        //insert file data
                        obj.DocFileName = "-1-" + fileName;
                        obj.FolderName = "Tax";
                        obj.Path = path;

                    }
                    else
                    {
                        ErrorMessage = "File Upload harus dengan format Word / Excel / PDF";
                        return RedirectToAction("Update", new { id, ErrorMessage = ErrorMessage });
                    }
                }
                if (file == null && (collection["docfilename"].ToString() == null || collection["docfilename"].ToString() == ""))
                {
                    obj.DocFileName = "-1-" + "";
                }
                /*DOK 2*/
                if (file2 == null && (collection["docfilename2"].ToString() != null && collection["docfilename2"].ToString() != ""))
                {
                    fileName = collection["docfilename2"].ToString();
                    obj.DocFileName = obj.DocFileName + "-2-" + fileName;
                    path = Path.Combine(Server.MapPath("~/content/document/tax/"), fileName);
                }
                //Upload File -2-
                if (file2 != null && file2.ContentLength > 0)
                {
                    fileType = Path.GetExtension(file2.FileName);
                    if (fileType.ToUpper().Contains("DOC") || fileType.ToUpper().Contains("XLS") || fileType.ToUpper().Contains("PDF"))
                    {
                        fileName = Path.GetFileName(file2.FileName);
                        path = Path.Combine(Server.MapPath("~/content/document/tax/"), fileName);
                        file2.SaveAs(path);

                        //insert file data
                        obj.DocFileName = obj.DocFileName + "-2-" + fileName;
                        obj.FolderName = "Tax";
                        obj.Path = path;

                    }
                    else
                    {
                        ErrorMessage = "File Upload harus dengan format Word / Excel / PDF";
                        return RedirectToAction("Create", new { ErrorMessage = ErrorMessage });
                    }
                }
                if (file2 == null && (collection["docfilename2"].ToString() == null || collection["docfilename2"].ToString() == ""))
                {
                    obj.DocFileName = obj.DocFileName + "-2-" + "";
                }
                /*DOK 3*/
                if (file3 == null && (collection["docfilename3"].ToString() != null && collection["docfilename3"].ToString() != ""))
                {
                    fileName = collection["docfilename3"].ToString();
                    obj.DocFileName = obj.DocFileName + "-3-" + fileName + "-4-";
                    path = Path.Combine(Server.MapPath("~/content/document/tax/"), fileName);
                }
                //Upload File -3-
                if (file3 != null && file3.ContentLength > 0)
                {
                    fileType = Path.GetExtension(file3.FileName);
                    if (fileType.ToUpper().Contains("DOC") || fileType.ToUpper().Contains("XLS") || fileType.ToUpper().Contains("PDF"))
                    {
                        fileName = Path.GetFileName(file3.FileName);
                        path = Path.Combine(Server.MapPath("~/content/document/tax/"), fileName);
                        file3.SaveAs(path);

                        //insert file data
                        obj.DocFileName = obj.DocFileName + "-3-" + fileName + "-4-";
                        obj.FolderName = "Tax";
                        obj.Path = path;

                    }
                    else
                    {
                        ErrorMessage = "File Upload harus dengan format Word / Excel / PDF";
                        return RedirectToAction("Create", new { ErrorMessage = ErrorMessage });
                    }
                }
                if (file3 == null && (collection["docfilename3"].ToString() == null || collection["docfilename3"].ToString() == ""))
                {
                    obj.DocFileName = obj.DocFileName + "-3-" + "" + "-4-";
                }

                

                #region cekduplicate
                Tax obj2 = new Tax();

                string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    using (var cmd = new SqlCommand("sp_CheckDuplicateTax", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@TaxID", id);
                        cmd.Parameters.AddWithValue("@GLID", obj.GLID);
                        cmd.Parameters.AddWithValue("@PaymentID", obj.PaymentID);
                        cmd.Parameters.AddWithValue("@TaxYearID", obj.TaxYearID);
                        cmd.Parameters.AddWithValue("@IDSAP", obj.IDSAP);
                        cmd.Parameters.AddWithValue("@FinanceReport", obj.FinanceReport);
                        cmd.Parameters.AddWithValue("@ATTBTypeID", obj.ATTBTypeID);
                        cmd.Parameters.AddWithValue("@ATTBName", obj.ATTBName);
                        cmd.Parameters.AddWithValue("@Branch", obj.Branch);
                        cmd.Parameters.AddWithValue("@RegionAssetID", obj.RegionAssetID);
                        cmd.Parameters.AddWithValue("@NOP", obj.NOP);
                        cmd.Parameters.AddWithValue("@ObjectLocation", obj.ObjectLocation);
                        cmd.Parameters.AddWithValue("@Kelurahan", obj.Kelurahan);
                        cmd.Parameters.AddWithValue("@Kecamatan", obj.Kecamatan);
                        cmd.Parameters.AddWithValue("@City", obj.City);
                        cmd.Parameters.AddWithValue("@TaxPayerName", obj.TaxPayerName);
                        cmd.Parameters.AddWithValue("@TaxPayerAddress", obj.TaxPayerAddress);
                        cmd.Parameters.AddWithValue("@TaxPaid", obj.TaxPaid);
                        cmd.Parameters.AddWithValue("@PaidBy", obj.PaidBy);
                        cmd.Parameters.AddWithValue("@BuildingArea", obj.BuildingArea);
                        cmd.Parameters.AddWithValue("@BuildingClass", obj.BuildingClass);
                        cmd.Parameters.AddWithValue("@BuildingNJOP", obj.BuildingNJOP);
                        cmd.Parameters.AddWithValue("@LandClass", obj.LandClass);
                        cmd.Parameters.AddWithValue("@LandArea", obj.LandArea);
                        cmd.Parameters.AddWithValue("@LandNJOP", obj.LandNJOP);
                        conn.Open();

                        using (var reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                obj2.TaxID = reader[0].ToString();
                                ErrorMessage = reader[1].ToString();
                            }
                        }
                    }

                }

                if (ErrorMessage != null && ErrorMessage != "")
                {
                    return await Update(id,ErrorMessage);
                }
                #endregion


                // TODO: Add update logic here
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(Baseurl2);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                
                
                //Get data Tax
                //Sending request to find web api REST service resource GetAllArticles using HttpClient  
                //HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.TaxAPI + "/" + id);

                //Checking the response is successful or not which is sent using HttpClient  
                //if (Res.IsSuccessStatusCode)
                //{
                //    //Storing the response details recieved from web api   
                //    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                //    //Deserializing the response recieved from web api and storing into the Employee list  
                //    objInfo = JsonConvert.DeserializeObject<Tax>(APIResponse);

                //}

                // TODO: Add update logic here
                HttpResponseMessage response = await client.PutAsJsonAsync(Properties.Settings.Default.TaxAPI + "/" + id, obj);
                response.EnsureSuccessStatusCode();
                
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }

        public List<LogHistory> GetListHistory(string id)
        {
            List<LogHistory> _list = new List<LogHistory>();
            

            string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(connectionstring))
            {
                using (var cmd = new SqlCommand("sp_ViewHistoryLog", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@TableID", id);
                    cmd.Parameters.AddWithValue("@TableName", "TAX");
                    conn.Open();

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            LogHistory obj = new LogHistory();
                            obj.LogID = reader[0].ToString();
                            obj.TableName = reader[1].ToString();
                            obj.TableID = reader[2].ToString();
                            obj.Action = reader[3].ToString();
                            obj.UpdatedDate = reader[4].ToString();
                            obj.UpdatedBy = reader[5].ToString();
                            _list.Add(obj);
                        }

                        
                    }
                }

            }

            return _list;
        }

        public PartialViewResult PartialTax(Tax obj, List<GL> _listGL, List<Payment> _listPayment , List<ATTBTypeModels> _listATTBType, List<RegionAssetModels> _listRegionAsset, List<YearTax> _listyearTax)
        {
            
            ViewBag.ListGL = _listGL;
            ViewBag.ListPayments = _listPayment;
            ViewBag.ListATTBType = _listATTBType;
            ViewBag.ListRegionAsset = _listRegionAsset;
            ViewBag.ListTaxYear = _listyearTax;

            Tax objInfo = obj;
            if (obj == null)
            {
                obj = new Tax();
                obj.BuildingClass = String.Format("{0:n0}", Convert.ToDouble(obj.BuildingClass)).Replace(",", ".");
                obj.BuildingNJOP = String.Format("{0:n0}", Convert.ToDouble(obj.BuildingNJOP)).Replace(",", ".");
                obj.LandArea = String.Format("{0:n0}", Convert.ToDouble(obj.LandArea)).Replace(",", ".");
                obj.LandClass = String.Format("{0:n0}", Convert.ToDouble(obj.LandClass)).Replace(",", ".");
                obj.LandNJOP = String.Format("{0:n0}", Convert.ToDouble(obj.LandNJOP)).Replace(",", ".");
            }
            return PartialView("PartialTax", obj);
        }

        public PartialViewResult PartialHistory()
        {
            return PartialView("PartialHistory", new List<LogHistory>());
        }


        //for view only
        public async Task<ActionResult> View(string id)
        {
            List<Tax> objInfo = new List<Tax>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl2);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllArticles using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.TaxAPI + "/" + id);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<List<Tax>>(APIResponse);

                }
                List<GL> _listGL = await GetListGL();
                ViewBag.ListGL = _listGL;

                List<Payment> _listPayments = await GetListPayment();
                ViewBag.ListPayments = _listPayments;

                List<ATTBTypeModels> _listATTBType = await GetListATTBType();
                ViewBag.ListATTBType = _listATTBType;

                List<RegionAssetModels> _listRegionAsset = await GetListRegionAsset();
                ViewBag.ListRegionAsset = _listRegionAsset;

                List<YearTax> _listTaxYear = await GetListTaxYear();
                ViewBag.ListTaxYear = _listTaxYear;

                List<LogHistory> _listHistories = GetListHistory(id);
                ViewBag.ListHistory = _listHistories;

                Tax objOne = new Tax();
                foreach (Tax Obj in objInfo)
                {
                    objOne = Obj;
                }
                objOne.BuildingClass = String.Format("{0:n0}", Convert.ToDouble(objOne.BuildingClass)).Replace(",", ".");
                objOne.BuildingNJOP = String.Format("{0:n0}", Convert.ToDouble(objOne.BuildingNJOP)).Replace(",", ".");
                objOne.LandArea = String.Format("{0:n0}", Convert.ToDouble(objOne.LandArea)).Replace(",", ".");
                objOne.LandClass = String.Format("{0:n0}", Convert.ToDouble(objOne.LandClass)).Replace(",", ".");
                objOne.LandNJOP = String.Format("{0:n0}", Convert.ToDouble(objOne.LandNJOP)).Replace(",", ".");
                objOne.BuildingArea = String.Format("{0:n0}", Convert.ToDouble(objOne.BuildingArea)).Replace(",", ".");
                objOne.TaxPaid = String.Format("{0:n0}", Convert.ToDouble(objOne.TaxPaid)).Replace(",", ".");

                ViewBag.TaxID = id;
                ViewBag.objInfo = objOne;
                return View(objOne);
            }

        }

        public PartialViewResult PartialTaxView(Tax obj, List<GL> _listGL, List<Payment> _listPayment, List<ATTBTypeModels> _listATTBType, List<RegionAssetModels> _listRegionAsset, List<YearTax> _listyearTax)
        {

            ViewBag.ListGL = _listGL;
            ViewBag.ListPayments = _listPayment;
            ViewBag.ListATTBType = _listATTBType;
            ViewBag.ListRegionAsset = _listRegionAsset;
            ViewBag.ListTaxYear = _listyearTax;

            Tax objInfo = obj;
            if (obj == null)
            {
                obj = new Tax();
                obj.BuildingClass = String.Format("{0:n0}", Convert.ToDouble(obj.BuildingClass)).Replace(",", ".");
                obj.BuildingNJOP = String.Format("{0:n0}", Convert.ToDouble(obj.BuildingNJOP)).Replace(",", ".");
                obj.LandArea = String.Format("{0:n0}", Convert.ToDouble(obj.LandArea)).Replace(",", ".");
                obj.LandClass = String.Format("{0:n0}", Convert.ToDouble(obj.LandClass)).Replace(",", ".");
                obj.LandNJOP = String.Format("{0:n0}", Convert.ToDouble(obj.LandNJOP)).Replace(",", ".");
            }
            return PartialView("PartialTaxView", obj);
        }

        public async Task<ActionResult> Duplicate(int id)
        {
            try
            {

                // TODO: Add update logic here
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(Baseurl2);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                // TODO: Add update logic here
                HttpResponseMessage response = await client.PostAsJsonAsync(Properties.Settings.Default.DuplicateTaxAPI + "/" + id+ "?UserName=" + User.Identity.Name.ToString(),id);
                response.EnsureSuccessStatusCode();
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }

        public async Task<ActionResult> Duplicate2(string id, string ErrorMessage)
        {
            List<Tax> objInfo = new List<Tax>();
            if (ErrorMessage != null)
            {
                ViewBag.ErrorMessage = ErrorMessage;
            }

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl2);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllArticles using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.TaxAPI + "/" + id);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<List<Tax>>(APIResponse);

                }

                List<GL> _listGL = await GetListGL();
                ViewBag.ListGL = _listGL;

                List<Payment> _listPayments = await GetListPayment();
                ViewBag.ListPayments = _listPayments;

                List<ATTBTypeModels> _listATTBType = await GetListATTBType();
                ViewBag.ListATTBType = _listATTBType;

                List<RegionAssetModels> _listRegionAsset = await GetListRegionAsset();
                ViewBag.ListRegionAsset = _listRegionAsset;

                List<YearTax> _listTaxYear = await GetListTaxYear();
                ViewBag.ListTaxYear = _listTaxYear;

                List<LogHistory> _listHistories = GetListHistory(id);
                ViewBag.ListHistory = _listHistories;

                ViewBag.TaxID = id;

                Tax objOne = new Tax();

                foreach (Tax Obj in objInfo)
                {
                    objOne = Obj;

                }
                objOne.BuildingClass = String.Format("{0:n0}", Convert.ToDouble(objOne.BuildingClass)).Replace(",", ".");
                objOne.BuildingNJOP = String.Format("{0:n0}", Convert.ToDouble(objOne.BuildingNJOP)).Replace(",", ".");
                objOne.LandArea = String.Format("{0:n0}", Convert.ToDouble(objOne.LandArea)).Replace(",", ".");
                objOne.LandClass = String.Format("{0:n0}", Convert.ToDouble(objOne.LandClass)).Replace(",", ".");
                objOne.LandNJOP = String.Format("{0:n0}", Convert.ToDouble(objOne.LandNJOP)).Replace(",", ".");
                objOne.BuildingArea = String.Format("{0:n0}", Convert.ToDouble(objOne.BuildingArea)).Replace(",", ".");
                objOne.DocFileName = "";
                objOne.DocFileName2 = "";
                objOne.DocFileName3 = "";
                ViewBag.objInfo = objOne;
                return View(objOne);
            }

        }

        [HttpPost]
        public async Task<ActionResult> Duplicate2(Tax objTax, FormCollection collection, HttpPostedFileBase file, HttpPostedFileBase file2, HttpPostedFileBase file3)
        {
            try
            {
                Tax obj = new Tax();

                obj.GLID = collection["GLID"].ToString();
                //obj.GLName = collection["gLName"].ToString();
                obj.PaymentID = collection["PaymentName"].ToString();
                if ((collection["OtherPayment"] != null && collection["OtherPayment"] != ""))
                {
                    obj.PaymentID = collection["OtherPayment"];
                }
                obj.TaxYearID = collection["TaxYearID"].ToString();
                //obj.TaxYear = collection["taxYear"].ToString();
                obj.IDSAP = collection["iDSAP"].ToString();
                obj.FinanceReport = collection["financeReport"].ToString();
                obj.ATTBTypeID = collection["ATTBTypeID"].ToString();
                //obj.ATTBType = collection["aTTBType"].ToString();
                obj.ATTBName = collection["aTTBName"].ToString();
                if ((collection["branch"] != null && collection["branch"] != ""))
                {
                    obj.Branch = collection["branch"].ToString();
                }
                obj.RegionAssetID = collection["RegionAssetID"].ToString();
                //obj.RegionAsset = collection["regionAsset"].ToString();
                obj.NOP = collection["nOP"].ToString();
                obj.ObjectLocation = collection["objectLocation"].ToString();
                obj.Kelurahan = collection["kelurahan"].ToString();
                obj.Kecamatan = collection["kecamatan"].ToString();
                obj.City = collection["city"].ToString();
                obj.TaxPayerName = collection["taxPayerName"].ToString();
                obj.TaxPayerAddress = collection["taxPayerAddress"].ToString();
                obj.TaxPaid = collection["taxPaid"].ToString();
                obj.PaidBy = collection["paidBy"].ToString();
                obj.Desciption = collection["desciption"].ToString();
                obj.BuildingArea = collection["buildingArea"].ToString();
                obj.BuildingNJOP = collection["buildingNJOP"].ToString();
                obj.BuildingClass = collection["buildingClass"].ToString();
                obj.LandArea = collection["landArea"].ToString();
                obj.LandNJOP = collection["landNJOP"].ToString();
                obj.LandClass = collection["landClass"].ToString();
                if (collection["docFileName"] != null && collection["docFileName"] != "")
                {
                    obj.DocFileName = collection["docFileName"];
                }
                if (collection["docFileName2"] != null && collection["docFileName2"] != "")
                {
                    obj.DocFileName2 = collection["docFileName2"];
                }
                if (collection["docFileName3"] != null && collection["docFileName3"] != "")
                {
                    obj.DocFileName3 = collection["docFileName3"];
                }
                obj.ActionSave = "INSERT";
                obj.TaxID = "0";
                obj.UserName = User.Identity.Name;

                string ErrorMessage = "";
                string fileName = "";
                string path = "";
                string fileType = "";
                //Upload File -1-
                if (file != null && file.ContentLength > 0)
                {
                    fileType = Path.GetExtension(file.FileName);
                    if (fileType.ToUpper().Contains("DOC") || fileType.ToUpper().Contains("XLS") || fileType.ToUpper().Contains("PDF"))
                    {
                        fileName = Path.GetFileName(file.FileName);
                        path = Path.Combine(Server.MapPath("~/content/document/tax/"), fileName);
                        file.SaveAs(path);

                        //insert file data
                        obj.DocFileName = "-1-" + fileName;
                        obj.FolderName = "Tax";
                        obj.Path = path;

                    }
                    else
                    {
                        ErrorMessage = "File Upload harus dengan format Word / Excel / PDF";
                        return RedirectToAction("Create", new { ErrorMessage = ErrorMessage });
                    }
                }
                else
                {
                    obj.DocFileName = "-1-" + "";
                }

                //Upload File -2-
                if (file2 != null && file2.ContentLength > 0)
                {
                    fileType = Path.GetExtension(file2.FileName);
                    if (fileType.ToUpper().Contains("DOC") || fileType.ToUpper().Contains("XLS") || fileType.ToUpper().Contains("PDF"))
                    {
                        fileName = Path.GetFileName(file2.FileName);
                        path = Path.Combine(Server.MapPath("~/content/document/tax/"), fileName);
                        file2.SaveAs(path);

                        //insert file data
                        obj.DocFileName = obj.DocFileName + "-2-" + fileName;
                        obj.FolderName = "Tax";
                        obj.Path = path;

                    }
                    else
                    {
                        ErrorMessage = "File Upload harus dengan format Word / Excel / PDF";
                        return RedirectToAction("Create", new { ErrorMessage = ErrorMessage });
                    }
                }
                else
                {
                    obj.DocFileName = obj.DocFileName + "-2-" + "";
                }

                //Upload File -2-
                if (file3 != null && file3.ContentLength > 0)
                {
                    fileType = Path.GetExtension(file3.FileName);
                    if (fileType.ToUpper().Contains("DOC") || fileType.ToUpper().Contains("XLS") || fileType.ToUpper().Contains("PDF"))
                    {
                        fileName = Path.GetFileName(file3.FileName);
                        path = Path.Combine(Server.MapPath("~/content/document/tax/"), fileName);
                        file3.SaveAs(path);

                        //insert file data
                        obj.DocFileName = obj.DocFileName + "-3-" + fileName + "-4-";
                        obj.FolderName = "Tax";
                        obj.Path = path;

                    }
                    else
                    {
                        ErrorMessage = "File Upload harus dengan format Word / Excel / PDF";
                        return RedirectToAction("Create", new { ErrorMessage = ErrorMessage });
                    }
                }
                else
                {
                    obj.DocFileName = obj.DocFileName + "-3-" + "" + "-4-";
                }

                Tax obj2 = new Tax();

                #region cekduplicate
                string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    using (var cmd = new SqlCommand("sp_CheckDuplicateTax", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@TaxID", "0");
                        cmd.Parameters.AddWithValue("@GLID", obj.GLID);
                        cmd.Parameters.AddWithValue("@PaymentID", obj.PaymentID);
                        cmd.Parameters.AddWithValue("@TaxYearID", obj.TaxYearID);
                        cmd.Parameters.AddWithValue("@IDSAP", obj.IDSAP);
                        cmd.Parameters.AddWithValue("@FinanceReport", obj.FinanceReport);
                        cmd.Parameters.AddWithValue("@ATTBTypeID", obj.ATTBTypeID);
                        cmd.Parameters.AddWithValue("@ATTBName", obj.ATTBName);
                        cmd.Parameters.AddWithValue("@Branch", obj.Branch);
                        cmd.Parameters.AddWithValue("@RegionAssetID", obj.RegionAssetID);
                        cmd.Parameters.AddWithValue("@NOP", obj.NOP);
                        cmd.Parameters.AddWithValue("@ObjectLocation", obj.ObjectLocation);
                        cmd.Parameters.AddWithValue("@Kelurahan", obj.Kelurahan);
                        cmd.Parameters.AddWithValue("@Kecamatan", obj.Kecamatan);
                        cmd.Parameters.AddWithValue("@City", obj.City);
                        cmd.Parameters.AddWithValue("@TaxPayerName", obj.TaxPayerName);
                        cmd.Parameters.AddWithValue("@TaxPayerAddress", obj.TaxPayerAddress);
                        cmd.Parameters.AddWithValue("@TaxPaid", obj.TaxPaid);
                        cmd.Parameters.AddWithValue("@PaidBy", obj.PaidBy);
                        cmd.Parameters.AddWithValue("@BuildingArea", obj.BuildingArea);
                        cmd.Parameters.AddWithValue("@BuildingClass", obj.BuildingClass);
                        cmd.Parameters.AddWithValue("@BuildingNJOP", obj.BuildingNJOP);
                        cmd.Parameters.AddWithValue("@LandClass", obj.LandClass);
                        cmd.Parameters.AddWithValue("@LandArea", obj.LandArea);
                        cmd.Parameters.AddWithValue("@LandNJOP", obj.LandNJOP);
                        conn.Open();

                        using (var reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                obj2.TaxID = reader[0].ToString();
                                ErrorMessage = reader[1].ToString();
                            }
                        }
                    }

                }

                if (ErrorMessage != null && ErrorMessage != "")
                {
                    //return await Create(ErrorMessage, obj);
                    return await Duplicate2(objTax.TaxID, ErrorMessage);
                }
                #endregion


                // TODO: Add update logic here
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(Baseurl2);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllArticles using HttpClient  
                HttpResponseMessage response = await client.PostAsJsonAsync(Properties.Settings.Default.TaxAPI + "/" + "0", obj);
                response.EnsureSuccessStatusCode();



                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }


        //public async Task<ActionResult> Delete(int id)
        //{
        //    try
        //    {

        //        // TODO: Add update logic here
        //        HttpClient client = new HttpClient();
        //        client.BaseAddress = new Uri(Baseurl2);
        //        client.DefaultRequestHeaders.Accept.Clear();
        //        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        //        // TODO: Add update logic here
        //        HttpResponseMessage response = await client.DeleteAsync(Properties.Settings.Default.TaxAPI + "/" + id + "?UserName=" + User.Identity.Name.ToString());
        //        response.EnsureSuccessStatusCode();
        //        return RedirectToAction("Index");
        //    }
        //    catch (Exception ex)
        //    {
        //        string err = ex.Message.ToString();
        //        return View();
        //    }
        //}

        public async Task<ActionResult> Delete(int id)
        {
            List<Tax> objInfo = new List<Tax>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl2);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllArticles using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.TaxAPI + "/" + id);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<List<Tax>>(APIResponse);

                }
                List<GL> _listGL = await GetListGL();
                ViewBag.ListGL = _listGL;

                List<Payment> _listPayments = await GetListPayment();
                ViewBag.ListPayments = _listPayments;

                List<ATTBTypeModels> _listATTBType = await GetListATTBType();
                ViewBag.ListATTBType = _listATTBType;

                List<RegionAssetModels> _listRegionAsset = await GetListRegionAsset();
                ViewBag.ListRegionAsset = _listRegionAsset;

                List<YearTax> _listTaxYear = await GetListTaxYear();
                ViewBag.ListTaxYear = _listTaxYear;

                List<LogHistory> _listHistories = GetListHistory(id.ToString());
                ViewBag.ListHistory = _listHistories;

                Tax objOne = new Tax();
                foreach (Tax Obj in objInfo)
                {
                    objOne = Obj;
                }

                ViewBag.TaxID = id;
                ViewBag.objInfo = objOne;
                return View(objOne);
            }
        }

        // POST: CRUD/Delete/5
        [HttpPost]
        public async Task<ActionResult> Delete(int id, Tax obj)
        {
            // TODO: Add delete logic here
            try
            {

                // TODO: Add update logic here
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(Baseurl2);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                // TODO: Add update logic here
                HttpResponseMessage response = await client.DeleteAsync(Properties.Settings.Default.TaxAPI + "/" + id + "?UserName=" + User.Identity.Name.ToString());
                response.EnsureSuccessStatusCode();
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }

        public static string MakeValidFileName(string name)
        {
            string invalidChars = Regex.Escape(new string(System.IO.Path.GetInvalidFileNameChars()));
            string invalidReStr = string.Format(@"[{0}]+", invalidChars);
            string replace = Regex.Replace(name, invalidReStr, "_").Replace(";", "").Replace(",", "");
            return replace;
        }

        public async Task<FileResult> DownloadExcel2()
        {

            
            string filename = "TaxData.xls";
            string path = Server.MapPath("~/Content/Download/");
            string fullPath = Path.Combine(Server.MapPath("~/Content/Download/"), filename);

            Workbook wb = new Workbook();
            Worksheet ws = new Worksheet("Data Tax");
            for (int c = 0; c <= 30; c++) // this will aply it form col 1 to 10
            {
                ws.ColumnWidths[c] = 20;
                ws.Cells[1, c].Bold = true;
                

            }

            ws.Cells[0, 0] = "SUMMARY";
            ws.Cells[1, 0] = "GL";
            ws.Cells[1, 1] = "Pembayaran";
            ws.Cells[1, 2] = "Tahun Pajak";
            ws.Cells[1, 3] = "Lap Keu";
            ws.Cells[1, 4] = "Jenis ATTB";
            ws.Cells[1, 5] = "ID SAP";
            ws.Cells[1, 6] = "Nama ATTB";
            ws.Cells[1, 7] = "Cabang";
            ws.Cells[1, 8] = "Region";
            ws.Cells[1, 9] = "NOP";
            ws.Cells[1, 10] = "Letak Objek Pajak";
            ws.Cells[1, 11] = "Nama Wajib Pajak";
            ws.Cells[1, 12] = "Alamat Wajib Pajak";
            ws.Cells[1, 13] = "Kelurahan";
            ws.Cells[1, 14] = "Kecamatan";
            ws.Cells[1, 15] = "Kota";
            ws.Cells[1, 16] = "Pajak Dibayar (Rp)";
            ws.Cells[1, 17] = "Dibayar Oleh";
            ws.Cells[1, 18] = "Kelas Tanah";
            ws.Cells[1, 19] = "Luas Tanah";
            ws.Cells[1, 20] = "NJOP Tanah";
            ws.Cells[1, 21] = "Total(Rp)";
            ws.Cells[1, 22] = "Kelas Bangunan";
            ws.Cells[1, 23] = "Luas Bangunan";
            ws.Cells[1, 24] = "NJOP Bangunan";
            ws.Cells[1, 25] = "Total(Rp)";
            ws.Cells[1, 26] = "Total NJOP (Rp)";
            ws.Cells[1, 27] = "Keterangan";
            ws.Cells[1, 28] = "Updated By";
            ws.Cells[1, 29] = "Updated Date";

            ws.Cells[0, 0].Bold = true;
            for (int c = 0; c <= 29; c++) // this will aply it form col 1 to 10
            {
                ws.Cells[1, c].Border = CellBorder.All;
                ws.Cells[1, c].Bold = true;
               

            }
            
            List<Tax> objInfo = new List<Tax>();


            double totalPajak = 0;
            double luasTanah = 0;
            double NJOPTanah = 0;
            double totalNJOPTanah = 0;
            double luasBangunan = 0;
            double NJOPBangunan = 0;
            double totalNJOPBangunan = 0;
            double totalNJOP = 0;

            using (var client = new HttpClient())
            {

                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl2);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.TaxAPI);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<List<Tax>>(APIResponse);


                    int ii = 1;
                    for (int i = 0; i < objInfo.Count; i++)
                    {
                        ws.Cells[ii + 1, 0] = objInfo[i].GLName;
                        ws.Cells[ii + 1, 1] = objInfo[i].PaymentName;
                        ws.Cells[ii + 1, 2] = objInfo[i].TaxYear;
                        ws.Cells[ii + 1, 3] = objInfo[i].FinanceReport;
                        ws.Cells[ii + 1, 4] = objInfo[i].ATTBType;
                        ws.Cells[ii + 1, 5] = objInfo[i].IDSAP;
                        ws.Cells[ii + 1, 6] = objInfo[i].ATTBName;
                        ws.Cells[ii + 1, 7] = objInfo[i].Branch;
                        ws.Cells[ii + 1, 8] = objInfo[i].RegionAsset;
                        ws.Cells[ii + 1, 9] = objInfo[i].NOP;
                        ws.Cells[ii + 1, 10] = objInfo[i].ObjectLocation;
                        ws.Cells[ii + 1, 11] = objInfo[i].TaxPayerName;
                        ws.Cells[ii + 1, 12] = objInfo[i].TaxPayerAddress;
                        ws.Cells[ii + 1, 13] = objInfo[i].Kelurahan;
                        ws.Cells[ii + 1, 14] = objInfo[i].Kecamatan;
                        ws.Cells[ii + 1, 15] = objInfo[i].City;
                        ws.Cells[ii + 1, 16] = String.Format("{0:n0}", Convert.ToDouble(objInfo[i].TaxPaid)).Replace(",", ".");
                        ws.Cells[ii + 1, 17] = objInfo[i].PaidBy;
                        ws.Cells[ii + 1, 18] = String.Format("{0:n0}", Convert.ToDouble(objInfo[i].LandClass)).Replace(",", ".");
                        ws.Cells[ii + 1, 19] = String.Format("{0:n0}", Convert.ToDouble(objInfo[i].LandArea)).Replace(",", ".");
                        ws.Cells[ii + 1, 20] = String.Format("{0:n0}", Convert.ToDouble(objInfo[i].LandNJOP)).Replace(",", ".");
                        ws.Cells[ii + 1, 21] = String.Format("{0:n0}", Convert.ToDouble(objInfo[i].LandNJOP) * Convert.ToDouble(objInfo[i].LandArea)).Replace(",", ".");
                        ws.Cells[ii + 1, 22] = String.Format("{0:n0}", Convert.ToDouble(objInfo[i].BuildingClass)).Replace(",", ".");
                        ws.Cells[ii + 1, 23] = String.Format("{0:n0}", Convert.ToDouble(objInfo[i].BuildingArea)).Replace(",", ".");
                        ws.Cells[ii + 1, 24] = String.Format("{0:n0}", Convert.ToDouble(objInfo[i].BuildingNJOP)).Replace(",", ".");
                        ws.Cells[ii + 1, 25] = String.Format("{0:n0}", Convert.ToDouble(objInfo[i].BuildingNJOP) * Convert.ToDouble(objInfo[i].BuildingArea)).Replace(",", ".");
                        ws.Cells[ii + 1, 26] = String.Format("{0:n0}", (Convert.ToDouble(objInfo[i].LandNJOP) * Convert.ToDouble(objInfo[i].LandArea)) + (Convert.ToDouble(objInfo[i].BuildingNJOP) * Convert.ToDouble(objInfo[i].BuildingArea))).Replace(",", ".");
                        ws.Cells[ii + 1, 27] = objInfo[i].Desciption;
                        ws.Cells[ii + 1, 28] = objInfo[i].UpdatedBy;
                        ws.Cells[ii + 1, 29] = objInfo[i].UpdatedDate;
                        ii = ii + 1;


                            totalPajak = totalPajak + Convert.ToDouble(objInfo[i].TaxPaid);
                            luasTanah = luasTanah + Convert.ToDouble(objInfo[i].LandArea);
                            NJOPTanah = NJOPTanah + Convert.ToDouble(objInfo[i].LandNJOP);
                            totalNJOPTanah = totalNJOPTanah + (Convert.ToDouble(objInfo[i].LandNJOP) * Convert.ToDouble(objInfo[i].LandArea));
                            luasBangunan = luasBangunan + Convert.ToDouble(objInfo[i].BuildingArea);
                            NJOPBangunan = NJOPBangunan + Convert.ToDouble(objInfo[i].BuildingNJOP);
                            totalNJOPBangunan = totalNJOPBangunan + (Convert.ToDouble(objInfo[i].BuildingNJOP) * Convert.ToDouble(objInfo[i].BuildingArea));
                            totalNJOP = totalNJOP + (Convert.ToDouble(objInfo[i].LandNJOP) * Convert.ToDouble(objInfo[i].LandArea)) + (Convert.ToDouble(objInfo[i].BuildingNJOP) * Convert.ToDouble(objInfo[i].BuildingArea));



                    }


                    ws.Cells[0, 16] = String.Format("{0:n0}", totalPajak).Replace(",", ".");
                    ws.Cells[0, 19] = String.Format("{0:n0}", luasTanah).Replace(",", ".");
                    ws.Cells[0, 20] = String.Format("{0:n0}", NJOPTanah).Replace(",", ".");
                    ws.Cells[0, 21] = String.Format("{0:n0}", totalNJOPTanah).Replace(",", ".");
                    ws.Cells[0, 23] = String.Format("{0:n0}", luasBangunan).Replace(",", ".");
                    ws.Cells[0, 24] = String.Format("{0:n0}", NJOPBangunan).Replace(",", ".");
                    ws.Cells[0, 25] = String.Format("{0:n0}", totalNJOPBangunan).Replace(",", ".");
                    ws.Cells[0, 26] = String.Format("{0:n0}", totalNJOP).Replace(",", ".");
                    wb.Add(ws);
                    wb.Save(fullPath);
                    filename = MakeValidFileName(filename);




                }

            }


            

            return File(fullPath, "application/vnd.ms-excel", filename);
            var fileStream = new MemoryStream();
            wb.Save(fileStream, Simplexcel.CompressionLevel.NoCompression);



        }

        public ActionResult Preview(string docfile)
        {
            
            ViewBag.DocFile = docfile;

            return View();
        

        }

        /*
        public async Task<FileResult> DownloadExcel()
        {

            Microsoft.Office.Interop.Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();

            Microsoft.Office.Interop.Excel.Workbook xlWorkBook;
            Microsoft.Office.Interop.Excel.Worksheet xlWorkSheet;

            object misValue = System.Reflection.Missing.Value;

            xlWorkBook = xlApp.Workbooks.Add(misValue);
            xlWorkSheet = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
            
            xlWorkSheet.Cells[1, 1] = "SUMMARY";
            xlWorkSheet.Cells[2, 1] = "GL";
            xlWorkSheet.Cells[2, 2] = "Pembayaran";
            xlWorkSheet.Cells[2, 3] = "Tahun Pajak";
            xlWorkSheet.Cells[2, 4] = "Lap Keu";
            xlWorkSheet.Cells[2, 5] = "Jenis ATTB";
            xlWorkSheet.Cells[2, 6] = "ID SAP";
            xlWorkSheet.Cells[2, 7] = "Nama ATTB";
            xlWorkSheet.Cells[2, 8] = "Cabang";
            xlWorkSheet.Cells[2, 9] = "Region";
            xlWorkSheet.Cells[2, 10] = "NOP";
            xlWorkSheet.Cells[2, 11] = "Letak Objek Pajak";
            xlWorkSheet.Cells[2, 12] = "Nama Wajib Pajak";
            xlWorkSheet.Cells[2, 13] = "Alamat Wajib Pajak";
            xlWorkSheet.Cells[2, 14] = "Kelurahan";
            xlWorkSheet.Cells[2, 15] = "Kecamatan";
            xlWorkSheet.Cells[2, 16] = "Kota";
            xlWorkSheet.Cells[2, 17] = "Pajak Dibayar (Rp)";
            xlWorkSheet.Cells[2, 18] = "Dibayar Oleh";
            xlWorkSheet.Cells[2, 19] = "Kelas Tanah";
            xlWorkSheet.Cells[2, 20] = "Luas Tanah";
            xlWorkSheet.Cells[2, 21] = "NJOP Tanah";
            xlWorkSheet.Cells[2, 22] = "Total(Rp)";
            xlWorkSheet.Cells[2, 23] = "Kelas Bangunan";
            xlWorkSheet.Cells[2, 24] = "Luas Bangunan";
            xlWorkSheet.Cells[2, 25] = "NJOP Bangunan";
            xlWorkSheet.Cells[2, 26] = "Total(Rp)";
            xlWorkSheet.Cells[2, 27] = "Total NJOP (Rp)";
            xlWorkSheet.Cells[2, 28] = "Keterangan";
            xlWorkSheet.Cells[2, 29] = "Updated By";
            xlWorkSheet.Cells[2, 30] = "Updated Date";

            for (int c = 1; c <= 30; c++) // this will aply it form col 1 to 10
            {
                xlWorkSheet.Columns[c].ColumnWidth = 20;
            }
            
            for (int aa = 1; aa <= 30; aa++) // this will aply it form col 1 to 10
            {
                xlWorkSheet.Cells[1, aa].Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Silver);
                xlWorkSheet.Cells[2, aa].Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Silver);
            }

             xlWorkSheet.Range[xlWorkSheet.Cells[1, 1], xlWorkSheet.Cells[1, 16]].Merge();

            xlWorkSheet.get_Range("A1", "AD2").Cells.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;


            List<Tax> objInfo = new List<Tax>();


            double totalPajak = 0;
            double luasTanah = 0;
            double NJOPTanah = 0;
            double totalNJOPTanah = 0;
            double luasBangunan = 0;
            double NJOPBangunan = 0;
            double totalNJOPBangunan = 0;

            using (var client = new HttpClient())
            {

                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl2);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.TaxAPI);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<List<Tax>>(APIResponse);


                    int ii = 2;
                    for (int i = 0; i < objInfo.Count; i++)
                    {
                        xlWorkSheet.Cells[ii + 1, 1] = objInfo[i].GLName;
                        xlWorkSheet.Cells[ii + 1, 2] = objInfo[i].PaymentName;
                        xlWorkSheet.Cells[ii + 1, 3] = objInfo[i].TaxYear;
                        xlWorkSheet.Cells[ii + 1, 4] = objInfo[i].FinanceReport;
                        xlWorkSheet.Cells[ii + 1, 5] = objInfo[i].ATTBType;
                        xlWorkSheet.Cells[ii + 1, 6] = objInfo[i].IDSAP;
                        xlWorkSheet.Cells[ii + 1, 7] = objInfo[i].ATTBName;
                        xlWorkSheet.Cells[ii + 1, 8] = objInfo[i].Branch;
                        xlWorkSheet.Cells[ii + 1, 9] = objInfo[i].RegionAsset;
                        xlWorkSheet.Cells[ii + 1, 10] = objInfo[i].NOP;
                        xlWorkSheet.Cells[ii + 1, 11] = objInfo[i].ObjectLocation;
                        xlWorkSheet.Cells[ii + 1, 12] = objInfo[i].TaxPayerName;
                        xlWorkSheet.Cells[ii + 1, 13] = objInfo[i].TaxPayerAddress;
                        xlWorkSheet.Cells[ii + 1, 14] = objInfo[i].Kelurahan;
                        xlWorkSheet.Cells[ii + 1, 15] = objInfo[i].Kecamatan;
                        xlWorkSheet.Cells[ii + 1, 16] = objInfo[i].City;
                        xlWorkSheet.Cells[ii + 1, 17] = objInfo[i].TaxPaid;
                        xlWorkSheet.Cells[ii + 1, 18] = objInfo[i].PaidBy;
                        xlWorkSheet.Cells[ii + 1, 19] = String.Format("{0:n0}", objInfo[i].LandClass).Replace(",", ".");
                        xlWorkSheet.Cells[ii + 1, 20] = String.Format("{0:n0}", objInfo[i].LandArea).Replace(",", ".");
                        xlWorkSheet.Cells[ii + 1, 21] = String.Format("{0:n0}", objInfo[i].LandNJOP).Replace(",", ".");
                        xlWorkSheet.Cells[ii + 1, 22] = String.Format("{0:n0}", Convert.ToDouble(objInfo[i].LandNJOP) * Convert.ToDouble(objInfo[i].LandArea)).Replace(",", ".");
                        xlWorkSheet.Cells[ii + 1, 23] = String.Format("{0:n0}", objInfo[i].BuildingClass).Replace(",", ".");
                        xlWorkSheet.Cells[ii + 1, 24] = String.Format("{0:n0}", objInfo[i].BuildingArea).Replace(",", ".");
                        xlWorkSheet.Cells[ii + 1, 25] = String.Format("{0:n0}", objInfo[i].BuildingNJOP).Replace(",", ".");
                        xlWorkSheet.Cells[ii + 1, 26] = String.Format("{0:n0}", Convert.ToDouble(objInfo[i].BuildingNJOP) * Convert.ToDouble(objInfo[i].BuildingArea)).Replace(",", ".");
                        xlWorkSheet.Cells[ii + 1, 27] = String.Format("{0:n0}", (Convert.ToDouble(objInfo[i].LandNJOP) * Convert.ToDouble(objInfo[i].LandArea)) + (Convert.ToDouble(objInfo[i].BuildingNJOP) * Convert.ToDouble(objInfo[i].BuildingArea))).Replace(",", ".");
                        xlWorkSheet.Cells[ii + 1, 28] = objInfo[i].Desciption;
                        xlWorkSheet.Cells[ii + 1, 29] = objInfo[i].UpdatedBy;
                        xlWorkSheet.Cells[ii + 1, 30] = objInfo[i].UpdatedDate;
                        ii = ii + 1;


                        
                            totalPajak = totalPajak + Convert.ToDouble(objInfo[i].TaxPaid);
                            luasTanah = luasTanah + Convert.ToDouble(objInfo[i].LandArea);
                            NJOPTanah = NJOPTanah + Convert.ToDouble(objInfo[i].LandNJOP);
                            totalNJOPTanah = totalNJOPTanah + (Convert.ToDouble(objInfo[i].TaxPaid) * Convert.ToDouble(objInfo[i].LandArea));
                            luasBangunan = luasBangunan + Convert.ToDouble(objInfo[i].BuildingArea);
                            NJOPBangunan = NJOPBangunan + Convert.ToDouble(objInfo[i].BuildingNJOP);
                            totalNJOPBangunan = totalNJOPBangunan + (Convert.ToDouble(objInfo[i].TaxPaid) * Convert.ToDouble(objInfo[i].BuildingArea));
                        


                    }


                    xlWorkSheet.Cells[1, 17] = String.Format("{0:n0}", totalPajak).Replace(",", ".");
                    xlWorkSheet.Cells[1, 20] = String.Format("{0:n0}", luasTanah).Replace(",", ".");
                    xlWorkSheet.Cells[1, 21] = String.Format("{0:n0}", NJOPTanah).Replace(",", ".");
                    xlWorkSheet.Cells[1, 22] = String.Format("{0:n0}", totalNJOPTanah).Replace(",", ".");
                    xlWorkSheet.Cells[1, 24] = String.Format("{0:n0}", luasBangunan).Replace(",", ".");
                    xlWorkSheet.Cells[1, 25] = String.Format("{0:n0}", NJOPBangunan).Replace(",", ".");
                    xlWorkSheet.Cells[1, 26] = String.Format("{0:n0}", totalNJOPBangunan).Replace(",", ".");
                    



                }

            }



            //var path = Server.MapPath("~/Content/Download/TaxData2.xls");
            var path = Server.MapPath("~/Content/Download/TaxData3.xls");
            //string fullPath = Path.Combine(Server.MapPath("~/Content/Download/"), filename);

            //var path = "C:\\Users\\Bu-e Thipa\\Desktop\\Mandiri-Visionet-Fase2\\Website\\Versi GIT\\Mandiri.CRE.Web-master\\Mandiri.CRE.Web-master\\Source\\Content\\Download\\TaxData2.xls";
            // xlWorkBook.SaveAs(path);
            xlApp.DisplayAlerts = false;
            xlWorkBook.SaveAs(path, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookDefault, Type.Missing, Type.Missing, true, false, XlSaveAsAccessMode.xlNoChange, XlSaveConflictResolution.xlLocalSessionChanges, Type.Missing, Type.Missing);

            xlWorkBook.Close(true, misValue, misValue);
            xlApp.Quit();


            //Marshal.ReleaseComObject(xlWorkSheet);
            //Marshal.ReleaseComObject(xlWorkBook);
            //Marshal.ReleaseComObject(xlApp);
            return File(path, "application/vnd.ms-excel", "TaxData-CRE Mandiri.xlsx");
        }*/


    }
    
}
