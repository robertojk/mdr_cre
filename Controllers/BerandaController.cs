﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using MandiriCRE.Models;
using System.Text;
using MandiriCRE.ViewModel;
using System.IO;
using System.Data.SqlClient;
using System.Data;

namespace MandiriCRE.Controllers
{
    public class BerandaController : Controller
    {
        // GET: Beranda
        string Baseurl = Properties.Settings.Default.BaseURI;
        public async Task<ActionResult> Index()
        {
            if (Session["roleId"] == null)
            {
                Session["roleId"] = 1;
            }
            if (Session["userName"] ==null)
            {
                Session["userName"] = "Test";
            }
            if (Session["teamId"] == null)
            {
                Session["teamId"] = 1;
            }

            string roleId = Session["roleId"].ToString();
            string userName = Session["userName"].ToString();
            string teamId = Session["teamId"].ToString();
            List<Photo> objPhoto = new List<Photo>();
            List<Articles> objArticle = new List<Articles>();
            List<NotificationViewAll> objNotification = new List<NotificationViewAll>();
            NotificationNewsViewModel objNotificationNews = new NotificationNewsViewModel();
            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage ResPhoto = await client.GetAsync(Properties.Settings.Default.FotoBerandaAPI);

                //Checking the response is successful or not which is sent using HttpClient  
                if (ResPhoto.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = ResPhoto.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objPhoto = JsonConvert.DeserializeObject<List<Photo>>(APIResponse);

                }
                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                //HttpResponseMessage ResNews = await client.GetAsync(Properties.Settings.Default.ArticleAPI);

                ////Checking the response is successful or not which is sent using HttpClient  
                //if (ResNews.IsSuccessStatusCode)
                //{
                //    //Storing the response details recieved from web api   
                //    var APIResponse = ResNews.Content.ReadAsStringAsync().Result;

                //    //Deserializing the response recieved from web api and storing into the Employee list  
                //    objArticle = JsonConvert.DeserializeObject<List<Articles>>(APIResponse);

                //}
                #region Slider
                StringBuilder str = new StringBuilder();
                str.Append("<ol class=\"carousel-indicators\">");
                int count1 = 0;
                foreach (Photo item in objPhoto)
                {
                    
                    string activeStr = "";
                    if (count1 == 0)
                    {
                        activeStr = "active";
                    }
                    str.Append("<li data-target=\"#myCarousel-2\" data-slide-to=\"0\" class=\"" + activeStr + "\"></li>");
                    count1++;
                }
                //str.Append("<li data-target=\"#myCarousel-2\" data-slide-to=\"0\" class=\"active\"></li>");
                //str.Append("<li data-target=\"#myCarousel-2\" data-slide-to=\"1\" class=\"\"></li>");
                //str.Append("<li data-target=\"#myCarousel-2\" data-slide-to=\"2\" class=\"\"></li>");
                str.Append("</ol>");
                str.Append("<div class=\"carousel-inner text-center\">");
                int count2 = 0;
                foreach (Photo item in objPhoto)
                {

                    string activeStr = "item";
                    if (count2 == 0)
                    {
                        activeStr = "item active";
                    }
                    //str.Append("<div class=\"text-center\">");
                    str.Append("<div style=height: 100px; class=\"" + activeStr + " text-center\">");
                    //str.Append("<img src=\"/Content/img/slider/" + item.DocumentName +"\" alt=\"\">");
                    str.Append("<img style=\"height: 340px;margin:auto;\" src=\"/Content/img/slider/" + item.DocumentName + "\" alt=\"\">");
                    str.Append("</div>");
                    //str.Append("</div>");
                    count2++;
                }
                //str.Append("<div class=\"item active\">");
                //str.Append("<img src=\"/Content/img/demo/m3.jpg\" alt=\"\">");
                //str.Append("</div>");
                //str.Append("<div class=\"item\">");
                //str.Append("<img src=\"/Content/img/demo/m1.jpg\" alt=\"\">");
                //str.Append("</div>");
                //str.Append("<div class=\"item\">");
                //str.Append("<img src=\"/Content/img/demo/m2.jpg\" alt =\"\">");
                //str.Append("</div>");
                str.Append("</div>");
                string htmlSlider = str.ToString();

                ViewBag.htmlSlider = htmlSlider;
                //returning the employee list to view  
                #endregion
                #region NewsFeed
                objArticle = GetListNews();
                #endregion
                #region Notifcations
                objNotification = await GetListNotifcation();
                #endregion
                objNotificationNews._listArticles = objArticle;
                objNotificationNews._listNotification = objNotification;
                return View(objNotificationNews);
            }
        }
        public async Task<ActionResult> News(string id)
        {
            Articles objInfo = new Articles();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllArticles using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.ArticleAPI + "/" + id);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<Articles>(APIResponse);

                }
                //returning the employee list to view  
                return View(objInfo);
            }
        }

        public async Task<List<NotificationViewAll>> GetListNotifcation()
        {
            string roleId = Session["roleId"].ToString();
            string userName = Session["userName"].ToString();
            string teamId = Session["teamId"].ToString();
            List<NotificationViewAll> retVal = new List<NotificationViewAll>();
            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllArticles using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.NotificationAllAPI + "/" + roleId + "/" + userName + "/" + teamId);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    retVal = JsonConvert.DeserializeObject< List<NotificationViewAll>>(APIResponse);

                }
                //returning the employee list to view  

            }

            return retVal;
        }
        public  List<Articles> GetListNews()
        {
            List<Articles> retVal = new List<Articles>();
            //using (var client = new HttpClient())
            //{
            //    //Passing service base url  
            //    client.BaseAddress = new Uri(Baseurl);

            //    client.DefaultRequestHeaders.Clear();
            //    //Define request data format  
            //    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            //    //Sending request to find web api REST service resource GetAllArticles using HttpClient  
            //    HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.ArticleAPI);

            //    //Checking the response is successful or not which is sent using HttpClient  
            //    if (Res.IsSuccessStatusCode)
            //    {
            //        //Storing the response details recieved from web api   
            //        var APIResponse = Res.Content.ReadAsStringAsync().Result;

            //        //Deserializing the response recieved from web api and storing into the Employee list  
            //        retVal = JsonConvert.DeserializeObject<List<Articles>>(APIResponse);

            //    }
            //    //returning the employee list to view  

            //  }
            string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(connectionstring))
            {
                using (var cmd = new SqlCommand("sp_ViewActiveNews", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    conn.Open();

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Articles obj = new Articles();

                            obj.NewsID = reader[0].ToString();
                            obj.Headline = reader[1].ToString();
                            obj.Content = reader[2].ToString();
                            obj.Position = reader[5].ToString();
                            obj.CreatedDate = reader[6].ToString();
                            obj.CreatedBy = reader[7].ToString();
                            obj.UpdatedDate= reader[8].ToString();
                            obj.UpdateBy = reader[9].ToString();

                            retVal.Add(obj);
                        }
                    }
                }

            }


            return retVal;
        }
        public async Task<ActionResult> PengadaanDitolak()
        {
            string roleId = Session["roleId"].ToString();
            string userName = Session["userName"].ToString();
            string teamId = Session["teamId"].ToString();
            List<NotificationPengadaanDitolak> objInfo = new List<NotificationPengadaanDitolak>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.NotificationPengadaanDitolakAPI + "/" + roleId + "/" + userName + "/" + teamId);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<List<NotificationPengadaanDitolak>>(APIResponse);

                }

                //returning the employee list to view  
                return View(objInfo);
            }
        }
        public async Task<ActionResult> ReviewPerjanjian()
        {
            string roleId = Session["roleId"].ToString();
            string userName = Session["userName"].ToString();
            string teamId = Session["teamId"].ToString();
            List<NotificationReviewPerjanjian> objInfo = new List<NotificationReviewPerjanjian>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.NotificationReviewPerjanjianAPI + "/" + roleId + "/" + userName + "/" + teamId);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<List<NotificationReviewPerjanjian>>(APIResponse);

                }

                //returning the employee list to view  
                return View(objInfo);
            }
        }
        public async Task<ActionResult> BelumUploadCapex()
        {
            string roleId = Session["roleId"].ToString();
            string userName = Session["userName"].ToString();
            string teamId = Session["teamId"].ToString();
            List<NotificationBelumUploadCapex> objInfo = new List<NotificationBelumUploadCapex>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.NotificationBelumUploadCapexAPI + "/" + roleId + "/" + userName + "/" + teamId);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<List<NotificationBelumUploadCapex>>(APIResponse);

                }

                //returning the employee list to view  
                return View(objInfo);
            }
        }
        public async Task<ActionResult> BelumUploadCeloxis()
        {
            string roleId = Session["roleId"].ToString();
            string userName = Session["userName"].ToString();
            string teamId = Session["teamId"].ToString();
            List<NotificationBelumUploadCeloxis> objInfo = new List<NotificationBelumUploadCeloxis>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.NotificationBelumUploadCeloxisAPI + "/" + roleId + "/" + userName + "/" + teamId);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<List<NotificationBelumUploadCeloxis>>(APIResponse);

                }

                //returning the employee list to view  
                return View(objInfo);
            }
        }
        public async Task<ActionResult> BelumUploadOpex()
        {
            string roleId = Session["roleId"].ToString();
            string userName = Session["userName"].ToString();
            string teamId = Session["teamId"].ToString();
            List<NotificationBelumUploadOpex> objInfo = new List<NotificationBelumUploadOpex>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.NotificationBelumUploadOpexAPI + "/" + roleId + "/" + userName + "/" + teamId);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<List<NotificationBelumUploadOpex>>(APIResponse);

                }

                //returning the employee list to view  
                return View(objInfo);
            }
        }
        public async Task<ActionResult> BelumUploadKPI()
        {
            string roleId = Session["roleId"].ToString();
            string userName = Session["userName"].ToString();
            string teamId = Session["teamId"].ToString();
            List<NotificationBelumUploadKPI> objInfo = new List<NotificationBelumUploadKPI>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.NotificationBelumUploadKPIAPI + "/" + roleId + "/" + userName + "/" + teamId);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<List<NotificationBelumUploadKPI>>(APIResponse);

                }

                //returning the employee list to view  
                return View(objInfo);
            }
        }
        public async Task<ActionResult> MenungguApprovalPengadaan()
        {
            string roleId = Session["roleId"].ToString();
            string userName = Session["userName"].ToString();
            string teamId = Session["teamId"].ToString();
            List<NotificationMenungguApprovalPengadaan> objInfo = new List<NotificationMenungguApprovalPengadaan>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.NotificationMenungguApprovalPengadaanAPI + "/" + roleId + "/" + userName + "/" + teamId);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<List<NotificationMenungguApprovalPengadaan>>(APIResponse);

                }

                //returning the employee list to view  
                return View(objInfo);
            }
        }
        public async Task<ActionResult> PerjanjianAkanJatuhTempo()
        {
            string roleId = Session["roleId"].ToString();
            string userName = Session["userName"].ToString();
            string teamId = Session["teamId"].ToString();
            List<NotificationPerjanjianAkanJatuhTempo> objInfo = new List<NotificationPerjanjianAkanJatuhTempo>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.NotificationPerjanjianAkanJatuhTempoAPI + "/" + roleId + "/" + userName + "/" + teamId);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<List<NotificationPerjanjianAkanJatuhTempo>>(APIResponse);

                }

                //returning the employee list to view  
                return View(objInfo);
            }
        }
        public async Task<ActionResult> PerjanjianTelahJatuhTempo()
        {
            string roleId = Session["roleId"].ToString();
            string userName = Session["userName"].ToString();
            string teamId = Session["teamId"].ToString();
            List<NotificationPerjanjianTelahJatuhTempo> objInfo = new List<NotificationPerjanjianTelahJatuhTempo>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.NotificationPerjanjianTelahJatuhTempoAPI + "/" + roleId + "/" + userName + "/" + teamId);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<List<NotificationPerjanjianTelahJatuhTempo>>(APIResponse);

                }

                //returning the employee list to view  
                return View(objInfo);
            }
        }
        public async Task<ActionResult> SudahUploadKPI()
        {
            string roleId = Session["roleId"].ToString();
            string userName = Session["userName"].ToString();
            string teamId = Session["teamId"].ToString();
            List<NotificationSudahUploadKPI> objInfo = new List<NotificationSudahUploadKPI>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.NotificationSudahUploadKPIAPI + "/" + roleId + "/" + userName + "/" + teamId);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<List<NotificationSudahUploadKPI>>(APIResponse);

                }

                //returning the employee list to view  
                return View(objInfo);
            }
        }
    }
}