﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using MandiriCRE.Models;
using System.IO;
using System.Data.SqlClient;
using System.Data;
using System.Web.UI;
using Simplexcel;
using System.Text.RegularExpressions;
using MandiriCRE.ViewModel;

namespace MandiriCRE.Controllers
{
    public class UtilitasGedungController : Controller
    {
        string Baseurl = Properties.Settings.Default.BaseURI;
        string Baseurl2 = Properties.Settings.Default.BaseURI;
        // GET: Resident

        public async Task<ActionResult> Index()
        {

            ViewBag.ListBuilding = await GetListBuilding();

            ViewBag.ListURCCode = await GetListURCCode();

            ViewBag.ListSubject = await GetListSubject();

            var model = new BuildingUtilityIndexViewModel();

            model.InitializePermissions(
                Session,
                "20"
            );

            model.BuildingUtilityJson = await JsonDownloader.GetJsonOrDefaultAsync(
                Baseurl2,
                null,
                EmptyJsonDefault.EmptyArray
            );

            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Index(FormCollection form)
        {


            string buildingID = form["BuildingID"].ToString();
            string yearID = form["YearID"].ToString();
            if (yearID == "")
            {
                yearID = "0";
            }
            string monthID = "0";
            if (form["MonthID"] != null && form["MonthID"] != "")
            {
                monthID = form["MonthID"].ToString();
            }
            string subjectID = form["SubjectID"].ToString();
            string uRCCodeID = form["URCCodeID"].ToString();

            String MonthName = "";
            switch (monthID)
            {
                case "1":
                    MonthName="Januari";
                    break;
                case "2":
                    MonthName = "Februari";
                    break;
                case "3":
                    MonthName = "Maret";
                    break;
                case "4":
                    MonthName = "April";
                    break;
                case "5":
                    MonthName = "Mei";
                    break;
                case "6":
                    MonthName = "Juni";
                    break;
                case "7":
                    MonthName = "Juli";
                    break;
                case "8":
                    MonthName = "Agustus";
                    break;
                case "9":
                    MonthName = "September";
                    break;
                case "10":
                    MonthName = "Oktober";
                    break;
                case "11":
                    MonthName = "November";
                    break;
                case "12":
                    MonthName = "Desember";
                    break;
            }

            ViewBag.buildingID = buildingID;
            
            ViewBag.monthID = monthID;
            ViewBag.MonthName = MonthName;
            ViewBag.subjectID = subjectID;
            ViewBag.uRCCodeID = uRCCodeID;


            ViewBag.ListBuilding = await GetListBuilding();


            ViewBag.ListURCCode = await GetListURCCode();

            ViewBag.ListSubject = await GetListSubject();



            if (form["Excel"] != null)
            {
                return RedirectToAction("DownloadExcel", "UtilitasGedung", new { buildingID = buildingID, yearID = yearID, monthID=monthID, subjectID=subjectID,uRCCodeID=uRCCodeID });
            }

            var model = new BuildingUtilityIndexViewModel();

            model.InitializePermissions(
                Session,
                "20"
            );

            model.BuildingUtilityJson = await JsonDownloader.GetJsonOrDefaultAsync(
                Baseurl2,
                Properties.Settings.Default.LaporanBuildingUtilityAPI+ "?BuildingId="+buildingID+ "&YearId="+yearID+ "&MonthId="+monthID+ "&SubjectId="+subjectID+ "&URCCodeId="+uRCCodeID,
                EmptyJsonDefault.EmptyArray
            );

            dynamic objInfo = JsonConvert.DeserializeObject(model.BuildingUtilityJson);


            double TotSetOff = 0;
            foreach (var itemBuildingUtility in objInfo)
            {
                TotSetOff = TotSetOff + (Convert.ToDouble(itemBuildingUtility.Invoice)- Convert.ToDouble(itemBuildingUtility.SetOff));
            }

            ViewBag.TotalSetOff = String.Format("{0:n0}", Convert.ToDouble(TotSetOff)).Replace(",", ".");

            //returning the employee list to view  

            if (yearID == "0")
            {
                yearID = "";
            }
            ViewBag.yearID = yearID;

            return View(model);            
        }

        public async Task<ActionResult> ViewDetail(string id)
        {
            List<BuildingOccupants> objInfo = new List<BuildingOccupants>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl2);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllArticles using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.BuildingOccupants + "/" + id);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<List<BuildingOccupants>>(APIResponse);

                }
                
                BuildingOccupants objOne = new BuildingOccupants();
                foreach (BuildingOccupants Obj in objInfo)
                {
                    objOne = Obj;
                }
                objOne.NumberOfEmployee = String.Format("{0:n0}", Convert.ToDouble(objOne.NumberOfEmployee)).Replace(",", ".");
                objOne.BuildingArea = String.Format("{0:n0}", Convert.ToDouble(objOne.BuildingArea)).Replace(",", ".");

                ViewBag.BuildingOccupantsID = id;
                ViewBag.objInfo = objOne;
                return View(objOne);
            }

        }

        public async Task<BuildingViewModel> GetListBuilding()
        {
            var _list = new BuildingViewModel();

            _list.InitializePermissions(
                Session,
                "20"
            );

            _list.BuildingJson = await JsonDownloader.GetJsonOrDefaultAsync(
                Baseurl2,
                Properties.Settings.Default.MasterBuildingAPI,
                EmptyJsonDefault.EmptyArray
            );

            return _list;
        }

        public async Task<URCCodeViewModel> GetListURCCode()
        {
            var _list = new URCCodeViewModel();

            _list.InitializePermissions(
               Session,
               "20"
           );

            _list.URCCodeJson = await JsonDownloader.GetJsonOrDefaultAsync(
                Baseurl2,
                Properties.Settings.Default.URCCodeAPI,
                EmptyJsonDefault.EmptyArray
            );
            return _list;
        }

        public async Task<SubjectViewModel> GetListSubject()
        {
            SubjectViewModel _list = new SubjectViewModel();

            _list.InitializePermissions(
               Session,
               "20"
           );

            _list.SubjectJson = await JsonDownloader.GetJsonOrDefaultAsync(
                Baseurl2,
                Properties.Settings.Default.SubjectAPI,
                EmptyJsonDefault.EmptyArray
            );
            return _list;
        }

        public ActionResult Preview(string docfile)
        {

            ViewBag.DocFile = docfile;

            return View();


        }

        public async Task<FileResult> DownloadExcel(string buildingID, string yearID, string monthID, string subjectID, string uRCCodeID)
        {
            string filename = "LaporanUtilitas.xls";
            string path = Server.MapPath("~/Content/Download/");
            string fullPath = Path.Combine(Server.MapPath("~/Content/Download/"), filename);


            Workbook wb = new Workbook();
            Worksheet ws = new Worksheet("Laporan Utilitas");
            for (int c = 0; c <= 10; c++) // this will aply it form col 1 to 10
            {
                ws.ColumnWidths[c] = 20;
                ws.ColumnWidths[4] = 40;

            }

            ws.Cells[0, 0] = "Tahun";
            ws.Cells[0, 1] = "Bulan";
            ws.Cells[0, 2] = "Pencadangan"; //req231018
            ws.Cells[0, 3] = "Gedung";
            ws.Cells[0, 4] = "URC"; //req231018
            ws.Cells[0, 5] = "Perihal";
            ws.Cells[0, 6] = "Tagihan (Rp)";
            ws.Cells[0, 7] = "SetOff (Rp)";
            ws.Cells[0, 8] = "FPP";

            for (int c = 0; c <= 10; c++) // this will aply it form col 1 to 10
            {
                ws.Cells[0, c].Bold = true;

            }

            ViewBag.ListBuilding = await GetListBuilding();

            ViewBag.ListURCCode = await GetListURCCode();

            ViewBag.ListSubject = await GetListSubject();


            List<BuildingUtility> objInfo = new List<BuildingUtility>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl2);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.LaporanBuildingUtilityAPI + "?BuildingId=" + buildingID + "&YearId=" + yearID + "&MonthId=" + monthID + "&SubjectId=" + subjectID + "&URCCodeId=" + uRCCodeID);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the BuildingUtility List
                    objInfo = JsonConvert.DeserializeObject<List<BuildingUtility>>(APIResponse);

                    for (int i = 0; i < objInfo.Count; i++)
                    {
                        ws.Cells[i + 1, 0] = objInfo[i].YearID;
                        ws.Cells[i + 1, 1] = objInfo[i].MonthName;
                        ws.Cells[i + 1, 2] = objInfo[i].Pencadangan; //String.Format("{0:n0}", Convert.ToDouble(objInfo[i].Pencadangan)).Replace(",", "."); //req231018
                        ws.Cells[i + 1, 3] = objInfo[i].BuildingName;
                        ws.Cells[i + 1, 4] = objInfo[i].URCCode; //req231018
                        ws.Cells[i + 1, 5] = objInfo[i].Subject;
                        ws.Cells[i + 1, 6] = String.Format("{0:n0}", Convert.ToDouble(objInfo[i].Invoice)).Replace(",", "."); 
                        ws.Cells[i + 1, 7] = String.Format("{0:n0}", Convert.ToDouble(objInfo[i].SetOff)).Replace(",", ".");
                        ws.Cells[i + 1, 8] = objInfo[i].DocFileName;
                    }
                    wb.Add(ws);
                    wb.Save(fullPath);
                    filename = MakeValidFileName(filename);
                    
                }

            }

            return File(fullPath, "application/vnd.ms-excel", filename);
            var fileStream = new MemoryStream();
            wb.Save(fileStream, Simplexcel.CompressionLevel.NoCompression);



        }

        public static string MakeValidFileName(string name)
        {
            string invalidChars = Regex.Escape(new string(System.IO.Path.GetInvalidFileNameChars()));
            string invalidReStr = string.Format(@"[{0}]+", invalidChars);
            string replace = Regex.Replace(name, invalidReStr, "_").Replace(";", "").Replace(",", "");
            return replace;
        }
    }
}