﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using MandiriCRE.Models;
using System.IO;
using System.Data.SqlClient;
using System.Data;
using System.Web.UI;
using Simplexcel;
using System.Text.RegularExpressions;
using MandiriCRE.ViewModel;

namespace MandiriCRE.Controllers
{
    public class MasterResidenceController : Controller
    {
        string Baseurl = Properties.Settings.Default.BaseURI;
        string Baseurl2 = Properties.Settings.Default.BaseURI;
        // GET: MasterResidence
        public async Task<ActionResult> Index()
        {
            var model = new MasterResidenceViewModel();

            model.InitializePermissions(
                Session,
                "20"
            );

            model.MasterResidenceJson = await JsonDownloader.GetJsonOrDefaultAsync(
                Baseurl2,
                Properties.Settings.Default.MasterResidenceAPI,
                EmptyJsonDefault.EmptyArray
            );

            return View(model);

        
        }

        public async Task<ActionResult> Details(string id)
        {
            MasterResidence objInfo = new MasterResidence();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl2);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllATTB using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.MasterResidenceAPI + "/" + id);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the ATTB list  
                    objInfo = JsonConvert.DeserializeObject<MasterResidence>(APIResponse);

                }
                ViewBag.MasterResidence = id;
                return View(objInfo);
            }
        }
        public async Task<ActionResult> View(string id)
        {
            List<MasterResidence> objInfo = new List<MasterResidence>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl2);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllArticles using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.MasterResidenceAPI + "/" + id);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<List<MasterResidence>>(APIResponse);

                }
                List<EmployeePosition> _listEmployeePosition = await GetEmployeePosition();
                ViewBag.ListEmployeePosition = _listEmployeePosition;

                List<City> _listCity = await GetCities();
                ViewBag.ListCity = _listCity;

                List<ResidenceClass> _listResidenceClass = await GetResidenceClass();
                ViewBag.ListResidenceClass = _listResidenceClass;

                List<ResidenceStatus> _listResidenceStatus = await GetResidenceStatus();
                ViewBag.ListResidenceStatus = _listResidenceStatus;

                List<ResidenceCondition> _listResidenceCondition = await GetResidenceCondition();
                ViewBag.ListResidenceCondition = _listResidenceCondition;

                List<RepairType> _listRepairType = await GetRepairType();
                ViewBag.ListRepairType = _listRepairType;

                List<LogHistory> _listHistories = GetListHistory(id);
                ViewBag.ListHistory = _listHistories;

                MasterResidence objOne = new MasterResidence();
                foreach (MasterResidence Obj in objInfo)
                {
                    objOne = Obj;
                }
                objOne.BuildingArea = String.Format("{0:n0}", Convert.ToDouble(objOne.BuildingArea)).Replace(",", ".");
                objOne.LandArea = String.Format("{0:n0}", Convert.ToDouble(objOne.LandArea)).Replace(",", ".");
                objOne.AcquisitionValue = String.Format("{0:n0}", Convert.ToDouble(objOne.AcquisitionValue)).Replace(",", ".");
                objOne.RenovationCost = String.Format("{0:n0}", Convert.ToDouble(objOne.RenovationCost)).Replace(",", ".");
                ViewBag.MasterResidenceID = id;
                ViewBag.objInfo = objOne;
                return View(objOne);
            }

        }

        public PartialViewResult PartialMasterResidenceView(MasterResidence obj, List<EmployeePosition> _listEmployeePosition, List<City> _listCity, List<ResidenceClass> _listResidenceClass, List<ResidenceStatus> _listResidenceStatus, List<ResidenceCondition> _listResidenceCondition, List<RepairType> _listRepairType)
        {
            ViewBag.ListEmployeePosition = _listEmployeePosition;
            ViewBag.ListCity = _listCity;
            ViewBag.ListResidenceClass = _listResidenceClass;
            ViewBag.ListResidenceStatus = _listResidenceStatus;
            ViewBag.ListResidenceCondition = _listResidenceCondition;
            ViewBag.ListRepairType = _listRepairType;
            

            MasterResidence objInfo = obj;
            if (obj == null)
            {
                obj = new MasterResidence();
            }
            return PartialView("PartialMasterResidenceView", obj);
        }

        public PartialViewResult PartialMasterResidence(MasterResidence obj, List<EmployeePosition> _listEmployeePosition, List<City> _listCity, List<ResidenceClass> _listResidenceClass, List<ResidenceStatus> _listResidenceStatus, List<ResidenceCondition> _listResidenceCondition, List<RepairType> _listRepairType)
        {

            ViewBag.ListEmployeePosition = _listEmployeePosition;
            ViewBag.ListCity = _listCity;
            ViewBag.ListResidenceClass = _listResidenceClass;
            ViewBag.ListResidenceStatus = _listResidenceStatus;
            ViewBag.ListResidenceCondition = _listResidenceCondition;
            ViewBag.ListRepairType = _listRepairType;


            MasterResidence objInfo = obj;
            if (obj == null)
            {
                obj = new MasterResidence();
            }
            return PartialView("PartialMasterResidence", obj);
        }

        //GET: Tax/Create
        public async Task<ActionResult> Create(string ErrorMessage, MasterResidence obj)
        {
            if (ErrorMessage != null)
            {
                ViewBag.ErrorMessage = ErrorMessage;
            }
            List<EmployeePosition> _listEmployeePosition = await GetEmployeePosition();
            ViewBag.ListEmployeePosition = _listEmployeePosition;

            List<City> _listCity = await GetCities();
            ViewBag.ListCity = _listCity;

            List<ResidenceClass> _listResidenceClass = await GetResidenceClass();
            ViewBag.ListResidenceClass = _listResidenceClass;

            List<ResidenceStatus> _listResidenceStatus = await GetResidenceStatus();
            ViewBag.ListResidenceStatus = _listResidenceStatus;

            List<ResidenceCondition> _listResidenceCondition = await GetResidenceCondition();
            ViewBag.ListResidenceCondition = _listResidenceCondition;

            List<RepairType> _listRepairType = await GetRepairType();
            ViewBag.ListRepairType = _listRepairType;

            MasterResidence objInfo = new MasterResidence();
            if (obj != null)
            {
                objInfo = obj;
            }
            if (ErrorMessage != null)
            {
                ViewBag.ErrorMessage = ErrorMessage;
            }

            //get RunningNumber resident

            String No = "";
            string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(connectionstring))
            {
                using (var cmd = new SqlCommand("sp_NoResident", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    conn.Open();

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            No = "0000" + reader[0].ToString();
                        }

                    }


                }
            }

            ViewBag.No = No.Substring(No.Length-5, 5);


            return View(objInfo);
        }
        // POST: Procurement/Create
        [HttpPost]
        public async Task<ActionResult> Create(MasterResidence objResidence, FormCollection collection, HttpPostedFileBase file)
        {
            try
            {
                MasterResidence obj = new MasterResidence();

                obj.AcquisitionValue = collection["AcquisitionValue"].ToString();
                obj.AcquisitionYear = collection["AcquisitionYear"].ToString();
                obj.Address = collection["Address"].ToString();
                obj.BuildingArea = collection["BuildingArea"].ToString();
                obj.CityID = collection["CityID"].ToString();
                obj.ClassID = collection["ClassID"].ToString();
                //obj.CreatedBy = collection["CreatedBy"].ToString();
                //obj.CreatedDate = collection["CreatedDate"].ToString();
                obj.Description = collection["Description"].ToString();
                //obj.DocFileName = collection["DocFileName"].ToString();
                obj.EmployeePositionID = collection["EmployeePositionID"].ToString();
                //obj.FolderName = collection["FolderName"].ToString();
                obj.LandArea = collection["LandArea"].ToString();
                obj.ListNo = collection["ListNo"].ToString();
                //obj.Path = collection["Path"].ToString();
                obj.PDAMID = collection["PDAMID"].ToString();
                obj.PLNID = collection["PLNID"].ToString();
                obj.RenovationCost = collection["RenovationCost"].ToString();
                obj.RenovationDate = collection["RenovationDate"].ToString();
                obj.RepairTypeID = "";
                if (collection["RepairTypeID"] != null)
                {
                    obj.RepairTypeID = collection["RepairTypeID"].ToString();
                }
                obj.ResidenceConditionID = collection["ResidenceConditionID"].ToString();
                obj.ResidenceStatusID = collection["ResidenceStatusID"].ToString();
                obj.ResidentName = collection["ResidentName"].ToString();
                obj.TelephoneID = collection["TelephoneID"].ToString();
                //obj.UpdatedBy = collection["UpdatedBy"].ToString();
                //obj.UpdatedDate = collection["UpdatedDate"].ToString();

                if (collection["docFileName"] != null && collection["docFileName"] != "")
                {
                    obj.DocFileName = collection["docFileName"];
                }
                if (collection["Path"] != null && collection["Path"] != "")
                {
                    obj.Path = collection["Path"];
                }
                if (collection["FolderName"] != null && collection["FolderName"] != "")
                {
                    obj.FolderName = collection["FolderName"];
                }
                obj.VendorResidentialManagement = collection["VendorResidentialManagement"].ToString();
                obj.ActionSave = "INSERT";
                obj.ResidentID = "0";
                obj.UserName = User.Identity.Name;

                string ErrorMessage = "";
                string fileName = "";
                string path = "";
                string fileType = "";
                //Upload File
                if (file != null && file.ContentLength > 0)
                {
                    fileType = Path.GetExtension(file.FileName);
                    if (fileType.ToUpper().Contains("PNG") || fileType.ToUpper().Contains("JPG"))// || fileType.ToUpper().Contains("JPEG"))
                    {
                        //if (file.ContentLength <= (2000 * 1024))
                        //{
                        fileName = Path.GetFileName(file.FileName);
                        path = Path.Combine(Server.MapPath("~/content/document/residence/"), fileName);
                        file.SaveAs(path);

                        //insert file data
                        obj.DocFileName = fileName;
                        obj.FolderName = "residence";
                        obj.Path = path;

                        //}
                        //else
                        //{
                        //    ErrorMessage = "File Upload tidak boleh lebih dari 2MB";
                        //    return RedirectToAction("CreateAttachmentPengadaan", new { id = id, Total = Total, ErrorMessage = ErrorMessage });
                        //}
                    }
                    else
                    {
                        ErrorMessage = "File Upload harus dengan format PNG dan JPG";
                        return RedirectToAction("Create", new { ErrorMessage = ErrorMessage });
                    }
                }

                #region cekduplicate
                MasterResidence obj2 = new MasterResidence();

                string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    using (var cmd = new SqlCommand("sp_CheckDuplicateResident", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ResidentID", 0);
                        cmd.Parameters.AddWithValue("@ListNo", obj.ListNo);
                        cmd.Parameters.AddWithValue("@EmployeePositionID", obj.EmployeePositionID);
                        cmd.Parameters.AddWithValue("@Address", obj.Address);
                        cmd.Parameters.AddWithValue("@CityID", obj.CityID);
                        cmd.Parameters.AddWithValue("@LandArea", obj.LandArea);
                        cmd.Parameters.AddWithValue("@BuildingArea", obj.BuildingArea);
                        cmd.Parameters.AddWithValue("@ClassID", obj.ClassID);
                        cmd.Parameters.AddWithValue("@ResidentName", obj.ResidentName);
                        cmd.Parameters.AddWithValue("@ResidenceStatusID", obj.ResidenceStatusID);
                        cmd.Parameters.AddWithValue("@ResidenceConditionID", obj.ResidenceConditionID);
                        cmd.Parameters.AddWithValue("@RepairTypeID", obj.RepairTypeID);
                        cmd.Parameters.AddWithValue("@Description", obj.Description);
                        cmd.Parameters.AddWithValue("@PLNID", obj.PLNID);
                        cmd.Parameters.AddWithValue("@PDAMID", obj.PDAMID);
                        cmd.Parameters.AddWithValue("@TelephoneID", obj.TelephoneID);
                        cmd.Parameters.AddWithValue("@VendorResidentialManagement", obj.VendorResidentialManagement);
                        cmd.Parameters.AddWithValue("@AcquisitionValue", obj.AcquisitionValue);
                        cmd.Parameters.AddWithValue("@AcquisitionYear", obj.AcquisitionYear);
                        cmd.Parameters.AddWithValue("@RenovationDate", obj.RenovationDate);
                        cmd.Parameters.AddWithValue("@RenovationCost", obj.RenovationCost);
                        conn.Open();

                        using (var reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                obj2.ResidentID = reader[0].ToString();
                                ErrorMessage = reader[1].ToString();
                            }
                        }
                    }

                }

                if (ErrorMessage != null && ErrorMessage != "")
                {
                    return await Create(ErrorMessage, obj);
                }
                #endregion

                if (ErrorMessage != null && ErrorMessage != "")
                {
                    return await Create(ErrorMessage, obj);
                }

                // TODO: Add update logic here
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(Baseurl2);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllArticles using HttpClient  
                HttpResponseMessage response = await client.PostAsJsonAsync(Properties.Settings.Default.MasterResidenceAPI + "/" + "0", obj);
                response.EnsureSuccessStatusCode();



                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }


        public async Task<ActionResult> Update(string id, string ErrorMessage)
        {
            List<MasterResidence> objInfo = new List<MasterResidence>();
            if (ErrorMessage != null)
            {
                ViewBag.ErrorMessage = ErrorMessage;
            }

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl2);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllArticles using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.MasterResidenceAPI + "/" + id);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<List<MasterResidence>>(APIResponse);

                }

                List<EmployeePosition> _listEmployeePosition = await GetEmployeePosition();
                ViewBag.ListEmployeePosition = _listEmployeePosition;

                List<City> _listCity = await GetCities();
                ViewBag.ListCity = _listCity;

                List<ResidenceClass> _listResidenceClass = await GetResidenceClass();
                ViewBag.ListResidenceClass = _listResidenceClass;

                List<ResidenceStatus> _listResidenceStatus = await GetResidenceStatus();
                ViewBag.ListResidenceStatus = _listResidenceStatus;

                List<ResidenceCondition> _listResidenceCondition = await GetResidenceCondition();
                ViewBag.ListResidenceCondition = _listResidenceCondition;

                List<RepairType> _listRepairTyper = await GetRepairType();
                ViewBag.ListRepairType = _listRepairTyper;

                List<LogHistory> _listHistories = GetListHistory(id);
                ViewBag.ListHistory = _listHistories;

                ViewBag.MasterResidenceID = id;

                MasterResidence objOne = new MasterResidence();

                foreach (MasterResidence Obj in objInfo)
                {
                    objOne = Obj;
                }

                objOne.BuildingArea = String.Format("{0:n0}", Convert.ToDouble(objOne.BuildingArea)).Replace(",", ".");
                objOne.LandArea = String.Format("{0:n0}", Convert.ToDouble(objOne.LandArea)).Replace(",", ".");
                objOne.AcquisitionValue = String.Format("{0:n0}", Convert.ToDouble(objOne.AcquisitionValue)).Replace(",", ".");
                objOne.RenovationCost = String.Format("{0:n0}", Convert.ToDouble(objOne.RenovationCost)).Replace(",", ".");

                ViewBag.objInfo = objOne;
                return View(objOne);
            }

        }

        [HttpPost]
        public async Task<ActionResult> Update(string id, MasterResidence objResidence, FormCollection collection, HttpPostedFileBase file)
        {
            try
            {
                MasterResidence objInfo = new MasterResidence();

                //Logic to save Resident
                MasterResidence obj = new MasterResidence();

                obj.AcquisitionValue = collection["AcquisitionValue"].ToString();
                obj.AcquisitionYear = collection["AcquisitionYear"].ToString();
                obj.Address = collection["Address"].ToString();
                obj.BuildingArea = collection["BuildingArea"].ToString();
                obj.CityID = collection["CityID"].ToString();
                obj.ClassID = collection["ClassID"].ToString();
                obj.Description = collection["Description"].ToString();
                obj.EmployeePositionID = collection["EmployeePositionID"].ToString();
                obj.LandArea = collection["LandArea"].ToString();
                obj.ListNo = collection["ListNo"].ToString();
                obj.PDAMID = collection["PDAMID"].ToString();
                obj.PLNID = collection["PLNID"].ToString();
                obj.RenovationCost = collection["RenovationCost"].ToString();
                obj.RenovationDate = collection["RenovationDate"].ToString();
                obj.RepairTypeID = "";
                if (collection["RepairTypeID"] != null)
                {
                    obj.RepairTypeID = collection["RepairTypeID"].ToString();
                }
                obj.ResidenceConditionID = collection["ResidenceConditionID"].ToString();
                obj.ResidenceStatusID = collection["ResidenceStatusID"].ToString();
                obj.ResidentName = collection["ResidentName"].ToString();
                obj.TelephoneID = collection["TelephoneID"].ToString();

                if (collection["docFileName"] != null && collection["docFileName"] != "")
                {
                    obj.DocFileName = collection["docFileName"];
                }
                if (collection["Path"] != null && collection["Path"] != "")
                {
                    obj.Path = collection["Path"];
                }
                if (collection["FolderName"] != null && collection["FolderName"] != "")
                {
                    obj.FolderName = collection["FolderName"];
                }
                obj.VendorResidentialManagement = collection["VendorResidentialManagement"].ToString();
                obj.ActionSave = "UPDATE";
                obj.ResidentID = id;
                obj.UserName = User.Identity.Name;

                string ErrorMessage = "";
                string fileName = "";
                string path = "";
                string fileType = "";
                //Upload File
                if (file != null && file.ContentLength > 0)
                {
                    fileType = Path.GetExtension(file.FileName);
                    if (fileType.ToUpper().Contains("PNG") || fileType.ToUpper().Contains("JPG"))// || fileType.ToUpper().Contains("JPEG"))
                    {
                        //if (file.ContentLength <= (2000 * 1024))
                        //{
                        fileName = Path.GetFileName(file.FileName);
                        path = Path.Combine(Server.MapPath("~/content/document/residence/"), fileName);
                        file.SaveAs(path);

                        //insert file data
                        obj.DocFileName = fileName;
                        obj.FolderName = "residence";
                        obj.Path = path;

                        //}
                        //else
                        //{
                        //    ErrorMessage = "File Upload tidak boleh lebih dari 2MB";
                        //    return RedirectToAction("CreateAttachmentPengadaan", new { id = id, Total = Total, ErrorMessage = ErrorMessage });
                        //}
                    }
                    else
                    {
                        ErrorMessage = "File Upload harus dengan format PNG dan JPG";
                        return RedirectToAction("Update", new { id, ErrorMessage = ErrorMessage });
                    }
                }

                if (file == null && (collection["filename"].ToString() != null || collection["filename"].ToString() != ""))
                {
                    obj.DocFileName = collection["filename"].ToString();
                    path = Path.Combine(Server.MapPath("~/content/document/residence/"), fileName);
                }


                #region cekduplicate
                MasterResidence obj2 = new MasterResidence();

                string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    using (var cmd = new SqlCommand("sp_CheckDuplicateResident", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ResidentID", id);
                        cmd.Parameters.AddWithValue("@ListNo", obj.ListNo);
                        cmd.Parameters.AddWithValue("@EmployeePositionID", obj.EmployeePositionID);
                        cmd.Parameters.AddWithValue("@Address", obj.Address);
                        cmd.Parameters.AddWithValue("@CityID", obj.CityID);
                        cmd.Parameters.AddWithValue("@LandArea", obj.LandArea);
                        cmd.Parameters.AddWithValue("@BuildingArea", obj.BuildingArea);
                        cmd.Parameters.AddWithValue("@ClassID", obj.ClassID);
                        cmd.Parameters.AddWithValue("@ResidentName", obj.ResidentName);
                        cmd.Parameters.AddWithValue("@ResidenceStatusID", obj.ResidenceStatusID);
                        cmd.Parameters.AddWithValue("@ResidenceConditionID", obj.ResidenceConditionID);
                        cmd.Parameters.AddWithValue("@RepairTypeID", obj.RepairTypeID);
                        cmd.Parameters.AddWithValue("@Description", obj.Description);
                        cmd.Parameters.AddWithValue("@PLNID", obj.PLNID);
                        cmd.Parameters.AddWithValue("@PDAMID", obj.PDAMID);
                        cmd.Parameters.AddWithValue("@TelephoneID", obj.TelephoneID);
                        cmd.Parameters.AddWithValue("@VendorResidentialManagement", obj.VendorResidentialManagement);
                        cmd.Parameters.AddWithValue("@AcquisitionValue", obj.AcquisitionValue);
                        cmd.Parameters.AddWithValue("@AcquisitionYear", obj.AcquisitionYear);
                        cmd.Parameters.AddWithValue("@RenovationDate", obj.RenovationDate);
                        cmd.Parameters.AddWithValue("@RenovationCost", obj.RenovationCost);
                        conn.Open();

                        using (var reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                obj2.ResidentID = reader[0].ToString();
                                ErrorMessage = reader[1].ToString();
                            }
                        }
                    }

                }

                if (ErrorMessage != null && ErrorMessage != "")
                {
                    return await Update(id, ErrorMessage);
                }
                #endregion

                if (ErrorMessage != null && ErrorMessage != "")
                {
                    return await Update(id, ErrorMessage);
                }

                List<LogHistory> _listHistories = GetListHistory(id);
                ViewBag.ListHistory = _listHistories;

                // TODO: Add update logic here
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(Baseurl2);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));


                //Get data Tax
                //Sending request to find web api REST service resource GetAllArticles using HttpClient  
                //HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.TaxAPI + "/" + id);

                //Checking the response is successful or not which is sent using HttpClient  
                //if (Res.IsSuccessStatusCode)
                //{
                //    //Storing the response details recieved from web api   
                //    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                //    //Deserializing the response recieved from web api and storing into the Employee list  
                //    objInfo = JsonConvert.DeserializeObject<Tax>(APIResponse);

                //}

                // TODO: Add update logic here
                HttpResponseMessage response = await client.PutAsJsonAsync(Properties.Settings.Default.MasterResidenceAPI + "/" + id, obj);
                response.EnsureSuccessStatusCode();

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }

        public async Task<ActionResult> Delete(int id)
        {
            List<MasterResidence> objInfo = new List<MasterResidence>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl2);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllArticles using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.MasterResidenceAPI + "/" + id);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<List<MasterResidence>>(APIResponse);

                }
                //List<ATTBStatusModels> _listATTBStatus = await GetListATTBStatus();
                //ViewBag.ListATTBStatus = _listATTBStatus;

                //List<OwnershipModels> _listOwnership = await GetListOwnership();
                //ViewBag.ListOwnership = _listOwnership;

                //List<ATTBTypeModels> _listATTBType = await GetListATTBType();
                //ViewBag.ListATTBType = _listATTBType;

                //List<RegionAssetModels> _listRegionAsset = await GetListRegionAsset();
                //ViewBag.ListRegionAsset = _listRegionAsset;

                //List<AssetControlModels> _listAssetControl = await GetListAssetControl();
                //ViewBag.ListAssetControl = _listAssetControl;

                //List<PrimaryUserModels> _listPrimaryUser = await GetListPrimaryUser();
                //ViewBag.ListPrimaryUser = _listPrimaryUser;

                //List<LogHistory> _listHistories = GetListHistory(id.ToString());
                //ViewBag.ListHistory = _listHistories;

                MasterResidence objOne = new MasterResidence();
                //foreach (ATTB Obj in objInfo)
                //{
                //    objOne = Obj;
                //}

                ViewBag.MasterResidenceID = id;
                ViewBag.objInfo = objOne;
                return View(objOne);
            }
        }

        // POST: CRUD/Delete/5
        [HttpPost]
        public async Task<ActionResult> Delete(int id, MasterResidence obj)
        {
            // TODO: Add delete logic here
            try
            {

                // TODO: Add update logic here
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(Baseurl2);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                // TODO: Add update logic here
                HttpResponseMessage response = await client.DeleteAsync(Properties.Settings.Default.MasterResidenceAPI + "/" + id + "?UserName=" + User.Identity.Name.ToString());
                response.EnsureSuccessStatusCode();
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }
        public PartialViewResult PartialHistory()
        {
            return PartialView("PartialHistory", new List<LogHistory>());
        }
        public async Task<List<EmployeePosition>> GetEmployeePosition()
        {
            List<EmployeePosition> _list = new List<EmployeePosition>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl2);
                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.EmployeePositionAPI);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    _list = JsonConvert.DeserializeObject<List<EmployeePosition>>(APIResponse);

                }
                //returning the employee list to view  

            }
            return _list;
        }
        public async Task<List<City>> GetCities()
        {
            List<City> _list = new List<City>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl2);
                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.CityAPI);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    _list = JsonConvert.DeserializeObject<List<City>>(APIResponse);

                }
                //returning the employee list to view  

            }
            return _list;
        }
        public async Task<List<ResidenceClass>> GetResidenceClass()
        {
            List<ResidenceClass> _list = new List<ResidenceClass>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl2);
                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.ResidenceClassAPI);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    _list = JsonConvert.DeserializeObject<List<ResidenceClass>>(APIResponse);

                }
                //returning the employee list to view  

            }
            return _list;
        }
        public async Task<List<ResidenceStatus>> GetResidenceStatus()
        {
            List<ResidenceStatus> _list = new List<ResidenceStatus>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl2);
                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.ResidenceStatusAPI);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    _list = JsonConvert.DeserializeObject<List<ResidenceStatus>>(APIResponse);

                }
                //returning the employee list to view  

            }
            return _list;
        }
        public async Task<List<ResidenceCondition>> GetResidenceCondition()
        {
            List<ResidenceCondition> _list = new List<ResidenceCondition>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl2);
                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.ResidenceConditionAPI);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    _list = JsonConvert.DeserializeObject<List<ResidenceCondition>>(APIResponse);

                }
                //returning the employee list to view  

            }
            return _list;
        }
        public async Task<List<RepairType>> GetRepairType()
        {
            List<RepairType> _list = new List<RepairType>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl2);
                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.RepairTypeAPI);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    _list = JsonConvert.DeserializeObject<List<RepairType>>(APIResponse);

                }
                //returning the employee list to view  

            }
            return _list;
        }

        public List<LogHistory> GetListHistory(string id)
        {
            List<LogHistory> _list = new List<LogHistory>();
            

            string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(connectionstring))
            {
                using (var cmd = new SqlCommand("sp_ViewHistoryLog", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@TableID", id);
                    cmd.Parameters.AddWithValue("@TableName", "Resident");
                    conn.Open();

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            LogHistory obj = new LogHistory();

                            obj.LogID = reader[0].ToString();
                            obj.TableName = reader[1].ToString();
                            obj.TableID = reader[2].ToString();
                            obj.Action = reader[3].ToString();
                            obj.UpdatedDate = reader[4].ToString();
                            obj.UpdatedBy = reader[5].ToString();

                            _list.Add(obj);
                        }

                       
                    }
                }

            }

            return _list;
        }

        public ActionResult Preview(string docfile)
        {

            ViewBag.DocFile = docfile;

            return View();


        }
    }
}