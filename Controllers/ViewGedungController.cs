﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using MandiriCRE.Models;
using System.IO;
using System.Data.SqlClient;
using System.Data;
using System.Web.UI;
using Simplexcel;
using System.Text.RegularExpressions;

namespace MandiriCRE.Controllers
{
    public class ViewGedungController : Controller
    {
        string Baseurl = Properties.Settings.Default.BaseURI;
        string Baseurl2 = Properties.Settings.Default.BaseURI;
        // GET: Resident

        public async Task<ActionResult> BuildingView()
        {

            List<Building> _listBuilding = await GetListBuilding();
            ViewBag.ListBuilding = _listBuilding;
            ViewBag.buildingID = 0;


            return View();
        }

        [HttpPost]
        public async Task<ActionResult> BuildingView(FormCollection form)
        {

            
            string buildingID = form["BuildingID"].ToString();
            string HTMLBuilding = "";

            List<Building> _listBuilding = await GetListBuilding();
            ViewBag.ListBuilding = _listBuilding;

            ViewBag.buildingID = buildingID;
            #region GetHTMLBuilding
            string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(connectionstring))
            {
                using (var cmd = new SqlCommand("sp_GenerateBuilding", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@BuildingID", buildingID);
                    conn.Open();

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            HTMLBuilding = reader[0].ToString();
                        }
                    }
                }

            }


            #endregion

            ViewBag.HTMLBuilding = HTMLBuilding;

            return View();
            
        }

        public async Task<ActionResult> ViewDetail(string id)
        {
            List<BuildingOccupants> objInfo = new List<BuildingOccupants>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl2);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllArticles using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.BuildingOccupants + "/" + id);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<List<BuildingOccupants>>(APIResponse);

                }
                
                BuildingOccupants objOne = new BuildingOccupants();
                foreach (BuildingOccupants Obj in objInfo)
                {
                    objOne = Obj;
                }
                objOne.NumberOfEmployee = String.Format("{0:n0}", Convert.ToDouble(objOne.NumberOfEmployee)).Replace(",", ".");
                objOne.BuildingArea = String.Format("{0:n0}", Convert.ToDouble(objOne.BuildingArea)).Replace(",", ".");

                ViewBag.BuildingOccupantsID = id;
                ViewBag.objInfo = objOne;
                return View(objOne);
            }

        }

        public async Task<List<Building>> GetListBuilding()
        {
            List<Building> _list = new List<Building>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl2);
                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.MasterBuildingAPI);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    _list = JsonConvert.DeserializeObject<List<Building>>(APIResponse);

                }
                //returning the employee list to view  

            }
            return _list;
        }

        public ActionResult Preview(string docfile)
        {

            ViewBag.DocFile = docfile;

            return View();


        }
    }
}