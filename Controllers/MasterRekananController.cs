﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using MandiriCRE.Models;
using System.IO;
using System.Data.SqlClient;
using System.Data;


namespace MandiriCRE.Controllers
{
    public class MasterRekananController : Controller
    {
        // GET: MasterRekanan
        string Baseurl = Properties.Settings.Default.BaseURI;
        public async Task<ActionResult> Index()
        {
            List<Partner> objInfo = new List<Partner>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.RekananAPI);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<List<Partner>>(APIResponse);

                }
                //returning the employee list to view  
                return View(objInfo);
            }
        }

        // GET: CRUD/Details/5
        public async Task<ActionResult> Details(int id)
        {
            Partner objInfo = new Partner();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.RekananAPI + "/" + id);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<Partner>(APIResponse);

                }
                //returning the employee list to view  
                return View(objInfo);
            }
        }

        // GET: CRUD/Create
        public ActionResult Create(string ErrorMessage)
        {
            Partner objInfo = new Partner();
            if (ErrorMessage != null)
            {
                ViewBag.ErrorMessage = ErrorMessage;
            }
            return View(objInfo);
        }

        // POST: CRUD/Create
        [HttpPost]
        public async Task<ActionResult> Create(Partner obj,FormCollection collection)
        {
            try
            {
                string ErrorMessage = "";
                if (obj.Industry == null)
                {
                    obj.Industry = "";
                }
                if (obj.Address == null)
                {
                    obj.Address = "";
                }
                if (obj.Phone == null)
                {
                    obj.Phone = "";
                }
                if (obj.Fax == null)
                {
                    obj.Fax = "";
                }
                obj.PIC = collection["picname"].ToString();
                obj.Mail = collection["email"].ToString();
                obj.UpdatedBy = User.Identity.Name;
                obj.UpdatedDate = DateTime.Now.ToString("dd/MM/yyyy");
                obj.CreatedBy = User.Identity.Name;
                obj.CreatedDate = DateTime.Now.ToString("dd/MM/yyyy");
                //check Duplicate
                Partner obj2 = new Partner();
                string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    using (var cmd = new SqlCommand("sp_CheckVendor", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@VendorName", obj.VendorName);
                        conn.Open();

                        using (var reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                obj2.VendorID = reader[0].ToString();
                            }
                        }
                    }

                }

                if (obj2.VendorID != null)
                {
                    ErrorMessage = "Nama Rekanan Sudah Ada";
                    return RedirectToAction("Create", new { ErrorMessage = ErrorMessage });
                }


                // TODO: Add update logic here
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                // TODO: Add update logic here
                HttpResponseMessage response = await client.PostAsJsonAsync(Properties.Settings.Default.RekananAPI, obj);
                //response.EnsureSuccessStatusCode();
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }

        // GET: CRUD/Edit/5
        public async Task<ActionResult> Edit(int id, string ErrorMessage)
        {
            Partner objInfo = new Partner();
            if (ErrorMessage != null)
            {
                ViewBag.ErrorMessage = ErrorMessage;
            }
            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.RekananAPI + "/" + id);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<Partner>(APIResponse);

                }
                //returning the employee list to view  
                return View(objInfo);
            }
        }

        // POST: CRUD/Edit/5
        [HttpPost]
        public async Task<ActionResult> Edit(int id, Partner obj,FormCollection collection)
        {
            try
            {
                string ErrorMessage = "";
                if (obj.Industry == null)
                {
                    obj.Industry = "";
                }
                if (obj.Address == null)
                {
                    obj.Address = "";
                }
                if (obj.Phone == null)
                {
                    obj.Phone = "";
                }
                if (obj.Fax == null)
                {
                    obj.Fax = "";
                }
                obj.PIC = collection["picname"].ToString();
                obj.Mail = collection["email"].ToString();
                obj.UpdatedBy = User.Identity.Name;
                obj.UpdatedDate = DateTime.Now.ToString("dd/MM/yyyy");
                obj.VendorID = id.ToString();
                //check Duplicate
                Partner obj2 = new Partner();
                string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    using (var cmd = new SqlCommand("sp_CheckVendorEdit", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@VendorName", obj.VendorName);
                        cmd.Parameters.AddWithValue("@VendorID", obj.VendorID);
                        conn.Open();

                        using (var reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                obj2.VendorID = reader[0].ToString();
                            }
                        }
                    }

                }

                if (obj2.VendorID != null)
                {
                    ErrorMessage = "Nama Rekanan Sudah Ada";
                    return RedirectToAction("Edit", new { ErrorMessage = ErrorMessage });
                }

                // TODO: Add update logic here
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                // TODO: Add update logic here
                HttpResponseMessage response = await client.PutAsJsonAsync(Properties.Settings.Default.RekananAPI + "/" + id, obj);
                //response.EnsureSuccessStatusCode();
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }

        // GET: CRUD/Delete/5
        public async Task<ActionResult> Delete(int id)
        {
            Partner objInfo = new Partner();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.RekananAPI + "/" + id);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<Partner>(APIResponse);

                }
                //returning the employee list to view  
                return View(objInfo);
            }
        }

        // POST: CRUD/Delete/5
        [HttpPost]
        public async Task<ActionResult> Delete(int id, Partner obj)
        {

            // TODO: Add delete logic here
            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                // TODO: Add update logic here
                HttpResponseMessage response = await client.PostAsJsonAsync(Properties.Settings.Default.RekananAPI + "/delete/" + id, obj);
                response.EnsureSuccessStatusCode();
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }

        public async Task<ActionResult> History(int id)
        {
            List<PartnerHistory> objInfo = new List<PartnerHistory>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.PartnerHistoryAPI + "/" + id);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<List<PartnerHistory>>(APIResponse);

                }
                ViewBag.PartnerName = "";
                if (objInfo.Count > 0)
                {
                    ViewBag.PartnerName = objInfo[0].VendorName;
                }
                //returning the employee list to view  
                return View(objInfo);
            }
        }
    }
}
