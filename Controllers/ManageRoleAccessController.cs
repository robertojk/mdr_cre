﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using MandiriCRE.Models;
using System.IO;
using System.Data.SqlClient;
using System.Data;

namespace MandiriCRE.Controllers
{
    public class ManageRoleAccessController : Controller
    {
        // GET: ManageRoleMember
        string Baseurl = Properties.Settings.Default.BaseURI;
        // GET: MasterHakAkses

        public async Task<ActionResult> Index()
        {
            List<MapRole> objInfo = new List<MapRole>();

            string roleId = Request.QueryString["roleId"];
            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.MapRoleAPI + "/" + roleId);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    var res = JsonConvert.DeserializeObject<List<UserRole>>(APIResponse);
                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<List<MapRole>>(APIResponse);

                }
                ViewBag.RoleId = roleId;
                //returning the employee list to view  
                return View(objInfo);
            }
        }
        // GET: CRUD/Create
        public async Task<ActionResult> Create(string ErrorMessage)
        {
            string roleId = Request.QueryString["roleId"];
            if (ErrorMessage != null)
            {
                ViewBag.ErrorMessage = ErrorMessage;
            }
            MapRole objInfo = new MapRole();
            List<Menu> _listMenus = await GetListMenus();
            ViewBag.ListMenus = _listMenus;
            ViewBag.roleId = roleId;
            return View(objInfo);
        }
        public async Task<List<Menu>> GetListMenus()
        {
            List<Menu> _list = new List<Menu>();


            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.MenuAPI);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    _list = JsonConvert.DeserializeObject<List<Menu>>(APIResponse);

                }
                //returning the employee list to view  

            }
            return _list;
        }
        // POST: CRUD/Create
        [HttpPost]
        public async Task<ActionResult> Create(MapRole obj, FormCollection collection)
        {
            try
            {
                string roleId = collection["roleId"].ToString();
                // TODO: Add update logic here
                obj.Menu_MenuID = collection["Menu_MenuID"].ToString();
                if (collection["viewAccess"] != null)
                {
                    obj.ViewAccess = "Y";
                }
                else
                {
                    obj.ViewAccess = "T";
                }
                if (collection["duplicateAccess"] != null)
                {
                    obj.DuplicateAccess = "Y";
                }
                else
                {
                    obj.DuplicateAccess = "T";
                }
                if (collection["createAccess"] != null)
                {
                    obj.CreateAccess = "Y";
                }
                else
                {
                    obj.CreateAccess = "T";
                }
                if (collection["approveAccess"] != null)
                {
                    obj.ApproveAccess = "Y";
                }
                else
                {
                    obj.ApproveAccess = "T";
                }
                if (collection["updateAccess"] != null)
                {
                    obj.UpdateAccess = "Y";
                }
                else
                {
                    obj.UpdateAccess = "T";
                }
                if (collection["uploadAccess"] != null)
                {
                    obj.UploadAccess = "Y";
                }
                else
                {
                    obj.UploadAccess = "T";
                }
                if (collection["deleteAccess"] != null)
                {
                    obj.DeleteAccess = "Y";
                }
                else
                {
                    obj.DeleteAccess = "T";
                }
                if (collection["downloadAccess"] != null)
                {
                    obj.DownloadAccess = "Y";
                }
                else
                {
                    obj.DownloadAccess = "T";
                }
                obj.Role_RoleID = roleId;
                obj.MenuMapID = collection["Menu_MenuID"].ToString();
                obj.MenuName = "";
                obj.RoleName = "";

                string ErrorMessage = "";
                string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                using (SqlConnection conn2 = new SqlConnection(connectionstring))
                {
                    using (var cmd2 = new SqlCommand("sp_CheckInsertMenuMap", conn2))
                    {
                        cmd2.CommandType = CommandType.StoredProcedure;
                        cmd2.Parameters.AddWithValue("@MENUID", obj.Menu_MenuID);
                        cmd2.Parameters.AddWithValue("@ROLEID", roleId);
                        conn2.Open();

                        using (var reader = cmd2.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                ErrorMessage = reader[1].ToString();
                            }
                        }
                        conn2.Close();
                    }

                }
                if (ErrorMessage != null && ErrorMessage != "")
                {
                    return RedirectToAction("Create", "ManageRoleAccess", new { roleId = roleId, ErrorMessage = ErrorMessage });
                }

                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                // TODO: Add update logic here
                HttpResponseMessage response = await client.PostAsJsonAsync(Properties.Settings.Default.MapRoleAPI + "/" + roleId, obj);
                //response.EnsureSuccessStatusCode();
                return RedirectToAction("Index", "ManageRoleAccess", new { roleId = roleId });
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }

        // GET: CRUD/Delete/5
        public async Task<ActionResult> Delete(string menuMapId, string roleId)
        {
            MapRole objUser = new MapRole();


            string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(connectionstring))
            {
                using (var cmd = new SqlCommand("sp_ViewMenuMapbyMenuMapID", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@MENUMAPID", menuMapId);
                    conn.Open();

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            objUser.MenuMapID = reader[0].ToString();
                            objUser.Menu_MenuID = reader[1].ToString();
                            objUser.MenuName = reader[2].ToString();
                            objUser.ViewAccess = reader[5].ToString();
                            objUser.CreateAccess = reader[6].ToString();
                            objUser.UpdateAccess = reader[7].ToString();
                            objUser.DeleteAccess = reader[8].ToString();
                            objUser.DuplicateAccess = reader[9].ToString();
                            objUser.ApproveAccess = reader[10].ToString();
                            objUser.DownloadAccess = reader[11].ToString();
                            objUser.UploadAccess = reader[12].ToString();

                        }
                    }
                    conn.Close();
                }
            }


            ViewBag.roleId = roleId;
            //return RedirectToAction("Index", "ManageRoleAccess", new { roleId = roleId });

            return View(objUser);
        }

        [HttpPost]
        public async Task<ActionResult> Delete(string menuMapId, string roleId, MapRole obj)
        {
            // TODO: Add delete logic here
            try
            {
                string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    using (var cmd = new SqlCommand("sp_DeleteMenuMap", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@MENUMAPID", menuMapId);
                        conn.Open();
                        int k = cmd.ExecuteNonQuery();
                        if (k != 0)
                        {
                            //sukses
                        }
                        conn.Close();
                    }
                }

                return RedirectToAction("Index", "ManageRoleAccess", new { roleId = roleId });

            }

            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }

        }


    }

    // POST: CRUD/Delete/5
    
    
}