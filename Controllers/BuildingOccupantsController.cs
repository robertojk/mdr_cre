﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using MandiriCRE.Models;
using System.IO;
using System.Data.SqlClient;
using System.Data;
using MandiriCRE.ViewModel;
using System.Web.UI;
using Simplexcel;
using System.Text.RegularExpressions;

namespace MandiriCRE.Controllers
{
    public class BuildingOccupantsController : Controller
    {
        string Baseurl = Properties.Settings.Default.BaseURI;
        string Baseurl2 = Properties.Settings.Default.BaseURI;
        // GET: Resident
        public async Task<ActionResult> Index()
        {

            var model = new BuildingOccupantsIndexViewModel();

            model.InitializePermissions(
                Session,
                "20"
            );

            model.BuildingOccupantsJson = await JsonDownloader.GetJsonOrDefaultAsync(
                Baseurl2,
                Properties.Settings.Default.BuildingOccupants,
                EmptyJsonDefault.EmptyArray
            );

            return View(model);
        }

        public async Task<ActionResult> Create(string ErrorMessage, BuildingOccupants objBuilding)
        {
            if (ErrorMessage != null)
            {
                ViewBag.ErrorMessage = ErrorMessage;
            }

            List<Building> _listBuilding = await GetListBuilding();
            ViewBag.ListBuilding = _listBuilding;

            List<BuildingFloor> _listBuildingFloor = await GetListBuildingFloor();
            ViewBag.ListBuildingFloor = _listBuildingFloor;

            List<BuildingPosition> _listBuildingPosition = await GetListBuildingPosition();
            ViewBag.ListBuildingPosition = _listBuildingPosition;

            List<URCCode> _listURCCode = await GetListURCCode();
            ViewBag.ListURCCode = _listURCCode;

            BuildingOccupants objInfo = new BuildingOccupants();
            if (objBuilding != null)
            {
                objInfo = objBuilding;
            }
            if (ErrorMessage != null)
            {
                ViewBag.ErrorMessage = ErrorMessage;
            }


            if (objInfo.BuildingArea == null || objInfo.BuildingArea =="" )
            {
                objInfo.BuildingArea = "0.00";
            }

            if (objInfo.NumberOfEmployee == null || objInfo.NumberOfEmployee == "")
            {
                objInfo.NumberOfEmployee = "0";
            }

            //ViewBag.Pegawai = "0";
            //ViewBag.Luas = "0.00";
            return View(objInfo);
        }

        // POST: BuildingOccupants/Create
        [HttpPost]
        //public async Task<ActionResult> Create(string id, string WF, ProcurementHeader obj, FormCollection collection)
        public async Task<ActionResult> Create(BuildingOccupants objBuilding, FormCollection collection, HttpPostedFileBase file)
        {
            try
            {
                BuildingOccupants obj = new BuildingOccupants();

                obj.BuildingID = collection["BuildingID"].ToString();
                obj.BuildingFloorID = "0";
                if (collection["BuildingFloorID"] != null && collection["BuildingFloorID"] != "")
                {
                    obj.BuildingFloorID = collection["BuildingFloorID"].ToString();
                }
                obj.BuildingPositionID = collection["BuildingPositionID"].ToString();
                obj.URCCodeID = collection["URCCodeID"].ToString();
                obj.URCCodeDesc = collection["URCCodeDesc"].ToString(); 
                obj.Description = collection["Description"].ToString();
                obj.NumberOfEmployee = collection["NumberOfEmployee"].ToString();
                obj.BuildingArea = collection["BuildingArea"].ToString();
                obj.SpaceLocation = collection["SpaceLocation"].ToString();
                obj.ActionSave = "INSERT";
                obj.BuildingOccupantsID = "0";
                obj.UserName = User.Identity.Name;

                string ErrorMessage = "";
                string fileName = "";
                string path = "";
                string fileType = "";

                ViewBag.Pegawai = obj.NumberOfEmployee;
                ViewBag.Luas = obj.BuildingArea;

                /*DOK 1*/
                if (file == null && (collection["DocumentName"].ToString() != null && collection["DocumentName"].ToString() != ""))
                {
                    fileName = collection["DocumentName"].ToString();
                    obj.DocFileName = fileName;
                    path = Path.Combine(Server.MapPath("~/content/document/buildingoccupants/"), fileName);
                }
                //Upload File
                /*DOK 1*/
                if (file != null && file.ContentLength > 0)
                {
                    fileType = Path.GetExtension(file.FileName);
                    if (fileType.ToUpper().Contains("JPG") || fileType.ToUpper().Contains("PNG") )
                    {
                        if (file.ContentLength <= (5000 * 1024))
                        {
                            fileName = Path.GetFileName(file.FileName);
                            path = Path.Combine(Server.MapPath("~/content/document/buildingoccupants/"), fileName);
                            file.SaveAs(path);

                            //insert file data
                            obj.DocFileName = fileName;
                            obj.FolderName = "buildingoccupants";
                            obj.Path = path;
                        }
                        else
                        {
                            ErrorMessage = "File Upload tidak boleh lebih dari 5 MB";
                            return await Create(ErrorMessage, obj);
                        }

                    }
                    else
                    {
                        ErrorMessage = "File Upload harus dengan format JPG atau PNG";
                        return await Create(ErrorMessage, obj);
                    }
                }


                #region cekduplicate
                BuildingOccupants obj2 = new BuildingOccupants();

                string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    using (var cmd = new SqlCommand("sp_CheckDuplicateBuildingOccupants", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@BuildingOccupantsID", 0);
                        cmd.Parameters.AddWithValue("@BuildingID", obj.BuildingID);
                        cmd.Parameters.AddWithValue("@BuildingPositionID", obj.BuildingPositionID);
                        cmd.Parameters.AddWithValue("@BuildingFloorID", obj.BuildingFloorID);
                        cmd.Parameters.AddWithValue("@URCCodeID", obj.URCCodeID);
                        cmd.Parameters.AddWithValue("@Description", obj.Description);
                        cmd.Parameters.AddWithValue("@NumberOfEmployee", obj.NumberOfEmployee);
                        cmd.Parameters.AddWithValue("@BuildingArea", obj.BuildingArea);
                        cmd.Parameters.AddWithValue("@SpaceLocation", obj.SpaceLocation);
                        conn.Open();

                        using (var reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                obj2.BuildingOccupantsID = reader[0].ToString();
                                ErrorMessage = reader[1].ToString();
                            }
                        }
                    }

                }

                if (ErrorMessage != null && ErrorMessage != "")
                {
                    return await Create(ErrorMessage, obj);
                }
                #endregion


                // TODO: Add update logic here
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(Baseurl2);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllArticles using HttpClient  
                HttpResponseMessage response = await client.PostAsJsonAsync(Properties.Settings.Default.BuildingOccupants + "/" + "0", obj);
                response.EnsureSuccessStatusCode();



                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }

        public async Task<ActionResult> Update(string id, string ErrorMessage)
        {
            List<BuildingOccupants> objInfo = new List<BuildingOccupants>();
            if (ErrorMessage != null)
            {
                ViewBag.ErrorMessage = ErrorMessage;
            }

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl2);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllArticles using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.BuildingOccupants + "/" + id);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<List<BuildingOccupants>>(APIResponse);

                }

                List<Building> _listBuilding = await GetListBuilding();
                ViewBag.ListBuilding = _listBuilding;

                

                List<BuildingPosition> _listBuildingPosition = await GetListBuildingPosition();
                ViewBag.ListBuildingPosition = _listBuildingPosition;

                List<URCCode> _listURCCode = await GetListURCCode();
                ViewBag.ListURCCode = _listURCCode;

                ViewBag.BuildingOccupantsID = id;

                BuildingOccupants objOne = new BuildingOccupants();

                foreach (BuildingOccupants Obj in objInfo)
                {
                    objOne = Obj;

                }

                //List<BuildingFloor> _listBuildingFloor = await GetListBuildingFloor();
                List<BuildingFloor> _listBuildingFloor = await GetFloorbyBuilding2(objOne.BuildingID);
                ViewBag.ListBuildingFloor = _listBuildingFloor;

                objOne.NumberOfEmployee = String.Format("{0:n0}", Convert.ToDouble(objOne.NumberOfEmployee)).Replace(",", ".");
                objOne.BuildingArea = String.Format("{0:n2}", Convert.ToDouble(objOne.BuildingArea));
                ViewBag.objInfo = objOne;

                List<LogHistory> _listHistories = GetListHistory(id);
                ViewBag.ListHistory = _listHistories;

                return View(objOne);
            }

        }

        public PartialViewResult PartialBuildingOccupants(BuildingOccupants obj, List<Building> _listBuilding, List<BuildingFloor> _listBuildingFloor, List<BuildingPosition> _listBuildingPosition, List<URCCode> _listURCCode)
        {

            ViewBag.ListBuilding = _listBuilding;
            ViewBag.ListBuildingFloor = _listBuildingFloor;
            ViewBag.ListBuildingPosition = _listBuildingPosition;
            ViewBag.ListURCCode = _listURCCode;

            BuildingOccupants objInfo = obj;
            if (obj == null)
            {
                obj = new BuildingOccupants();
                obj.NumberOfEmployee = String.Format("{0:n0}", Convert.ToDouble(obj.NumberOfEmployee)).Replace(",", ".");
                obj.BuildingArea = String.Format("{0:n0}", Convert.ToDouble(obj.BuildingArea)).Replace(",", ".");
            }
            return PartialView("PartialBuildingOccupants", obj);
        }

        [HttpPost]
        public async Task<ActionResult> Update(string id, BuildingOccupants obj, FormCollection collection, HttpPostedFileBase file)
        {
            try
            {
                BuildingOccupants objInfo = new BuildingOccupants();

                //Logic to save Tax
                //ProcurementDetail objDetail = new ProcurementDetail();\
                //objDetail.ProcurementID = id;

                obj.BuildingID = collection["BuildingID"].ToString();
                obj.BuildingFloorID = "0";
                if (collection["BuildingFloorID"] != null && collection["BuildingFloorID"] != "")
                {
                    obj.BuildingFloorID = collection["BuildingFloorID"].ToString();
                }
                obj.BuildingPositionID = collection["BuildingPositionID"].ToString();
                obj.URCCodeID = collection["URCCodeID"].ToString();
                obj.Description = collection["Description"].ToString();
                obj.NumberOfEmployee = collection["NumberOfEmployee"].ToString();
                obj.BuildingArea = collection["BuildingArea"].ToString();
                obj.SpaceLocation = collection["SpaceLocation"].ToString();
                obj.ActionSave = "UPDATE";
                obj.BuildingOccupantsID = id;
                obj.UserName = User.Identity.Name;


                string ErrorMessage = "";
                string fileName = "";
                string path = "";
                string fileType = "";
                
                /*DOK 1*/
                if (file == null && (collection["DocumentName"].ToString() != null && collection["DocumentName"].ToString() != ""))
                {
                    fileName = collection["DocumentName"].ToString();
                    obj.DocFileName = fileName;
                    path = Path.Combine(Server.MapPath("~/content/document/buildingoccupants/"), fileName);
                }
                //Upload File
                /*DOK 1*/
                if (file != null && file.ContentLength > 0)
                {
                    fileType = Path.GetExtension(file.FileName);
                    if (fileType.ToUpper().Contains("JPG") || fileType.ToUpper().Contains("PNG"))
                    {
                        if (file.ContentLength <= (5000 * 1024))
                        {
                            fileName = Path.GetFileName(file.FileName);
                            path = Path.Combine(Server.MapPath("~/content/document/buildingoccupants/"), fileName);
                            file.SaveAs(path);

                            //insert file data
                            obj.DocFileName = fileName;
                            obj.FolderName = "buildingoccupants";
                            obj.Path = path;
                        }
                        else
                        {
                            ErrorMessage = "File Upload tidak boleh lebih dari 5 MB";
                            return RedirectToAction("Update", new { id, ErrorMessage = ErrorMessage });
                        }

                    }
                    else
                    {
                        ErrorMessage = "File Upload harus dengan format JPG atau PNG";
                        return RedirectToAction("Update", new { id, ErrorMessage = ErrorMessage });
                    }
                }
                

                #region cekduplicate
                BuildingOccupants obj2 = new BuildingOccupants();

                string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    using (var cmd = new SqlCommand("sp_CheckDuplicateBuildingOccupants", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@BuildingOccupantsID", int.Parse(id));
                        cmd.Parameters.AddWithValue("@BuildingID", int.Parse(obj.BuildingID));
                        cmd.Parameters.AddWithValue("@BuildingPositionID", int.Parse(obj.BuildingPositionID));
                        cmd.Parameters.AddWithValue("@BuildingFloorID", int.Parse(obj.BuildingFloorID));
                        cmd.Parameters.AddWithValue("@URCCodeID", int.Parse(obj.URCCodeID));
                        cmd.Parameters.AddWithValue("@Description", obj.Description);
                        cmd.Parameters.AddWithValue("@NumberOfEmployee", int.Parse(obj.NumberOfEmployee));
                        cmd.Parameters.AddWithValue("@BuildingArea", decimal.Parse(obj.BuildingArea));
                        cmd.Parameters.AddWithValue("@SpaceLocation", obj.SpaceLocation);
                        conn.Open();
                       
                        using (var reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                obj2.BuildingOccupantsID = reader[0].ToString();
                                ErrorMessage = reader[1].ToString();
                            }
                        }
                    }

                }

                if (ErrorMessage != null && ErrorMessage != "")
                {
                    return await Update(id, ErrorMessage);
                }
                #endregion


                // TODO: Add update logic here
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(Baseurl2);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                

                // TODO: Add update logic here
                HttpResponseMessage response = await client.PutAsJsonAsync(Properties.Settings.Default.BuildingOccupants + "/" + id, obj);
                response.EnsureSuccessStatusCode();

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }

        public async Task<ActionResult> View(string id)
        {
            List<BuildingOccupants> objInfo = new List<BuildingOccupants>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl2);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllArticles using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.BuildingOccupants + "/" + id);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<List<BuildingOccupants>>(APIResponse);

                }
                List<Building> _listBuilding = await GetListBuilding();
                ViewBag.ListBuilding = _listBuilding;

                List<BuildingFloor> _listBuildingFloor = await GetListBuildingFloor();
                ViewBag.ListBuildingFloor = _listBuildingFloor;

                List<BuildingPosition> _listBuildingPosition = await GetListBuildingPosition();
                ViewBag.ListBuildingPosition = _listBuildingPosition;

                List<URCCode> _listURCCode = await GetListURCCode();
                ViewBag.ListURCCode = _listURCCode;

                List<LogHistory> _listHistories = GetListHistory(id);
                ViewBag.ListHistory = _listHistories;

                BuildingOccupants objOne = new BuildingOccupants();
                foreach (BuildingOccupants Obj in objInfo)
                {
                    objOne = Obj;
                }
                objOne.NumberOfEmployee = String.Format("{0:n0}", Convert.ToDouble(objOne.NumberOfEmployee)).Replace(",", ".");
                objOne.BuildingArea = String.Format("{0:n0}", Convert.ToDouble(objOne.BuildingArea)).Replace(",", ".");

                ViewBag.BuildingOccupantsID = id;
                ViewBag.objInfo = objOne;
                return View(objOne);
            }

        }

        public PartialViewResult PartialBuildingOccupantsView(BuildingOccupants obj, List<Building> _listBuilding, List<BuildingFloor> _listBuildingFloor, List<BuildingPosition> _listBuildingPosition, List<URCCode> _listURCCode)
        {

            ViewBag.ListBuilding = _listBuilding;
            ViewBag.ListBuildingFloor = _listBuildingFloor;
            ViewBag.ListBuildingPosition = _listBuildingPosition;
            ViewBag.ListURCCode = _listURCCode;

            BuildingOccupants objInfo = obj;
            if (obj == null)
            {
                obj = new BuildingOccupants();
                obj.NumberOfEmployee = String.Format("{0:n0}", Convert.ToDouble(obj.NumberOfEmployee)).Replace(",", ".");
                obj.BuildingArea = String.Format("{0:n0}", Convert.ToDouble(obj.BuildingArea)).Replace(",", ".");
            }
            return PartialView("PartialBuildingOccupantsView", obj);
        }

        public async Task<List<Building>> GetListBuilding()
        {
            List<Building> _list = new List<Building>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl2);
                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.MasterBuildingAPI);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    _list = JsonConvert.DeserializeObject<List<Building>>(APIResponse);

                }
                //returning the employee list to view  

            }
            return _list;
        }

        public async Task<List<BuildingPosition>> GetListBuildingPosition()
        {
            List<BuildingPosition> _list = new List<BuildingPosition>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl2);
                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.BuildingPositionAPI);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    _list = JsonConvert.DeserializeObject<List<BuildingPosition>>(APIResponse);

                }
                //returning the employee list to view  

            }
            return _list;
        }

        public async Task<List<BuildingFloor>> GetListBuildingFloor()
        {
            List<BuildingFloor> _list = new List<BuildingFloor>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl2);
                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.BuildingFloorAPI);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    _list = JsonConvert.DeserializeObject<List<BuildingFloor>>(APIResponse);

                }
                //returning the employee list to view  

            }
            return _list;
        }

        public async Task<List<URCCode>> GetListURCCode()
        {
            List<URCCode> _list = new List<URCCode>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl2);
                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.URCCodeAPI);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    _list = JsonConvert.DeserializeObject<List<URCCode>>(APIResponse);

                }
                //returning the employee list to view  

            }
            return _list;
        }

        public List<LogHistory> GetListHistory(string id)
        {
            List<LogHistory> _list = new List<LogHistory>();
            //LogHistory obj = new LogHistory();

            string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(connectionstring))
            {
                using (var cmd = new SqlCommand("sp_ViewHistoryLog", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@TableID", id);
                    cmd.Parameters.AddWithValue("@TableName", "BuildingOccupants");
                    conn.Open();

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            LogHistory obj = new LogHistory();

                            obj.LogID = reader[0].ToString();
                            obj.TableName = reader[1].ToString();
                            obj.TableID = reader[2].ToString();
                            obj.Action = reader[3].ToString();
                            obj.UpdatedDate = reader[4].ToString();
                            obj.UpdatedBy = reader[5].ToString();

                            _list.Add(obj);
                        }

                        
                    }
                }

            }

            return _list;
        }

        public ActionResult Preview(string docfile)
        {

            ViewBag.DocFile = docfile;

            return View();


        }

        public async Task<String> GetURCDesc(String URCID)
        {
            List<URCCode> _list = new List<URCCode>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl2);
                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.URCCodeAPI);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Subject list  
                    _list = JsonConvert.DeserializeObject<List<URCCode>>(APIResponse);

                }
                //returning the Subject list to view  

            }

            List<URCCode> URCList = new List<URCCode>(_list.Where(Subject => Subject.URCCodeID.Equals(URCID.ToString())));

            URCCode objOne = new URCCode();
            string URCDescription = "";

            foreach (URCCode Obj in URCList)
            {
                URCDescription = Obj.Description.ToString();
                //URCDescription = Obj.URCCodeName.ToString(); //req131018

            }

            Response.Write(JsonConvert.SerializeObject(URCDescription));
            Response.End();

            return JsonConvert.SerializeObject(URCDescription);

        }

        public async Task<JsonResult> GetFloorbyBuilding(String BuildingID)
        {
            List<BuildingFloor> _list = new List<BuildingFloor>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl2);
                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.BuildingFloorAPI);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Subject list  
                    _list = JsonConvert.DeserializeObject<List<BuildingFloor>>(APIResponse);

                }
                //returning the Subject list to view  

            }

            List<BuildingFloor> SubjectList = new List<BuildingFloor>();

            if (BuildingID.ToString() == "6" || BuildingID.ToString() == "3") //Menara mandiri I dan II
            {
                SubjectList = new List<BuildingFloor>(_list.Where(Subject => Subject.BuildingFloorName != "13" && Subject.BuildingFloorName != "12A"));
            }
            else if (BuildingID.ToString() == "1" || BuildingID.ToString() == "5") //wisma mandiri I dan II
            {
                SubjectList = new List<BuildingFloor>(_list.Where(Subject => Subject.BuildingFloorName != "13"));
            }
            else
            {
                SubjectList = new List<BuildingFloor>(_list.Where(Subject => Subject.BuildingFloorName != "12A"));
            }
            
            
            List<SelectListItem> subjects = new List<SelectListItem>();

            foreach (var item in SubjectList)
            {
                SelectListItem subject = new SelectListItem
                {
                    Text = item.BuildingFloorName,
                    Value = item.BuildingFloorID
                };
                subjects.Add(subject);
            }
            return Json(subjects, JsonRequestBehavior.AllowGet);

        }

        public async Task<List<BuildingFloor>> GetFloorbyBuilding2(String BuildingID)
        {
            List<BuildingFloor> _list = new List<BuildingFloor>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl2);
                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.BuildingFloorAPI);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Subject list  
                    _list = JsonConvert.DeserializeObject<List<BuildingFloor>>(APIResponse);

                }
                //returning the Subject list to view  

            }

            List<BuildingFloor> SubjectList = new List<BuildingFloor>();

            if (BuildingID.ToString() == "6" || BuildingID.ToString() == "3") //Menara mandiri I dan II
            {
                SubjectList = new List<BuildingFloor>(_list.Where(Subject => Subject.BuildingFloorName != "13" && Subject.BuildingFloorName != "12A"));
            }
            else if (BuildingID.ToString() == "1" || BuildingID.ToString() == "5") //wisma mandiri I dan II
            {
                SubjectList = new List<BuildingFloor>(_list.Where(Subject => Subject.BuildingFloorName != "13"));
            }
            else
            {
                SubjectList = new List<BuildingFloor>(_list.Where(Subject => Subject.BuildingFloorName != "12A"));
            }
            
            return SubjectList;

        }
    }
}