﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using MandiriCRE.Models;
using System.IO;
using System.Data.SqlClient;
using System.Data;

namespace MandiriCRE.Controllers
{
    public class AddendumController : Controller
    {
        string Baseurl = Properties.Settings.Default.BaseURI;
        // GET: Addendum
        public ActionResult Index()
        {
            List<Addendum> objInfo = new List<Addendum>();
            objInfo = GetAddendumList();


            return View(objInfo);
        }

        public List<Addendum> GetAddendumList()
        {
            List<Addendum> _AddendumList = new List<Addendum>();

            string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(connectionstring))
            {
                using (var cmd = new SqlCommand("sp_ViewAddendumList", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    //cmd.Parameters.AddWithValue("@pMyParamater", myParamaterValue);
                    conn.Open();

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Addendum obj = new Addendum();

                            obj.AddendumID = Convert.ToInt32(reader[0]);
                            obj.AddendumNo = reader[1].ToString();
                            obj.AgreementID = Convert.ToInt32(reader[2]);
                            obj.DocumentNo = reader[3].ToString();
                            obj.AgreementName = reader[4].ToString();
                            obj.Workflow_WorkflowID = Convert.ToInt32(reader[5]);
                            obj.StepName = reader[6].ToString();
                            obj.RolePending = reader[7].ToString();
                            obj.WFStatusName = reader[8].ToString();
                            obj.No = reader[9].ToString();
                            _AddendumList.Add(obj);
                        }
                    }
                }

            }
            return _AddendumList;



        }

        public ActionResult Create(string ErrorMessage,Addendum objAddendum)
        {
            Addendum objInfo = new Addendum();
            List<Agreement> _listAgreement = GetListAgreement();
            ViewBag.ListAgreement = _listAgreement;
            if (ErrorMessage != null)
            {
                ViewBag.ErrorMessage = ErrorMessage;
            }
            if (objAddendum != null)
            {
                objInfo = objAddendum;
            }
            return View(objInfo);

        }

        [HttpPost]
        public ActionResult Create(Addendum obj, FormCollection form)
        {
            try
            {

                obj.AddendumNo = form["AddendumNo"].ToString();
                obj.AgreementID = Convert.ToInt32(form["AgreementID"]);
                obj.AddendumInfo = form["AddendumInfo"].ToString();
                // string Action_ = form["Review"].ToString();
                string Action_ = "";
                if (form["Review"] != null)
                {
                    Action_ = "REVIEW";
                }
                else
                {
                    Action_ = "DONE";
                }
                string ErrorMessage = "";
                string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    using (var cmd = new SqlCommand("sp_CheckDuplicateAddendum", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@AddendumNo", obj.AddendumNo);
                        cmd.Parameters.AddWithValue("@AddendumID", "");
                        conn.Open();

                        using (var reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                //obj.AddendumID = Convert.ToInt32(reader[0]);
                                ErrorMessage = reader[1].ToString();
                            }
                        }
                    }

                }

                if (ErrorMessage != null && ErrorMessage != "")
                {
                    //return RedirectToAction("Create", new { ErrorMessage = ErrorMessage });
                    return Create(ErrorMessage,obj);
                }


                // TODO: Add update logic here
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    using (var cmd = new SqlCommand("sp_InsertAddendum", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@AddendumNo", obj.AddendumNo);
                        cmd.Parameters.AddWithValue("@AgreementID", obj.AgreementID);
                        cmd.Parameters.AddWithValue("@AddendumInfo", obj.AddendumInfo);
                        cmd.Parameters.AddWithValue("@USERNAME", User.Identity.Name);
                        cmd.Parameters.AddWithValue("@ActionSave", Action_);
                        conn.Open();
                        int k = cmd.ExecuteNonQuery();
                        if (k != 0)
                        {
                            //sukses
                        }
                        conn.Close();
                    }
                }
                return RedirectToAction("Index", "Addendum");
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }

        }

        public ActionResult Sign(int id, string WF)
        {
            Addendum objInfo = new Addendum();
            List<Agreement> _listAgreement = GetListAgreement();
            objInfo = GetListAddendumById(id);
            ViewBag.ListAgreement = _listAgreement;
            return View(objInfo);

        }

        [HttpPost]
        public ActionResult Sign(string id, string WF, Addendum obj, FormCollection form)
        {
            try
            {

                obj.AddendumNo = form["AddendumNo"].ToString();
                obj.AgreementID = Convert.ToInt32(form["AgreementID"]);
                obj.AddendumInfo = form["AddendumInfo"].ToString();
                // string Action_ = form["Review"].ToString();
                //string Action_ = "";
                //if (form["Review"] != null)
                //{
                //    Action_ = "REVIEW";
                //}
                //else
                //{
                //    Action_ = "DONE";
                //}

                // TODO: Add update logic here
                string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    using (var cmd = new SqlCommand("sp_UpdateAddendum", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@AddendumID", id);
                        cmd.Parameters.AddWithValue("@AddendumNo", obj.AddendumNo);
                        cmd.Parameters.AddWithValue("@AgreementID", obj.AgreementID);
                        cmd.Parameters.AddWithValue("@AddendumInfo", obj.AddendumInfo);
                        cmd.Parameters.AddWithValue("@UserName", User.Identity.Name);
                        conn.Open();
                        int k = cmd.ExecuteNonQuery();
                        if (k != 0)
                        {
                            //sukses
                        }
                        conn.Close();


                    }

                    using (SqlConnection conn2 = new SqlConnection(connectionstring))
                    {
                        using (var cmd = new SqlCommand("sp_GetNextApproval", conn2))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@WFName", "WF_ADDENDUM");
                            cmd.Parameters.AddWithValue("@Action", "SIGN");
                            cmd.Parameters.AddWithValue("@WorkflowId", WF);
                            cmd.Parameters.AddWithValue("@MainTableId", id);
                            cmd.Parameters.AddWithValue("@RejectReason", "");
                            cmd.Parameters.AddWithValue("@USER_NAME", User.Identity.Name);
                            conn2.Open();
                            int k = cmd.ExecuteNonQuery();
                            if (k != 0)
                            {
                                //sukses
                            }
                            conn2.Close();
                        }
                    }
                }
                return RedirectToAction("Index", "Addendum");
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }

        }

        public async Task<ActionResult> Duplicate(int id)
        {
            ProcurementHeader objInfo = new ProcurementHeader();

            //using (var client = new HttpClient())
            //{
            //    //Passing service base url  
            //    client.BaseAddress = new Uri(Baseurl);

            //    client.DefaultRequestHeaders.Clear();
            //    //Define request data format  
            //    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            //    //Sending request to find web api REST service resource GetAllArticles using HttpClient  
            //    HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.ArticleAPI + "/" + id);

            //    //Checking the response is successful or not which is sent using HttpClient  
            //    if (Res.IsSuccessStatusCode)
            //    {
            //        //Storing the response details recieved from web api   
            //        var APIResponse = Res.Content.ReadAsStringAsync().Result;

            //        //Deserializing the response recieved from web api and storing into the Employee list  
            //        objInfo = JsonConvert.DeserializeObject<ProcurementHeader>(APIResponse);

            //    }
            //    //returning the employee list to view  
            //    return View(objInfo);
            //}

            // TODO: Add update logic here
            string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(connectionstring))
            {
                using (var cmd = new SqlCommand("sp_DuplicatebyAddendumID", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@AddendumID", id);
                    cmd.Parameters.AddWithValue("@USERNAME", User.Identity.Name);
                    conn.Open();
                    int k = cmd.ExecuteNonQuery();
                    if (k != 0)
                    {
                        //sukses
                    }
                    conn.Close();
                }
            }

            return RedirectToAction("Index", "Addendum");
        }

        [HttpPost]
        public ActionResult PartialAddendum(string id, FormCollection form, Addendum obj)
        {

            try
            {
                id = form["AddendumID"].ToString();
                obj.AddendumNo = form["AddendumNo"].ToString();
                obj.AgreementID = Convert.ToInt32(form["AgreementID"]);
                obj.AddendumInfo = form["AddendumInfo"].ToString();
                obj.Workflow_WorkflowID = Convert.ToInt32(form["wfid"]);
                //string Action_ = form["Review"].ToString();
                string Action_ = "";
                if (form["Review"] != null)
                {
                    Action_ = "REVIEW";
                }
                else if (form["Done"] != null)
                {
                    Action_ = "DONE";
                }
                else
                {
                    Action_ = "SIGN";
                }

                string ErrorMessage = "";
                string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    using (var cmd = new SqlCommand("sp_CheckDuplicateAddendum", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@AddendumNo", obj.AddendumNo);
                        cmd.Parameters.AddWithValue("@AddendumID",id);
                        conn.Open();

                        using (var reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                //obj.AddendumID = Convert.ToInt32(reader[0]);
                                ErrorMessage = reader[1].ToString();
                            }
                        }
                    }

                }

                if (ErrorMessage != null && ErrorMessage != "")
                {
                    return RedirectToAction("DetailAddendum", new {id=id, ErrorMessage = ErrorMessage });
                }

                // TODO: Add update logic here
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    using (var cmd = new SqlCommand("sp_UpdateAddendum", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@AddendumID", id);
                        cmd.Parameters.AddWithValue("@AddendumNo", obj.AddendumNo);
                        cmd.Parameters.AddWithValue("@AgreementID", obj.AgreementID);
                        cmd.Parameters.AddWithValue("@AddendumInfo", obj.AddendumInfo);
                        cmd.Parameters.AddWithValue("@UserName", User.Identity.Name);
                        //cmd.Parameters.AddWithValue("@ActionSave", Action_);
                        conn.Open();
                        int k = cmd.ExecuteNonQuery();
                        if (k != 0)
                        {
                            //sukses
                        }
                        conn.Close();
                    }


                    if (Action_ == "DONE")
                    {
                        using (SqlConnection conn2 = new SqlConnection(connectionstring))
                        {
                            using (var cmd = new SqlCommand("sp_GetNextApproval", conn2))
                            {
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Parameters.AddWithValue("@WFName", "WF_ADDENDUM");
                                cmd.Parameters.AddWithValue("@Action", "DONE");
                                cmd.Parameters.AddWithValue("@WorkflowId", obj.Workflow_WorkflowID);
                                cmd.Parameters.AddWithValue("@MainTableId", id);
                                cmd.Parameters.AddWithValue("@RejectReason", "");
                                cmd.Parameters.AddWithValue("@USER_NAME", User.Identity.Name);
                                conn2.Open();
                                int k = cmd.ExecuteNonQuery();
                                if (k != 0)
                                {
                                    //sukses
                                }
                                conn2.Close();
                            }
                        }

                    }

                    if (Action_ == "SIGN")
                    {
                        using (SqlConnection conn2 = new SqlConnection(connectionstring))
                        {
                            using (var cmd = new SqlCommand("sp_GetNextApproval", conn2))
                            {
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Parameters.AddWithValue("@WFName", "WF_ADDENDUM");
                                cmd.Parameters.AddWithValue("@Action", "SIGN");
                                cmd.Parameters.AddWithValue("@WorkflowId", obj.Workflow_WorkflowID);
                                cmd.Parameters.AddWithValue("@MainTableId", id);
                                cmd.Parameters.AddWithValue("@RejectReason", "");
                                cmd.Parameters.AddWithValue("@USER_NAME", User.Identity.Name);
                                conn2.Open();
                                int k = cmd.ExecuteNonQuery();
                                if (k != 0)
                                {
                                    //sukses
                                }
                                conn2.Close();
                            }
                        }

                    }

                }
                Session["activeTabPengadaan"] =null;

                return RedirectToAction("Index", "Addendum");
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }

        }

        [HttpPost]
        public ActionResult PartialAddendumSign(string id, FormCollection form, Addendum obj)
        {

            try
            {
                id = form["AddendumID"].ToString();
                obj.AddendumNo = form["AddendumNo"].ToString();
                obj.AgreementID = Convert.ToInt32(form["AgreementID"]);
                obj.AddendumInfo = form["AddendumInfo"].ToString();
                obj.Workflow_WorkflowID = Convert.ToInt32(form["wfid"]);
                //string Action_ = form["Review"].ToString();
                //string Action_ = "";
                //if (form["Review"] != null)
                //{
                //    Action_ = "REVIEW";
                //}
                //else
                //{
                //    Action_ = "DONE";
                //}

                // TODO: Add update logic here
                string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    using (var cmd = new SqlCommand("sp_UpdateAddendum", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@AddendumID", id);
                        cmd.Parameters.AddWithValue("@AddendumNo", obj.AddendumNo);
                        cmd.Parameters.AddWithValue("@AgreementID", obj.AgreementID);
                        cmd.Parameters.AddWithValue("@AddendumInfo", obj.AddendumInfo);
                        cmd.Parameters.AddWithValue("@UserName", User.Identity.Name);
                        //cmd.Parameters.AddWithValue("@ActionSave", Action_);
                        conn.Open();
                        int k = cmd.ExecuteNonQuery();
                        if (k != 0)
                        {
                            //sukses
                        }
                        conn.Close();
                    }



                    using (SqlConnection conn2 = new SqlConnection(connectionstring))
                    {
                        using (var cmd = new SqlCommand("sp_GetNextApproval", conn2))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@WFName", "WF_ADDENDUM");
                            cmd.Parameters.AddWithValue("@Action", "SIGN");
                            cmd.Parameters.AddWithValue("@WorkflowId", obj.Workflow_WorkflowID);
                            cmd.Parameters.AddWithValue("@MainTableId", id);
                            cmd.Parameters.AddWithValue("@RejectReason", "");
                            cmd.Parameters.AddWithValue("@USER_NAME", User.Identity.Name);
                            conn2.Open();
                            int k = cmd.ExecuteNonQuery();
                            if (k != 0)
                            {
                                //sukses
                            }
                            conn2.Close();
                        }
                    }



                }


                return RedirectToAction("Index", "Addendum");
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }

        }
        public List<Agreement> GetListAgreement()
        {
            List<Agreement> _list = new List<Agreement>();
            string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(connectionstring))
            {
                using (var cmd = new SqlCommand("sp_viewagreementlistSign", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    //cmd.Parameters.AddWithValue("@pMyParamater", myParamaterValue);
                    conn.Open();

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Agreement obj = new Agreement();

                            obj.AgreementID = reader[0].ToString();
                            obj.DocumentNo = reader[1].ToString();
                            obj.AgreementName = reader[8].ToString();

                            _list.Add(obj);
                        }
                    }
                }

            }

            return _list;
        }

        public ActionResult DetailAddendum(int id, string ErrorMessage)
        {
            Addendum objInfo = new Addendum();
            List<Agreement> _listAgreement = GetListAgreement();
            objInfo = GetListAddendumById(id);
            ViewBag.ListAgreement = _listAgreement;
            if (ErrorMessage != null)
            {
                ViewBag.ErrorMessage = ErrorMessage;
            }
            return View(objInfo);
        }
        public Addendum GetListAddendumById(int id)
        {
            Addendum _list = new Addendum();
            string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(connectionstring))
            {
                using (var cmd = new SqlCommand("sp_ViewAddendum", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@AddendumID", id);
                    conn.Open();

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Addendum obj = new Addendum();

                            obj.AddendumID = Convert.ToInt32(reader[0]);
                            obj.AddendumNo = reader[1].ToString();
                            obj.AddendumInfo = reader[2].ToString();
                            obj.AgreementID = Convert.ToInt32(reader[3]);
                            obj.ProcurementName = reader[6].ToString();
                            obj.DocumentNo = reader[4].ToString();
                            obj.AgreementDate = reader[7].ToString();
                            obj.InputDate = reader[8].ToString();
                            obj.JobTypeName = reader[10].ToString();
                            obj.AgreementName = reader[11].ToString();
                            obj.ObjectName = reader[12].ToString();
                            obj.Department_DepartmentID = Convert.ToInt32(reader[13]);
                            obj.EmployeePIC_Name = reader[15].ToString();
                            obj.VendorName = reader[17].ToString();
                            obj.StartDate = reader[18].ToString();
                            obj.EndDate = reader[19].ToString();
                            obj.AgreementValue = Convert.ToDouble(reader[20]).ToString("N0");
                            obj.SLA = reader[21].ToString();
                            obj.AditionalInformation = reader[22].ToString();
                            obj.Workflow_WorkflowID = Convert.ToInt32(reader[23]);
                            obj.StepName = reader[24].ToString();
                            obj.DepartmentName = reader[27].ToString();
                            _list = obj;
                        }
                    }
                }

            }

            return _list;
        }
        //[HttpPost]
        //public async Task<ActionResult> DetailAddendum(Agreement obj, FormCollection collection)
        //{

        //    try
        //    {
        //        obj.ProcurementID = obj.ProcurementID;
        //        obj.DocumentNo = collection["DocumentNo"].ToString() + collection["documentNumber"].ToString();
        //        obj.AgreementDate = collection["agreementdate"].ToString();
        //        obj.InputDate = collection["inputdate"].ToString();
        //        obj.JobTypeID = obj.JobTypeID;
        //        obj.AgreementName = collection["agreementname"].ToString();
        //        obj.ObjectName = collection["objectname"].ToString();
        //        obj.DepartmentID = obj.DepartmentID;
        //        obj.EmployeePIC = obj.EmployeePIC;
        //        obj.VendorID = obj.VendorID;
        //        obj.StartDate = collection["startdate"].ToString();
        //        obj.EndDate = collection["enddate"].ToString();
        //        //obj.AgreementValue= collection["agreementvalue"];
        //        obj.SLA = collection["sla"].ToString();
        //        obj.BASE_SLA = collection["basesla"].ToString();
        //        obj.AdditionalInformation = collection["description"].ToString();
        //        obj.UserName = User.Identity.Name;
        //        obj.ActionSave = "REVIEW"; //value REVIEW or DONE
        //        // TODO: Add update logic here
        //        HttpClient client = new HttpClient();
        //        client.BaseAddress = new Uri(Baseurl);
        //        client.DefaultRequestHeaders.Accept.Clear();
        //        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        //        // TODO: Add update logic here
        //        HttpResponseMessage response = await client.PostAsJsonAsync(Properties.Settings.Default.AgreementAPI + "/" + "0", obj);
        //        response.EnsureSuccessStatusCode();
        //        return RedirectToAction("Index");
        //    }
        //    catch (Exception ex)
        //    {
        //        string err = ex.Message.ToString();
        //        return View();
        //    }

        //}
        public PartialViewResult PartialAddendum(int id, List<Agreement> listAgreement)
        {

            ViewBag.ListAgreement = listAgreement;
            Addendum objInfo = new Addendum();
            objInfo = GetListAddendumById(id);
            return PartialView(objInfo);
        }

        public PartialViewResult PartialAddendumSign(int id, List<Agreement> listAgreement)
        {

            ViewBag.ListAgreement = listAgreement;
            Addendum objInfo = new Addendum();
            objInfo = GetListAddendumById(id);
            return PartialView(objInfo);
        }
        public PartialViewResult PartialAttachment(int id)
        {
            List<AddendumDoc> objInfo = new List<AddendumDoc>();

            objInfo = GetAddendumDocById(id);
            ViewBag.AddendumID = id;
            ViewBag.Total = 0;
            foreach (AddendumDoc vardoc in objInfo)
            {
                ViewBag.Total = vardoc.Total;
            }
            return PartialView(objInfo);
        }

        public List<AddendumDoc> GetAddendumDocById(int id)
        {
            List<AddendumDoc> _list = new List<AddendumDoc>();
            string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(connectionstring))
            {
                using (var cmd = new SqlCommand("sp_viewaddendumdoc", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@AddendumID", id);
                    conn.Open();

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            AddendumDoc obj = new AddendumDoc();

                            obj.UplodedDate = reader[3].ToString();
                            obj.UplodedBy = reader[4].ToString();
                            obj.DocName = reader[1].ToString();
                            obj.DescriptionFile = reader[2].ToString();
                            obj.AddendumDocID = reader[0].ToString();
                            obj.Total = Convert.ToInt32(reader[9]);
                            _list.Add(obj);
                        }
                    }
                }

            }

            return _list;
        }

        public ActionResult CreateAttachment(string id, int Total, string ErrorMessage)
        {
            Session["activeTabPengadaan"] = "s2";
            AddendumDoc objInfo = new AddendumDoc();
            if (ErrorMessage != null)
            {
                ViewBag.ErrorMessage = ErrorMessage;
            }
            ViewBag.Total = Total;
            return View(objInfo);
        }
        // POST: CRUD/Create
        [HttpPost]
        public async Task<ActionResult> CreateAttachment(string id, int Total, ProcurementDoc obj, FormCollection form, HttpPostedFileBase file)
        {
            try
            {
                Session["activeTabPengadaan"] = "s2";
                string ErrorMessage = "";
                string fileType = "";
                string fileName = "";
                string path = "";
                if (Total < 10)
                {
                    if (file != null && file.ContentLength > 0)
                    {
                        fileType = Path.GetExtension(file.FileName);
                        if (fileType.ToUpper().Contains("DOC") || fileType.ToUpper().Contains("XLS") || fileType.ToUpper().Contains("PPT") || fileType.ToUpper().Contains("PDF"))
                        {
                            if (file.ContentLength <= (2000 * 1024))
                            {

                                fileName = Path.GetFileName(file.FileName);
                                path = Path.Combine(Server.MapPath("~/content/document/addendum/"), fileName);
                                file.SaveAs(path);
                            }
                            else
                            {
                                ErrorMessage = "File Upload tidak boleh lebih dari 2MB";
                                return RedirectToAction("CreateAttachment", "Addendum", new { id = id, Total = Total, ErrorMessage = ErrorMessage });
                            }
                        }
                        else
                        {
                            ErrorMessage = "File Upload harus dengan format All Office atau PDF";
                            return RedirectToAction("CreateAttachment", "Addendum", new { id = id, Total = Total, ErrorMessage = ErrorMessage });
                        }
                    }
                }
                else
                {
                    ErrorMessage = "Penambahan gagal, jumlah dokumen maksimal adalah 10";
                    return RedirectToAction("CreateAttachment", "Addendum", new { id = id, Total = Total, ErrorMessage = ErrorMessage });
                }
                obj.DocumentName = fileName;
                obj.createdDate = DateTime.Now.ToString("dd/MM/yyyy");
                obj.CreatedBy = User.Identity.Name;
                obj.UplodedDate = DateTime.Now.ToString("dd/MM/yyyy");
                obj.UplodedBy = User.Identity.Name;
                obj.ModulName = "ADDENDUM";
                obj.TableId = id;
                obj.Description = form["description"].ToString();
                obj.FolderName = "addendum";
                obj.User_Name = User.Identity.Name;
                // TODO: Add update logic here
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                // TODO: Add update logic here
                HttpResponseMessage response = await client.PostAsJsonAsync(Properties.Settings.Default.ProcurementDocsAPI + "/" + id + "/0", obj);
                response.EnsureSuccessStatusCode();
                return RedirectToAction("DetailAddendum", "Addendum", new { id = id });
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }

        public ActionResult EditAttachment(string id, string AddendumId, string ErrorMessage)
        {
            // TODO: Add delete logic here
            Session["activeTabPengadaan"] = "s2";
            AddendumDoc objInfo = new AddendumDoc();
            if (ErrorMessage != null)
            {
                ViewBag.ErrorMessage = ErrorMessage;
            }
            string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(connectionstring))
            {
                using (var cmd = new SqlCommand("sp_ViewAddendumDocbyAddendumDocID", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@AddendumDocID", id);
                    conn.Open();

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            AddendumDoc obj = new AddendumDoc();

                            obj.UplodedDate = reader[3].ToString();
                            obj.UplodedBy = reader[4].ToString();
                            obj.DocName = reader[1].ToString();
                            obj.DescriptionFile = reader[2].ToString();
                            obj.Description = reader[2].ToString();
                            obj.AddendumDocID = reader[0].ToString();
                            objInfo = obj;
                        }
                    }
                }

            }

            ViewBag.AddendumID = AddendumId;
            ViewBag.id = id;
            //returning the employee list to view  
            return View(objInfo);
        }
        // POST: CRUD/Create
        [HttpPost]
        public ActionResult EditAttachment(string id, string AddendumId, AddendumDoc obj, FormCollection form, HttpPostedFileBase file)
        {

            try
            {
                Session["activeTabPengadaan"] = "s2";
                //string procurementId = form["procurementId"].ToString();
                string ErrorMessage = "";
                string fileType = "";
                string fileName = "";
                string path = "";
                if (file != null && file.ContentLength > 0)
                {
                    fileType = Path.GetExtension(file.FileName);
                    if (fileType.ToUpper().Contains("DOC") || fileType.ToUpper().Contains("XLS") || fileType.ToUpper().Contains("PPT") || fileType.ToUpper().Contains("PDF"))
                    {
                        if (file.ContentLength <= (2000 * 1024))
                        {

                            fileName = Path.GetFileName(file.FileName);
                            path = Path.Combine(Server.MapPath("~/content/document/addendum/"), fileName);
                            file.SaveAs(path);
                        }
                        else
                        {
                            ErrorMessage = "File Upload tidak boleh lebih dari 2MB";
                            return RedirectToAction("EditAttachment", "Addendum", new { id = id, AddendumId = AddendumId, ErrorMessage = ErrorMessage });
                        }
                    }
                    else
                    {
                        ErrorMessage = "File Upload harus dengan format All Office atau PDF";
                        return RedirectToAction("EditAttachment", "Addendum", new { id = id, AddendumId = AddendumId, ErrorMessage = ErrorMessage });
                    }
                }
                if (file == null && (form["filename"].ToString() != null || form["filename"].ToString() != ""))
                {
                    fileName = form["filename"].ToString();
                    path = Path.Combine(Server.MapPath("~/content/document/addendum/"), fileName);
                }
                obj.AddendumDocID = id;
                obj.DocName = fileName;
                obj.UserName = User.Identity.Name;
                obj.UplodedDate = DateTime.Now.ToString("dd/MM/yyyy");
                obj.UplodedBy = User.Identity.Name;
                obj.ModulName = "ADDENDUM";
                obj.TableId = AddendumId;
                obj.Description = form["description"].ToString();
                obj.FolderName = "addendum";
                obj.User_Name = User.Identity.Name;
                // TODO: Add update logic here

                string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    using (var cmd = new SqlCommand("sp_UpdateAddendumDoc", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@AddendumDocID", id);
                        cmd.Parameters.AddWithValue("@DocName", obj.DocName);
                        cmd.Parameters.AddWithValue("@Description", obj.Description);
                        cmd.Parameters.AddWithValue("@UserName", obj.User_Name);
                        conn.Open();
                        int k = cmd.ExecuteNonQuery();
                        if (k != 0)
                        {
                            //sukses
                        }
                        conn.Close();


                    }

                }

                return RedirectToAction("DetailAddendum", "Addendum", new { id = AddendumId });
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }

        public ActionResult DeleteAttachment(string id, string AddendumId)
        {
            // TODO: Add delete logic here
            Session["activeTabPengadaan"] = "s2";
            AddendumDoc objInfo = new AddendumDoc();

            string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(connectionstring))
            {
                using (var cmd = new SqlCommand("sp_ViewAddendumDocbyAddendumDocID", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@AddendumDocID", id);
                    conn.Open();

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            AddendumDoc obj = new AddendumDoc();

                            obj.UplodedDate = reader[3].ToString();
                            obj.UplodedBy = reader[4].ToString();
                            obj.DocName = reader[1].ToString();
                            obj.DescriptionFile = reader[2].ToString();
                            obj.Description = reader[2].ToString();
                            obj.AddendumDocID = reader[0].ToString();
                            objInfo = obj;
                        }
                    }
                }

            }

            ViewBag.AddendumID = AddendumId;
            ViewBag.id = id;
            //returning the employee list to view  
            return View(objInfo);
        }
        // POST: CRUD/Delete/5
        [HttpPost]
        public ActionResult DeleteAttachment(string id, AddendumDoc obj, FormCollection collection)
        {
            try
            {
                Session["activeTabPengadaan"] = "s2";
                string AddendumId = collection["AddendumId"].ToString();
                string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    using (var cmd = new SqlCommand("sp_DeleteAddendumDoc", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@AddendumDocID", id);
                        conn.Open();
                        int k = cmd.ExecuteNonQuery();
                        if (k != 0)
                        {
                            //sukses
                        }
                        conn.Close();
                    }
                }

                return RedirectToAction("DetailAddendum", "Addendum", new { id = AddendumId });
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }

        public PartialViewResult PartialHistory(string id)
        {
            List<WorkFlowHistory> objInfo = new List<WorkFlowHistory>();
            objInfo = GetWokflow(id);
            //returning the employee list to view  
            return PartialView(objInfo);


        }

        public List<WorkFlowHistory> GetWokflow(string id)
        {
            List<WorkFlowHistory> _list = new List<WorkFlowHistory>();
            string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(connectionstring))
            {
                using (var cmd = new SqlCommand("sp_ViewWorkflowHistory", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@TableID", id);
                    cmd.Parameters.AddWithValue("@WorkflowType", "ADDENDUM");
                    conn.Open();

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            WorkFlowHistory obj = new WorkFlowHistory();

                            obj.WorkFlowType = reader[0].ToString();
                            obj.TableID = reader[1].ToString();
                            obj.Name = reader[2].ToString();
                            obj.UpdatedDate = reader[3].ToString();
                            obj.UpdatedBy = reader[4].ToString();
                            obj.StepName = reader[5].ToString();
                            obj.WorkflowStatus = reader[6].ToString();
                            obj.RejectReason = reader[7].ToString();
                            obj.WorkflowID = reader[8].ToString();
                            _list.Add(obj);
                        }
                    }
                }

            }

            return _list;
        }
        public async Task<List<Employee>> GetListEmployees()
        {
            List<Employee> _list = new List<Employee>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.EmployeeAPI);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    _list = JsonConvert.DeserializeObject<List<Employee>>(APIResponse);

                }
                //returning the employee list to view  

            }
            return _list;
        }
        public async Task<List<Numbering>> GetListDocs()
        {
            List<Numbering> _list = new List<Numbering>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.PenomoranAPI);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    _list = JsonConvert.DeserializeObject<List<Numbering>>(APIResponse);

                }
                //returning the employee list to view  

            }
            return _list;
        }
        public async Task<List<Partner>> GetListPartner()
        {
            List<Partner> _list = new List<Partner>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.RekananAPI);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    _list = JsonConvert.DeserializeObject<List<Partner>>(APIResponse);

                }
                //returning the employee list to view  

            }
            return _list;
        }

        //view only
        public ActionResult View(int id)
        {
            Addendum objInfo = new Addendum();
            List<Agreement> _listAgreement = GetListAgreement();
            objInfo = GetListAddendumById(id);
            ViewBag.ListAgreement = _listAgreement;
            return View(objInfo);
        }

        public PartialViewResult PartialAddendumView(int id, List<Agreement> listAgreement)
        {

            ViewBag.ListAgreement = listAgreement;
            Addendum objInfo = new Addendum();
            objInfo = GetListAddendumById(id);
            return PartialView(objInfo);
        }

        public PartialViewResult PartialAttachmentView(int id)
        {
            List<AddendumDoc> objInfo = new List<AddendumDoc>();

            objInfo = GetAddendumDocById(id);
            ViewBag.AddendumID = id;
            return PartialView(objInfo);
        }
    }

}