﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using MandiriCRE.Models;
using System.IO;
using System.Data.SqlClient;
using System.Data;

namespace MandiriCRE.Controllers
{
    public class PerjanjianController : Controller
    {
        string Baseurl = Properties.Settings.Default.BaseURI;
        // GET: Perjanjian
        public async Task<ActionResult> Index()
        {
            List<Agreement> objInfo = new List<Agreement>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.AgreementAPI);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<List<Agreement>>(APIResponse);

                }
                //returning the employee list to view  
                return View(objInfo);
            }
        }
        public async Task<List<AgreementDoc>> ListAttachments()
        {
            List<AgreementDoc> objInfo = new List<AgreementDoc>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.AgreementDocAPI);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<List<AgreementDoc>>(APIResponse);

                }
                //returning the employee list to view  
                return objInfo;
            }
        }
        public async Task<ActionResult> DetailPerjanjian()
        {
            List<Partner> _listPartner = await GetListPartner();
            ViewBag.ListPartner = _listPartner;
            List<Employee> _listEmployee = await GetListEmployees();
            ViewBag.ListEmployee = _listEmployee;
            //Agreement objInfo = new Agreement();
            List<Numbering> _listTemplates = await GetListDocs();
            ViewBag.ListTemplates = _listTemplates;
            List<Procurements> _listProcurements = await GetListProcurement();
            ViewBag.ListProcurement = _listProcurements;
            List<Department> _listDepartment = await GetListDepartment();
            ViewBag.ListDepartment = _listDepartment;
            List<JobType> _listJobType = await GetListJobType();
            ViewBag.ListJobType = _listJobType;

            return View();
        }
        public PartialViewResult PartialPerjanjian(List<JobType> _listJobType, List<Department> _listDepartment, List<Partner> _listPartner, List<Employee> _listEmployee, List<Numbering> _listTemplates,List<Procurements> _listProcurements,Agreement objInfo)
        {
            ViewBag.ListPartner = _listPartner;
            ViewBag.ListEmployee = _listEmployee;
            ViewBag.ListTemplates = _listTemplates;
            ViewBag.ListProcurement = _listProcurements;
            ViewBag.ListDepartment = _listDepartment;
            ViewBag.ListJobType = _listJobType;
            if (objInfo == null)
            {
                objInfo = new Agreement();
            }
            return PartialView("PartialPerjanjian", objInfo);
        } 
        public async Task<ActionResult> PartialAttachment()
        {
            List<AgreementDoc> objInfo = new List<AgreementDoc>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.AgreementDocAPI);
                //HttpResponseMessage Res = Task.Run(async () => { await client.GetAsync(Properties.Settings.Default.RekananAPI); }).Wait();

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<List<AgreementDoc>>(APIResponse);

                }
                //returning the employee list to view  
                return View(objInfo);
            }
        }
        [HttpPost]
        public async Task<ActionResult> DetailPerjanjian(Agreement obj, FormCollection collection)
        {

            try
            {
                if (collection["buttonSaveReview"] != null)
                {
                    obj.ActionSave = "REVIEW";
                }
                if (collection["buttonSaveDone"] != null)
                {
                    obj.ActionSave = "DONE";
                }
                //obj.ProcurementID = collection["ProcurementID"].ToString();
                obj.ProcurementID = collection["Procurement_ProcurementID"].ToString();
                //obj.DocumentNo = collection["DocumentNo"].ToString() + collection["documentNumber"].ToString();
                obj.DocumentNo =  collection["documentNumber"].ToString();
                obj.AgreementDate = collection["agreementdate"].ToString();
                obj.InputDate = collection["inputdate"].ToString();
                obj.JobTypeID = "1";
                obj.AgreementName = collection["agreementname"].ToString();
                obj.ObjectName= collection["objectname"].ToString();
                obj.DepartmentID = collection["Department_DepartmentID"].ToString();
                obj.EmployeePIC = collection["EmployeePIC"].ToString();
                obj.VendorID = "1";
                obj.VendorID = collection["Vendor_VendorID"].ToString();
                obj.StartDate= collection["startdate"].ToString();
                obj.EndDate = collection["finishdate"].ToString();
                obj.SLA = collection["sla"].ToString();
                obj.BASE_SLA = collection["basesla"].ToString();
                obj.AdditionalInformation = collection["description"].ToString();
                obj.UserName = User.Identity.Name;
                //obj.ActionSave = "REVIEW"; //value REVIEW or DONE
                // TODO: Add update logic here
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                // TODO: Add update logic here
                HttpResponseMessage response = await client.PostAsJsonAsync(Properties.Settings.Default.AgreementAPI, obj);
                response.EnsureSuccessStatusCode();
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }

        }
        public PartialViewResult PartialHistory()
        {
            return PartialView("PartialHistory", new List<WorkFlowHistory>());          
        }
        public async Task<List<Employee>> GetListEmployees()
        {
            List<Employee> _list = new List<Employee>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.EmployeeAPI);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    _list = JsonConvert.DeserializeObject<List<Employee>>(APIResponse);

                }
                //returning the employee list to view  

            }
            return _list;
        }
        public async Task<List<Numbering>> GetListDocs()
        {
            List<Numbering> _list = new List<Numbering>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.PenomoranAPI);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    _list = JsonConvert.DeserializeObject<List<Numbering>>(APIResponse);

                }
                //returning the employee list to view  

            }
            return _list;
        }
        public async Task<List<AgreementDoc>> GetListAgreementDocs(string id)
        {
            List<AgreementDoc> _list = new List<AgreementDoc>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.AgreementDocAPI + "/" + id);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    _list = JsonConvert.DeserializeObject<List<AgreementDoc>>(APIResponse);

                }
                //returning the employee list to view  

            }
            return _list;
        }
        public async Task<List<AgreementHistory>> GetListAgreementHistory(string id)
        {
            List<AgreementHistory> _list = new List<AgreementHistory>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.AgreementHistoryAPI + "/" + id);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    _list = JsonConvert.DeserializeObject<List<AgreementHistory>>(APIResponse);

                }
                //returning the employee list to view  

            }
            return _list;
        }
        public async Task<List<Partner>> GetListPartner()
        {
            List<Partner> _list = new List<Partner>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.RekananAPI);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    _list = JsonConvert.DeserializeObject<List<Partner>>(APIResponse);

                }
                //returning the employee list to view  

            }
            return _list;
        }
        public async Task<List<Department>> GetListDepartment()
        {
            List<Department> _list = new List<Department>();
            string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(connectionstring))
            {
                using (var cmd = new SqlCommand("sp_ViewDepartment", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    conn.Open();

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Department obj = new Department();

                            obj.DepartmentID = Convert.ToInt32(reader[0]);
                            obj.DepartmentName = reader[1].ToString();
                            _list.Add(obj);

                        }
                    }
                }

            }

            return _list;
        }
        public async Task<List<Procurements>> GetListProcurement()
        {
            List<Procurements> _list = new List<Procurements>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.ProcurementAPI);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    _list = JsonConvert.DeserializeObject<List<Procurements>>(APIResponse);

                }

                //returning the employee list to view  
                return _list.Where(x=> x.status.ToString() == "APPROVED").ToList();
            }
        }
        public async Task<List<JobType>> GetListJobType()
        {
            List<JobType> _list = new List<JobType>();
            string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(connectionstring))
            {
                using (var cmd = new SqlCommand("sp_ViewJobType", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    conn.Open();

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            JobType obj = new JobType();

                            obj.JobTypeID = reader[0].ToString();
                            obj.JobTypeName = reader[1].ToString();
                            _list.Add(obj);

                        }
                    }
                }

            }

            return _list;
        }
        public ActionResult CreateAttachment(string id, int Total, string step, string ErrorMessage)
        {
            Session["activeTabPengadaan"] = "s2";
            AgreementDoc objInfo = new AgreementDoc();
            ViewBag.step = step;
            if (ErrorMessage != null)
            {
                ViewBag.ErrorMessage = ErrorMessage;
            }
            ViewBag.Total = Total;
            return View(objInfo);
        }
        // POST: CRUD/Create
        [HttpPost]
        public async Task<ActionResult> CreateAttachment(string id, int Total, string step, AgreementDoc obj, FormCollection form, HttpPostedFileBase file)
        {
            try
            {
                Session["activeTabPengadaan"] = "s2";
                string ErrorMessage = "";
                string fileName = "";
                string path = "";
                string fileType = "";
                if (Total < 10)
                {
                    if (file != null && file.ContentLength > 0)
                    {

                        fileType = Path.GetExtension(file.FileName);
                        //if (step == "DONE" && (!fileName.ToUpper().Contains("FINAL") && !fileName.ToUpper().Contains("SIGNED")))
                        //{
                        //    ViewBag.Error = "File Upload harus mengandung kata-kata Final atau Signed";
                        //    return RedirectToAction("Edit", "Perjanjian", new { id = id+ ViewBag.Error });

                        //}
                        if (fileType.ToUpper().Contains("DOC") || fileType.ToUpper().Contains("XLS") || fileType.ToUpper().Contains("PPT") || fileType.ToUpper().Contains("PDF"))
                        {
                            if (file.ContentLength <= (2000 * 1024))
                            {
                                fileName = Path.GetFileName(file.FileName);
                                path = Path.Combine(Server.MapPath("~/content/document/perjanjian/"), fileName);
                                file.SaveAs(path);
                            }
                            else
                            {
                                ErrorMessage = "File Upload tidak boleh lebih dari 2MB";
                                return RedirectToAction("CreateAttachment", "Perjanjian", new { id = id, Total = Total, step = step, ErrorMessage = ErrorMessage });
                            }
                        }
                        else
                        {
                            ErrorMessage = "File Upload harus dengan format All Office atau PDF";
                            return RedirectToAction("CreateAttachment", "Perjanjian", new { id = id, Total = Total, step = step, ErrorMessage = ErrorMessage });
                        }
                    }
                }
                else
                {
                    ErrorMessage = "Penambahan gagal, jumlah dokumen maksimal adalah 10";
                    return RedirectToAction("CreateAttachment", "Perjanjian", new { id = id, Total = Total, step = step, ErrorMessage = ErrorMessage });
                }

                obj.DocumentName = fileName;
                obj.createdDate = DateTime.Now.ToString("dd/MM/yyyy");
                obj.CreatedBy = User.Identity.Name;
                obj.UplodedDate = DateTime.Now.ToString("dd/MM/yyyy");
                obj.UplodedBy = User.Identity.Name;
                obj.ModulName = "PERJANJIAN";
                obj.TableId = id;
                obj.AgreementID = id;
                obj.Description = form["description"].ToString();
                obj.FolderName = "perjanjian";
                obj.User_Name = User.Identity.Name;
                obj.MemoLO= form["memolo"].ToString();
                // TODO: Add update logic here
                //HttpClient client = new HttpClient();
                //client.BaseAddress = new Uri(Baseurl);
                //client.DefaultRequestHeaders.Accept.Clear();
                //client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //// TODO: Add update logic here
                //HttpResponseMessage response = await client.PostAsJsonAsync(Properties.Settings.Default.AgreementDocAPI + "/" + id + "/" + id, obj);
                //response.EnsureSuccessStatusCode();
                string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    using (var cmd = new SqlCommand("sp_SaveAttachmentPerjanjian", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@DocumentName", obj.DocumentName);
                        cmd.Parameters.AddWithValue("@Description", obj.Description);
                        cmd.Parameters.AddWithValue("@ModulName", "PERJANJIAN");
                        cmd.Parameters.AddWithValue("@TableId", obj.AgreementID);
                        cmd.Parameters.AddWithValue("@USER_NAME", obj.User_Name);
                        cmd.Parameters.AddWithValue("@MemoLO", obj.MemoLO);
                        conn.Open();
                        int k = cmd.ExecuteNonQuery();
                        if (k != 0)
                        {
                            //sukses
                        }
                        conn.Close();
                    }
                }

                return RedirectToAction("Edit", "Perjanjian", new { id = id });
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }
        public async Task<ActionResult> Edit(string id,string ErrorMessage)
        {
            Agreement objInfo = new Agreement();

            if (id.ToUpper().Contains("FINAL ATAU SIGNED"))
            {
                ViewBag.Error = "File Upload harus mengandung kata-kata Final atau Signed";
                id = id.Replace("file upload harus mengandung kata-kata final atau signed","");
            }
            if (ErrorMessage != null)
            {
                ViewBag.ErrorMessage = ErrorMessage;
            }

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllArticles using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.AgreementAPI + "/" + id);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<Agreement>(APIResponse);

                }
                objInfo.JobTypeID = objInfo.JobType_JobTypeID;
                ViewBag.AgreementID = id;
                ViewBag.step = objInfo.StepName;
                List<Employee> _listEmployee = await GetListEmployees();
                ViewBag.ListEmployee = _listEmployee;
                List<AgreementDoc> _listAgreementDocs = await GetListAgreementDocs(id);
                ViewBag.ListAgreementsDoc = _listAgreementDocs;
                ViewBag.Total = 0;
                foreach (AgreementDoc vardoc in _listAgreementDocs)
                {
                    ViewBag.Total = vardoc.Total;
                }

                ViewBag.objInfo = objInfo;
                List<AgreementHistory> _listHistories = await GetListAgreementHistory(id);
                ViewBag.ListAgreementsHistory = _listHistories;
                List<Partner> _listPartner = await GetListPartner();
                ViewBag.ListPartner = _listPartner;
                //Agreement objInfo = new Agreement();
                List<Numbering> _listTemplates = await GetListDocs();
                ViewBag.ListTemplates = _listTemplates;
                List<Procurements> _listProcurements = await GetListProcurement();
                ViewBag.ListProcurement = _listProcurements;
                List<Department> _listDepartment = await GetListDepartment();
                ViewBag.ListDepartment = _listDepartment;
                List<JobType> _listJobType = await GetListJobType();
                ViewBag.ListJobType = _listJobType;
                //returning the employee list to view  
                return View(objInfo);
            }
        }
        // POST: Procurement/Edit/5
        [HttpPost]
        public async Task<ActionResult> Edit(string id, Agreement obj, FormCollection collection)
        {
            try
            {
                string ErrorMessage = "";
                if (collection["buttonSaveReview"] != null)
                {
                    obj.ActionSave = "REVIEW";
                }
                if (collection["buttonSaveDone"] != null)
                {
                    obj.ActionSave = "DONE";
                }
                if (collection["buttonSaveSign"] != null)
                {
                    obj.ActionSave = "SIGN";
                    List<AgreementDoc> _listAgreementDocs = await GetListAgreementDocs(id);
                    string final = "";
                    foreach (AgreementDoc objdoc in _listAgreementDocs)
                    {
                        
                        if(objdoc.DocName.ToUpper().Contains("FINAL") || objdoc.DocName.ToUpper().Contains("SIGNED"))
                        {
                            final = "Y";
                        }
                    }
                    if (final == "")
                    {
                        ErrorMessage = "Proses tidak dapat dilanjutkan.Tidak ada file yang mengandung kata-kata Final atau Signed";
                        return RedirectToAction("Edit", new { id = id, ErrorMessage = ErrorMessage });
                    }
                      
                    }
                obj.ProcurementID = collection["Procurement_ProcurementID"].ToString();
                obj.DocumentNo = collection["documentNumber"].ToString();
                obj.AgreementDate = collection["agreementdate"].ToString();
                obj.InputDate = collection["inputdate"].ToString();
                obj.JobTypeID = collection["JobTypeID"].ToString();
                if (collection["PekerjaanLain"] != null && collection["PekerjaanLain"] != "")
                {
                    obj.JobTypeID = collection["PekerjaanLain"].ToString();
                }
                obj.AgreementName = collection["agreementname"].ToString();
                obj.ObjectName = collection["objectname"].ToString();
                obj.DepartmentID = collection["Department_DepartmentID"].ToString(); 
                obj.EmployeePIC = collection["EmployeePIC"].ToString();
                if (collection["picName"] != null && collection["picName"] != "")
                {
                    obj.EmployeePIC = collection["picName"].ToString();
                }
                obj.VendorID = collection["Vendor_VendorID"].ToString();
                obj.StartDate = collection["startdate"].ToString();
                obj.EndDate = collection["finishdate"].ToString();
                obj.SLA = collection["sla"].ToString();
                obj.BASE_SLA = collection["basesla"].ToString();
                if (collection["description"] != null)
                {
                    obj.AdditionalInformation = collection["description"].ToString();
                }
                obj.Workflow_WorkflowID = collection["wfid"].ToString();
                obj.UserName = User.Identity.Name;
                //obj.ActionSave = "REVIEW"; //value REVIEW or DONE

                //check duplicate pengadaan dan nomor perjanjian
                
                Agreement obj2 = new Agreement();
                string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    using (var cmd = new SqlCommand("sp_CheckDuplicatePengadaan", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ProcurementID", obj.ProcurementID);
                        cmd.Parameters.AddWithValue("@AgreementID", id);
                        cmd.Parameters.AddWithValue("@DocumentNo", obj.DocumentNo);
                        conn.Open();

                        using (var reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                obj2.AgreementID = reader[0].ToString();
                                ErrorMessage = reader[1].ToString();
                            }
                        }
                    }

                }

                if (ErrorMessage != null && ErrorMessage != "")
                {
                    return RedirectToAction("Edit", new {id=id, ErrorMessage = ErrorMessage });
                }

                // TODO: Add update logic here
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                // TODO: Add update logic here
                HttpResponseMessage response = await client.PutAsJsonAsync(Properties.Settings.Default.AgreementAPI + "/" + id, obj);
                response.EnsureSuccessStatusCode();
                if (obj.ActionSave=="DONE")
                {
                    //Done process
                    using (SqlConnection conn = new SqlConnection(connectionstring))
                    {
                        using (var cmd = new SqlCommand("sp_GetNextApproval", conn))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@WFName", "WF_PERJANJIAN");
                            cmd.Parameters.AddWithValue("@Action", "DONE");
                            cmd.Parameters.AddWithValue("@WorkflowId", obj.Workflow_WorkflowID);
                            cmd.Parameters.AddWithValue("@MainTableId", id);
                            cmd.Parameters.AddWithValue("@RejectReason", "");
                            cmd.Parameters.AddWithValue("@USER_NAME", User.Identity.Name);
                            conn.Open();
                            int k = cmd.ExecuteNonQuery();
                            if (k != 0)
                            {
                                //sukses
                            }
                            conn.Close();
                        }
                    }
                }
                if (obj.ActionSave == "SIGN")
                {
                    //Done process
                    using (SqlConnection conn = new SqlConnection(connectionstring))
                    {
                        using (var cmd = new SqlCommand("sp_GetNextApproval", conn))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@WFName", "WF_PERJANJIAN");
                            cmd.Parameters.AddWithValue("@Action", "SIGN");
                            cmd.Parameters.AddWithValue("@WorkflowId", obj.Workflow_WorkflowID);
                            cmd.Parameters.AddWithValue("@MainTableId", id);
                            cmd.Parameters.AddWithValue("@RejectReason", "");
                            cmd.Parameters.AddWithValue("@USER_NAME", User.Identity.Name);
                            conn.Open();
                            int k = cmd.ExecuteNonQuery();
                            if (k != 0)
                            {
                                //sukses
                            }
                            conn.Close();
                        }
                    }
                }

                Session["activeTabPengadaan"] = null;
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }
        public async Task<ActionResult> Sign(string id, string WF)
        {
            Agreement objInfo = new Agreement();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllArticles using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.AgreementAPI + "/" + id);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<Agreement>(APIResponse);

                }
                ViewBag.AgreementID = id;
                List<Employee> _listEmployee = await GetListEmployees();
                ViewBag.ListEmployee = _listEmployee;
                List<AgreementDoc> _listAgreementDocs = await GetListAgreementDocs(id);
                ViewBag.ListAgreementsDoc = _listAgreementDocs;
                ViewBag.objInfo = objInfo;
                List<AgreementHistory> _listHistories = await GetListAgreementHistory(id);
                ViewBag.ListAgreementsHistory = _listHistories;

                List<Partner> _listPartner = await GetListPartner();
                ViewBag.ListPartner = _listPartner;
                //Agreement objInfo = new Agreement();
                List<Numbering> _listTemplates = await GetListDocs();
                ViewBag.ListTemplates = _listTemplates;
                List<Procurements> _listProcurements = await GetListProcurement();
                ViewBag.ListProcurement = _listProcurements;
                List<Department> _listDepartment = await GetListDepartment();
                ViewBag.ListDepartment = _listDepartment;
                //returning the employee list to view  

                ViewBag.WF = WF;
                return View(objInfo);
            }
        }
        // POST: Procurement/Edit/5
        [HttpPost]
        public async Task<ActionResult> Sign(string id, string WF, Agreement obj, FormCollection collection)
        {
            try
            {
                //if (collection["buttonSaveReview"] != null)
                //{
                //    obj.ActionSave = "REVIEW";
                //}
                //if (collection["buttonSaveDone"] != null)
                //{
                //    obj.ActionSave = "DONE";
                //}
                obj.ProcurementID = collection["Procurement_ProcurementID"].ToString();
                obj.DocumentNo = collection["documentNumber"].ToString();
                obj.AgreementDate = collection["agreementdate"].ToString();
                obj.InputDate = collection["inputdate"].ToString();
                obj.JobTypeID = "1";
                obj.AgreementName = collection["agreementname"].ToString();
                obj.ObjectName = collection["objectname"].ToString();
                obj.DepartmentID = collection["Department_DepartmentID"].ToString();
                obj.EmployeePIC = collection["EmployeePIC"].ToString();
                obj.VendorID = collection["Vendor_VendorID"].ToString();
                obj.StartDate = collection["startdate"].ToString();
                obj.EndDate = collection["finishdate"].ToString();
                obj.SLA = collection["sla"].ToString();
                obj.BASE_SLA = collection["basesla"].ToString();
                obj.AdditionalInformation = collection["description"].ToString();
                obj.UserName = User.Identity.Name;
                //obj.ActionSave = "REVIEW"; //value REVIEW or DONE
                // TODO: Add update logic here
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                // TODO: Add update logic here
                HttpResponseMessage response = await client.PutAsJsonAsync(Properties.Settings.Default.AgreementAPI + "/" + id, obj);
                response.EnsureSuccessStatusCode();

                //sign process
                string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    using (var cmd = new SqlCommand("sp_GetNextApproval", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@WFName", "WF_PERJANJIAN");
                        cmd.Parameters.AddWithValue("@Action", "SIGN");
                        cmd.Parameters.AddWithValue("@WorkflowId", WF);
                        cmd.Parameters.AddWithValue("@MainTableId", id);
                        cmd.Parameters.AddWithValue("@RejectReason", "");
                        cmd.Parameters.AddWithValue("@USER_NAME", User.Identity.Name);
                        conn.Open();
                        int k = cmd.ExecuteNonQuery();
                        if (k != 0)
                        {
                            //sukses
                        }
                        conn.Close();
                    }
                }
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }
        public async Task<ActionResult> Duplicate(string id)
        {
            try
            {
                Agreement obj = new Agreement();
                obj.UserName = User.Identity.Name;
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                // TODO: Add update logic here
                HttpResponseMessage response = await client.PostAsJsonAsync(Properties.Settings.Default.AgreementDuplicateAPI + "/" + id, obj);
                response.EnsureSuccessStatusCode();
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }

        public async Task<ActionResult> DeleteAttachment(string id, string AgreementId)
        {
            try
            {
                Session["activeTabPengadaan"] = "s2";
                // TODO: Add delete logic here
                AgreementDoc objInfo = new AgreementDoc();

                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(Baseurl);

                    client.DefaultRequestHeaders.Clear();
                    //Define request data format  
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                    HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.AgreementDocAPI + "/" + AgreementId + "/" + id);

                    //Checking the response is successful or not which is sent using HttpClient  
                    if (Res.IsSuccessStatusCode)
                    {
                        //Storing the response details recieved from web api   
                        var APIResponse = Res.Content.ReadAsStringAsync().Result;

                        //Deserializing the response recieved from web api and storing into the Employee list  
                        objInfo = JsonConvert.DeserializeObject<AgreementDoc>(APIResponse);

                    }
                    ViewBag.AgreementID = AgreementId;
                    ViewBag.id = id;
                    //returning the employee list to view  
                    return View(objInfo);
                }
            }
            catch
            {
                return View();
            }
        }
        // POST: CRUD/Delete/5
        [HttpPost]
        public async Task<ActionResult> DeleteAttachment(string id, string AgreementId, AgreementDoc obj, FormCollection collection)
        {
            // TODO: Add delete logic here
            try
            {
                Session["activeTabPengadaan"] = "s2";
                //obj.ProcurementDocID = collection["ProcurementDocID"].ToString();
                AgreementId = collection["AgreementId"].ToString();
                // TODO: Add update logic here
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                // TODO: Add update logic here
                HttpResponseMessage response = await client.DeleteAsync(Properties.Settings.Default.AgreementDocAPI + "/" + AgreementId + "/" + id);
                response.EnsureSuccessStatusCode();
                return RedirectToAction("Edit", "Perjanjian", new { id = AgreementId });
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }

        public async Task<ActionResult> CreatePerjanjian(string ErrorMessage,Agreement objAgreement)
        {
            Agreement objInfo = new Agreement();
            if (objAgreement != null)
            {
                objInfo = objAgreement;
            }
            if (ErrorMessage != null)
            {
                ViewBag.ErrorMessage = ErrorMessage;
            }
            List<Partner> _listPartner = await GetListPartner();
            ViewBag.ListPartner = _listPartner;
            List<Employee> _listEmployee = await GetListEmployees();
            ViewBag.ListEmployee = _listEmployee;
            List<Numbering> _listTemplates = await GetListDocs();
            ViewBag.ListTemplates = _listTemplates;
            List<Procurements> _listProcurements = await GetListProcurement();
            ViewBag.ListProcurement = _listProcurements;
            List<Department> _listDepartment = await GetListDepartment();
            ViewBag.ListDepartment = _listDepartment;
            List<JobType> _listJobType = await GetListJobType();
            ViewBag.ListJobType = _listJobType;

            return View(objInfo);
        }
        // POST: Procurement/Create
        [HttpPost]
        public async Task<ActionResult> CreatePerjanjian(Agreement obj, FormCollection collection)
        {
            try
            {
                if (collection["buttonSaveReview"] != null)
                {
                    obj.ActionSave = "REVIEW";
                }
                if (collection["buttonSaveDone"] != null)
                {
                    obj.ActionSave = "DONE";
                }
                //obj.ProcurementID = collection["ProcurementID"].ToString();
                obj.ProcurementID = collection["Procurement_ProcurementID"].ToString();
                obj.DocumentNo = collection["DocumentNo"].ToString() + collection["documentNumber"].ToString();
                //obj.DocumentNo = collection["documentNumber"].ToString();
                obj.AgreementDate = collection["agreementdate"].ToString();
                obj.JobTypeID = collection["JobTypeID"].ToString();
                if (collection["PekerjaanLain"]!= null && collection["PekerjaanLain"] != "")
                {
                    obj.JobTypeID = collection["PekerjaanLain"].ToString();
                }
                obj.InputDate = collection["inputdate"].ToString();
                obj.AgreementName = collection["agreementname"].ToString();
                obj.ObjectName = collection["objectname"].ToString();
                obj.DepartmentID = collection["Department_DepartmentID"].ToString();
                obj.EmployeePIC = collection["EmployeePIC"].ToString();
                if (collection["picName"] != null && collection["picName"] != "")
                {
                    obj.EmployeePIC = collection["picName"].ToString();
                }
                obj.VendorID = "1";
                obj.VendorID = collection["Vendor_VendorID"].ToString();
                obj.StartDate = collection["startdate"].ToString();
                obj.EndDate = collection["finishdate"].ToString();
                obj.SLA = collection["sla"].ToString();
                obj.BASE_SLA = collection["basesla"].ToString();
                obj.AdditionalInformation = "";
                if (collection["description"] != null)
                {
                    obj.AdditionalInformation = collection["description"].ToString();
                }
                obj.UserName = User.Identity.Name;

                //check duplicate pengadaan dan nomor perjanjian
                string ErrorMessage = "";
                Agreement obj2 = new Agreement();
                string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    using (var cmd = new SqlCommand("sp_CheckDuplicatePengadaan", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ProcurementID", obj.ProcurementID);
                        cmd.Parameters.AddWithValue("@AgreementID", "");
                        cmd.Parameters.AddWithValue("@DocumentNo",obj.DocumentNo);
                        conn.Open();

                        using (var reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                obj2.AgreementID = reader[0].ToString();
                                ErrorMessage = reader[1].ToString();
                            }
                        }
                    }

                }
                Agreement objAgreement = new Agreement();
                if (ErrorMessage != null && ErrorMessage != "")
                {
                    obj.DocumentNo = collection["documentNumber"].ToString();
                    obj.JobTypeID = null;
                    obj.EmployeePIC = null;
                    //return RedirectToAction("CreatePerjanjian", new { ErrorMessage = ErrorMessage, objAgreement =obj });
                    return await CreatePerjanjian(ErrorMessage, obj);
                }
                //obj.ActionSave = "REVIEW"; //value REVIEW or DONE
                // TODO: Add update logic here
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                // TODO: Add update logic here
                HttpResponseMessage response = await client.PostAsJsonAsync(Properties.Settings.Default.AgreementAPI, obj);
                response.EnsureSuccessStatusCode();
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }

        //view only
        public async Task<ActionResult> View(string id)
        {
            Agreement objInfo = new Agreement();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllArticles using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.AgreementAPI + "/" + id);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<Agreement>(APIResponse);

                }
                ViewBag.AgreementID = id;
                List<Employee> _listEmployee = await GetListEmployees();
                ViewBag.ListEmployee = _listEmployee;
                List<AgreementDoc> _listAgreementDocs = await GetListAgreementDocs(id);
                ViewBag.ListAgreementsDoc = _listAgreementDocs;
                ViewBag.objInfo = objInfo;
                List<AgreementHistory> _listHistories = await GetListAgreementHistory(id);
                ViewBag.ListAgreementsHistory = _listHistories;

                List<Partner> _listPartner = await GetListPartner();
                ViewBag.ListPartner = _listPartner;
                //Agreement objInfo = new Agreement();
                List<Numbering> _listTemplates = await GetListDocs();
                ViewBag.ListTemplates = _listTemplates;
                List<Procurements> _listProcurements = await GetListProcurement();
                ViewBag.ListProcurement = _listProcurements;
                List<Department> _listDepartment = await GetListDepartment();
                ViewBag.ListDepartment = _listDepartment;
                //returning the employee list to view  
                return View(objInfo);
            }
        }
        public PartialViewResult PartialPerjanjianView(List<Department> _listDepartment, List<Partner> _listPartner, List<Employee> _listEmployee, List<Numbering> _listTemplates, List<Procurements> _listProcurements, Agreement objInfo)
        {
            ViewBag.ListPartner = _listPartner;
            ViewBag.ListEmployee = _listEmployee;
            ViewBag.ListTemplates = _listTemplates;
            ViewBag.ListProcurement = _listProcurements;
            ViewBag.ListDepartment = _listDepartment;
            if (objInfo == null)
            {
                objInfo = new Agreement();
            }
            return PartialView("PartialPerjanjianView", objInfo);
        }
        public async Task<ActionResult> PartialAttachmentView()
        {
            List<AgreementDoc> objInfo = new List<AgreementDoc>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.AgreementDocAPI);
                //HttpResponseMessage Res = Task.Run(async () => { await client.GetAsync(Properties.Settings.Default.RekananAPI); }).Wait();

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<List<AgreementDoc>>(APIResponse);

                }
                //returning the employee list to view  
                return View(objInfo);
            }
        }

        public async Task<ActionResult> EditAttachment(string id, string step,string AgreementID,string ErrorMessage)
        {
            try
            {
                Session["activeTabPengadaan"] = "s2";
                // TODO: Add delete logic here
                AgreementDoc objInfo = new AgreementDoc();
                if (ErrorMessage != null)
                {
                    ViewBag.ErrorMessage = ErrorMessage;
                }
                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(Baseurl);

                    client.DefaultRequestHeaders.Clear();
                    //Define request data format  
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                    HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.AgreementDocAPI + "/" + AgreementID + "/" + id);

                    //Checking the response is successful or not which is sent using HttpClient  
                    if (Res.IsSuccessStatusCode)
                    {
                        //Storing the response details recieved from web api   
                        var APIResponse = Res.Content.ReadAsStringAsync().Result;

                        //Deserializing the response recieved from web api and storing into the Employee list  
                        objInfo = JsonConvert.DeserializeObject<AgreementDoc>(APIResponse);

                    }
                    ViewBag.AgreementID = AgreementID;
                    ViewBag.id = id;
                    ViewBag.step = step;
                    //returning the employee list to view  
                    return View(objInfo);
                }
            }
            catch
            {
                return View();
            }
        }
        // POST: CRUD/Create
        [HttpPost]
        public async Task<ActionResult> EditAttachment(string id,string step, AgreementDoc obj, FormCollection form, HttpPostedFileBase file)
        {

            try
            {
                Session["activeTabPengadaan"] = "s2";
                string ErrorMessage = "";
                string AgreementID = form["AgreementID"].ToString();
                string fileName = "";
                string path = "";
                string fileType = "";
                if (file != null && file.ContentLength > 0)
                {

                    fileType = Path.GetExtension(file.FileName);
                    //if (step == "DONE" && (!fileName.ToUpper().Contains("FINAL") && !fileName.ToUpper().Contains("SIGNED")))
                    //{
                    //    ViewBag.Error = "File Upload harus mengandung kata-kata Final atau Signed";
                    //    return RedirectToAction("Edit", "Perjanjian", new { id = id+ ViewBag.Error });

                    //}
                    if (fileType.ToUpper().Contains("DOC") || fileType.ToUpper().Contains("XLS") || fileType.ToUpper().Contains("PPT") || fileType.ToUpper().Contains("PDF"))
                    {
                        if (file.ContentLength <= (2000 * 1024))
                        {
                            fileName = Path.GetFileName(file.FileName);
                            path = Path.Combine(Server.MapPath("~/content/document/perjanjian/"), fileName);
                            file.SaveAs(path);
                        }
                        else
                        {
                            ErrorMessage = "File Upload tidak boleh lebih dari 2MB";
                            return RedirectToAction("EditAttachment", "Perjanjian", new { id = id,step=step, AgreementID= AgreementID, ErrorMessage = ErrorMessage });
                        }
                    }
                    else
                    {
                        ErrorMessage = "File Upload harus dengan format All Office atau PDF";
                        return RedirectToAction("EditAttachment", "Perjanjian", new { id = id, step = step, AgreementID = AgreementID, ErrorMessage = ErrorMessage });
                    }
                }
                if (file == null && (form["filename"].ToString() != null || form["filename"].ToString() != ""))
                {
                    fileName = form["filename"].ToString();
                    path = Path.Combine(Server.MapPath("~/content/document/agreement/"), fileName);
                }
                obj.AgreementDocID = id;
                obj.DocName = fileName;
                obj.UserName = User.Identity.Name;
                obj.UplodedDate = DateTime.Now.ToString("dd/MM/yyyy");
                obj.UplodedBy = User.Identity.Name;
                obj.ModulName = "PERJANJIAN";
                obj.TableId = id;
                obj.Description = form["description"].ToString();
                obj.FolderName = "template";
                obj.User_Name = User.Identity.Name;
                obj.MemoLO = form["memolo"].ToString();
                // TODO: Add update logic here
                //HttpClient client = new HttpClient();
                //client.BaseAddress = new Uri(Baseurl);
                //client.DefaultRequestHeaders.Accept.Clear();
                //client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //// TODO: Add update logic here
                //HttpResponseMessage response = await client.PutAsJsonAsync(Properties.Settings.Default.AgreementDocAPI + "/" + id + "/0", obj);
                //response.EnsureSuccessStatusCode();

                string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    using (var cmd = new SqlCommand("sp_UpdateAgreementDoc", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@AgreementDocID", id);
                        cmd.Parameters.AddWithValue("@DocName", obj.DocName);
                        cmd.Parameters.AddWithValue("@Description", obj.Description);
                        cmd.Parameters.AddWithValue("@UserName", obj.User_Name);
                        cmd.Parameters.AddWithValue("@MemoLO", obj.MemoLO);
                        conn.Open();
                        int k = cmd.ExecuteNonQuery();
                        if (k != 0)
                        {
                            //sukses
                        }
                        conn.Close();


                    }

                }
                return RedirectToAction("Edit", "Perjanjian", new { id = AgreementID });

            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }
    }

}