﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using MandiriCRE.Models;
using System.IO;
using System.Data.SqlClient;
using System.Data;

namespace MandiriCRE.Controllers
{
    public class MasterTemplateController : Controller
    {
        // GET: MasterTemplate
        public ActionResult Index1()
        {
            return View();
        }
        string Baseurl = Properties.Settings.Default.BaseURI;
        public async Task<ActionResult> Index()
        {
            List<Template> objInfo = new List<Template>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.TemplateAPI);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<List<Template>>(APIResponse);

                }
                //returning the employee list to view  
                return View(objInfo);
            }
        }
        // GET: CRUD/Details/5
        public async Task<ActionResult> Details(int id)
        {
            Template objInfo = new Template();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.TemplateAPI + "/" + id);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<Template>(APIResponse);

                }
                //returning the employee list to view  
                return View(objInfo);
            }
        }

        // GET: CRUD/Create
        public ActionResult Create(string ErrorMessage)
        {
            Template objInfo = new Template();
            if (ErrorMessage != null)
            {
                ViewBag.ErrorMessage = ErrorMessage;
            }
            return View(objInfo);
        }

        // POST: CRUD/Create
        [HttpPost]
        public async Task<ActionResult> Create(Template obj, FormCollection form, HttpPostedFileBase file)
        {
            try
            {
                string ErrorMessage = "";
                string fileName = "";
                string path = "";
                string fileType = "";
                if (file != null && file.ContentLength > 0)
                {
                    fileType = Path.GetExtension(file.FileName);
                    if (fileType.ToUpper().Contains("DOC") || fileType.ToUpper().Contains("XLS") || fileType.ToUpper().Contains("PPT") || fileType.ToUpper().Contains("PDF"))
                    {
                        if (file.ContentLength <= (2000 * 1024))
                        {
                            fileName = Path.GetFileName(file.FileName);
                            path = Path.Combine(Server.MapPath("~/content/document/template/"), fileName);
                            file.SaveAs(path);
                        }
                        else
                        {
                            ErrorMessage = "File Upload tidak boleh lebih dari 2MB";
                            return RedirectToAction("Create", new { ErrorMessage = ErrorMessage });
                        }
                    }
                    else
                    {
                        ErrorMessage = "File Upload harus dengan format All Office atau PDF";
                        return RedirectToAction("Create",new { ErrorMessage = ErrorMessage });
                    }
                    
                }
                obj.UpdateBy = User.Identity.Name;
                obj.UpdateDate = DateTime.Now.ToString("dd/MM/yyyy");
                obj.CreateBy = User.Identity.Name;
                obj.CreateDate = DateTime.Now.ToString("dd/MM/yyyy");
                obj.DocumentName = fileName;
                obj.TemplateName = form["documentName"].ToString();
                obj.FolderName = "template";
                obj.Path = path;
                //check Duplicate
                Template obj2 = new Template();
                string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    using (var cmd = new SqlCommand("sp_CheckTemplate", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@TemplateName", obj.TemplateName);
                        cmd.Parameters.AddWithValue("@DocumentName", obj.DocumentName);
                        cmd.Parameters.AddWithValue("@TemplateID","");
                        conn.Open();

                        using (var reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                obj2.TemplateID = reader[0].ToString();
                            }
                        }
                    }

                }

                if (obj2.TemplateID != null)
                {
                    ErrorMessage = "Nama Template atau Dokumen Sudah Ada";
                    return RedirectToAction("Create", new { ErrorMessage = ErrorMessage });
                }

                // TODO: Add update logic here
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                // TODO: Add update logic here
                HttpResponseMessage response = await client.PostAsJsonAsync(Properties.Settings.Default.TemplateAPI, obj);
                response.EnsureSuccessStatusCode();
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }

        // GET: CRUD/Edit/5
        public async Task<ActionResult> Edit(int id,string ErrorMessage)
        {
            Template objInfo = new Template();
            if (ErrorMessage != null)
            {
                ViewBag.ErrorMessage = ErrorMessage;
            }
            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.TemplateAPI + "/" + id);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<Template>(APIResponse);

                }
                //returning the employee list to view  
                return View(objInfo);
            }
        }

        // POST: CRUD/Edit/5
        [HttpPost]
        public async Task<ActionResult> Edit(int id, Template obj, FormCollection form, HttpPostedFileBase file)
        {
            try
            {
                string fileName = "";
                string path = "";
                string fileType = "";
                string ErrorMessage = "";
                if (file != null && file.ContentLength > 0)
                {
                    fileType = Path.GetExtension(file.FileName);
                    if (fileType.ToUpper().Contains("DOC") || fileType.ToUpper().Contains("XLS") || fileType.ToUpper().Contains("PPT") || fileType.ToUpper().Contains("PDF"))
                    {
                        if (file.ContentLength <= (2000 * 1024))
                        {
                            fileName = Path.GetFileName(file.FileName);
                            path = Path.Combine(Server.MapPath("~/content/document/template/"), fileName);
                            file.SaveAs(path);
                        }
                        else
                        {
                            ErrorMessage = "File Upload tidak boleh lebih dari 2MB";
                            return RedirectToAction("Edit", new { id = id, ErrorMessage = ErrorMessage });
                        }
                    }
                    else
                    {
                        ErrorMessage = "File Upload harus dengan format All Office atau PDF";
                        return RedirectToAction("Edit", new {id=id, ErrorMessage = ErrorMessage });
                    }

                }
                obj.UpdateBy = User.Identity.Name;
                obj.UpdateDate = DateTime.Now.ToString("dd/MM/yyyy");
                obj.TemplateID = id.ToString();
                obj.DocumentName = fileName;
                if (file == null && (form["filename"].ToString() != null || form["filename"].ToString() != ""))
                 {
                    obj.DocumentName = form["filename"].ToString();
                    path = Path.Combine(Server.MapPath("~/content/document/template/"), fileName);
                }
                
                obj.TemplateName = form["documentName"].ToString();
                obj.FolderName = "template";
                obj.Path = path;
                //check Duplicate
                Template obj2 = new Template();
                string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    using (var cmd = new SqlCommand("sp_CheckTemplate", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@TemplateName", obj.TemplateName);
                        cmd.Parameters.AddWithValue("@DocumentName", obj.DocumentName);
                        cmd.Parameters.AddWithValue("@TemplateID", obj.TemplateID);
                        conn.Open();

                        using (var reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                obj2.TemplateID = reader[0].ToString();
                            }
                        }
                    }

                }

                if (obj2.TemplateID != null)
                {
                    ErrorMessage = "Nama Template atau Dokumen Sudah Ada";
                    return RedirectToAction("Edit", new { id = id, ErrorMessage = ErrorMessage });
                }
                // TODO: Add update logic here
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                // TODO: Add update logic here
                HttpResponseMessage response = await client.PutAsJsonAsync(Properties.Settings.Default.TemplateAPI + "/" + id, obj);
                response.EnsureSuccessStatusCode();
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }

        // GET: CRUD/Delete/5
        public async Task<ActionResult> Delete(int id)
        {
            Template objInfo = new Template();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.TemplateAPI + "/" + id);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<Template>(APIResponse);

                }
                //returning the employee list to view  
                return View(objInfo);
            }
        }

        // POST: CRUD/Delete/5
        [HttpPost]
        public async Task<ActionResult> Delete(int id, Template obj)
        {
            // TODO: Add delete logic here
            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                // TODO: Add update logic here
                HttpResponseMessage response = await client.PostAsJsonAsync(Properties.Settings.Default.TemplateAPI + "/delete/" + id, obj);
                response.EnsureSuccessStatusCode();
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }
    }
}
