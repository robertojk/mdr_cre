﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using MandiriCRE.Models;
using System.IO;
using System.Data.SqlClient;
using System.Data;

namespace MandiriCRE.Controllers
{
    public class MasterPenomoranController : Controller
    {
        // GET: MasterPenomoran
        public ActionResult Index1()
        {
            return View();
        }
        string Baseurl = Properties.Settings.Default.BaseURI;
        public async Task<ActionResult> Index()
        {
            List<Numbering> objInfo = new List<Numbering>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.PenomoranAPI);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<List<Numbering>>(APIResponse);

                }
                //returning the employee list to view  
                return View(objInfo);
            }
        }
        // GET: CRUD/Details/5
        public async Task<ActionResult> Details(int id)
        {
            Numbering objInfo = new Numbering();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.PenomoranAPI + "/" + id);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<Numbering>(APIResponse);

                }
                //returning the employee list to view  
                return View(objInfo);
            }
        }

        // GET: CRUD/Create
        public ActionResult Create(string ErrorMessage)
        {
            Numbering objInfo = new Numbering();
            if (ErrorMessage != null)
            {
                ViewBag.ErrorMessage = ErrorMessage;
            }
            return View(objInfo);
        }

        // POST: CRUD/Create
        [HttpPost]
        public async Task<ActionResult> Create(Numbering obj)
        {
            try
            {
                obj.UpdatedBy = User.Identity.Name;
                obj.UpdatedDate = DateTime.Now.ToString("dd/MM/yyyy");
                obj.CreatedBy = User.Identity.Name;
                obj.UpdateBy = User.Identity.Name;
                obj.CreatedDate = DateTime.Now.ToString("dd/MM/yyyy");
                obj.USERNAME = User.Identity.Name;
                //check Duplicate
                string ErrorMessage = "";
                Numbering obj2 = new Numbering();
                string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    using (var cmd = new SqlCommand("sp_CheckNumbering", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@NumberingType", obj.NumberingType);
                        cmd.Parameters.AddWithValue("@NumberingContent", obj.NumberingContent);
                        cmd.Parameters.AddWithValue("@NumberingID", "");
                        conn.Open();

                        using (var reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                obj2.NumberingID = reader[0].ToString();
                            }
                        }
                    }

                }

                if (obj2.NumberingID != null)
                {
                    ErrorMessage = "Jenis Penomoran atau Nomor Sudah Ada";
                    return RedirectToAction("Create", new { ErrorMessage = ErrorMessage });
                }

                //Insert process
                using (SqlConnection conn2 = new SqlConnection(connectionstring))
                {
                    using (var cmd2 = new SqlCommand("sp_InsertNumbering", conn2))
                    {
                        cmd2.CommandType = CommandType.StoredProcedure;
                        cmd2.Parameters.AddWithValue("@NUMBERINGTYPE", obj.NumberingType);
                        cmd2.Parameters.AddWithValue("@NUMBERINGCONTENT", obj.NumberingContent);
                        cmd2.Parameters.AddWithValue("@USERNAME", User.Identity.Name);
                        conn2.Open();
                        int k = cmd2.ExecuteNonQuery();
                        if (k != 0)
                        {
                            //sukses
                        }
                        conn2.Close();
                    }
                }


                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }

        // GET: CRUD/Edit/5
        public async Task<ActionResult> Edit(int id, string ErrorMessage)
        {
            Numbering objInfo = new Numbering();
            if (ErrorMessage != null)
            {
                ViewBag.ErrorMessage = ErrorMessage;
            }
            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.PenomoranAPI + "/" + id);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<Numbering>(APIResponse);

                }
                //returning the employee list to view  
                return View(objInfo);
            }
        }

        // POST: CRUD/Edit/5
        [HttpPost]
        public async Task<ActionResult> Edit(int id, Numbering obj)
        {
            
                try
                {
                obj.UpdatedBy = User.Identity.Name;
                obj.UpdateBy = User.Identity.Name;
                obj.UpdatedDate = DateTime.Now.ToString("dd/MM/yyyy");
                obj.NumberingID = id.ToString();
                obj.USERNAME = User.Identity.Name;
                //check Duplicate
                string ErrorMessage = "";
                Numbering obj2 = new Numbering();
                string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    using (var cmd = new SqlCommand("sp_CheckNumbering", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@NumberingType", obj.NumberingType);
                        cmd.Parameters.AddWithValue("@NumberingContent", obj.NumberingContent);
                        cmd.Parameters.AddWithValue("@NumberingID", obj.NumberingID);
                        conn.Open();

                        using (var reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                obj2.NumberingID = reader[0].ToString();
                            }
                        }
                    }

                }

                if (obj2.NumberingID != null)
                {
                    ErrorMessage = "Jenis Penomoran atau Nomor Sudah Ada";
                    return RedirectToAction("Edit", new {id=id, ErrorMessage = ErrorMessage });
                }

                //Update process
                using (SqlConnection conn2 = new SqlConnection(connectionstring))
                {
                    using (var cmd2 = new SqlCommand("sp_UpdateNumbering", conn2))
                    {
                        cmd2.CommandType = CommandType.StoredProcedure;
                        cmd2.Parameters.AddWithValue("@NUMBERINGID", obj.NumberingID);
                        cmd2.Parameters.AddWithValue("@NUMBERINGTYPE", obj.NumberingType);
                        cmd2.Parameters.AddWithValue("@NUMBERINGCONTENT", obj.NumberingContent);
                        cmd2.Parameters.AddWithValue("@USERNAME", User.Identity.Name);
                        conn2.Open();
                        int k = cmd2.ExecuteNonQuery();
                        if (k != 0)
                        {
                            //sukses
                        }
                        conn2.Close();
                    }
                }
                // TODO: Add update logic here
                //HttpClient client = new HttpClient();
                //    client.BaseAddress = new Uri(Baseurl);
                //    client.DefaultRequestHeaders.Accept.Clear();
                //    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //    // TODO: Add update logic here
                //    HttpResponseMessage response = await client.PutAsJsonAsync(Properties.Settings.Default.PenomoranAPI + "/" + id, obj);
                //    //response.EnsureSuccessStatusCode();
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    string err = ex.Message.ToString();
                    return View();
                }

        }

        // GET: CRUD/Delete/5
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                // TODO: Add delete logic here
                Numbering objInfo = new Numbering();

                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(Baseurl);

                    client.DefaultRequestHeaders.Clear();
                    //Define request data format  
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                    HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.PenomoranAPI + "/" + id);

                    //Checking the response is successful or not which is sent using HttpClient  
                    if (Res.IsSuccessStatusCode)
                    {
                        //Storing the response details recieved from web api   
                        var APIResponse = Res.Content.ReadAsStringAsync().Result;

                        //Deserializing the response recieved from web api and storing into the Employee list  
                        objInfo = JsonConvert.DeserializeObject<Numbering>(APIResponse);

                    }
                    //returning the employee list to view  
                    return View(objInfo);
                }
            }
            catch
            {
                return View();
            }
        }

        // POST: CRUD/Delete/5
        [HttpPost]
        public async Task<ActionResult> Delete(int id, Numbering obj)   
        {
            // TODO: Add delete logic here
            try
            {
                // TODO: Add update logic here
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                // TODO: Add update logic here
                HttpResponseMessage response = await client.DeleteAsync(Properties.Settings.Default.PenomoranAPI + "/" + id);
                response.EnsureSuccessStatusCode();
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }
    }
}