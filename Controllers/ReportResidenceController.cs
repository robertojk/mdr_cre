﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using MandiriCRE.Models;
using System.IO;
using System.Data.SqlClient;
using System.Data;
using System.Web.UI;
using Simplexcel;
using System.Text.RegularExpressions;


namespace MandiriCRE.Controllers
{
    public class ReportResidenceController : Controller
    {
        string Baseurl = Properties.Settings.Default.BaseURI;
        string Baseurl2 = Properties.Settings.Default.BaseURI;
        // GET: ReportResidence
        public async Task<ActionResult> Index()
        {
            List<MasterResidence> objInfo = new List<MasterResidence>();

            string id = "0";
            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl2);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllATTB using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.MasterResidenceAPI + "/" + id);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the ATTB list  
                    objInfo = JsonConvert.DeserializeObject<List<MasterResidence>>(APIResponse);

                }



                //returning the MasterResidence list to view  
                return View(objInfo);

            }
        }
    }
}