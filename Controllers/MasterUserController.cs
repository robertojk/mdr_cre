﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using MandiriCRE.Models;
using MandiriCRE.ViewModel;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.IO;
using System.Data.SqlClient;
using System.Data;

namespace MandiriCRE.Controllers
{
    public class MasterUserController : Controller
    {
        // GET: MasterUser
        public ActionResult Index1()
        {
            return View();
        }
        string Baseurl = Properties.Settings.Default.BaseURI;
        public async Task<ActionResult> Index()
        {
            List<User> objInfo = new List<User>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.UserAPI);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objInfo = JsonConvert.DeserializeObject<List<User>>(APIResponse);

                }
                //returning the employee list to view  
                return View(objInfo);
            }
        }

        // GET: CRUD/Edit/5
        public async Task<ActionResult> Edit(string id)
        {
            User objUser = new User();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.UserAPI + "/" + id);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    objUser = JsonConvert.DeserializeObject<User>(APIResponse);

                }
                UserTeamViewModel objUserTeam = new UserTeamViewModel();
                objUserTeam._user = objUser;
                objUserTeam._listTeam = await getTeamList();
                //returning the employee list to view  
                return View(objUserTeam);
            }
        }

        public async Task<List<Team>> getTeamList()
        {
            List<Team> retVal = new List<Team>();
            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllArticles using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(Properties.Settings.Default.TeamAPI);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var APIResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    retVal = JsonConvert.DeserializeObject<List<Team>>(APIResponse);

                }
                //returning the employee list to view  

            }

            return retVal;
        }
        // POST: CRUD/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(string id, User obj, FormCollection collection)
        {
            try
            {

                // TODO: Add update logic here
                //obj.Team_TeamID = collection["_user.Team_TeamID"].ToString();
                obj.Team_TeamID = collection["TeamHidden"].ToString();
                obj.Status = collection["_user.Status"].ToString();
                obj.UserID = id;
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                // TODO: Add update logic here
                HttpResponseMessage response = await client.PutAsJsonAsync(Properties.Settings.Default.UserAPI + "/" + id, obj);
                response.EnsureSuccessStatusCode();

                if (collection["newPassword"] != null && collection["newPassword"] != "")
                {
                    await ChangePassword(id,collection["newPassword"].ToString());
                }

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return View();
            }
        }

        
        public static async Task ChangePassword(string id,string newpassword)
        {
            // string resetToken = await UserManager.GeneratePasswordResetTokenAsync(User.Identity.Name);
            //IdentityResult passwordChangeResult = await UserManager.ResetPasswordAsync(id, resetToken, collection["_user.NewPassword"].ToString(););

           // UserManager<IdentityUser> userManager = new UserManager<IdentityUser>(new UserStore<IdentityUser>());

            //userManager.RemovePassword(id);

            //userManager.AddPassword(id, newpassword);
            var userStore = new UserStore<IdentityUser>();
            var userManager = new UserManager<IdentityUser>(userStore);
            //string userName = UserName.Text;
            var user = userManager.FindById(id);
            var validPass = await userManager.PasswordValidator.ValidateAsync(newpassword);
            if (validPass.Succeeded)
            {
               
                    //var user = userManager.FindById(id);
                    user.PasswordHash = userManager.PasswordHasher.HashPassword(newpassword);
                    //var res = userManager.Update(user);
                    //var res = userManager.RemovePassword(user.Id);
                    //var ressecurity = userManager.UpdateSecurityStamp(user.Id);
                    
                    var cUser = await userStore.FindByIdAsync(id);
                    await userStore.SetPasswordHashAsync(cUser, user.PasswordHash);
                    await userStore.UpdateAsync(cUser);
            
            }

        }

        

        
    }
}
