﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MandiriCRE.Properties {
    
    
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "14.0.0.0")]
    internal sealed partial class Settings : global::System.Configuration.ApplicationSettingsBase {
        
        private static Settings defaultInstance = ((Settings)(global::System.Configuration.ApplicationSettingsBase.Synchronized(new Settings())));
        
        public static Settings Default {
            get {
                return defaultInstance;
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("http://localhost:8082/")]
        public string BaseURI {
            get {
                return ((string)(this["BaseURI"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("api/master/articles")]
        public string ArticleAPI {
            get {
                return ((string)(this["ArticleAPI"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("api/master/photos")]
        public string FotoBerandaAPI {
            get {
                return ((string)(this["FotoBerandaAPI"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("api/master/numberings")]
        public string PenomoranAPI {
            get {
                return ((string)(this["PenomoranAPI"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("api/master/partners")]
        public string RekananAPI {
            get {
                return ((string)(this["RekananAPI"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("api/master/templates")]
        public string TemplateAPI {
            get {
                return ((string)(this["TemplateAPI"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("api/master/users")]
        public string UserAPI {
            get {
                return ((string)(this["UserAPI"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("C:\\WebApp\\ASP.NetMVC5_Full_Version\\webapp\\Content\\img")]
        public string UploadFilePath {
            get {
                return ((string)(this["UploadFilePath"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("api/master/partners/history")]
        public string PartnerHistoryAPI {
            get {
                return ((string)(this["PartnerHistoryAPI"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("api/home/notifications/all")]
        public string NotificationAllAPI {
            get {
                return ((string)(this["NotificationAllAPI"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("api/home/notifications/PengadaanDitolak")]
        public string NotificationPengadaanDitolakAPI {
            get {
                return ((string)(this["NotificationPengadaanDitolakAPI"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("api/home/notifications/ReviewPerjanjian")]
        public string NotificationReviewPerjanjianAPI {
            get {
                return ((string)(this["NotificationReviewPerjanjianAPI"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("api/home/notifications/BelumUploadCapex")]
        public string NotificationBelumUploadCapexAPI {
            get {
                return ((string)(this["NotificationBelumUploadCapexAPI"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("api/home/notifications/BelumUploadCeloxis")]
        public string NotificationBelumUploadCeloxisAPI {
            get {
                return ((string)(this["NotificationBelumUploadCeloxisAPI"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("api/home/notifications/BelumUploadOpex")]
        public string NotificationBelumUploadOpexAPI {
            get {
                return ((string)(this["NotificationBelumUploadOpexAPI"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("api/home/notifications/BelumUploadKPI")]
        public string NotificationBelumUploadKPIAPI {
            get {
                return ((string)(this["NotificationBelumUploadKPIAPI"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("api/home/notifications/MenungguApprovalPengadaan")]
        public string NotificationMenungguApprovalPengadaanAPI {
            get {
                return ((string)(this["NotificationMenungguApprovalPengadaanAPI"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("api/home/notifications/PerjanjianAkanJatuhTempo")]
        public string NotificationPerjanjianAkanJatuhTempoAPI {
            get {
                return ((string)(this["NotificationPerjanjianAkanJatuhTempoAPI"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("api/home/notifications/PerjanjianTelahJatuhTempo")]
        public string NotificationPerjanjianTelahJatuhTempoAPI {
            get {
                return ((string)(this["NotificationPerjanjianTelahJatuhTempoAPI"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("api/home/notifications/SudahUploadKPI")]
        public string NotificationSudahUploadKPIAPI {
            get {
                return ((string)(this["NotificationSudahUploadKPIAPI"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("api/master/team")]
        public string TeamAPI {
            get {
                return ((string)(this["TeamAPI"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("api/master/role")]
        public string RoleAPI {
            get {
                return ((string)(this["RoleAPI"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("api/master/role/member")]
        public string UserRoleAPI {
            get {
                return ((string)(this["UserRoleAPI"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("api/master/role/access")]
        public string MapRoleAPI {
            get {
                return ((string)(this["MapRoleAPI"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("api/master/menu")]
        public string MenuAPI {
            get {
                return ((string)(this["MenuAPI"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("api/contractManagement/procurements")]
        public string ProcurementAPI {
            get {
                return ((string)(this["ProcurementAPI"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("api/contractManagement/procurements/docs")]
        public string ProcurementDocsAPI {
            get {
                return ((string)(this["ProcurementDocsAPI"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("api/contractManagement/procurements/details")]
        public string ProcurementDetailsAPI {
            get {
                return ((string)(this["ProcurementDetailsAPI"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("api/contractManagement/procurements/vendors")]
        public string ProcurementVendorsAPI {
            get {
                return ((string)(this["ProcurementVendorsAPI"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("api/contractManagement/procurements/negotiations")]
        public string ProcurementNegotiationsAPI {
            get {
                return ((string)(this["ProcurementNegotiationsAPI"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("api/contractManagement/procurements/history")]
        public string ProcurementHistoriesAPI {
            get {
                return ((string)(this["ProcurementHistoriesAPI"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("api/contractManagement/procurements/header")]
        public string ProcurementHeaderAPI {
            get {
                return ((string)(this["ProcurementHeaderAPI"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("api/reports/LaporanRincianPerjanjian")]
        public string LaporanRincianPerjanjianAPI {
            get {
                return ((string)(this["LaporanRincianPerjanjianAPI"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("api/reports/LaporanRekapReviewPerjanjian")]
        public string LaporanRekapReviewPerjanjianAPI {
            get {
                return ((string)(this["LaporanRekapReviewPerjanjianAPI"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("api/reports/LaporanKPIGroup")]
        public string LaporanKPIGroupAPI {
            get {
                return ((string)(this["LaporanKPIGroupAPI"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("api/reports/LaporanSPOATBL")]
        public string LaporanSPOATBLAPI {
            get {
                return ((string)(this["LaporanSPOATBLAPI"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("api/reports/LaporanSPOProcurement")]
        public string LaporanSPOProcurementAPI {
            get {
                return ((string)(this["LaporanSPOProcurementAPI"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("api/reports/LaporanPTO")]
        public string LaporanPTOAPI {
            get {
                return ((string)(this["LaporanPTOAPI"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("api/reports/LaporanCeloxis")]
        public string LaporanCeloxisAPI {
            get {
                return ((string)(this["LaporanCeloxisAPI"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("api/reports/LaporanOPEX")]
        public string LaporanOPEXAPI {
            get {
                return ((string)(this["LaporanOPEXAPI"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("api/reports/LaporanCapex")]
        public string LaporanCapexAPI {
            get {
                return ((string)(this["LaporanCapexAPI"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("api/contractManagement/spksspps")]
        public string SPKSPPAPI {
            get {
                return ((string)(this["SPKSPPAPI"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("api/master/employee")]
        public string EmployeeAPI {
            get {
                return ((string)(this["EmployeeAPI"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("api/contractManagement/agreements/docs")]
        public string AgreementDocAPI {
            get {
                return ((string)(this["AgreementDocAPI"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("api/contractManagement/agreements/history")]
        public string AgreementHistoryAPI {
            get {
                return ((string)(this["AgreementHistoryAPI"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("api/contractManagement/agreements/duplicate")]
        public string AgreementDuplicateAPI {
            get {
                return ((string)(this["AgreementDuplicateAPI"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("api/contractManagement/agreements")]
        public string AgreementAPI {
            get {
                return ((string)(this["AgreementAPI"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("api/Tax")]
        public string TaxAPI {
            get {
                return ((string)(this["TaxAPI"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("http://localhost:8087/")]
        public string BaseURI2 {
            get {
                return ((string)(this["BaseURI2"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("api/master/GL")]
        public string GLAPI {
            get {
                return ((string)(this["GLAPI"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("api/master/Payment")]
        public string PaymentAPI {
            get {
                return ((string)(this["PaymentAPI"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("api/master/ATTBType")]
        public string ATTBTypeAPI {
            get {
                return ((string)(this["ATTBTypeAPI"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("api/master/RegionAsset")]
        public string RegionAssetAPI {
            get {
                return ((string)(this["RegionAssetAPI"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("api/master/TaxYear")]
        public string TaxYearAPI {
            get {
                return ((string)(this["TaxYearAPI"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("api/Tax/duplicate")]
        public string DuplicateTaxAPI {
            get {
                return ((string)(this["DuplicateTaxAPI"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("api/ATTB")]
        public string ATTBAPI {
            get {
                return ((string)(this["ATTBAPI"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("api/master/ATTBStatus")]
        public string ATTBStatusAPI {
            get {
                return ((string)(this["ATTBStatusAPI"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("api/master/Ownership")]
        public string OwnershipAPI {
            get {
                return ((string)(this["OwnershipAPI"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("api/master/AssetControl")]
        public string AssetControlAPI {
            get {
                return ((string)(this["AssetControlAPI"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("api/master/PrimaryUser")]
        public string PrimaryUserAPI {
            get {
                return ((string)(this["PrimaryUserAPI"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("api/ATTB/duplicate")]
        public string DuplicateATTBAPI {
            get {
                return ((string)(this["DuplicateATTBAPI"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("api/resident")]
        public string ResidentAPI {
            get {
                return ((string)(this["ResidentAPI"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("api/BuildingOccupants")]
        public string BuildingOccupants {
            get {
                return ((string)(this["BuildingOccupants"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("api/master/Building")]
        public string MasterBuildingAPI {
            get {
                return ((string)(this["MasterBuildingAPI"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("api/master/BuildingPosition")]
        public string BuildingPositionAPI {
            get {
                return ((string)(this["BuildingPositionAPI"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("api/master/BuildingFloor")]
        public string BuildingFloorAPI {
            get {
                return ((string)(this["BuildingFloorAPI"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("api/master/URCCode")]
        public string URCCodeAPI {
            get {
                return ((string)(this["URCCodeAPI"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("api/buildingUtility")]
        public string BuildingUtilityAPI {
            get {
                return ((string)(this["BuildingUtilityAPI"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("api/master/Subject")]
        public string SubjectAPI {
            get {
                return ((string)(this["SubjectAPI"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("api/buildingUtility/report")]
        public string LaporanBuildingUtilityAPI {
            get {
                return ((string)(this["LaporanBuildingUtilityAPI"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("api/resident")]
        public string MasterResidenceAPI {
            get {
                return ((string)(this["MasterResidenceAPI"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("api/resident/report")]
        public string ReportResidenceAPI {
            get {
                return ((string)(this["ReportResidenceAPI"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("api/master/EmployeePosition")]
        public string EmployeePositionAPI {
            get {
                return ((string)(this["EmployeePositionAPI"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("api/master/City")]
        public string CityAPI {
            get {
                return ((string)(this["CityAPI"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("api/master/Class")]
        public string ResidenceClassAPI {
            get {
                return ((string)(this["ResidenceClassAPI"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("api/master/ResidenceStatus")]
        public string ResidenceStatusAPI {
            get {
                return ((string)(this["ResidenceStatusAPI"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("api/master/ResidenceCondition")]
        public string ResidenceConditionAPI {
            get {
                return ((string)(this["ResidenceConditionAPI"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("api/master/RepairType")]
        public string RepairTypeAPI {
            get {
                return ((string)(this["RepairTypeAPI"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("api/resident/report")]
        public string ResidenceReportAPI {
            get {
                return ((string)(this["ResidenceReportAPI"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("api/OfficialResident")]
        public string OfficialResidentAPI {
            get {
                return ((string)(this["OfficialResidentAPI"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("api/OfficialResident/report")]
        public string OfficialResidentReportAPI {
            get {
                return ((string)(this["OfficialResidentReportAPI"]));
            }
        }
    }
}
